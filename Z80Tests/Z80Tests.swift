//
//  Z80Tests.swift
//  Z80Tests
//
//  Created by Jeremy Pereira on 28/03/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import XCTest
import Toolbox
@testable import Z80
import Foundation

extension Z80A.Status: CustomStringConvertible
{
    public var description: String
    {
        var ret = ""
        ret.append(self.contains(.s) ? "S" : "-")
        ret.append(self.contains(.z) ? "Z" : "-")
        ret.append(self.contains(.y) ? "Y" : "-")
        ret.append(self.contains(.h) ? "H" : "-")
        ret.append(self.contains(.x) ? "X" : "-")
        ret.append(self.contains(.pv) ? "V" : "-")
        ret.append(self.contains(.n) ? "N" : "-")
        ret.append(self.contains(.c) ? "C" : "-")
        return ret
    }
}

class Z80Tests: XCTestCase
{
    
    override func setUp()
    {
        super.setUp()
        Logger.pushLevel(.info, forName: "Z80.Decoder")
    }
    
    override func tearDown()
    {
        Logger.popLevel(forName: "Z80.Decoder")
        super.tearDown()
    }
    
    func testReset()
    {
        Logger.pushLevel(.debug, forName: "Z80.Decoder")
        defer { Logger.popLevel(forName: "Z80.Decoder") }
        let z80 = Z80A()
        z80.setDebug(pattern: 0xdd)
        XCTAssert(z80.notReset, "Z80 reset not high")
        z80.notReset = false
        z80.run(tStates: 3)
        z80.notReset = true
        z80.run(tStates: 1)
        let af = z80.register16("af")!
        XCTAssert(af == 0xffff, "af should be 0xffff but is \(af.hexString)")
        XCTAssert(z80.register16("sp") == 0xffff, "sp should be 0xffff")
        let pc = z80.register16("pc")!
        XCTAssert(pc == 1, "pc should be 0x0001 not 0x\(pc.hexString)")
        XCTAssert(z80.flipFlop("iff1") == false, "iff1 should be false")
    }

    func createCPU(loadFileName: String) -> Z80A
    {
        let z80 = Z80A()
        let myBundle = Bundle(for: type(of: self))
        let binaryFileUrl = myBundle.url(forResource: loadFileName, withExtension: "bin")
        if let binaryFileUrl = binaryFileUrl
        {
            let program = try? Data(contentsOf: binaryFileUrl)

            if let program = program
            {
                var programBytes = [UInt8](repeating: 0, count: program.count)
                program.copyBytes(to: &programBytes, count: programBytes.count)
                var currentAddress: UInt16 = 0
                for aByte in programBytes
                {
                    z80.set(byte: aByte, at: currentAddress)
                    currentAddress++
                }
            }
            else
            {
                fatalError("Could not read test coverage program")
            }
        }
        else
        {
            fatalError("Could not locate coverage test binary")
        }
		return z80
    }

    func reset(_ cpu: Z80A)
    {
        cpu.reportFatal = {
            (message: String) in
            XCTFail(message)
        }
        cpu.notReset = false
        cpu.run(tStates: 3)
        cpu.notReset = true
        cpu.run(tStates: 1)
    }

    func load8BitTestRegs(cpu: Z80A)
    {
        for regString in ["a", "b", "c", "d", "e", "f", "h", "i", "l", "r",
                          "ixl", "ixh", "iyl", "iyh"]
        {
			guard cpu.setRegister8(regString, value: regString.utf8.first!)
            else
            {
                fatalError("\(regString) does not appear to be a supoorted register")
            }
        }
    }


    func load16BitTestRegs(cpu: Z80A, values: [(String, UInt16)])
    {
        for (regString, value) in values
        {
            guard cpu.setRegister16(regString, value: value)
            else
            {
                fatalError("\(regString) does not appear to be a supoorted register")
            }
        }
    }

    func runWithTStateCheck(cpu: Z80A, tStates: Int, failDescription: String)
    {
		let startTStates = cpu.cycleCount
        cpu.run(tStates: tStates)
        let actualTStates = cpu.cycleCount - startTStates
        XCTAssert(actualTStates == tStates, "\(failDescription): mismatched tstates, actual = \(actualTStates), expected = \(tStates)")
    }

    /// All the 8 bit load instructions from register to register
    func testLoad8_r_r()
    {
        let regs =  ["b", "c", "d", "e", "h", "l", "a"]
        let z80 = createCPU(loadFileName: "load8_r_r")
        reset(z80)
		for toReg in regs
        {
            for fromReg in regs
            {
                load8BitTestRegs(cpu: z80)
                z80.run(tStates: 4)
                let regValue = z80.register8(toReg)!
                XCTAssert(regValue == fromReg.utf8.first!, "ld \(toReg),\(fromReg) failed value is 0x\(regValue.hexString)")
            }
        }
    }

    /// All the 8 bit load instructions from register to register
    func testLoad8_r_r_ix_subst()
    {
        let fromRegs =  ["h", "l"]
        let toReg = "b"
        let z80 = createCPU(loadFileName: "load8_r_r_ix")
        reset(z80)
        for fromReg in fromRegs
        {
            guard z80.setRegister8("ix" + fromReg, value: fromReg.utf8.first!)
                else { fatalError("Cannot set ix byte") }

            z80.run(tStates: 8)
            let regValue = z80.register8(toReg)!
            XCTAssert(regValue == fromReg.utf8.first!, "ld \("ix" + toReg),\(fromReg) failed value is 0x\(regValue.hexString)")
        }
        guard z80.setRegister8("ixh", value: "h".utf8.first!)
            else { fatalError("Cannot set ix byte") }
        guard z80.setRegister8("ixl", value: "l".utf8.first!)
            else { fatalError("Cannot set ix byte") }
        z80.run(tStates: 8)
        let regValue = z80.register8("ixl")!
        XCTAssert(regValue == "h".utf8.first!, "ld ixl,ixh failed value is 0x\(regValue.hexString)")

    }

    /// All the 8 bit load instructions from register to register
    func testLoad8_r_r_iy_subst()
    {
        let fromRegs =  ["h", "l"]
        let toReg = "b"
        let z80 = createCPU(loadFileName: "load8_r_r_iy")
        reset(z80)
        for fromReg in fromRegs
        {
            guard z80.setRegister8("iy" + fromReg, value: fromReg.utf8.first!)
                else { fatalError("Cannot set iy byte") }

            z80.run(tStates: 8)
            let regValue = z80.register8(toReg)!
            XCTAssert(regValue == fromReg.utf8.first!, "ld \("iy" + toReg),\(fromReg) failed value is 0x\(regValue.hexString)")
        }
        guard z80.setRegister8("iyh", value: "h".utf8.first!)
            else { fatalError("Cannot set iy byte") }
        guard z80.setRegister8("ixl", value: "l".utf8.first!)
            else { fatalError("Cannot set iy byte") }
        z80.run(tStates: 8)
        let regValue = z80.register8("iyl")!
        XCTAssert(regValue == "h".utf8.first!, "ld iyl,iyh failed value is 0x\(regValue.hexString)")
    }

    func testLoad8_r_n()
    {
        let regs =  ["b", "c", "d", "e", "h", "l", "a"]
        let z80 = createCPU(loadFileName: "ld8_r_n")
        reset(z80)
        for toReg in regs
        {
            load8BitTestRegs(cpu: z80)
            z80.run(tStates: 7)
            let regValue = z80.register8(toReg)!
            XCTAssert(regValue == 1, "ld \(toReg),1 failed value is 0x\(regValue.hexString)")
        }
    }

    func testLoad8_r_n_ix_iy()
    {
        let toRegs = ["ixh", "ixl", "iyh", "iyl"]
        let z80 = createCPU(loadFileName: "ld8_r_n_ix_iy_subst")
        reset(z80)
        guard z80.setRegister16("ix", value: 0xcdcd) else { fatalError("Cannot set ix") }
        guard z80.setRegister16("iy", value: 0xcdcd) else { fatalError("Cannot set iy") }
		for reg in toRegs
        {
        	let expectedCycles = 11
            let startTStates = z80.cycleCount
            z80.run(tStates: expectedCycles)
            let newValue = z80.register8(reg)!
            XCTAssert(newValue == 1, "ld \(reg),1 failed, value is \(reg)")
			let actualCycles = z80.cycleCount - startTStates
            XCTAssert(expectedCycles == actualCycles, "Wrong cycle count, got \(actualCycles) expected \(expectedCycles)")
        }
    }

    func testLoad8_r_address_in_hl()
    {
        let regs =  ["b", "c", "d", "e", "h", "l", "a"]
        let z80 = createCPU(loadFileName: "ld8_r_address_in_hl")
        reset(z80)
        load8BitTestRegs(cpu: z80)
        z80.run(tStates: 14)		// loads hl with source address
        for toReg in regs
        {
            z80.run(tStates: 7)
            let regValue = z80.register8(toReg)!
            XCTAssert(regValue == 1, "ld \(toReg),(hl) failed value is 0x\(regValue.hexString)")
            if toReg == "h" || toReg == "l"
            {
                z80.run(tStates: 14)		// reloads hl with source address
            }
        }
    }

    func testLoad8_address_in_hl_r()
    {
        let regs =  ["b", "c", "d", "e", "h", "l", "a"]
        let z80 = createCPU(loadFileName: "ld8_address_in_hl_r")
        reset(z80)
        load8BitTestRegs(cpu: z80)
        let destinationAddress = z80.register16("hl")!
        for fromReg in regs
        {
            z80.run(tStates: 7)
            let destValue = z80.byte(at: destinationAddress)
            XCTAssert(destValue == fromReg.utf8.first!, "ld (hl),\(fromReg), failed value is 0x\(destValue.hexString)")
        }
    }

	func testLoad8_r_ix_d()
    {
        let regsAndResults =  ["b" : 0, "c" : 2, "d" : 4, "e" : 0xfe, "h" : 2, "l": 4, "a" : 2]
        let z80 = createCPU(loadFileName: "ld8_r_ix_d")
        reset(z80)
        load8BitTestRegs(cpu: z80)
        z80.run(tStates: 22)	// Load ix with the address
        for reg in regsAndResults.keys.sorted()
        {
            let expectedCycles = 19
            let startTStates = z80.cycleCount
            let result = regsAndResults[reg]!
            z80.run(tStates: expectedCycles)
            let regValue = z80.register8(reg)!
            XCTAssert(regValue == UInt8(result), "ld \(reg),(ix+n) Unexpected result, got \(regValue), expected \(result)")
            let actualCycles = z80.cycleCount - startTStates
            XCTAssert(actualCycles == expectedCycles, "ld \(reg),(ix+n) Wrong cycle count, got \(actualCycles), expected \(expectedCycles)")
        }

    }

    func testLoad8_r_iy_d()
    {
        let regsAndResults =  ["b" : 0, "c" : 2, "d" : 4, "e" : 0xfe, "h" : 2, "l": 4, "a" : 2]
        let z80 = createCPU(loadFileName: "ld8_r_iy_d")
        reset(z80)
        load8BitTestRegs(cpu: z80)
        z80.run(tStates: 22)	// Load ix with the address
        for reg in regsAndResults.keys.sorted()
        {
            let result = regsAndResults[reg]!
            z80.run(tStates: 19)
            let regValue = z80.register8(reg)!
            XCTAssert(regValue == UInt8(result), "ld \(reg),(iy+n) Unexpected result, got \(regValue), expected \(result)")
        }
    }

    func testLoad8_ix_d_iy_d_r()
    {
        let regsAndOffsets: [(String, Int)] =  [("b", 1), ("c",2), ("d", 3), ("e",4), ("h", 5), ("l", 6), ("a", 0xffff)]
        let z80 = createCPU(loadFileName: "ld8_ix_d_iy_d_r")
        reset(z80)
        load8BitTestRegs(cpu: z80)
        let ixAddress: UInt16 = 0x200
        let iyAddress: UInt16 = 0x400
        z80.setRegister16("ix", value: ixAddress)
        z80.setRegister16("iy", value: iyAddress)
        // ix test
        for (reg, offset) in regsAndOffsets
        {
            let expectedCycles = 19
            let startTStates = z80.cycleCount
            z80.run(tStates: 19)
            let expectedResult = reg.utf8.first!
            let resultAddress = ixAddress &+ UInt16(offset)
            let result = z80.byte(at: resultAddress)
            XCTAssert(result == expectedResult, "ld (ix+\(offset),\(reg) got \(result), expected \(expectedResult)")
            let actualCycles = z80.cycleCount - startTStates
            XCTAssert(actualCycles == expectedCycles, "ld (ix+n),\(reg) Wrong cycle count, got \(actualCycles), expected \(expectedCycles)")
        }
        // iy test
        for (reg, offset) in regsAndOffsets
        {
            z80.run(tStates: 19)
            let expectedResult = reg.utf8.first!
            let resultAddress = iyAddress &+ UInt16(offset)
            let result = z80.byte(at: resultAddress)
            XCTAssert(result == expectedResult, "ld (iy+\(offset),\(reg) got \(result), expected \(expectedResult)")
        }
    }

    func testLoad8_indirect_n()
    {
        let regsOffsetsAndTStates = [("hl",0, 10), ("ix", 1, 19), ("iy", 2, 19)]
        let z80 = createCPU(loadFileName: "ld8_indirect_n")
        reset(z80)
        load8BitTestRegs(cpu: z80)
        let ixAddress: UInt16 = 0x200
        let iyAddress: UInt16 = 0x400
        let hlAddress: UInt16 = 0x600
        z80.setRegister16("ix", value: UInt16(ixAddress))
        z80.setRegister16("iy", value: UInt16(iyAddress))
        z80.setRegister16("hl", value: UInt16(hlAddress))

        for (reg, offset, tStates) in regsOffsetsAndTStates
        {
            let startTStates = z80.cycleCount
            z80.run(tStates: tStates)
            let expectedResult = UInt8(offset) &+ 1
            let baseAddress = z80.register16(reg)!
            let result = z80.byte(at: baseAddress &+ UInt16(offset))
            XCTAssert(result == expectedResult,
                      "ld (\(reg)+\(offset)),\(expectedResult) incorrect, result is \(result), address is 0x\((baseAddress &+ UInt16(offset)).hexString)")
            let endTStates = z80.cycleCount
            let discrepancyTStates = (endTStates - startTStates) - tStates
            XCTAssert(discrepancyTStates == 0, "ld (\(reg)+\(offset)),\(expectedResult) tstates is wrong by \(discrepancyTStates) too many")
        }
    }

    func testLoad8_a_address_in_bc_or_de()
    {
        let z80 = createCPU(loadFileName: "ld8_a_address_in_bc_de")
        reset(z80)
        load8BitTestRegs(cpu: z80)
        let bcAddress: UInt16 = 0x200
        let deAddress: UInt16 = 0x400
        z80.setRegister16("bc", value: bcAddress)
        z80.setRegister16("de", value: deAddress)
        z80.set(byte: 0xbc, at: bcAddress)
        z80.set(byte: 0xde, at: deAddress)
        let startTStates = z80.cycleCount
        z80.run(tStates: 7) // from bc
        let bcResult: UInt8 = z80.register8("a")!
        XCTAssert(bcResult == 0xbc, "ld a,(bc) wrong: expected 0xbc, got 0x\(bcResult.hexString)")
        z80.run(tStates: 7) // from de
        let deResult: UInt8 = z80.register8("a")!
        XCTAssert(deResult == 0xde, "ld a,(de) wrong: expected 0xde, got 0x\(bcResult.hexString)")
        let statesUsed = z80.cycleCount - startTStates
        XCTAssert(statesUsed == 14, "Wrong number of states used \(statesUsed), expected 14")
    }

    func testLoad8_a_addressIn_nn()
    {
        let z80 = createCPU(loadFileName: "ld8_a_address_in_nn")
        reset(z80)
        load8BitTestRegs(cpu: z80)
        let startTStates = z80.cycleCount
		z80.run(tStates: 13)
        let aValue: UInt8 = z80.register8("a")!
        XCTAssert(aValue == 0xaa, "ld a,(nn) Invalid result \(aValue), expected 0xaa")
        let tStatesUsed = z80.cycleCount - startTStates
        XCTAssert(tStatesUsed == 13, "Wrong number of t states \(tStatesUsed)")
    }

    func testLoad8_addressInThing_a()
    {
        let z80 = createCPU(loadFileName: "load8_address_in_reg_a")
        reset(z80)
        load8BitTestRegs(cpu: z80)
        let bcAddress: UInt16 = 0x400
        let deAddress: UInt16 = 0x600
        z80.setRegister16("bc", value: bcAddress)
        z80.setRegister16("de", value: deAddress)
        let startTStates = z80.cycleCount
        z80.run(tStates: 7)
        let bcResult = z80.byte(at: bcAddress)
        XCTAssert(bcResult == "a".utf8.first!, "ld (bc),a failed, got \(bcResult), expected \("a".utf8.first!)")
        z80.run(tStates: 7)
        let deResult = z80.byte(at: bcAddress)
        XCTAssert(deResult == "a".utf8.first!, "ld (bc),a failed, got \(deResult), expected \("a".utf8.first!)")
        z80.run(tStates: 13)
        let nnnResult = z80.byte(at: bcAddress)
        XCTAssert(nnnResult == "a".utf8.first!, "ld (bc),a failed, got \(nnnResult), expected \("a".utf8.first!)")

        let tStatesUsed = z80.cycleCount - startTStates
        XCTAssert(tStatesUsed == 27, "Wrong number of t states \(tStatesUsed)")
    }

    func testLoad_a_i_0()
    {
        let z80 = Z80A()
        z80.set(bytes: [0xed, 0x57], at: 0) // ld a,i
        reset(z80)
        load8BitTestRegs(cpu: z80)
        guard z80.setRegister8("i", value: 0) else { fatalError("Can't set i register") }
        let oldFlags = z80.flags
		z80.run(tStates: 9)
        let aResult = z80.register8("a")!
        XCTAssert(aResult == 0, "ld a,i failed")
        XCTAssert(z80.flags.contains(Z80A.Status.z), "Zero flag should be set")
        XCTAssert(!z80.flags.contains(Z80A.Status.s), "Sign flag should not be set")
        XCTAssert(!z80.flags.contains(Z80A.Status.y), "Y flag should not be set")
        XCTAssert(!z80.flags.contains(Z80A.Status.x), "X flag should not be set")
        XCTAssert(!z80.flags.contains(Z80A.Status.h), "H flag should not be set")
        XCTAssert(!z80.flags.contains(Z80A.Status.n), "N flag should not be set")
        XCTAssert(!z80.flags.contains(Z80A.Status.pv), "P flag should not be set (IFF2 is false")
        if oldFlags.contains(Z80A.Status.c)
        {
            XCTAssert(z80.flags.contains(Z80A.Status.c), "Carry flag should be set")
        }
        else
        {
            XCTAssert(!z80.flags.contains(Z80A.Status.c), "Carry flag should not be set")
        }
    }

    func testLoad_a_i_ff()
    {
        let z80 = Z80A()
        z80.set(bytes: [0xed, 0x57], at: 0) // ld a,i
        reset(z80)
        load8BitTestRegs(cpu: z80)
        guard z80.setRegister8("i", value: 0xff) else { fatalError("Can't set i register") }
        let oldFlags = z80.flags
        z80.run(tStates: 9)
        let aResult = z80.register8("a")!
        XCTAssert(aResult == 0xff, "ld a,i failed")
        XCTAssert(!z80.flags.contains(Z80A.Status.z), "Zero flag should not be set")
        XCTAssert(z80.flags.contains(Z80A.Status.s), "Sign flag should be set")
        XCTAssert(z80.flags.contains(Z80A.Status.y), "Y flag should be set")
        XCTAssert(z80.flags.contains(Z80A.Status.x), "X flag should be set")
        XCTAssert(!z80.flags.contains(Z80A.Status.h), "H flag should not be set")
        XCTAssert(!z80.flags.contains(Z80A.Status.n), "N flag should not be set")
        XCTAssert(!z80.flags.contains(Z80A.Status.pv), "P flag should not be set (IFF2 is false")
        if oldFlags.contains(Z80A.Status.c)
        {
            XCTAssert(z80.flags.contains(Z80A.Status.c), "Carry flag should be set")
        }
        else
        {
            XCTAssert(!z80.flags.contains(Z80A.Status.c), "Carry flag should not be set")
        }
    }

    func testLoad_a_r_0()
    {
        let z80 = Z80A()
        z80.set(bytes: [0xed, 0x5f], at: 0) // ld a,r
        reset(z80)
        load8BitTestRegs(cpu: z80)
        guard z80.setRegister8("r", value: 0) else { fatalError("Can't set r register") }
        let oldFlags = z80.flags
        z80.run(tStates: 9)
        let aResult = z80.register8("a")!
        XCTAssert(aResult == 0, "ld a,r failed")
        XCTAssert(z80.flags.contains(Z80A.Status.z), "Zero flag should be set")
        XCTAssert(!z80.flags.contains(Z80A.Status.s), "Sign flag should not be set")
        XCTAssert(!z80.flags.contains(Z80A.Status.y), "Y flag should not be set")
        XCTAssert(!z80.flags.contains(Z80A.Status.x), "X flag should not be set")
        XCTAssert(!z80.flags.contains(Z80A.Status.h), "H flag should not be set")
        XCTAssert(!z80.flags.contains(Z80A.Status.n), "N flag should not be set")
        XCTAssert(!z80.flags.contains(Z80A.Status.pv), "P flag should not be set (IFF2 is false")
        if oldFlags.contains(Z80A.Status.c)
        {
            XCTAssert(z80.flags.contains(Z80A.Status.c), "Carry flag should be set")
        }
        else
        {
            XCTAssert(!z80.flags.contains(Z80A.Status.c), "Carry flag should not be set")
        }
    }

    func testLoad_a_r_ff()
    {
        let z80 = Z80A()
        z80.set(bytes: [0xed, 0x5f], at: 0) // ld a,r
        reset(z80)
        load8BitTestRegs(cpu: z80)
        guard z80.setRegister8("r", value: 0xff) else { fatalError("Can't set i register") }
        let oldFlags = z80.flags
        z80.run(tStates: 9)
        let aResult = z80.register8("a")!
        XCTAssert(aResult == 0xff, "ld a,r failed")
        XCTAssert(!z80.flags.contains(Z80A.Status.z), "Zero flag should not be set")
        XCTAssert(z80.flags.contains(Z80A.Status.s), "Sign flag should be set")
        XCTAssert(z80.flags.contains(Z80A.Status.y), "Y flag should be set")
        XCTAssert(z80.flags.contains(Z80A.Status.x), "X flag should be set")
        XCTAssert(!z80.flags.contains(Z80A.Status.h), "H flag should not be set")
        XCTAssert(!z80.flags.contains(Z80A.Status.n), "N flag should not be set")
        XCTAssert(!z80.flags.contains(Z80A.Status.pv), "P flag should not be set (IFF2 is false")
        if oldFlags.contains(Z80A.Status.c)
        {
            XCTAssert(z80.flags.contains(Z80A.Status.c), "Carry flag should be set")
        }
        else
        {
            XCTAssert(!z80.flags.contains(Z80A.Status.c), "Carry flag should not be set")
        }
    }

    func testLoad_i_a()
    {
        let z80 = Z80A()
        z80.set(bytes: [0xed, 0x47], at: 0) // ld i,a
        reset(z80)
        let startTStates = z80.cycleCount
        load8BitTestRegs(cpu: z80)
        let oldFlags = z80.flags
        z80.run(tStates: 9)
        let aResult = z80.register8("i")!
        XCTAssert(aResult == "a".utf8.first!, "ld i,a failed")
        XCTAssert(z80.flags == oldFlags, "Flags are wrong")
        XCTAssert(z80.cycleCount - startTStates == 9, "Wrong tstate count")
    }

    func testLoad_r_a()
    {
        let z80 = Z80A()
        z80.set(bytes: [0xed, 0x4f], at: 0) // ld r,a
        reset(z80)
        let startTStates = z80.cycleCount
        load8BitTestRegs(cpu: z80)
        let oldFlags = z80.flags
        z80.run(tStates: 9)
        let aResult = z80.register8("r")!
        XCTAssert(aResult == "a".utf8.first!, "ld r,a failed")
        XCTAssert(z80.flags == oldFlags, "Flags are wrong")
        XCTAssert(z80.cycleCount - startTStates == 9, "Wrong tstate count")
    }

    func testLoad_reg16_nn()
    {
        let regs = ["bc", "de", "hl", "sp", "ix", "iy"]
        let z80 = createCPU(loadFileName: "load8_reg16_nn")
        reset(z80)
        load8BitTestRegs(cpu: z80)
		for reg in regs
        {
            let expectedTStates = 10 + ((reg == "ix" || reg == "iy") ? 4 : 0)
            z80.run(tStates: expectedTStates)
			let result = z80.register16(reg)!
            XCTAssert(result == 0x1234, "ld \(reg),0x1234 Unexpected result 0x\(result.hexString)")
        }
    }

    func testLoad_reg16_address()
    {
        let regs = [("bc", 20), ("de", 20), ("hl", 16), ("sp", 20), ("ix", 20), ("iy", 20), ("hl", 20)]
        let z80 = createCPU(loadFileName: "ld16_dd_address")
        reset(z80)
        load8BitTestRegs(cpu: z80)
        for (reg, expectedTStates) in regs
        {
            z80.run(tStates: expectedTStates)
            let result = z80.register16(reg)!
            XCTAssert(result == 0x1234, "ld \(reg),(address) Unexpected result 0x\(result.hexString)")
        }
    }

    func testLoad_address_reg16()
    {
        let regs: [(String, Int, UInt16)]
            = [("bc", 20, 0x6263),
               ("de", 20, 0x6465),
               ("hl", 16, 0x686c),
               ("sp", 20, 0xffff),
               ("ix", 20, 0x5878),
               ("iy", 20, 0x5979),
               ("hl", 20, 0x686c)]
        let z80 = createCPU(loadFileName: "ld16_address_dd")
        reset(z80)
        z80.run(tStates: 10)		// Fetch the address in hl
        let address = z80.register16("hl")!
        print("Load address is 0x\(address.hexString)")
        load16BitTestRegs(cpu: z80, values: regs.map({ ($0.0, $0.2)}))
        for (reg, expectedTStates, expectedResult) in regs
        {
            let startTStates = z80.cycleCount
            z80.run(tStates: expectedTStates)
            let result = UInt16(low: z80.byte(at: address), high: z80.byte(at: address &+ 1))
            XCTAssert(result == expectedResult, "ld (address),\(reg) Unexpected result 0x\(result.hexString), expected 0x\(expectedResult.hexString)")
            let tStatesUsed = z80.cycleCount - startTStates
            XCTAssert(tStatesUsed == expectedTStates, "Invalid cycle count \(tStatesUsed), expected \(expectedTStates)")
        }
    }

    func testLoad16_sp_reg()
    {
        let regs: [(String, Int, UInt16)]
            = [("hl", 6, 0x6263),
               ("ix", 10, 0x6465),
               ("iy", 10, 0x686c)]
        let z80 = createCPU(loadFileName: "ld16_sp_reg")
        reset(z80)
        load16BitTestRegs(cpu: z80, values: regs.map({ ($0.0, $0.2)}))
        for (reg, expectedTStates, expectedResult) in regs
        {
            let startTStates = z80.cycleCount
            z80.run(tStates: expectedTStates)
            let result = z80.register16("sp")!
            XCTAssert(result == expectedResult, "ld sp,\(reg) Unexpected result 0x\(result.hexString), expected 0x\(expectedResult.hexString)")
            let tStatesUsed = z80.cycleCount - startTStates
            XCTAssert(tStatesUsed == expectedTStates, "Invalid cycle count \(tStatesUsed), expected \(expectedTStates)")
        }
    }

    func testLoad16_push_qq()
    {
        let regs: [(String, Int, UInt16)]
            = [("bc", 11, 0x6263),
               ("de", 11, 0x6465),
               ("hl", 11, 0x686c),
               ("af", 11, 0x6166),
               ("ix", 15, 0x5878),
               ("iy", 15, 0x5979)]
        let z80 = createCPU(loadFileName: "ld16_push_qq")
        reset(z80)
        load16BitTestRegs(cpu: z80, values: regs.map({ ($0.0, $0.2)}))
		for (reg, expectedTStates, expectedResult) in regs
        {
            let startTStates = z80.cycleCount
            let spBefore = z80.register16("sp")!
            z80.run(tStates: expectedTStates)
            let spAfter = z80.register16("sp")!
            let actualResult = UInt16(low: z80.byte(at: spAfter), high: z80.byte(at: spAfter &+ 1))
            let tStatesUsed = z80.cycleCount - startTStates
            XCTAssert(actualResult == expectedResult, "push \(reg) failed, got 0x\(actualResult.hexString), expected 0x\(expectedResult.hexString)")
            XCTAssert(spBefore &- spAfter == 2, "push \(reg) failed, sp wrong")
            XCTAssert(tStatesUsed == expectedTStates, "Invalid cycle count \(tStatesUsed), expected \(expectedTStates)")
        }
    }

    func testLoad16_pop_qq()
    {
        let regs: [(String, Int, UInt16)]
            = [("bc", 10, 0x0102),
               ("de", 10, 0x0304),
               ("hl", 10, 0x0506),
               ("af", 10, 0x0708),
               ("ix", 14, 0x090a),
               ("iy", 14, 0x0b0c)]
        let z80 = createCPU(loadFileName: "ld16_pop_qq")
        reset(z80)
        z80.run(tStates: 10)	// Set up the stack pointer
        for (reg, expectedTStates, expectedResult) in regs
        {
            let startTStates = z80.cycleCount
            let spBefore = z80.register16("sp")!
            z80.run(tStates: expectedTStates)
            let spAfter = z80.register16("sp")!
            let actualResult = z80.register16(reg)!
            let tStatesUsed = z80.cycleCount - startTStates
            XCTAssert(actualResult == expectedResult, "pop \(reg) failed, got 0x\(actualResult.hexString), expected 0x\(expectedResult.hexString)")
            XCTAssert(spAfter &- spBefore == 2, "pop \(reg) failed, sp wrong")
            XCTAssert(tStatesUsed == expectedTStates, "Invalid cycle count \(tStatesUsed), expected \(expectedTStates)")
        }
    }

    func testExchange_de_hl()
    {
        let regs: [(String, UInt16)] = [("de", 0x0304), ("hl", 0x0506)]
        let z80 = Z80A()
        z80.set(byte: 0xeb, at: 0)
        reset(z80)
        load16BitTestRegs(cpu: z80, values: regs)
        let startTStates = z80.cycleCount
        let expectedTStates = 4
        z80.run(tStates: expectedTStates)
        let de = z80.register16("de")!
        let hl = z80.register16("hl")!
        XCTAssert(de == 0x0506, "ex de,hl failed, de == 0x\(de.hexString)")
        XCTAssert(hl == 0x0304, "ex de,hl failed, hl == 0x\(hl.hexString)")
        let tStatesUsed = z80.cycleCount - startTStates
        XCTAssert(tStatesUsed == expectedTStates, "Invalid cycle count \(tStatesUsed), expected \(expectedTStates)")
    }

    func testExchange_af_af_alt()
    {
        let regs: [(String, UInt16)] = [("af", 0x0304), ("af'", 0x0506)]
        let z80 = Z80A()
        z80.set(byte: 0x08, at: 0)
        reset(z80)
        load16BitTestRegs(cpu: z80, values: regs)
        let startTStates = z80.cycleCount
        let expectedTStates = 4
        z80.run(tStates: expectedTStates)
        let af = z80.register16("af")!
        let afAlt = z80.register16("af'")!
        XCTAssert(af == 0x0506, "ex af,af' failed, de == 0x\(af.hexString)")
        XCTAssert(afAlt == 0x0304, "ex af,af' failed, hl == 0x\(afAlt.hexString)")
        let tStatesUsed = z80.cycleCount - startTStates
        XCTAssert(tStatesUsed == expectedTStates, "Invalid cycle count \(tStatesUsed), expected \(expectedTStates)")
    }

    func testEXX()
    {
        let regs: [(String, UInt16)] =
            [
                ("bc", 0x1122),
                ("de", 0x3344),
                ("hl", 0x5566),
                ("bc'", 0x7788),
                ("de'", 0x99aa),
                ("hl'", 0xbbcc),
            ]
        let z80 = Z80A()
        z80.set(byte: 0xd9, at: 0)
        reset(z80)
        load16BitTestRegs(cpu: z80, values: regs)
        let startTStates = z80.cycleCount
        let expectedTStates = 4
        z80.run(tStates: expectedTStates)
        let bc = z80.register16("bc")
        let de = z80.register16("de")
        let hl = z80.register16("hl")
        let bcAlt = z80.register16("bc'")
        let deAlt = z80.register16("de'")
        let hlAlt = z80.register16("hl'")
        XCTAssert(bc == 0x7788, "bc is wrong")
        XCTAssert(de == 0x99aa, "de is wrong")
        XCTAssert(hl == 0xbbcc, "hl is wrong")
        XCTAssert(bcAlt == 0x1122, "bc' is wrong")
        XCTAssert(deAlt == 0x3344, "de' is wrong")
        XCTAssert(hlAlt == 0x5566, "hl' is wrong")
        let tStatesUsed = z80.cycleCount - startTStates
        XCTAssert(tStatesUsed == expectedTStates, "Invalid cycle count \(tStatesUsed), expected \(expectedTStates)")
    }

    func testExchangeAddressSP_reg()
    {
        let regs: [(String, Int, UInt16)]
            = [("hl", 19, 0x686c),
               ("ix", 23, 0x5878),
               ("iy", 23, 0x5979)]
        let z80 = createCPU(loadFileName: "exchange_addressSP_reg")
        reset(z80)
        load16BitTestRegs(cpu: z80, values: regs.map({ ($0.0, $0.2) }))
        z80.run(tStates: 10)	// Load the stack pointer
        let sp = z80.register16("sp")!
        var stackTop = UInt16(low: z80.byte(at: sp), high: z80.byte(at: sp &+ 1))
		for (reg, expectedTStates, regValue) in regs
        {
            runWithTStateCheck(cpu: z80, tStates: expectedTStates, failDescription: "ex (sp),\(reg)")
            let actualReg = z80.register16(reg)!
            XCTAssert(actualReg == stackTop, "ex (sp),\(reg) failed, \(reg) = 0x\(actualReg.hexString), expected 0x\(stackTop.hexString)")
            stackTop = UInt16(low: z80.byte(at: sp), high: z80.byte(at: sp &+ 1))
            XCTAssert(stackTop == regValue, "ex (sp),\(reg) failed, (sp) = 0x\(stackTop.hexString), expected 0x\(regValue.hexString)")
        }
    }

    func testLDI()
    {
        let z80 = createCPU(loadFileName: "ldi")
        reset(z80)
        z80.setRegister8("a", value: 1)
        z80.run(tStates: 30)	// load the three registers
        let bcStart = z80.register16("bc")!
        let deStart = z80.register16("de")!
        let hlStart = z80.register16("hl")!
        precondition(z80.byte(at: deStart) != z80.byte(at: hlStart),
                     "Byteto transfer should be different")
        var startFlags = z80.flags
        startFlags.insert(.h)
        startFlags.remove(.pv)
        startFlags.insert(.n)
		startFlags.remove(.y)
        startFlags.insert(.x)
        z80.flags = startFlags
        runWithTStateCheck(cpu: z80, tStates: 16, failDescription: "ldi [1]")
        let bcMiddle = z80.register16("bc")!
        let deMiddle = z80.register16("de")!
        let hlMiddle = z80.register16("hl")!
        let middleFlags = z80.flags
        XCTAssert(bcMiddle == bcStart - 1, "bc not decremented correctly")
        XCTAssert(deMiddle == deStart + 1, "de not incremented correctly")
        XCTAssert(hlMiddle == hlStart + 1, "hl not incremented correctly")
		XCTAssert(z80.byte(at: deStart) == z80.byte(at: hlStart), "Byte not transferred")
        var changedFlags = middleFlags.symmetricDifference(startFlags)
		XCTAssert(!changedFlags.contains(Z80A.Status.s), "S flag not preserved")
        XCTAssert(!changedFlags.contains(Z80A.Status.z), "Z flag not preserved")
        XCTAssert(!changedFlags.contains(Z80A.Status.c), "C flag not preserved")
        XCTAssert(changedFlags.contains(Z80A.Status.h), "H flag not reset")
        XCTAssert(changedFlags.contains(Z80A.Status.pv), "PV flag not set")
        XCTAssert(changedFlags.contains(Z80A.Status.n), "N flag not reset")
        XCTAssert(changedFlags.contains(Z80A.Status.y), "Y flag not set")
        XCTAssert(changedFlags.contains(Z80A.Status.x), "X flag not reset")

        precondition(z80.byte(at: deMiddle) != z80.byte(at: hlMiddle),
                     "Byteto transfer should be different")

        runWithTStateCheck(cpu: z80, tStates: 16, failDescription: "ldi [2]")
        let bcEnd = z80.register16("bc")!
        let deEnd = z80.register16("de")!
        let hlEnd = z80.register16("hl")!
        let endFlags = z80.flags
        XCTAssert(bcEnd == bcStart - 2, "bc not decremented correctly")
        XCTAssert(deEnd == deStart + 2, "de not incremented correctly")
        XCTAssert(hlEnd == hlStart + 2, "hl not incremented correctly")
        XCTAssert(z80.byte(at: deMiddle) == z80.byte(at: hlMiddle), "Byte not transferred")
		changedFlags = endFlags.symmetricDifference(startFlags)
        XCTAssert(!changedFlags.contains(Z80A.Status.s), "S flag not preserved")
        XCTAssert(!changedFlags.contains(Z80A.Status.z), "Z flag not preserved")
        XCTAssert(!changedFlags.contains(Z80A.Status.c), "C flag not preserved")
        XCTAssert(changedFlags.contains(Z80A.Status.h), "H flag not reset")
        XCTAssert(!changedFlags.contains(Z80A.Status.pv), "PV flag not set")
        XCTAssert(changedFlags.contains(Z80A.Status.n), "N flag not reset")
        XCTAssert(changedFlags.contains(Z80A.Status.y), "Y flag not set")
        XCTAssert(changedFlags.contains(Z80A.Status.x), "X flag not reset")
    }


    func testLDIR()
    {
        let z80 = createCPU(loadFileName: "ldir")
        reset(z80)
        z80.setRegister8("a", value: 1)
        z80.run(tStates: 30)	// load the three registers
        let bcStart = z80.register16("bc")!
        let deStart = z80.register16("de")!
        let hlStart = z80.register16("hl")!
        precondition(z80.byte(at: deStart) != z80.byte(at: hlStart),
                     "Byteto transfer should be different")
        var startFlags = z80.flags
        startFlags.insert(.h)
        startFlags.remove(.pv)
        startFlags.insert(.n)
        startFlags.remove(.y)
        startFlags.insert(.x)
        z80.flags = startFlags
        runWithTStateCheck(cpu: z80, tStates: 21, failDescription: "ldir [1]")
        let bcMiddle = z80.register16("bc")!
        let deMiddle = z80.register16("de")!
        let hlMiddle = z80.register16("hl")!
        let middleFlags = z80.flags
        XCTAssert(bcMiddle == bcStart - 1, "bc not decremented correctly")
        XCTAssert(deMiddle == deStart + 1, "de not incremented correctly")
        XCTAssert(hlMiddle == hlStart + 1, "hl not incremented correctly")
        XCTAssert(z80.byte(at: deStart) == z80.byte(at: hlStart), "Byte not transferred")
        var changedFlags = middleFlags.symmetricDifference(startFlags)
        XCTAssert(!changedFlags.contains(Z80A.Status.s), "S flag not preserved")
        XCTAssert(!changedFlags.contains(Z80A.Status.z), "Z flag not preserved")
        XCTAssert(!changedFlags.contains(Z80A.Status.c), "C flag not preserved")
        XCTAssert(changedFlags.contains(Z80A.Status.h), "H flag not reset")
        XCTAssert(changedFlags.contains(Z80A.Status.pv), "PV flag not set")
        XCTAssert(changedFlags.contains(Z80A.Status.n), "N flag not reset")
        XCTAssert(changedFlags.contains(Z80A.Status.y), "Y flag not set")
        XCTAssert(changedFlags.contains(Z80A.Status.x), "X flag not reset")

        precondition(z80.byte(at: deMiddle) != z80.byte(at: hlMiddle),
                     "Byteto transfer should be different")

        runWithTStateCheck(cpu: z80, tStates: 16, failDescription: "ldir [2]")
        let bcEnd = z80.register16("bc")!
        let deEnd = z80.register16("de")!
        let hlEnd = z80.register16("hl")!
        let endFlags = z80.flags
        XCTAssert(bcEnd == bcStart - 2, "bc not decremented correctly")
        XCTAssert(deEnd == deStart + 2, "de not incremented correctly")
        XCTAssert(hlEnd == hlStart + 2, "hl not incremented correctly")
        XCTAssert(z80.byte(at: deMiddle) == z80.byte(at: hlMiddle), "Byte not transferred")
        changedFlags = endFlags.symmetricDifference(startFlags)
        XCTAssert(!changedFlags.contains(Z80A.Status.s), "S flag not preserved")
        XCTAssert(!changedFlags.contains(Z80A.Status.z), "Z flag not preserved")
        XCTAssert(!changedFlags.contains(Z80A.Status.c), "C flag not preserved")
        XCTAssert(changedFlags.contains(Z80A.Status.h), "H flag not reset")
        XCTAssert(!changedFlags.contains(Z80A.Status.pv), "PV flag not set")
        XCTAssert(changedFlags.contains(Z80A.Status.n), "N flag not reset")
        XCTAssert(changedFlags.contains(Z80A.Status.y), "Y flag not set")
        XCTAssert(changedFlags.contains(Z80A.Status.x), "X flag not reset")
        runWithTStateCheck(cpu: z80, tStates: 7, failDescription: "ldir: ld a,n")
        let a = z80.register8("a")
        XCTAssert(a == 1, "Didn't execute instruction after ldir")
    }

    func testLDD()
    {
        let z80 = createCPU(loadFileName: "ldd")
        reset(z80)
        z80.setRegister8("a", value: 1)
        z80.run(tStates: 30)	// load the three registers
        let bcStart = z80.register16("bc")!
        let deStart = z80.register16("de")!
        let hlStart = z80.register16("hl")!
        precondition(z80.byte(at: deStart) != z80.byte(at: hlStart),
                     "Byteto transfer should be different")
        let startFlags = z80.flags.union([.h, .n, .x]).subtracting([.p, .y])
        z80.flags = startFlags
        runWithTStateCheck(cpu: z80, tStates: 16, failDescription: "ldd [1]")
        let bcMiddle = z80.register16("bc")!
        let deMiddle = z80.register16("de")!
        let hlMiddle = z80.register16("hl")!
        let middleFlags = z80.flags
        XCTAssert(bcMiddle == bcStart &- 1, "bc not decremented correctly")
        XCTAssert(deMiddle == deStart &- 1, "de not decremented correctly")
        XCTAssert(hlMiddle == hlStart &- 1, "hl not decremented correctly")
        XCTAssert(z80.byte(at: deStart) == z80.byte(at: hlStart), "Byte not transferred")
        var changedFlags = middleFlags.symmetricDifference(startFlags)
        XCTAssert(!changedFlags.contains(Z80A.Status.s), "S flag not preserved")
        XCTAssert(!changedFlags.contains(Z80A.Status.z), "Z flag not preserved")
        XCTAssert(!changedFlags.contains(Z80A.Status.c), "C flag not preserved")
        XCTAssert(changedFlags.contains(Z80A.Status.h), "H flag not reset")
        XCTAssert(changedFlags.contains(Z80A.Status.pv), "PV flag not set")
        XCTAssert(changedFlags.contains(Z80A.Status.n), "N flag not reset")
        XCTAssert(changedFlags.contains(Z80A.Status.y), "Y flag not set")
        XCTAssert(changedFlags.contains(Z80A.Status.x), "X flag not reset")

        precondition(z80.byte(at: deMiddle) != z80.byte(at: hlMiddle),
                     "Byteto transfer should be different")

        runWithTStateCheck(cpu: z80, tStates: 16, failDescription: "ldd [2]")
        let bcEnd = z80.register16("bc")!
        let deEnd = z80.register16("de")!
        let hlEnd = z80.register16("hl")!
        let endFlags = z80.flags
        XCTAssert(bcEnd == bcStart &- 2, "bc not decremented correctly")
        XCTAssert(deEnd == deStart &- 2, "de not decremented correctly")
        XCTAssert(hlEnd == hlStart &- 2, "hl not decremented correctly")
        XCTAssert(z80.byte(at: deMiddle) == z80.byte(at: hlMiddle), "Byte not transferred")
        changedFlags = endFlags.symmetricDifference(startFlags)
        XCTAssert(!changedFlags.contains(Z80A.Status.s), "S flag not preserved")
        XCTAssert(!changedFlags.contains(Z80A.Status.z), "Z flag not preserved")
        XCTAssert(!changedFlags.contains(Z80A.Status.c), "C flag not preserved")
        XCTAssert(changedFlags.contains(Z80A.Status.h), "H flag not reset")
        XCTAssert(!changedFlags.contains(Z80A.Status.pv), "PV flag not set")
        XCTAssert(changedFlags.contains(Z80A.Status.n), "N flag not reset")
        XCTAssert(changedFlags.contains(Z80A.Status.y), "Y flag not set")
        XCTAssert(changedFlags.contains(Z80A.Status.x), "X flag not reset")
    }


    func testLDDR()
    {
        let z80 = createCPU(loadFileName: "lddr")
        reset(z80)
        z80.setRegister8("a", value: 1)
        z80.run(tStates: 30)	// load the three registers
        let bcStart = z80.register16("bc")!
        let deStart = z80.register16("de")!
        let hlStart = z80.register16("hl")!
        precondition(z80.byte(at: deStart) != z80.byte(at: hlStart),
                     "Byteto transfer should be different")
        var startFlags = z80.flags
        startFlags.insert(.h)
        startFlags.remove(.pv)
        startFlags.insert(.n)
        startFlags.remove(.y)
        startFlags.insert(.x)
        z80.flags = startFlags
        runWithTStateCheck(cpu: z80, tStates: 21, failDescription: "lddr [1]")
        let bcMiddle = z80.register16("bc")!
        let deMiddle = z80.register16("de")!
        let hlMiddle = z80.register16("hl")!
        let middleFlags = z80.flags
        XCTAssert(bcMiddle == bcStart &- 1, "bc not decremented correctly")
        XCTAssert(deMiddle == deStart &- 1, "de not incremented correctly")
        XCTAssert(hlMiddle == hlStart &- 1, "hl not incremented correctly")
        XCTAssert(z80.byte(at: deStart) == z80.byte(at: hlStart), "Byte not transferred")
        var changedFlags = middleFlags.symmetricDifference(startFlags)
        XCTAssert(!changedFlags.contains(Z80A.Status.s), "S flag not preserved")
        XCTAssert(!changedFlags.contains(Z80A.Status.z), "Z flag not preserved")
        XCTAssert(!changedFlags.contains(Z80A.Status.c), "C flag not preserved")
        XCTAssert(changedFlags.contains(Z80A.Status.h), "H flag not reset")
        XCTAssert(changedFlags.contains(Z80A.Status.pv), "PV flag not set")
        XCTAssert(changedFlags.contains(Z80A.Status.n), "N flag not reset")
        XCTAssert(changedFlags.contains(Z80A.Status.y), "Y flag not set")
        XCTAssert(changedFlags.contains(Z80A.Status.x), "X flag not reset")

        precondition(z80.byte(at: deMiddle) != z80.byte(at: hlMiddle),
                     "Byteto transfer should be different")

        runWithTStateCheck(cpu: z80, tStates: 16, failDescription: "lddr [2]")
        let bcEnd = z80.register16("bc")!
        let deEnd = z80.register16("de")!
        let hlEnd = z80.register16("hl")!
        let endFlags = z80.flags
        XCTAssert(bcEnd == bcStart &- 2, "bc not decremented correctly")
        XCTAssert(deEnd == deStart &- 2, "de not incremented correctly")
        XCTAssert(hlEnd == hlStart &- 2, "hl not incremented correctly")
        XCTAssert(z80.byte(at: deMiddle) == z80.byte(at: hlMiddle), "Byte not transferred")
        changedFlags = endFlags.symmetricDifference(startFlags)
        XCTAssert(!changedFlags.contains(Z80A.Status.s), "S flag not preserved")
        XCTAssert(!changedFlags.contains(Z80A.Status.z), "Z flag not preserved")
        XCTAssert(!changedFlags.contains(Z80A.Status.c), "C flag not preserved")
        XCTAssert(changedFlags.contains(Z80A.Status.h), "H flag not reset")
        XCTAssert(!changedFlags.contains(Z80A.Status.pv), "PV flag not set")
        XCTAssert(changedFlags.contains(Z80A.Status.n), "N flag not reset")
        XCTAssert(changedFlags.contains(Z80A.Status.y), "Y flag not set")
        XCTAssert(changedFlags.contains(Z80A.Status.x), "X flag not reset")
        runWithTStateCheck(cpu: z80, tStates: 7, failDescription: "lddr: ld a,n")
        let a = z80.register8("a")
        XCTAssert(a == 1, "Didn't execute instruction after lddr")
    }

    func testCPD()
    {
        let z80 = createCPU(loadFileName: "cpd")
        reset(z80)
        z80.run(tStates: 27)	// Two 16 bit loads, 1 8 bit load
        let bcStart = z80.register16("bc")!
        let hlStart = z80.register16("hl")!
        z80.flags = Z80A.Status.c
        runWithTStateCheck(cpu: z80, tStates: 16, failDescription: "ldi [1]")
        let bcMiddle = z80.register16("bc")!
        let hlMiddle = z80.register16("hl")!
        XCTAssert(bcMiddle == bcStart &- 1, "bc not decremented correctly")
        XCTAssert(hlMiddle == hlStart &- 1, "hl not incremented correctly")
        XCTAssert(z80.flags.contains(.s), "S should be set")
        XCTAssert(!z80.flags.contains(.z), "Z should not be set")
        XCTAssert(z80.flags.contains(.h), "H should be set")
        XCTAssert(z80.flags.contains(.p), "P should be set")
        XCTAssert(z80.flags.contains(.n), "N should be set")
        XCTAssert(z80.flags.contains(.c), "C should not be affected")

        runWithTStateCheck(cpu: z80, tStates: 16, failDescription: "ldi [2]")
        let bcEnd = z80.register16("bc")!
        let hlEnd = z80.register16("hl")!
        XCTAssert(bcEnd == bcStart &- 2, "bc not decremented correctly")
        XCTAssert(hlEnd == hlStart &- 2, "hl not incremented correctly")
        XCTAssert(!z80.flags.contains(.s), "S should not be set")
        XCTAssert(z80.flags.contains(.z), "Z should be set")
        XCTAssert(!z80.flags.contains(.h), "H should not be set")
        XCTAssert(!z80.flags.contains(.p), "P should not be set")
        XCTAssert(z80.flags.contains(.n), "N should be set")
        XCTAssert(z80.flags.contains(.c), "C should not be affected")
    }

    func testCPDR()
    {
        let z80 = createCPU(loadFileName: "cpdr")
        reset(z80)
        z80.run(tStates: 27)	// Two 16 bit loads, 1 8 bit load
        let bcStart = z80.register16("bc")!
        let hlStart = z80.register16("hl")!
        z80.flags = Z80A.Status.c
        runWithTStateCheck(cpu: z80, tStates: 21, failDescription: "cpdr [1]")
        let bcMiddle = z80.register16("bc")!
        let hlMiddle = z80.register16("hl")!
        XCTAssert(bcMiddle == bcStart &- 1, "bc not decremented correctly")
        XCTAssert(hlMiddle == hlStart &- 1, "hl not incremented correctly")
        XCTAssert(z80.flags.contains(.s), "S should be set")
        XCTAssert(!z80.flags.contains(.z), "Z should not be set")
        XCTAssert(z80.flags.contains(.h), "H should be set")
        XCTAssert(z80.flags.contains(.p), "P should be set")
        XCTAssert(z80.flags.contains(.n), "N should be set")
        XCTAssert(z80.flags.contains(.c), "C should not be affected")

        runWithTStateCheck(cpu: z80, tStates: 16, failDescription: "cpdr [2]")
        let bcEnd = z80.register16("bc")!
        let hlEnd = z80.register16("hl")!
        XCTAssert(bcEnd == bcStart &- 2, "bc not decremented correctly")
        XCTAssert(hlEnd == hlStart &- 2, "hl not incremented correctly")
        XCTAssert(!z80.flags.contains(.s), "S should not be set")
        XCTAssert(z80.flags.contains(.z), "Z should be set")
        XCTAssert(!z80.flags.contains(.h), "H should not be set")
        XCTAssert(!z80.flags.contains(.p), "P should not be set")
        XCTAssert(z80.flags.contains(.n), "N should be set")
        XCTAssert(z80.flags.contains(.c), "C should not be affected")
    }

    func testCPI()
    {
        let z80 = createCPU(loadFileName: "cpi")
        reset(z80)
        z80.run(tStates: 27)	// Two 16 bit loads, 1 8 bit load
        let bcStart = z80.register16("bc")!
        let hlStart = z80.register16("hl")!
        z80.flags = Z80A.Status.c
        runWithTStateCheck(cpu: z80, tStates: 16, failDescription: "cpi [1]")
        let bcMiddle = z80.register16("bc")!
        let hlMiddle = z80.register16("hl")!
        XCTAssert(bcMiddle == bcStart &- 1, "bc not decremented correctly")
        XCTAssert(hlMiddle == hlStart &+ 1, "hl not incremented correctly")
        XCTAssert(!z80.flags.contains(.s), "S should not be set")
        XCTAssert(!z80.flags.contains(.z), "Z should not be set")
        XCTAssert(!z80.flags.contains(.h), "H should not be set")
        XCTAssert(z80.flags.contains(.p), "P should be set")
        XCTAssert(z80.flags.contains(.n), "N should be set")
        XCTAssert(z80.flags.contains(.c), "C should not be affected")

        runWithTStateCheck(cpu: z80, tStates: 16, failDescription: "cpi [2]")
        let bcEnd = z80.register16("bc")!
        let hlEnd = z80.register16("hl")!
        XCTAssert(bcEnd == bcStart &- 2, "bc not decremented correctly")
        XCTAssert(hlEnd == hlStart &+ 2, "hl not incremented correctly")
        XCTAssert(!z80.flags.contains(.s), "S should not be set")
        XCTAssert(z80.flags.contains(.z), "Z should be set")
        XCTAssert(!z80.flags.contains(.h), "H should not be set")
        XCTAssert(!z80.flags.contains(.p), "P should not be set")
        XCTAssert(z80.flags.contains(.n), "N should be set")
        XCTAssert(z80.flags.contains(.c), "C should not be affected")
    }

    func testCPIR()
    {
        let z80 = createCPU(loadFileName: "cpir")
        reset(z80)
        z80.run(tStates: 27)	// Two 16 bit loads, 1 8 bit load
        let bcStart = z80.register16("bc")!
        let hlStart = z80.register16("hl")!
        z80.flags = Z80A.Status.c
        runWithTStateCheck(cpu: z80, tStates: 21, failDescription: "cpdr [1]")
        let bcMiddle = z80.register16("bc")!
        let hlMiddle = z80.register16("hl")!
        XCTAssert(bcMiddle == bcStart &- 1, "bc not decremented correctly")
        XCTAssert(hlMiddle == hlStart &+ 1, "hl not incremented correctly")
        XCTAssert(!z80.flags.contains(.s), "S should not be set")
        XCTAssert(!z80.flags.contains(.z), "Z should not be set")
        XCTAssert(!z80.flags.contains(.h), "H should not be set")
        XCTAssert(z80.flags.contains(.p), "P should be set")
        XCTAssert(z80.flags.contains(.n), "N should be set")
        XCTAssert(z80.flags.contains(.c), "C should not be affected")

        runWithTStateCheck(cpu: z80, tStates: 16, failDescription: "cpdr [2]")
        let bcEnd = z80.register16("bc")!
        let hlEnd = z80.register16("hl")!
        XCTAssert(bcEnd == bcStart &- 2, "bc not decremented correctly")
        XCTAssert(hlEnd == hlStart &+ 2, "hl not incremented correctly")
        XCTAssert(!z80.flags.contains(.s), "S should not be set")
        XCTAssert(z80.flags.contains(.z), "Z should be set")
        XCTAssert(!z80.flags.contains(.h), "H should not be set")
        XCTAssert(!z80.flags.contains(.p), "P should not be set")
        XCTAssert(z80.flags.contains(.n), "N should be set")
        XCTAssert(z80.flags.contains(.c), "C should not be affected")
    }


    func testJp_nn()
    {
        let z80 = Z80A()
        let destination: UInt16 = 0x100
        z80.set(bytes: [0xc3, destination.lowByte, destination.highByte], at: 0) // jp 100h
        reset(z80)
		runWithTStateCheck(cpu: z80, tStates: 10, failDescription: "jp 100h")
        let pc = z80.register16("pc")!
        XCTAssert(pc == destination + 1, "PC wrong after jump, got \(pc.hexString)h, expected \((destination + 1).hexString)h")
    }

    func testCall()
    {
        let z80 = Z80A()

        let start: UInt16 = 0x101
        let destination: UInt16 = 0x204
        let spBase: UInt16 = 0x1000
        z80.set(bytes: [0xc3, start.lowByte, start.highByte], at: 0) // jp 101h
        z80.set(bytes: [0xcd, destination.lowByte, destination.highByte], at: start) // call 204h
        reset(z80)
        z80.setRegister16("sp", value: spBase)
        runWithTStateCheck(cpu: z80, tStates: 10, failDescription: "jp \(start.hexString)h")
        runWithTStateCheck(cpu: z80, tStates: 17, failDescription: "call \(destination.hexString)h")
        let pc = z80.register16("pc")!
        XCTAssert(pc == destination + 1, "PC wrong after jump, got \(pc.hexString)h, expected \((destination + 1).hexString)h")
        let sp = z80.register16("sp")!
        XCTAssert(sp == spBase &- 2, "sp is wrong, got \(sp.hexString)h, expected \((spBase &- 2).hexString)h")
        let savedPC = UInt16(low: z80.byte(at: sp), high: z80.byte(at: sp &+ 1))
        XCTAssert(savedPC == start &+ 3, "Invalid saved pc, got \(savedPC.hexString)h expected \((start &+ 3).hexString)h")
    }

    func testRet()
    {
        let z80 = Z80A()

        let start: UInt16 = 0x101
        let destination: UInt16 = 0x204
        let spBase: UInt16 = 0x1000
        z80.set(bytes: [0xc3, start.lowByte, start.highByte], at: 0) // jp 101h
        z80.set(byte: 0xc9, at: start) // ret
        reset(z80)
        z80.setRegister16("sp", value: spBase &- 2)
        z80.set(byte: destination.lowByte, at: spBase &- 2)
        z80.set(byte: destination.highByte, at: spBase &- 1)
        runWithTStateCheck(cpu: z80, tStates: 10, failDescription: "jp \(start.hexString)h")
        runWithTStateCheck(cpu: z80, tStates: 10, failDescription: "ret")
        let sp = z80.register16("sp")!
        XCTAssert(sp == spBase, "sp is wrong, got \(sp.hexString)h, expected \((spBase).hexString)h")
        let pc = z80.register16("pc")!
        XCTAssert(pc == destination + 1, "PC is wrong, get \(pc.hexString)h, expected \((destination + 1).hexString)h")
    }

    func testReti()
    {
        // TODO: reti is supposed to do something externally which is not implemented yet
        let z80 = Z80A()

        let start: UInt16 = 0x101
        let destination: UInt16 = 0x204
        let spBase: UInt16 = 0x1000
        z80.set(bytes: [0xc3, start.lowByte, start.highByte], at: 0) // jp 101h
        z80.set(bytes: [0xed, 0x4d], at: start) // reti
        reset(z80)
        z80.setRegister16("sp", value: spBase &- 2)
        z80.set(byte: destination.lowByte, at: spBase &- 2)
        z80.set(byte: destination.highByte, at: spBase &- 1)
        runWithTStateCheck(cpu: z80, tStates: 10, failDescription: "jp \(start.hexString)h")
        runWithTStateCheck(cpu: z80, tStates: 14, failDescription: "reti")
        let sp = z80.register16("sp")!
        XCTAssert(sp == spBase, "sp is wrong, got \(sp.hexString)h, expected \((spBase).hexString)h")
        let pc = z80.register16("pc")!
        XCTAssert(pc == destination + 1, "PC is wrong, get \(pc.hexString)h, expected \((destination + 1).hexString)h")
    }

    func test_arithmetic16_inc_reg()
    {
        let regs: [(String, UInt16)] =
            [
                ("bc", 0x1122),
                ("de", 0x3344),
                ("hl", 0x5566),
                ("sp", 0xffff),
                ("ix", 0x0000),
                ("iy", 0x1111),
            ]
        let z80 = createCPU(loadFileName: "arithmetic16_inc_reg")
        reset(z80)
        load16BitTestRegs(cpu: z80, values: regs)

        for (reg, startValue) in regs
        {
            let tStatesNeeded = reg.hasPrefix("i") ? 10 : 6
            runWithTStateCheck(cpu: z80, tStates: tStatesNeeded, failDescription: "inc \(reg)")
            let newValue = z80.register16(reg)!
            XCTAssert(newValue == startValue &+ 1, "inc \(reg) : wrong value, got \(newValue.hexString)h, expected \((startValue &+ 1).hexString)h")
        }
    }

    func test_arithmetic16_dec_reg()
    {
        let regs: [(String, UInt16)] =
            [
                ("bc", 0x1122),
                ("de", 0x3344),
                ("hl", 0x5566),
                ("sp", 0xffff),
                ("ix", 0x0000),
                ("iy", 0x1111),
                ]
        let z80 = createCPU(loadFileName: "arithmetic16_dec_reg")
        reset(z80)
        load16BitTestRegs(cpu: z80, values: regs)

        for (reg, startValue) in regs
        {
            let tStatesNeeded = reg.hasPrefix("i") ? 10 : 6
            runWithTStateCheck(cpu: z80, tStates: tStatesNeeded, failDescription: "dec \(reg)")
            let newValue = z80.register16(reg)!
            XCTAssert(newValue == startValue &- 1, "dec \(reg) : wrong value, got \(newValue.hexString)h, expected \((startValue &+ 1).hexString)h")
        }
    }

    func testOrFlags()
    {
        let z80 = createCPU(loadFileName: "or")
        reset(z80)
        let address: UInt16 = 0x400
        let aValue: UInt8 = 0xf0
        let otherValue: UInt8 = 0xaa
        z80.setRegister8("a", value: aValue)
        z80.set(byte: otherValue, at: address)
        z80.setRegister16("hl", value: address)
        runWithTStateCheck(cpu: z80, tStates: 7, failDescription: "or (hl)")
        let flags = z80.flags
        XCTAssert(flags.contains(.s), "S should be set")
        XCTAssert(!flags.contains(.z), "Z should be reset")
        XCTAssert(!flags.contains(.h), "H should be reset")
        XCTAssert(flags.contains(.pv), "PV should be set (even parity)")
        XCTAssert(!flags.contains(.n), "N should be reset")
        XCTAssert(!flags.contains(.c), "C should be reset")
    }

    func binary8Test(name: String, operation: (UInt8, UInt8) -> UInt8)
    {
        let z80 = createCPU(loadFileName: name)
        reset(z80)
        let address: UInt16 = 0x400
        let aValue: UInt8 = 0xf0
        let otherValue: UInt8 = 0xaa
        z80.flags.remove(.c)
        z80.setRegister8("a", value: aValue)
        z80.set(byte: otherValue, at: address)
        z80.setRegister16("hl", value: address)
        runWithTStateCheck(cpu: z80, tStates: 7, failDescription: "\(name) (hl)")
        let hlAddressResult = z80.register8("a")!
        XCTAssert(hlAddressResult == operation(aValue, otherValue), "\(name) (hl) wrong result \(hlAddressResult.hexString)h, expected \((aValue &+ otherValue).hexString)h")
        // Register operations
        let reg8bits = [ "b", "c", "d", "e", "h", "l", "a", "ixh", "ixl", "iyh", "iyl"]
        for reg in reg8bits
        {
            z80.setRegister8(reg, value: otherValue)
            z80.flags.remove(.c)
            z80.setRegister8("a", value: aValue)
            let tStatesNeeded: Int
            if reg.hasPrefix("ix") || reg.hasPrefix("iy")
            {
                tStatesNeeded = 8
            }
            else
            {
                tStatesNeeded = 4
            }
            runWithTStateCheck(cpu: z80, tStates: tStatesNeeded, failDescription: "\(name) \(reg)")
            let regResult = z80.register8("a")!
            if reg == "a"
            {
                XCTAssert(regResult == operation(aValue, aValue), "\(name) \(reg) wrong result \(regResult.hexString)h, expected \(operation(aValue, aValue).hexString)h")
            }
            else
            {
                XCTAssert(regResult == operation(aValue, otherValue), "\(name) \(reg) wrong result \(regResult.hexString)h, expected \(operation(aValue, otherValue).hexString)h")
            }
        }
        // op n
        z80.flags.remove(.c)
        z80.setRegister8("a", value: aValue)
        runWithTStateCheck(cpu: z80, tStates: 7, failDescription: "\(name) 0xaa")
        let ornResult = z80.register8("a")!
        XCTAssert(ornResult == operation(aValue, otherValue), "\(name) 0xaa wrong result \(ornResult.hexString)h, expected \(operation(aValue, otherValue).hexString)h")

        // (ix+d)
        z80.flags.remove(.c)
        z80.setRegister8("a", value: aValue)
        z80.setRegister16("ix", value: address &- 2)
        runWithTStateCheck(cpu: z80, tStates: 19, failDescription: "\(name) (ix+2)")
        let ixdResult = z80.register8("a")!
        XCTAssert(ixdResult == operation(aValue, otherValue), "\(name) 0xaa wrong result \(ixdResult.hexString)h, expected \(operation(aValue, otherValue).hexString)h")
        // (iy+d)
        z80.flags.remove(.c)
        z80.setRegister8("a", value: aValue)
        z80.setRegister16("iy", value: address &- 2)
        runWithTStateCheck(cpu: z80, tStates: 19, failDescription: "\(name) (iy+2)")
        let iydResult = z80.register8("a")!
        XCTAssert(iydResult == operation(aValue, otherValue), "\(name) 0xaa wrong result \(iydResult.hexString)h, expected \(operation(aValue, otherValue).hexString)h")
    }



    func testBinary8Ops()
    {
        binary8Test(name: "or", operation: |)
        binary8Test(name: "xor", operation: ^)
        binary8Test(name: "and", operation: &)
        binary8Test(name: "add", operation: &+)
        binary8Test(name: "adc", operation: &+)
        binary8Test(name: "sub", operation: &-)
        binary8Test(name: "sbc", operation: &-)
    }


    func testXorFlags()
    {
        let z80 = createCPU(loadFileName: "xor")
        reset(z80)
        let address: UInt16 = 0x400
        let aValue: UInt8 = 0xf0
        let otherValue: UInt8 = 0xaa
        z80.setRegister8("a", value: aValue)
        z80.set(byte: otherValue, at: address)
        z80.setRegister16("hl", value: address)
        runWithTStateCheck(cpu: z80, tStates: 7, failDescription: "xor (hl)")
        let flags = z80.flags
        XCTAssert(!flags.contains(.s), "S should not be set")
        XCTAssert(!flags.contains(.z), "Z should be reset")
        XCTAssert(!flags.contains(.h), "H should be reset")
        XCTAssert(flags.contains(.pv), "PV should be set (even parity)")
        XCTAssert(!flags.contains(.n), "N should be reset")
        XCTAssert(!flags.contains(.c), "C should be reset")
    }

    func testAndFlags()
    {
        let z80 = createCPU(loadFileName: "and")
        reset(z80)
        let address: UInt16 = 0x400
        let aValue: UInt8 = 0xf0
        let otherValue: UInt8 = 0xaa
        z80.setRegister8("a", value: aValue)
        z80.set(byte: otherValue, at: address)
        z80.setRegister16("hl", value: address)
        runWithTStateCheck(cpu: z80, tStates: 7, failDescription: "or (hl)")
        let flags = z80.flags
        XCTAssert(flags.contains(.s), "S should be set")
        XCTAssert(!flags.contains(.z), "Z should be reset")
        XCTAssert(flags.contains(.h), "H should be set")
        XCTAssert(flags.contains(.pv), "PV should be set (even parity)")
        XCTAssert(!flags.contains(.n), "N should be reset")
        XCTAssert(!flags.contains(.c), "C should be reset")
    }

    func testCp()
    {
        let z80 = createCPU(loadFileName: "cp")
        reset(z80)
        let address: UInt16 = 0x400
        let aValue: UInt8 = 0xf0
        let otherValue: UInt8 = 0xaa
        z80.setRegister8("a", value: aValue)
        z80.set(byte: otherValue, at: address)
        z80.setRegister16("hl", value: address)
        runWithTStateCheck(cpu: z80, tStates: 7, failDescription: "cp (hl)")
        let hlAddressResult = z80.register8("a")!
        XCTAssert(hlAddressResult == (aValue), "cp (hl) wrong result \(hlAddressResult.hexString)h, expected \((aValue | otherValue).hexString)h")
        // Register ors
        let reg8bits = [ "b", "c", "d", "e", "h", "l", "a", "ixh", "ixl", "iyh", "iyl"]
        for reg in reg8bits
        {
            z80.setRegister8(reg, value: otherValue)
            z80.setRegister8("a", value: aValue)
            let tStatesNeeded: Int
            if reg.hasPrefix("ix") || reg.hasPrefix("iy")
            {
                tStatesNeeded = 8
            }
            else
            {
                tStatesNeeded = 4
            }
            runWithTStateCheck(cpu: z80, tStates: tStatesNeeded, failDescription: "cp \(reg)")
            let regResult = z80.register8("a")!
            if reg != "a"
            {
                XCTAssert(regResult == (aValue), "cp \(reg) wrong result \(regResult.hexString)h, expected \((aValue & otherValue).hexString)h")
            }
            else
            {
                XCTAssert(regResult == aValue, "cp \(reg) wrong result \(regResult.hexString)h, expected \(aValue.hexString)h")
            }
        }
        // or n
        z80.setRegister8("a", value: aValue)
        runWithTStateCheck(cpu: z80, tStates: 7, failDescription: "or 0xaa")
        let ornResult = z80.register8("a")!
        XCTAssert(ornResult == (aValue), "cp 0xaa wrong result \(ornResult.hexString)h, expected \((aValue | otherValue).hexString)h")

        // (ix+d)
        z80.setRegister8("a", value: aValue)
        z80.setRegister16("ix", value: address &- 2)
        runWithTStateCheck(cpu: z80, tStates: 19, failDescription: "cp (ix+2)")
        let ixdResult = z80.register8("a")!
        XCTAssert(ixdResult == (aValue), "cp 0xaa wrong result \(ixdResult.hexString)h, expected \((aValue | otherValue).hexString)h")
        // (iy+d)
        z80.setRegister8("a", value: aValue)
        z80.setRegister16("iy", value: address &- 2)
        runWithTStateCheck(cpu: z80, tStates: 19, failDescription: "cp (iy+2)")
        let iydResult = z80.register8("a")!
        XCTAssert(iydResult == (aValue), "cp 0xaa wrong result \(iydResult.hexString)h, expected \((aValue | otherValue).hexString)h")
    }

    func testCpFlags()
    {
        let z80 = createCPU(loadFileName: "cp")
        reset(z80)
        let address: UInt16 = 0x400
        let aValue: UInt8 = 0xf0
        let otherValue: UInt8 = 0xaa
        z80.setRegister8("a", value: aValue)
        z80.set(byte: otherValue, at: address)
        z80.setRegister16("hl", value: address)
        runWithTStateCheck(cpu: z80, tStates: 7, failDescription: "cp (hl)")
        let flags = z80.flags
        XCTAssert(!flags.contains(.s), "S should be reset")
        XCTAssert(!flags.contains(.z), "Z should be reset")
        XCTAssert(flags.contains(.h), "H should be set")
        XCTAssert(!flags.contains(.pv), "PV should be reset")
        XCTAssert(flags.contains(.n), "N should be set")
        XCTAssert(!flags.contains(.c), "C should be reset")
    }

    struct SbcCase: CustomStringConvertible
    {
        let a: UInt8
        let b: UInt8
        let result: UInt8
        let flagsBefore: Z80A.Status
        let flagsAfter: Z80A.Status

        var description: String
        {
            return "a=0x\(a.hexString), b=0x\(b.hexString), result=0x\(result.hexString), flagsBefore=\(flagsBefore), flagsAfter\(flagsAfter)"
        }
    }

    func testSbcBoundaries()
    {
        let testCases: [SbcCase] = [
            SbcCase(a: 0, b: 0, result: 0, flagsBefore: [.h, .s], flagsAfter: [.z, .n]),
            SbcCase(a: 0, b: 0, result: 0xff, flagsBefore: [.c, .h, .s], flagsAfter: [.s, .h, .n, .c, .y, .x]),
            SbcCase(a: 0x80, b: 0x1, result: 0x7f, flagsBefore: [], flagsAfter: [.pv, .h, .n, .y, .x]),
            SbcCase(a: 0x80, b: 0x1, result: 0x7e, flagsBefore: [.c], flagsAfter: [.pv, .h, .n, .y, .x]),
            SbcCase(a: 0x80, b: 0x0, result: 0x7f, flagsBefore: [.c], flagsAfter: [.pv, .h, .n, .y, .x]),
            SbcCase(a: 0x80, b: 0xff, result: 0x81, flagsBefore: [], flagsAfter: [.s, .h, .n, .c]),
            SbcCase(a: 0x0, b: 0x80, result: 0x80, flagsBefore: [], flagsAfter: [.s, .pv, .n, .c]),
            SbcCase(a: 0x0, b: 0x7f, result: 0x80, flagsBefore: [.c], flagsAfter: [.s, .h, .n, .c]),
        ]
        let z80 = Z80A()
        z80.set(byte: 0x98, at: 0)
		for testCase in testCases
        {
            reset(z80)
			z80.setRegister8("a", value: testCase.a)
            z80.setRegister8("b", value: testCase.b)
            z80.flags = testCase.flagsBefore
            runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "sbc a,b")
            let result = z80.register8("a")!
			XCTAssert(result == testCase.result, "Unexpected result for \(testCase), \ngot 0x\(result.hexString)")
            let flags = z80.flags
            XCTAssert(flags == testCase.flagsAfter, "Unexpected flags for \(testCase), \ngot \(flags)")
        }

    }

    func testJp_cc_nn()
    {
        let conditions: [(UInt8, Z80A.Status, Bool, String)] = [
            (0xc2, .z, false, "NZ"),	// jp nz,nn
            (0xca, .z, true, "Z"),		// jp z,nn
            (0xd2, .c, false, "NC"),	// jp nz,nn
            (0xda, .c, true, "C"),		// jp z,nn
            (0xe2, .pv, false, "PO"),	// jp nz,nn
            (0xea, .pv, true, "PE")	,	// jp z,nn
            (0xf2, .s, false, "P"),	// jp nz,nn
            (0xfa, .s, true, "M")		// jp z,nn
        ]

        let z80 = Z80A()
        let target: UInt16 = 0x400
        // Set a template for the jump
        z80.set(bytes: [ 0x00, target.lowByte, target.highByte], at: 0x00)
        for (opCode, flag, jpIfSet, flagDescription) in conditions
        {
			z80.set(byte: opCode, at: 0)
            reset(z80)
            z80.flags.set(flag, from: jpIfSet)
            runWithTStateCheck(cpu: z80, tStates: 10, failDescription: "jp \(flagDescription),0x\(target.hexString)")
            var pc = z80.register16("pc")!
            XCTAssert(pc == target &+ 1, "jp \(flagDescription),0x\(target.hexString): Failed to jump, pc == 0x\(pc.hexString)")
            // Now test branch not taken
            reset(z80)
            z80.flags.set(flag, from: !jpIfSet) // flag is set the other way
            runWithTStateCheck(cpu: z80, tStates: 10, failDescription: "jp \(flagDescription),0x\(target.hexString)")
            pc = z80.register16("pc")!
            XCTAssert(pc == 4, "jp \(flagDescription),0x\(target.hexString): Failed to not jump, pc == 0x\(pc.hexString)")

        }
    }

    func testJr_cc_nn()
    {
        let conditions: [(UInt8, Z80A.Status, Bool, String)] = [
            (0x20, .z, false, "NZ"),	// jr nz,nn
            (0x28, .z, true, "Z"),		// jr z,nn
            (0x30, .c, false, "NC"),	// jr nc,nn
            (0x38, .c, true, "C"),		// jr c,nn
        ]

        let z80 = Z80A()
        let target: UInt16 = 0x20	// Relative,
        // Set a template for the jump
        z80.set(bytes: [ 0x0, (target &- 2).lowByte], at: 0x00)
        for (opCode, flag, jpIfSet, flagDescription) in conditions
        {
            z80.set(byte: opCode, at: 0)
            reset(z80)
            z80.flags.set(flag, from: jpIfSet)
            runWithTStateCheck(cpu: z80, tStates: 12, failDescription: "jr \(flagDescription),0x\(target.hexString)")
            var pc = z80.register16("pc")!
            XCTAssert(pc == target &+ 1, "jr \(flagDescription),0x\(target.hexString): Failed to jump, pc == 0x\(pc.hexString)")
            // Now test branch not taken
            reset(z80)
            z80.flags.set(flag, from: !jpIfSet) // flag is set the other way
            runWithTStateCheck(cpu: z80, tStates: 7, failDescription: "jr \(flagDescription),0x\(target.hexString)")
            pc = z80.register16("pc")!
            XCTAssert(pc == 3, "jr \(flagDescription),0x\(target.hexString): Failed to not jump, pc == 0x\(pc.hexString)")
            
        }
    }


    func test_call_cc_nn()
    {
        let conditions: [(UInt8, Z80A.Status, Bool, String)] = [
            (0xc4, .z, false, "NZ"),	// call nz,nn
            (0xcc, .z, true, "Z"),		// call z,nn
            (0xd4, .c, false, "NC"),	// call nz,nn
            (0xdc, .c, true, "C"),		// call z,nn
            (0xe4, .pv, false, "PO"),	// call nz,nn
            (0xec, .pv, true, "PE")	,	// call z,nn
            (0xf4, .s, false, "P"),		// call nz,nn
            (0xfc, .s, true, "M")		// call z,nn
        ]

        let z80 = Z80A()
        let target: UInt16 = 0x400
        let stackTop: UInt16 = 0x800
        // Set a template for the jump
        z80.set(bytes: [ 0x00, target.lowByte, target.highByte], at: 0x00)
        for (opCode, flag, jpIfSet, flagDescription) in conditions
        {
            z80.set(byte: opCode, at: 0)
            reset(z80)
            z80.setRegister16("sp", value: stackTop)		// Reset the stack
            z80.set(bytes: [0xdd, 0xdd], at: stackTop - 2)	// Blank pattern for the stack
            z80.flags.set(flag, from: jpIfSet)
            runWithTStateCheck(cpu: z80, tStates: 17, failDescription: "jp \(flagDescription),0x\(target.hexString)")
            var pc = z80.register16("pc")!
            XCTAssert(pc == target &+ 1, "call \(flagDescription),0x\(target.hexString): Failed to jump, pc == 0x\(pc.hexString)")
            var sp = z80.register16("sp")!
            XCTAssert(sp == stackTop &- 2, "call \(flagDescription),0x\(target.hexString): Failed to save sp, sp == 0x\(sp.hexString)")
            var savedAddress = UInt16(low: z80.byte(at: sp), high: z80.byte(at: sp + 1))
            XCTAssert(savedAddress == 3, "call \(flagDescription),0x\(target.hexString): Wrong address on stack 0x\(savedAddress.hexString)")
           // Now test branch not taken
            reset(z80)
            z80.setRegister16("sp", value: stackTop)		// Reset the stack
            z80.set(bytes: [0xdd, 0xdd], at: stackTop - 2)	// Blank pattern for the stack
            z80.flags.set(flag, from: !jpIfSet) // flag is set the other way
            runWithTStateCheck(cpu: z80, tStates: 10, failDescription: "call \(flagDescription),0x\(target.hexString)")
            pc = z80.register16("pc")!
            XCTAssert(pc == 4, "jp \(flagDescription),0x\(target.hexString): Failed to not jump, pc == 0x\(pc.hexString)")
            sp = z80.register16("sp")!
            XCTAssert(sp == stackTop, "call \(flagDescription),0x\(target.hexString): sp should not change, sp == 0x\(sp.hexString)")
            savedAddress.lowByte = z80.byte(at: sp &- 2)
            savedAddress.highByte = z80.byte(at: sp &- 1)
            XCTAssert(savedAddress == 0xdddd, "call \(flagDescription),0x\(target.hexString): Wrong address on stack")
       }
    }


    func test_ret_cc_nn()
    {
        let conditions: [(UInt8, Z80A.Status, Bool, String)] = [
            (0xc0, .z, false, "NZ"),	// ret nz,nn
            (0xc8, .z, true, "Z"),		// ret z,nn
            (0xd0, .c, false, "NC"),	// ret nz,nn
            (0xd8, .c, true, "C"),		// ret z,nn
            (0xe0, .pv, false, "PO"),	// ret nz,nn
            (0xe8, .pv, true, "PE")	,	// ret z,nn
            (0xf0, .s, false, "P"),		// ret nz,nn
            (0xf8, .s, true, "M")		// ret z,nn
        ]

        let z80 = Z80A()
        let target: UInt16 = 0x400
        let stackTop: UInt16 = 0x800
        // Set a template for the jump
        z80.set(bytes: [ target.lowByte, target.highByte], at: stackTop &- 2)
        for (opCode, flag, jpIfSet, flagDescription) in conditions
        {
            z80.set(byte: opCode, at: 0)
            reset(z80)
            z80.setRegister16("sp", value: stackTop &- 2)		// Reset the stack
            z80.flags.set(flag, from: jpIfSet)
            runWithTStateCheck(cpu: z80, tStates: 11, failDescription: "ret \(flagDescription)")
            var pc = z80.register16("pc")!
            XCTAssert(pc == target &+ 1, "ret \(flagDescription): Failed to return, pc == 0x\(pc.hexString)")
            var sp = z80.register16("sp")!
            XCTAssert(sp == stackTop, "ret \(flagDescription): Failed to set sp, sp == 0x\(sp.hexString)")

            // Now test branch not taken
            reset(z80)
            z80.setRegister16("sp", value: stackTop &- 2)		// Reset the stack
            z80.flags.set(flag, from: !jpIfSet) // flag is set the other way
            runWithTStateCheck(cpu: z80, tStates: 5, failDescription: "ret \(flagDescription)")
            pc = z80.register16("pc")!
            XCTAssert(pc == 2, "ret \(flagDescription): Failed to not return, pc == 0x\(pc.hexString)")
            sp = z80.register16("sp")!
            XCTAssert(sp == stackTop &- 2, "ret \(flagDescription): sp should not change, sp == 0x\(sp.hexString)")
        }
    }


    func testAdd_a_n()
    {
        let cases: [(UInt8, UInt8)] =
        [
            (0xf2, 0x21),
            (0x12, 0x43),
            (0xff, 0xff),
            (0xfe, 0x02)
        ]
        let z80 = Z80A()
        for (aValue, nValue) in cases
        {
            z80.set(bytes: [0xc6, nValue], at: 0)
            reset(z80)
            z80.setRegister8("a", value: aValue)
            runWithTStateCheck(cpu: z80, tStates: 7, failDescription: "add a,0x\(nValue.hexString)")
            let newA = z80.register8("a")!
            XCTAssert(newA == aValue &+ nValue, "add a,0x\(nValue.hexString) failed, got 0x\(newA.hexString), expected 0x\((aValue &+ nValue).hexString)")
            XCTAssert(z80.flags.isSet(.n, sense: false))
            XCTAssert(z80.flags.isSet(.h, sense: (aValue & 0xf) + (nValue & 0xf) > 0xf), "H set incorrectly")
            XCTAssert(z80.flags.isSet(.c, sense: UInt(aValue) + UInt(nValue) > 0xff), "C set incorrectly")
            XCTAssert(z80.flags.isSet(.s, sense: newA > 0x7f), "S is set incorrectly")
            XCTAssert(z80.flags.isSet(.z, sense: newA == 0), "Z is set incorrectly")
            XCTAssert(z80.flags.isSet(.pv,
                                     sense: (aValue < 0x80 && nValue < 0x80 && newA > 0x7f)
                                            || (aValue > 0x7f && nValue > 0x7f && newA < 0x80)),
                      "PV set incorrectly")

        }
    }

    func testSub_a_n()
    {
        let cases: [(UInt8, UInt8)] =
            [
                (0xf2, 0x21),
                (0x12, 0x43),
                (0xff, 0xff),
                (0xfe, 0x02)
        ]
        let z80 = Z80A()
        for (aValue, nValue) in cases
        {
            z80.set(bytes: [0xd6, nValue], at: 0)
            reset(z80)
            z80.setRegister8("a", value: aValue)
            runWithTStateCheck(cpu: z80, tStates: 7, failDescription: "sub a,0x\(nValue.hexString)")
            let newA = z80.register8("a")!
            XCTAssert(newA == aValue &- nValue, "sub a,0x\(nValue.hexString) failed, got 0x\(newA.hexString), expected 0x\((aValue &- nValue).hexString)")
            XCTAssert(z80.flags.isSet(.n, sense: true))
            XCTAssert(z80.flags.isSet(.h, sense: (aValue & 0xf) < (nValue & 0xf)), "H set incorrectly")
            XCTAssert(z80.flags.isSet(.c, sense: aValue < nValue), "C set incorrectly")
            XCTAssert(z80.flags.isSet(.s, sense: newA > 0x7f), "S is set incorrectly")
            XCTAssert(z80.flags.isSet(.z, sense: newA == 0), "Z is set incorrectly")
            XCTAssert(z80.flags.isSet(.pv,
                                      sense: (aValue < 0x80 && nValue >= 0x80 && newA >= 0x80)
                                        || (aValue >= 0x80 && nValue < 0x80 && newA < 0x80)),
                      "PV set incorrectly")

        }
    }


    func testAdc_a_n()
    {
        let cases: [(UInt8, UInt8)] =
            [
                (0xf2, 0x21),
                (0x12, 0x43),
                (0xff, 0xff),
                (0xfe, 0x02)
        ]
        let z80 = Z80A()
        for (aValue, nValue) in cases
        {
            z80.set(bytes: [0xce, nValue], at: 0)
            reset(z80)
            z80.setRegister8("a", value: aValue)
            z80.flags.remove(.c)
            runWithTStateCheck(cpu: z80, tStates: 7, failDescription: "add a,0x\(nValue.hexString)")
            let newA = z80.register8("a")!
            XCTAssert(newA == aValue &+ nValue, "dd a,0x\(nValue.hexString) failed, got 0x\(newA.hexString), expected 0x\((aValue &+ nValue).hexString)")
            XCTAssert(z80.flags.isSet(.n, sense: false))
            XCTAssert(z80.flags.isSet(.h, sense: (aValue & 0xf) + (nValue & 0xf) > 0xf), "H set incorrectly")
            XCTAssert(z80.flags.isSet(.c, sense: UInt(aValue) + UInt(nValue) > 0xff), "C set incorrectly")
            XCTAssert(z80.flags.isSet(.s, sense: newA > 0x7f), "S is set incorrectly")
            XCTAssert(z80.flags.isSet(.z, sense: newA == 0), "Z is set incorrectly")
            XCTAssert(z80.flags.isSet(.pv,
                                      sense: (aValue < 0x80 && nValue < 0x80 && newA > 0x7f)
                                        || (aValue > 0x7f && nValue > 0x7f && newA < 0x80)),
                      "PV set incorrectly: \(z80.flags.contains(.v)) a=0x\(aValue.hexString), n=0x\(nValue.hexString), newA=0x\(newA.hexString)")

        }
        for (aValue, nValue) in cases
        {
            z80.set(bytes: [0xce, nValue], at: 0)
            reset(z80)
            z80.setRegister8("a", value: aValue)
            z80.flags.insert(.c)
            runWithTStateCheck(cpu: z80, tStates: 7, failDescription: "add a,0x\(nValue.hexString)")
            let newA = z80.register8("a")!
            XCTAssert(newA == aValue &+ nValue &+ 1, "dd a,0x\(nValue.hexString) failed, got 0x\(newA.hexString), expected 0x\((aValue &+ nValue).hexString)")
            XCTAssert(z80.flags.isSet(.n, sense: false))
            XCTAssert(z80.flags.isSet(.h, sense: (aValue & 0xf) + (nValue & 0xf) + 1 > 0xf), "H set incorrectly")
            XCTAssert(z80.flags.isSet(.c, sense: UInt(aValue) + UInt(nValue) + 1 > 0xff), "C set incorrectly")
            XCTAssert(z80.flags.isSet(.s, sense: newA > 0x7f), "S is set incorrectly")
            XCTAssert(z80.flags.isSet(.z, sense: newA == 0), "Z is set incorrectly")
            XCTAssert(z80.flags.isSet(.pv,
                                      sense: (aValue < 0x80 && nValue < 0x80 && newA > 0x7f)
                                        || (aValue > 0x7f && nValue > 0x7f && newA < 0x80)),
                      "PV set incorrectly")

        }
    }


    func testSbc_a_n()
    {
        let cases: [(UInt8, UInt8)] =
            [
                (0xf2, 0x21),
                (0x12, 0x43),
                (0xff, 0xff),
                (0xfe, 0x02)
        ]
        let z80 = Z80A()
        for (aValue, nValue) in cases
        {
            z80.set(bytes: [0xde, nValue], at: 0)
            reset(z80)
            z80.setRegister8("a", value: aValue)
            z80.flags.remove(.c)
            runWithTStateCheck(cpu: z80, tStates: 7, failDescription: "add a,0x\(nValue.hexString)")
            let newA = z80.register8("a")!
            XCTAssert(newA == aValue &- nValue, "sbc a,0x\(nValue.hexString) failed, got 0x\(newA.hexString), expected 0x\((aValue &+ nValue).hexString)")
            XCTAssert(z80.flags.isSet(.n, sense: true))
            XCTAssert(z80.flags.isSet(.h, sense: (aValue & 0xf) < (nValue & 0xf)), "H set incorrectly")
            XCTAssert(z80.flags.isSet(.c, sense: aValue < nValue), "C set incorrectly")
            XCTAssert(z80.flags.isSet(.s, sense: newA > 0x7f), "S is set incorrectly")
            XCTAssert(z80.flags.isSet(.z, sense: newA == 0), "Z is set incorrectly")
            XCTAssert(z80.flags.isSet(.pv,
                                      sense: (aValue < 0x80 && nValue >= 0x80 && newA > 0x7f)
                                        || (aValue > 0x7f && nValue <= 0x7f && newA < 0x80)),
                      "PV set incorrectly \(z80.flags.isSet(.pv, sense: true)) for 0x\(aValue.hexString) - 0x\(nValue.hexString)")

        }
        for (aValue, nValue) in cases
        {
            z80.set(bytes: [0xde, nValue], at: 0)
            reset(z80)
            z80.setRegister8("a", value: aValue)
            z80.flags.insert(.c)
            runWithTStateCheck(cpu: z80, tStates: 7, failDescription: "sbc a,0x\(nValue.hexString)")
            let newA = z80.register8("a")!
            XCTAssert(newA == aValue &- nValue &- 1, "sbc a,0x\(nValue.hexString) failed, got 0x\(newA.hexString), expected 0x\((aValue &- nValue).hexString)")
            XCTAssert(z80.flags.isSet(.n, sense: true))
            XCTAssert(z80.flags.isSet(.h, sense: ((aValue &- 1) & 0xf) < (nValue & 0xf) || aValue == 0),
                      "H set incorrectly \(z80.flags.isSet(.h, sense: true)) for 0x\(aValue.hexString) - 0x\(nValue.hexString) - 1")
            XCTAssert(z80.flags.isSet(.c, sense: UInt(aValue) - 1 < UInt(nValue)), "C set incorrectly")
            XCTAssert(z80.flags.isSet(.s, sense: newA > 0x7f), "S is set incorrectly")
            XCTAssert(z80.flags.isSet(.z, sense: newA == 0), "Z is set incorrectly")
            XCTAssert(z80.flags.isSet(.pv,
                                      sense: (aValue < 0x80 && nValue >= 0x80 && newA > 0x7f)
                                        || (aValue > 0x7f && nValue <= 0x7f && newA < 0x80)),
                      "PV set incorrectly \(z80.flags.isSet(.pv, sense: true)) for 0x\(aValue.hexString) - 0x\(nValue.hexString) - 1")

        }
    }

    func testAdd_hl_rr()
    {
        let cases: [(UInt8, String, UInt16, UInt16)] =
        [
            (0x09, "bc", 0xf234, 0x4321),
            (0x19, "de", 0x1234, 0x4321),
            (0x29, "hl", 0xffff, 0xffff),
            (0x39, "sp", 0xfffe, 0x0002)
        ]
        let z80 = Z80A()
		for (opCode, reg, hlValue, regValue) in cases
        {
            z80.set(byte: opCode, at: 0)
            reset(z80)
            z80.setRegister16("hl", value: hlValue)
            z80.setRegister16(reg, value: regValue)
            runWithTStateCheck(cpu: z80, tStates: 11, failDescription: "add hl,\(reg)")
			let newHL = z80.register16("hl")!
            XCTAssert(newHL == regValue &+ hlValue, "add hl,\(reg) failed, got 0x\(newHL.hexString), expected 0x\((hlValue &+ regValue).hexString)")
            XCTAssert(z80.flags.isSet(.n, sense: false))
            let hSense = (hlValue & 0xfff) + (regValue & 0xfff) > 0xfff
            XCTAssert(z80.flags.isSet(.h, sense: hSense),
                      "H set incorrectly \(z80.flags.contains(.h)): hlValue= 0x\(hlValue.hexString), regValue=0x\(regValue.hexString), newHL=0x\(newHL.hexString)")
            let cSense = UInt(hlValue) &+ UInt(regValue) > 0xffff
            XCTAssert(z80.flags.isSet(.c, sense: cSense), "C set incorrectly \(z80.flags.contains(.c)): hlValue= 0x\(hlValue.hexString), regValue=0x\(regValue.hexString), newHL=0x\(newHL.hexString)")
        }
    }

    func testAdd_ix_rr()
    {
        let cases: [(UInt8, String, UInt16, UInt16)] =
            [
                (0x09, "bc", 0xf234, 0x4321),
                (0x19, "de", 0x1234, 0x4321),
                (0x29, "ix", 0xffff, 0xffff),
                (0x39, "sp", 0xfffe, 0x0002)
        ]
        let z80 = Z80A()
        z80.set(byte: 0xdd, at: 0)

        for (opCode, reg, hlValue, regValue) in cases
        {
            z80.set(byte: opCode, at: 1)
            reset(z80)
            z80.setRegister16("ix", value: hlValue)
            z80.setRegister16(reg, value: regValue)
            runWithTStateCheck(cpu: z80, tStates: 15, failDescription: "add ix,\(reg)")
            let newHL = z80.register16("ix")!
            XCTAssert(newHL == regValue &+ hlValue, "add ix,\(reg) failed, got 0x\(newHL.hexString), expected 0x\((hlValue &+ regValue).hexString)")
            XCTAssert(z80.flags.isSet(.n, sense: false))
            let hSense = (hlValue & 0xfff) + (regValue & 0xfff) > 0xfff
            XCTAssert(z80.flags.isSet(.h, sense: hSense), "H set incorrectly")
            let cSense = UInt(hlValue) + UInt(regValue) > 0xffff
            XCTAssert(z80.flags.isSet(.c, sense: cSense), "C set incorrectly")
        }
    }

    func testAdd_iy_rr()
    {
        let cases: [(UInt8, String, UInt16, UInt16)] =
            [
                (0x09, "bc", 0xf234, 0x4321),
                (0x19, "de", 0x1234, 0x4321),
                (0x29, "iy", 0xffff, 0xffff),
                (0x39, "sp", 0xfffe, 0x0002)
        ]
        let z80 = Z80A()
        z80.set(byte: 0xfd, at: 0)

        for (opCode, reg, hlValue, regValue) in cases
        {
            z80.set(byte: opCode, at: 1)
            reset(z80)
            z80.setRegister16("iy", value: hlValue)
            z80.setRegister16(reg, value: regValue)
            runWithTStateCheck(cpu: z80, tStates: 15, failDescription: "add iy,\(reg)")
            let newHL = z80.register16("iy")!
            XCTAssert(newHL == regValue &+ hlValue, "add iy,\(reg) failed, got 0x\(newHL.hexString), expected 0x\((hlValue &+ regValue).hexString)")
            XCTAssert(z80.flags.isSet(.n, sense: false))
            let hSense = (hlValue & 0xfff) + (regValue & 0xfff) > 0xfff
            XCTAssert(z80.flags.isSet(.h, sense: hSense), "H set incorrectly")
            let cSense = UInt(hlValue) + UInt(regValue) > 0xffff
            XCTAssert(z80.flags.isSet(.c, sense: cSense), "C set incorrectly")
        }
    }


    func testSbc_hl_rr()
    {
        let cases: [(UInt8, String, UInt16, UInt16)] =
            [
                (0x42, "bc", 0xf234, 0x4321),
                (0x52, "de", 0x1234, 0x4321),
                (0x62, "hl", 0xffff, 0xffff),
                (0x72, "sp", 0xfffe, 0x0002)
        ]
        let z80 = Z80A()
        // Start with carry clear
        for (opCode, reg, hlValue, regValue) in cases
        {
            z80.set(bytes: [0xed, opCode], at: 0)
            reset(z80)
            z80.flags.remove(.c)
            z80.setRegister16("hl", value: hlValue)
            z80.setRegister16(reg, value: regValue)
            runWithTStateCheck(cpu: z80, tStates: 15, failDescription: "sbc hl,\(reg)")
            let newHL = z80.register16("hl")!
            XCTAssert(newHL == hlValue &- regValue, "sbc hl,\(reg) failed, got 0x\(newHL.hexString), expected 0x\((hlValue &+ regValue).hexString)")
            XCTAssert(z80.flags.isSet(.n, sense: true), "sbc hl,\(reg) n flag should be set")
            let hSense = (hlValue & 0xfff) < (regValue & 0xfff)
            XCTAssert(z80.flags.isSet(.h, sense: hSense), "H set incorrectly")
            let cSense = UInt(hlValue) &- UInt(regValue) > 0xffff
            XCTAssert(z80.flags.isSet(.c, sense: cSense), "C set incorrectly")
            XCTAssert(z80.flags[.x] == ((newHL >> 8) & 0b00001000 != 0), "adc hl,\(reg) X flag wrong for 0x\(newHL.hexString), flags: \(z80.flags)")
            XCTAssert(z80.flags[.y] == ((newHL >> 8) & 0b00100000 != 0), "adc hl,\(reg) Y flag wrong for 0x\(newHL.hexString), flags: \(z80.flags)")
        }
        // carry set tests
        for (opCode, reg, hlValue, regValue) in cases
        {
            z80.set(bytes: [0xed, opCode], at: 0)
            reset(z80)
            z80.flags.insert(.c)
            z80.setRegister16("hl", value: hlValue)
            z80.setRegister16(reg, value: regValue)
            runWithTStateCheck(cpu: z80, tStates: 15, failDescription: "sbc hl,\(reg)")
            let newHL = z80.register16("hl")!
            XCTAssert(newHL == hlValue &- regValue &- 1, "sbc hl,\(reg) failed, got 0x\(newHL.hexString), expected 0x\((hlValue &+ regValue).hexString)")
            XCTAssert(z80.flags.isSet(.n, sense: true), "sbc hl,\(reg) n flag should be set")
            let hSense = ((hlValue &- 1) & 0xfff) < (regValue & 0xfff)
            XCTAssert(z80.flags.isSet(.h, sense: hSense), "H set incorrectly")
            let cSense = UInt(hlValue) &- UInt(regValue) &- 1 > 0xffff
            XCTAssert(z80.flags.isSet(.c, sense: cSense), "C set incorrectly")
            XCTAssert(z80.flags[.x] == ((newHL >> 8) & 0b00001000 != 0), "adc hl,\(reg) X flag wrong for 0x\(newHL.hexString), flags: \(z80.flags)")
            XCTAssert(z80.flags[.y] == ((newHL >> 8) & 0b00100000 != 0), "adc hl,\(reg) Y flag wrong for 0x\(newHL.hexString), flags: \(z80.flags)")
        }
    }

    func testAdc_hl_rr()
    {
        let cases: [(UInt8, String, UInt16, UInt16)] =
            [
                (0x4a, "bc", 0xf234, 0x4321),
                (0x5a, "de", 0x1234, 0x4321),
                (0x6a, "hl", 0xffff, 0xffff),
                (0x7a, "sp", 0xfffe, 0x0002)
        ]
        let z80 = Z80A()
        // Start with carry clear
        for (opCode, reg, hlValue, regValue) in cases
        {
            z80.set(bytes: [0xed, opCode], at: 0)
            reset(z80)
            z80.flags.remove(.c)
            z80.setRegister16("hl", value: hlValue)
            z80.setRegister16(reg, value: regValue)
            runWithTStateCheck(cpu: z80, tStates: 15, failDescription: "adc hl,\(reg)")
            let newHL = z80.register16("hl")!
            XCTAssert(newHL == hlValue &+ regValue, "adc hl,\(reg) failed, got 0x\(newHL.hexString), expected 0x\((hlValue &+ regValue).hexString)")
            XCTAssert(z80.flags.isSet(.n, sense: false), "adc hl,\(reg) n flag should be set")
            let hSense = (hlValue & 0xfff) &+ (regValue & 0xfff) > 0xfff
            XCTAssert(z80.flags.isSet(.h, sense: hSense), "H set incorrectly")
            let cSense = UInt(hlValue) &+ UInt(regValue) > 0xffff
            XCTAssert(z80.flags.isSet(.c, sense: cSense), "C set incorrectly")
            XCTAssert(z80.flags.isSet(.s, sense: newHL > 0x7fff))
            XCTAssert(z80.flags.isSet(.z, sense: newHL == 0))
            XCTAssert(z80.flags[.x] == ((newHL >> 8) & 0b00001000 != 0), "adc hl,\(reg) X flag wrong for 0x\(newHL.hexString), flags: \(z80.flags)")
            XCTAssert(z80.flags[.y] == ((newHL >> 8) & 0b00100000 != 0), "adc hl,\(reg) Y flag wrong for 0x\(newHL.hexString), flags: \(z80.flags)")
        }
        // carry set tests
        for (opCode, reg, hlValue, regValue) in cases
        {
            z80.set(bytes: [0xed, opCode], at: 0)
            reset(z80)
            z80.flags.insert(.c)
            z80.setRegister16("hl", value: hlValue)
            z80.setRegister16(reg, value: regValue)
            runWithTStateCheck(cpu: z80, tStates: 15, failDescription: "adc hl,\(reg)")
            let newHL = z80.register16("hl")!
            XCTAssert(newHL == hlValue &+ regValue &+ 1, "adc hl,\(reg) failed, got 0x\(newHL.hexString), expected 0x\((hlValue &+ regValue).hexString)")
            XCTAssert(z80.flags.isSet(.n, sense: false), "adc hl,\(reg) n flag should be set")
            let hSense = (hlValue & 0xfff) &+ (regValue & 0xfff) &+ 1 > 0xfff
            XCTAssert(z80.flags.isSet(.h, sense: hSense), "H set incorrectly")
            let cSense = UInt(hlValue) &+ UInt(regValue) &+ 1 > 0xffff
            XCTAssert(z80.flags.isSet(.c, sense: cSense), "C set incorrectly")
            XCTAssert(z80.flags[.x] == ((newHL >> 8) & 0b00001000 != 0), "adc hl,\(reg) X flag wrong for 0x\(newHL.hexString), flags: \(z80.flags)")
            XCTAssert(z80.flags[.y] == ((newHL >> 8) & 0b00100000 != 0), "adc hl,\(reg) Y flag wrong for 0x\(newHL.hexString), flags: \(z80.flags)")
        }
    }

    func testRLA()
    {
        let z80 = Z80A()
        z80.set(bytes: [ 0x17, 0x17 ], at: 0) // RLA x 2
        let initialA: UInt8 = 0x86
        reset(z80)
        z80.setRegister8("a", value: initialA)
        z80.flags.remove(.c)
        runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "rla")
        let a1 = z80.register8("a")!
        XCTAssert(a1 == initialA << 1, "rla failed, got 0x\(a1.hexString), expected 0x\(initialA << 1)")
        XCTAssert(z80.flags.isSet(.c, sense: true), "Carry  cleared")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
        runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "rlca")
        let a2 = z80.register8("a")!
        XCTAssert(a2 == (initialA << 2) | 0x1, "rla [2] failed, got 0x\(a2.hexString), expected 0x\(((initialA << 1) | 1).hexString)")
        XCTAssert(z80.flags.isSet(.c, sense: false), "Carry not set")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
    }

    func testRL_ix_d()
    {
        let z80 = Z80A()
        let address: UInt16 = 0x100
        let displacement: UInt8 = 0x2

        z80.set(bytes: [ 0xdd, 0xcb, displacement, 0x16,
                         0xdd, 0xcb, displacement, 0x16 ], at: 0) // RL (ix+2) x 2
        let initialA: UInt8 = 0x86
        reset(z80)
        z80.set(byte: initialA, at: address)
        z80.setRegister16("ix", value: address &- UInt16(displacement))
        z80.flags.remove(.c)
        runWithTStateCheck(cpu: z80, tStates: 23, failDescription: "rla")
        let a1 = z80.byte(at: address)
        XCTAssert(a1 == initialA << 1, "rla failed, got 0x\(a1.hexString), expected 0x\(initialA << 1)")
        XCTAssert(z80.flags.isSet(.c, sense: true), "Carry  cleared")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
        runWithTStateCheck(cpu: z80, tStates: 23, failDescription: "rlca")
        let a2 = z80.byte(at: address)
        XCTAssert(a2 == (initialA << 2) | 0x1, "rla [2] failed, got 0x\(a2.hexString), expected 0x\(((initialA << 1) | 1).hexString)")
        XCTAssert(z80.flags.isSet(.c, sense: false), "Carry not set")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
    }

    func testRL_iy_d()
    {
        let z80 = Z80A()
        let address: UInt16 = 0x100
        let displacement: UInt8 = 0x2

        z80.set(bytes: [ 0xfd, 0xcb, displacement, 0x16,
                         0xfd, 0xcb, displacement, 0x16 ], at: 0) // RL (ix+2) x 2
        let initialA: UInt8 = 0x86
        reset(z80)
        z80.set(byte: initialA, at: address)
        z80.setRegister16("iy", value: address &- UInt16(displacement))
        z80.flags.remove(.c)
        runWithTStateCheck(cpu: z80, tStates: 23, failDescription: "rla")
        let a1 = z80.byte(at: address)
        XCTAssert(a1 == initialA << 1, "rla failed, got 0x\(a1.hexString), expected 0x\(initialA << 1)")
        XCTAssert(z80.flags.isSet(.c, sense: true), "Carry  cleared")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
        runWithTStateCheck(cpu: z80, tStates: 23, failDescription: "rlca")
        let a2 = z80.byte(at: address)
        XCTAssert(a2 == (initialA << 2) | 0x1, "rla [2] failed, got 0x\(a2.hexString), expected 0x\(((initialA << 1) | 1).hexString)")
        XCTAssert(z80.flags.isSet(.c, sense: false), "Carry not set")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
    }
    

    func testRLCA()
    {
        let z80 = Z80A()
        z80.set(bytes: [ 0x07, 0x07], at: 0) // Two rotates
        let initialA: UInt8 = 0x61
        reset(z80)
        z80.setRegister8("a", value: initialA)
		runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "rlca")
        let a1 = z80.register8("a")!
        XCTAssert(a1 == initialA << 1, "Rotate1 failed")
        XCTAssert(z80.flags.isSet(.c, sense: false), "Carry not cleared")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
        runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "rlca")
        let a2 = z80.register8("a")!
        XCTAssert(a2 == (initialA << 2) | 1, "Rotate2 failed, got 0x\(a2.hexString), expected 0x\(((initialA << 1) | 1).hexString)")
        XCTAssert(z80.flags.isSet(.c, sense: true), "Carry not set")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
    }


    func testRLC_ix_d()
    {
        let address: UInt16 = 0x100
        let displacement: UInt8 = 0x2
        let z80 = Z80A()
        z80.set(bytes: [ 0xdd, 0xcb, displacement, 0x06,
                         0xdd, 0xcb, displacement, 0x06], at: 0) // Two rotates
        let initialA: UInt8 = 0x61
        reset(z80)
        z80.set(byte: initialA, at: address)
        z80.setRegister16("ix", value: address &- UInt16(displacement))
        runWithTStateCheck(cpu: z80, tStates: 23, failDescription: "rlc (ix+2)")
        let a1 = z80.byte(at: address)
        XCTAssert(a1 == initialA << 1, "Rotate1 failed")
        XCTAssert(z80.flags.isSet(.c, sense: false), "Carry not cleared")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
        runWithTStateCheck(cpu: z80, tStates: 23, failDescription: "rlc (ix+2)")
        let a2 = z80.byte(at: address)
        XCTAssert(a2 == (initialA << 2) | 1, "Rotate2 failed, got 0x\(a2.hexString), expected 0x\(((initialA << 1) | 1).hexString)")
        XCTAssert(z80.flags.isSet(.c, sense: true), "Carry not set")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
    }

    func testRLC_iy_d()
    {
        let address: UInt16 = 0x100
        let displacement: UInt8 = 0x2
        let z80 = Z80A()
        z80.set(bytes: [ 0xfd, 0xcb, displacement, 0x06,
                         0xfd, 0xcb, displacement, 0x06], at: 0) // Two rotates
        let initialA: UInt8 = 0x61
        reset(z80)
        z80.set(byte: initialA, at: address)
        z80.setRegister16("iy", value: address &- UInt16(displacement))
        runWithTStateCheck(cpu: z80, tStates: 23, failDescription: "rlc (ix+2)")
        let a1 = z80.byte(at: address)
        XCTAssert(a1 == initialA << 1, "Rotate1 failed")
        XCTAssert(z80.flags.isSet(.c, sense: false), "Carry not cleared")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
        runWithTStateCheck(cpu: z80, tStates: 23, failDescription: "rlc (ix+2)")
        let a2 = z80.byte(at: address)
        XCTAssert(a2 == (initialA << 2) | 1, "Rotate2 failed, got 0x\(a2.hexString), expected 0x\(((initialA << 1) | 1).hexString)")
        XCTAssert(z80.flags.isSet(.c, sense: true), "Carry not set")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
    }


    func testRRCA()
    {
        let z80 = Z80A()
        z80.set(bytes: [ 0x0f, 0x0f], at: 0) // Two rotates
        let initialA: UInt8 = 0x86
        reset(z80)
        z80.setRegister8("a", value: initialA)
        runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "rlca")
        let a1 = z80.register8("a")!
        XCTAssert(a1 == initialA >> 1, "Rotate1 failed")
        XCTAssert(z80.flags.isSet(.c, sense: false), "Carry not cleared")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
        runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "rlca")
        let a2 = z80.register8("a")!
        XCTAssert(a2 == (initialA >> 2) | 0x80, "Rotate2 failed, got 0x\(a2.hexString), expected 0x\(((initialA << 1) | 1).hexString)")
        XCTAssert(z80.flags.isSet(.c, sense: true), "Carry not set")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
    }

    func testRRC_ix_d()
    {
        let address: UInt16 = 0x100
        let displacement: UInt8 = 0x2
        let z80 = Z80A()
        z80.set(bytes: [ 0xdd, 0xcb, displacement, 0x0e,
                         0xdd, 0xcb, displacement, 0x0e], at: 0) // Two rotates
        let initialA: UInt8 = 0x86
        reset(z80)
        z80.set(byte: initialA, at: address)
        z80.setRegister16("ix", value: address &- UInt16(displacement))
        runWithTStateCheck(cpu: z80, tStates: 23, failDescription: "rrc (ix+2)")
        let a1 = z80.byte(at: address)
        XCTAssert(a1 == initialA >> 1, "Rotate1 failed")
        XCTAssert(z80.flags.isSet(.c, sense: false), "Carry not cleared")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
        runWithTStateCheck(cpu: z80, tStates: 23, failDescription: "rrc (ix+2)")
        let a2 = z80.byte(at: address)
        XCTAssert(a2 == (initialA >> 2) | 0x80, "Rotate2 failed, got 0x\(a2.hexString), expected 0x\(((initialA << 1) | 1).hexString)")
        XCTAssert(z80.flags.isSet(.c, sense: true), "Carry not set")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
    }

    func testRRC_iy_d()
    {
        let address: UInt16 = 0x100
        let displacement: UInt8 = 0x2
        let z80 = Z80A()
        z80.set(bytes: [ 0xfd, 0xcb, displacement, 0x0e,
                         0xfd, 0xcb, displacement, 0x0e], at: 0) // Two rotates
        let initialA: UInt8 = 0x86
        reset(z80)
        z80.set(byte: initialA, at: address)
        z80.setRegister16("iy", value: address &- UInt16(displacement))
        runWithTStateCheck(cpu: z80, tStates: 23, failDescription: "rrc (iy+2)")
        let a1 = z80.byte(at: address)
        XCTAssert(a1 == initialA >> 1, "Rotate1 failed")
        XCTAssert(z80.flags.isSet(.c, sense: false), "Carry not cleared")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
        runWithTStateCheck(cpu: z80, tStates: 23, failDescription: "rrc (iy+2)")
        let a2 = z80.byte(at: address)
        XCTAssert(a2 == (initialA >> 2) | 0x80, "Rotate2 failed, got 0x\(a2.hexString), expected 0x\(((initialA << 1) | 1).hexString)")
        XCTAssert(z80.flags.isSet(.c, sense: true), "Carry not set")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
    }

    func testRRA()
    {
        let z80 = Z80A()
        z80.set(bytes: [ 0x1f, 0x1f ], at: 0) // RRA x 2
        let initialA: UInt8 = 0x85
        reset(z80)
        z80.setRegister8("a", value: initialA)
        z80.flags.remove(.c)
        runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "rra")
        let a1 = z80.register8("a")!
        XCTAssert(a1 == initialA >> 1, "rra failed, got 0x\(a1.hexString), expected 0x\(initialA << 1)")
        XCTAssert(z80.flags.isSet(.c, sense: true), "Carry  cleared")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
        runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "rlca")
        let a2 = z80.register8("a")!
        XCTAssert(a2 == (initialA >> 2) | 0x80, "rra [2] failed, got 0x\(a2.hexString), expected 0x\(((initialA << 1) | 1).hexString)")
        XCTAssert(z80.flags.isSet(.c, sense: false), "Carry not set")
        XCTAssert(z80.flags.isSet(.h , sense: false), "H not cleared")
        XCTAssert(z80.flags.isSet(.n, sense: false), "H not cleared")
    }

    func test8BitDec()
    {
        let z80 = createCPU(loadFileName: "dec")
        reset(z80)
        let address: UInt16 = 0x400
        let otherValue: UInt8 = 0xaa
        z80.set(byte: otherValue, at: address)
        z80.setRegister16("hl", value: address)
        runWithTStateCheck(cpu: z80, tStates: 11, failDescription: "dec (hl)")
        let hlAddressResult = z80.byte(at: address)
        XCTAssert(hlAddressResult == (otherValue - 1), "dec  (hl) wrong result \(hlAddressResult.hexString)h, expected \((otherValue - 1).hexString)h")
        // Register ors
        let reg8bits = [ "b", "c", "d", "e", "h", "l", "a", "ixh", "ixl", "iyh", "iyl"]
        for reg in reg8bits
        {
            z80.setRegister8(reg, value: otherValue)
            let tStatesNeeded: Int
            if reg.hasPrefix("ix") || reg.hasPrefix("iy")
            {
                tStatesNeeded = 8
            }
            else
            {
                tStatesNeeded = 4
            }
            runWithTStateCheck(cpu: z80, tStates: tStatesNeeded, failDescription: "dec \(reg)")
            let regResult = z80.register8(reg)!
        	XCTAssert(regResult == (otherValue - 1), "dec \(reg) wrong result \(regResult.hexString)h, expected \((otherValue - 1).hexString)h")
        }

        // (ix+d)
        z80.setRegister16("ix", value: address &- 2)
        z80.set(byte: otherValue, at: address)
        runWithTStateCheck(cpu: z80, tStates: 23, failDescription: "dec (ix+2)")
        let ixdResult = z80.byte(at: address)
        XCTAssert(ixdResult == (otherValue &- 1), "dec 0xaa wrong result \(ixdResult.hexString)h, expected \((otherValue &- 1).hexString)h")
        // (iy+d)
        z80.setRegister16("iy", value: address &- 2)
        z80.set(byte: otherValue, at: address)
        runWithTStateCheck(cpu: z80, tStates: 23, failDescription: "dec (iy+2)")
        let iydResult = z80.byte(at: address)
        XCTAssert(iydResult == (otherValue &- 1), "dec 0xaa wrong result \(iydResult.hexString)h, expected \((otherValue &- 1).hexString)h")
    }

    func test8BitInc()
    {
        let z80 = createCPU(loadFileName: "inc")
        reset(z80)
        let address: UInt16 = 0x400
        let otherValue: UInt8 = 0xaa
        z80.set(byte: otherValue, at: address)
        z80.setRegister16("hl", value: address)
        runWithTStateCheck(cpu: z80, tStates: 11, failDescription: "inc (hl)")
        let hlAddressResult = z80.byte(at: address)
        XCTAssert(hlAddressResult == (otherValue + 1), "inc  (hl) wrong result \(hlAddressResult.hexString)h, expected \((otherValue + 1).hexString)h")
        // Register ors
        let reg8bits = [ "b", "c", "d", "e", "h", "l", "a", "ixh", "ixl", "iyh", "iyl"]
        for reg in reg8bits
        {
            z80.setRegister8(reg, value: otherValue)
            let tStatesNeeded: Int
            if reg.hasPrefix("ix") || reg.hasPrefix("iy")
            {
                tStatesNeeded = 8
            }
            else
            {
                tStatesNeeded = 4
            }
            runWithTStateCheck(cpu: z80, tStates: tStatesNeeded, failDescription: "inc \(reg)")
            let regResult = z80.register8(reg)!
            XCTAssert(regResult == (otherValue + 1), "inc \(reg) wrong result \(regResult.hexString)h, expected \((otherValue + 1).hexString)h")
        }

        // (ix+d)
        z80.setRegister16("ix", value: address &- 2)
        z80.set(byte: otherValue, at: address)
        runWithTStateCheck(cpu: z80, tStates: 23, failDescription: "inc (ix+2)")
        let ixdResult = z80.byte(at: address)
        XCTAssert(ixdResult == (otherValue &+ 1), "inc 0xaa wrong result \(ixdResult.hexString)h, expected \((otherValue &+ 1).hexString)h")
        // (iy+d)
        z80.setRegister16("iy", value: address &- 2)
        z80.set(byte: otherValue, at: address)
       	runWithTStateCheck(cpu: z80, tStates: 23, failDescription: "dec (iy+2)")
        let iydResult = z80.byte(at: address)
        XCTAssert(iydResult == (otherValue &+ 1), "inc 0xaa wrong result \(iydResult.hexString)h, expected \((otherValue &+ 1).hexString)h")
    }

    func testDI_EI()
    {
		let z80 = Z80A()
        z80.set(bytes: [ 0xf3, 0xfb, 0xf3 ], at: 0)
    	reset(z80)
        runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "di")
        var iff1 = z80.flipFlop("iff1")!
        var iff2 = z80.flipFlop("iff2")!
        XCTAssert(!iff1, "iff1 should be reset")
        XCTAssert(!iff2, "iff2 should be reset")
        runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "di")
        iff1 = z80.flipFlop("iff1")!
        iff2 = z80.flipFlop("iff2")!
        XCTAssert(iff1, "iff1 should be reset")
        XCTAssert(iff2, "iff2 should be reset")
        runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "di")
        iff1 = z80.flipFlop("iff1")!
        iff2 = z80.flipFlop("iff2")!
        XCTAssert(!iff1, "iff1 should be reset")
        XCTAssert(!iff2, "iff2 should be reset")
	}

	func testNOP()
    {
        let regs = ["af", "bc", "de", "hl", "sp", "ix", "iy"]
        var initialRegValues: [String: UInt16] = [:]
        let z80 = Z80A()
        z80.set(byte: 0, at: 0)
        reset(z80)
        load8BitTestRegs(cpu: z80)
        for reg in regs
        {
            initialRegValues[reg] = z80.register16(reg)!
        }
		runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "nop")
		for reg in regs
        {
            let oldValue = initialRegValues[reg]!
            let newValue = z80.register16(reg)!
            XCTAssert(newValue == oldValue, "Register \(reg) got updated")
        }
        let pc = z80.register16("pc")!
    	XCTAssert(pc == 2, "pc not incremented 0x\(pc.hexString)")
    }

    func testBit_r()
    {
        let z80 = Z80A()
        z80.set(byte: 0xcb, at: 0)
        var cState = false
        for (index, reg): (UInt8, String) in [(0, "b"), (1, "c"), (2, "d"), (3, "e"), (4, "h"), (5, "l"), (7, "a")]
        {
            let testPattern: UInt8 = 0xaa
            z80.setRegister8(reg, value: testPattern)
            for bit: UInt8 in 0 ... 7
            {
				let bitOp = 0b01000000 | (bit << 3) | index
                z80.set(byte: bitOp, at: 1)
				reset(z80)
                z80.flags = cState ? .c : Z80A.Status()
                z80.setRegister8(reg, value: testPattern)

                runWithTStateCheck(cpu: z80, tStates: 8, failDescription: "bit \(bit), \(reg)")
				XCTAssert(z80.flags.isSet(.z, sense: (testPattern >> bit) & 1 == 0), "bit \(bit), \(reg) Z flag is wrong")
                XCTAssert(z80.flags.contains(.h), "h should be set")
                XCTAssert(!z80.flags.contains(.n), "n should not be set")
                XCTAssert(z80.flags[.x] == testPattern.isSet(bit: 3), "X flag is incorrect")
                XCTAssert(z80.flags[.y] == testPattern.isSet(bit: 5), "Y flag is incorrect")
                XCTAssert(z80.flags[.p] == z80.flags[.z], "bit \(bit), (ix+2) P flag is wrong")
                XCTAssert(z80.flags[.c] == cState, "C was not preserved, c should be \(cState), flags are \(z80.flags)")
                XCTAssert(z80.flags[.s] == (testPattern > 0x7f && bit == 7), "S was not set correctly, flags are \(z80.flags)")
                cState = !cState
            }
        }
    }

    func testBit_addressInHl()
    {
        let z80 = Z80A()
        let testPattern: UInt8 = 0xaa
        let address: UInt16 = 0b0000_0111_1111_1111
        z80.set(bytes: [0xcb, 0x0], at: 0)
        z80.set(byte: testPattern, at: address)
        z80.setRegister16("hl", value: address)
        for bit: UInt8 in 0 ... 7
        {
            let bitOp = 0b01000110 | (bit << 3)
            z80.set(byte: bitOp, at: 1)
            reset(z80)

            runWithTStateCheck(cpu: z80, tStates: 12, failDescription: "bit \(bit), (hl)")
            XCTAssert(z80.flags.isSet(.z, sense: (testPattern >> bit) & 1 == 0), "bit \(bit), (hl) Z flag is wrong")
            XCTAssert(z80.flags.contains(.h), "h should be set")
            XCTAssert(!z80.flags.contains(.n), "n should not be set")
            // This should be what wz is but it could be anythig.
            //XCTAssert(z80.flags[.x] == ((address + 1).highByte & 0x08 != 0), "X flag is incorrect \(z80.flags)")
            XCTAssert(z80.flags[.y] == ((address + 1).highByte & 0x20 != 0), "Y flag is incorrect")
        }
    }

    func testBit_addressInIX_d()
    {
        let z80 = Z80A()
        let testPattern: UInt8 = 0xaa
        let offset: UInt8 = 2
        let index: UInt16 = 0x0800
        z80.set(bytes: [0xdd, 0xcb, offset, 0x0], at: 0) // bit n,(ix+2)
        z80.set(byte: testPattern, at: index + UInt16(offset))
        z80.setRegister16("ix", value: index)
        var cState = false
        for bit: UInt8 in 0 ... 7
        {
            let bitOp = 0b01000110 | (bit << 3)
            z80.set(byte: bitOp, at: 3)
            reset(z80)
            z80.flags = cState ? .c : Z80A.Status()
            runWithTStateCheck(cpu: z80, tStates: 20, failDescription: "bit \(bit), (ix+2)")
            XCTAssert(z80.flags[.z] == ((testPattern >> bit) & 1 == 0), "bit \(bit), (ix+2) Z flag is wrong")
            XCTAssert(z80.flags[.p] == z80.flags[.z], "bit \(bit), (ix+2) P flag is wrong")
            XCTAssert(z80.flags[.h], "h should be set")
            XCTAssert(!z80.flags[.n], "n should not be set")
            XCTAssert(z80.flags[.x] == ((index + UInt16(offset)).highByte & 0x08 != 0), "X flag is incorrect \(z80.flags)")
            XCTAssert(z80.flags[.y] == ((index + UInt16(offset)).highByte & 0x20 != 0), "Y flag is incorrect")
            XCTAssert(z80.flags[.c] == cState, "C was not preserved, c should be \(cState), flags are \(z80.flags)")
            XCTAssert(z80.flags[.s] == (testPattern > 0x7f && bit == 7), "S was not set correctly, flags are \(z80.flags)")
            cState = !cState
        }
    }

    func testBit_addressInIY_d()
    {
        let z80 = Z80A()
        let testPattern: UInt8 = 0xaa
        z80.set(bytes: [0xfd, 0xcb, 0x02, 0x0, testPattern], at: 0) // bit n,(ix+2)
        z80.setRegister16("iy", value: 2)
        for bit: UInt8 in 0 ... 7
        {
            let bitOp = 0b01000110 | (bit << 3)
            z80.set(byte: bitOp, at: 3)
            reset(z80)

            runWithTStateCheck(cpu: z80, tStates: 20, failDescription: "bit \(bit), (iy+2)")
            XCTAssert(z80.flags.isSet(.z, sense: (testPattern >> bit) & 1 == 0), "bit \(bit), (iy+2) Z flag is wrong")
            XCTAssert(z80.flags.contains(.h), "h should be set")
            XCTAssert(!z80.flags.contains(.n), "n should not be set")
        }
    }

    func testDAA()
    {
        let testCases: [(UInt8, Z80A.Status, UInt8)] =
            [
                (0x99, [], 0x99),
                (0x92, [.h], 0x98),
                (0x3c, [.h], 0x42),
                (0x8a, [], 0x90),

                (0x99, [.n], 0x99),
                (0x86, [.n, .h], 0x80),
                (0x89, [.n], 0x89),
            ]
        let z80 = Z80A()
        z80.set(byte: 0x27, at: 0) // daa
		for (aValue, flags, expectedResult) in testCases
        {
            reset(z80)
            z80.setRegister8("a", value: aValue)
            z80.flags = flags
			z80.run(tStates: 4)
            let result = z80.register8("a")!

            XCTAssert(result == expectedResult, "Incorrect result 0x\(result.hexString), expected \(expectedResult.hexString)")
        }
    }

    func testCPL()
    {
        let z80 = Z80A()
        z80.set(byte: 0x2f, at: 0) // cpl
        reset(z80)
		z80.flags = []
        let aValue:UInt8 = 0xaa
        z80.setRegister8("a", value: aValue)
        runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "cpl")
        let result = z80.register8("a")!
		XCTAssert(result == ~aValue, "Unexpected result 0x\(result.hexString), expected 0x\((~aValue).hexString)")
        XCTAssert(z80.flags.contains([.h, .n]), "Flags are wrong \(z80.flags)")
    }

    func testNEG()
    {
        let tests: [UInt8] = [0, 0x80, 0x1, 0xf ]
        let z80 = Z80A()
        z80.set(bytes: [0xed, 0x44], at: 0) // neg
        for aValue in tests
        {
            reset(z80)
            z80.flags = []
            z80.setRegister8("a", value: aValue)
            runWithTStateCheck(cpu: z80, tStates: 8, failDescription: "neg")
            let result = z80.register8("a")!
            XCTAssert(result == 0 &- aValue, "Unexpected result 0x\(result.hexString), expected 0x\((~aValue).hexString)")
        }
    }

    func testSCF()
    {
        let z80 = Z80A()
        z80.set(byte: 0x37, at: 0) // scf
        reset(z80)
        z80.flags = [.h, .n]
        runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "scf")
        XCTAssert(!z80.flags.contains([.h]), "H flag shopuld not be set \(z80.flags)")
        XCTAssert(!z80.flags.contains([.n]), "N flag shopuld not be set \(z80.flags)")
        XCTAssert(z80.flags.contains([.c]), "C flag shopuld be set \(z80.flags)")
    }

    func testCCF()
    {
        let z80 = Z80A()
        z80.set(byte: 0x3f, at: 0) // ccf
        reset(z80)
        z80.flags = [.h, .n]
        runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "ccf")
        XCTAssert(!z80.flags.contains([.h]), "H flag shopuld not be set \(z80.flags)")
        XCTAssert(!z80.flags.contains([.n]), "N flag shopuld not be set \(z80.flags)")
        XCTAssert(z80.flags.contains([.c]), "C flag shopuld be set \(z80.flags)")
        reset(z80)
        z80.flags = [.c, .n]
        runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "ccf")
        XCTAssert(z80.flags.contains([.h]), "H flag should  be set \(z80.flags)")
        XCTAssert(!z80.flags.contains([.n]), "N flag shopuld not be set \(z80.flags)")
        XCTAssert(!z80.flags.contains([.c]), "C flag shopuld not be set \(z80.flags)")
    }

    func testRRD()
    {
        let z80 = Z80A()
        z80.set(bytes: [0xed, 0x67], at: 0) // rrd
        reset(z80)
        let aValue: UInt8 = 0x12
        let hlValue: UInt8 = 0x34
        let expectedA = (aValue & 0xf0) | (hlValue & 0xf)
        let expectedHL = (aValue << 4) | (hlValue >> 4)
        let address: UInt16 = 0x100
        z80.setRegister8("a", value: aValue)
        z80.setRegister16("hl", value: address)
        z80.set(byte: hlValue, at: address)
		runWithTStateCheck(cpu: z80, tStates: 18, failDescription: "rrd")
		let newAValue = z80.register8("a")!
        let newHLValue = z80.byte(at: address)

        XCTAssert(newAValue == expectedA, "a value 0x\(newAValue.hexString) incorrect, expected 0x\(expectedA.hexString)")
        XCTAssert(newHLValue == expectedHL, "hl value 0x\(newHLValue.hexString) incorrect, expected 0x\(expectedHL.hexString)")
    }

    func testRLD()
    {
        let z80 = Z80A()
        z80.set(bytes: [0xed, 0x6f], at: 0) // rld
        reset(z80)
        let aValue: UInt8 = 0x12
        let hlValue: UInt8 = 0x34
        let expectedA = (aValue & 0xf0) | (hlValue >> 4)
        let expectedHL = (aValue & 0xf) | (hlValue << 4)
        let address: UInt16 = 0x100
        z80.setRegister8("a", value: aValue)
        z80.setRegister16("hl", value: address)
        z80.set(byte: hlValue, at: address)
        runWithTStateCheck(cpu: z80, tStates: 18, failDescription: "rld")
        let newAValue = z80.register8("a")!
        let newHLValue = z80.byte(at: address)

        XCTAssert(newAValue == expectedA, "a value 0x\(newAValue.hexString) incorrect, expected 0x\(expectedA.hexString)")
        XCTAssert(newHLValue == expectedHL, "hl value 0x\(newHLValue.hexString) incorrect, expected 0x\(expectedHL.hexString)")
    }

    func testSLA()
    {
        let z80 = Z80A()
        z80.set(bytes: [0xcb, 0x26, 0xcb, 0x26, 0xcb, 0x26], at: 0) // sla (hl) times three
        reset(z80)
        let testByte: UInt8 = 0xc0
        let address: UInt16 = 0x100
        z80.set(byte: testByte, at: address)
        z80.setRegister16("hl", value: address)
        for i in 1 ... 3
        {
            runWithTStateCheck(cpu: z80, tStates: 15, failDescription: "sla (hl)")
            let result = z80.byte(at: address)
            XCTAssert(result == testByte << UInt8(i), "sla[\(i)] failed")
            XCTAssert(z80.flags.isSet(.s, sense: result > 0x7f), "S flag not correct")
            XCTAssert(z80.flags.isSet(.z, sense: result == 0), "Z flag not correct")

        }
    }

    func testSLL()
    {
        let z80 = Z80A()
        z80.set(bytes: [0xdd, 0xcb, 0x0, 0x36,
                        0xfd, 0xcb, 0x0, 0x36,
                        0xcb, 0x36], at: 0) // sll (ix+0), sll  (iy+0), sll (hl) times three
        reset(z80)
        let testByte: UInt8 = 0xc0
        let address: UInt16 = 0x100
        z80.set(byte: testByte, at: address)
        z80.setRegister16("hl", value: address)
        z80.setRegister16("ix", value: address)
        z80.setRegister16("iy", value: address)
        for (i, tstates, name) in [(1, 23, "sll (ix+0)"), (2, 23, "sll (iy+0)"), (3, 15, "sll (hl)")]
        {
            runWithTStateCheck(cpu: z80, tStates: tstates, failDescription: name)
            let result = z80.byte(at: address)
            XCTAssert(result == (testByte << UInt8(i)) | ~(0xff << UInt8(i)), "sll[\(i)] failed")
            XCTAssert(z80.flags.isSet(.s, sense: result > 0x7f), "S flag not correct")
            XCTAssert(z80.flags.isSet(.z, sense: result == 0), "Z flag not correct")

        }
    }

    func testSRL()
    {
        let z80 = Z80A()
        z80.set(bytes: [0xdd, 0xcb, 0x0, 0x3e,
                        0xfd, 0xcb, 0x0, 0x3e,
                        0xcb, 0x3e], at: 0) // srl (ix+0), srl  (iy+0), srl (hl) times three
        reset(z80)
        let testByte: UInt8 = 0x03
        let address: UInt16 = 0x100
        z80.set(byte: testByte, at: address)
        z80.setRegister16("hl", value: address)
        z80.setRegister16("ix", value: address)
        z80.setRegister16("iy", value: address)
        for (i, tstates, name) in [(1, 23, "srl (ix+0)"), (2, 23, "srl (iy+0)"), (3, 15, "srl (hl)")]
        {
            runWithTStateCheck(cpu: z80, tStates: tstates, failDescription: name)
            let result = z80.byte(at: address)
            XCTAssert(result == testByte >> UInt8(i), "srl[\(i)] failed")
            XCTAssert(z80.flags.isSet(.s, sense: result > 0x7f), "S flag not correct")
            XCTAssert(z80.flags.isSet(.z, sense: result == 0), "Z flag not correct")
            XCTAssert(z80.flags[.c] == ((testByte >> UInt8(i - 1) & 1) != 0), "Z flag not correct")

        }
    }

    func testSRA()
    {
        let z80 = Z80A()
        z80.set(bytes: [0xdd, 0xcb, 0x00, 0x2e,
                        0xdd, 0xcb, 0x00, 0x2e,
                        0xdd, 0xcb, 0x00, 0x2e], at: 0) // sra (ix+0) times three
        reset(z80)
        let testByte: UInt8 = 0xc0
        let address: UInt16 = 0x100
        z80.set(byte: testByte, at: address)
        z80.setRegister16("ix", value: address)
        for i in 1 ... 3
        {
            runWithTStateCheck(cpu: z80, tStates: 23, failDescription: "sra (ix+0)")
            let result = z80.byte(at: address)
            let expectedResult = UInt8((testByte.signExtended() >> UInt16(i)) & 0xff)
            XCTAssert(result == expectedResult, "sra[\(i)] failed")
            XCTAssert(z80.flags.isSet(.s, sense: result > 0x7f), "S flag not correct")
            XCTAssert(z80.flags.isSet(.z, sense: result == 0), "Z flag not correct")

        }
    }

    func testResetBitReg()
    {
        let z80 = Z80A()
        z80.set(bytes: [0xcb, 0x00], at: 0) //res 0,b
        for (reg, regIndex) in [("b", 0), ("c", 1), ("d", 2), ("e", 3), ("h", 4), ("l", 5), ("a", 7)]
        {
            for bitNumber in 0 ... 7
            {
                let opCode = UInt8(0x80 | (bitNumber << 3) | regIndex)
                z80.set(byte: opCode, at: 1)
                reset(z80)
                z80.setRegister8(reg, value: 0xff)
                runWithTStateCheck(cpu: z80, tStates: 8, failDescription: "res \(bitNumber),\(reg)")
                let result = z80.register8(reg)!
				XCTAssert(result == UInt8(0xff & ~(1 << bitNumber)), "Wrong result for res \(bitNumber),\(reg), got 0x\(result.hexString)")
            }
        }
        reset(z80)
    }

    func testSetBitReg()
    {
        let z80 = Z80A()
        z80.set(bytes: [0xcb, 0x00], at: 0) //res 0,b
        for (reg, regIndex) in [("b", 0), ("c", 1), ("d", 2), ("e", 3), ("h", 4), ("l", 5), ("a", 7)]
        {
            for bitNumber in 0 ... 7
            {
                let opCode = UInt8(0xc0 | (bitNumber << 3) | regIndex)
                z80.set(byte: opCode, at: 1)
                reset(z80)
                z80.setRegister8(reg, value: 0x0)
                runWithTStateCheck(cpu: z80, tStates: 8, failDescription: "res \(bitNumber),\(reg)")
                let result = z80.register8(reg)!
                XCTAssert(result == UInt8(1 << bitNumber), "Wrong result for res \(bitNumber),\(reg), got 0x\(result.hexString)")
            }
        }
        reset(z80)
    }

    func testSetBitIX()
    {
        let z80 = Z80A()
        z80.set(bytes: [0xdd, 0xcb, 0x00, 0x00, 0x00], at: 0) //res 0,(ix+0)
        for bitNumber in 0 ... 7
        {
            let opCode = UInt8(0xc6 | (bitNumber << 3))
            z80.set(byte: opCode, at: 3)
            z80.set(byte: 0, at: 4)
            reset(z80)
            z80.setRegister16("ix", value: 0x4)
            runWithTStateCheck(cpu: z80, tStates: 23, failDescription: "res \(bitNumber),(ix+0)")
            let result = z80.byte(at: 4)
            XCTAssert(result == UInt8(1 << bitNumber), "Wrong result for res \(bitNumber),(ix+0), got 0x\(result.hexString)")
        }
        reset(z80)
    }


    func testDJNZ()
    {
        let z80 = createCPU(loadFileName: "djnz")
        reset(z80)
		runWithTStateCheck(cpu: z80, tStates: 7, failDescription: "ld b,4")
        runWithTStateCheck(cpu: z80, tStates: 7, failDescription: "ld d,4")
        let loopStart = z80.register16("pc")!
        for countdown: UInt8 in [4, 3, 2, 1]
        {
            let bValue = z80.register8("b")!
            let dValue = z80.register8("d")!
            let pc = z80.register16("pc")!
            XCTAssert(bValue == countdown, "b is wrong")
            XCTAssert(dValue == countdown, "d is wrong")
            XCTAssert(pc == loopStart, "Loop pc is wrong")
            runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "dec d")
            runWithTStateCheck(cpu: z80, tStates: countdown > 1 ? 13 : 8, failDescription: "djnz loop")
        }
        let bValue = z80.register8("b")!
        let dValue = z80.register8("d")!
        let pc = z80.register16("pc")!
        XCTAssert(bValue == 0, "b is wrong")
        XCTAssert(dValue == 0, "d is wrong")
        XCTAssert(pc > loopStart, "Loop pc is wrong")
    }

    func testJR()
    {
        let z80 = createCPU(loadFileName: "jr")
        reset(z80)
        runWithTStateCheck(cpu: z80, tStates: 10, failDescription: "ld bc,loop1")
        runWithTStateCheck(cpu: z80, tStates: 10, failDescription: "ld de,loop2")
        let loop1 = z80.register16("bc")!
        let loop2 = z80.register16("de")!
        var pc = z80.register16("pc")!
        precondition(pc == loop1 &+ 1, "Incorrect pc")
        runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "nop")
        runWithTStateCheck(cpu: z80, tStates: 12, failDescription: "jr e")
		pc = z80.register16("pc")!
        XCTAssert(pc == loop2 &+ 1, "loop 2 jump failed")
        runWithTStateCheck(cpu: z80, tStates: 12, failDescription: "jr e")
        pc = z80.register16("pc")!
        XCTAssert(pc == loop1 &+ 1, "loop 1 jump failed")
    }

    func testJP_rr()
    {
        let z80 = createCPU(loadFileName: "jp_rr")
        reset(z80)
        runWithTStateCheck(cpu: z80, tStates: 10, failDescription: "ld hl,hlTarget")
        runWithTStateCheck(cpu: z80, tStates: 14, failDescription: "ld ix,ixTarget")
        runWithTStateCheck(cpu: z80, tStates: 14, failDescription: "ld iy,iyTarget")
        let hlTarget = z80.register16("hl")!
        let ixTarget = z80.register16("ix")!
        let iyTarget = z80.register16("iy")!
        assert(hlTarget != ixTarget && hlTarget != iyTarget && iyTarget != ixTarget)
		runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "jp (hl)")
        let pcHL = z80.register16("pc")!
        XCTAssert(pcHL == hlTarget &+ 1, "HL jump failed")
        runWithTStateCheck(cpu: z80, tStates: 8, failDescription: "jp (ix)")
        let pcIX = z80.register16("pc")!
        XCTAssert(pcIX == ixTarget &+ 1, "IX jump failed")
        runWithTStateCheck(cpu: z80, tStates: 8, failDescription: "jp (iy)")
        let pcIY = z80.register16("pc")!
        XCTAssert(pcIY == iyTarget &+ 1, "IY jump failed")
    }

    func testRST()
    {
        let z80 = createCPU(loadFileName: "rst")
        reset(z80)
        var pc = z80.register16("pc")!
        var sp: UInt16 = 0xf000
        z80.setRegister16("sp", value: sp)
        runWithTStateCheck(cpu: z80, tStates: 11, failDescription: "rst 0x08")
		var newPc = z80.register16("pc")!
        var newSp = z80.register16("sp")!
        XCTAssert(sp &- newSp == 2, "sp not adjusted")
        XCTAssert(newPc == 0x9, "pc incorrect")
        var savedPc = UInt16(low: z80.byte(at: newSp), high: z80.byte(at: newSp &+ 1))
        XCTAssert(savedPc == pc, "Wrong value saved on stack")
        // ---
        pc = newPc
        sp = newSp
        runWithTStateCheck(cpu: z80, tStates: 11, failDescription: "rst 0x10")
        newPc = z80.register16("pc")!
        newSp = z80.register16("sp")!
        XCTAssert(sp &- newSp == 2, "sp not adjusted")
        XCTAssert(newPc == pc &+ 8, "pc incorrect")
        savedPc = UInt16(low: z80.byte(at: newSp), high: z80.byte(at: newSp &+ 1))
        XCTAssert(savedPc == pc, "Wrong value saved on stack")
        // ---
        pc = newPc
        sp = newSp
        runWithTStateCheck(cpu: z80, tStates: 11, failDescription: "rst 0x18")
        newPc = z80.register16("pc")!
        newSp = z80.register16("sp")!
        XCTAssert(sp &- newSp == 2, "sp not adjusted")
        XCTAssert(newPc == pc &+ 8, "pc incorrect")
        savedPc = UInt16(low: z80.byte(at: newSp), high: z80.byte(at: newSp &+ 1))
        XCTAssert(savedPc == pc, "Wrong value saved on stack")
        // ---
        pc = newPc
        sp = newSp
        runWithTStateCheck(cpu: z80, tStates: 11, failDescription: "rst 0x20")
        newPc = z80.register16("pc")!
        newSp = z80.register16("sp")!
        XCTAssert(sp &- newSp == 2, "sp not adjusted")
        XCTAssert(newPc == pc &+ 8, "pc incorrect")
        savedPc = UInt16(low: z80.byte(at: newSp), high: z80.byte(at: newSp &+ 1))
        XCTAssert(savedPc == pc, "Wrong value saved on stack")
        // ---
        pc = newPc
        sp = newSp
        runWithTStateCheck(cpu: z80, tStates: 11, failDescription: "rst 0x28")
        newPc = z80.register16("pc")!
        newSp = z80.register16("sp")!
        XCTAssert(sp &- newSp == 2, "sp not adjusted")
        XCTAssert(newPc == pc &+ 8, "pc incorrect")
        savedPc = UInt16(low: z80.byte(at: newSp), high: z80.byte(at: newSp &+ 1))
        XCTAssert(savedPc == pc, "Wrong value saved on stack")
        // ---
        pc = newPc
        sp = newSp
        runWithTStateCheck(cpu: z80, tStates: 11, failDescription: "rst 0x30")
        newPc = z80.register16("pc")!
        newSp = z80.register16("sp")!
        XCTAssert(sp &- newSp == 2, "sp not adjusted")
        XCTAssert(newPc == pc &+ 8, "pc incorrect")
        savedPc = UInt16(low: z80.byte(at: newSp), high: z80.byte(at: newSp &+ 1))
        XCTAssert(savedPc == pc, "Wrong value saved on stack")
        // ---
        pc = newPc
        sp = newSp
        runWithTStateCheck(cpu: z80, tStates: 11, failDescription: "rst 0x38")
        newPc = z80.register16("pc")!
        newSp = z80.register16("sp")!
        XCTAssert(sp &- newSp == 2, "sp not adjusted")
        XCTAssert(newPc == pc &+ 8, "pc incorrect")
        savedPc = UInt16(low: z80.byte(at: newSp), high: z80.byte(at: newSp &+ 1))
        XCTAssert(savedPc == pc, "Wrong value saved on stack")
        // ---
        pc = newPc
        sp = newSp
        runWithTStateCheck(cpu: z80, tStates: 11, failDescription: "rst 0x00")
        newPc = z80.register16("pc")!
        newSp = z80.register16("sp")!
        XCTAssert(sp &- newSp == 2, "sp not adjusted")
        XCTAssert(newPc == 1, "pc incorrect")
        savedPc = UInt16(low: z80.byte(at: newSp), high: z80.byte(at: newSp &+ 1))
        XCTAssert(savedPc == pc, "Wrong value saved on stack")
    }

    func testRETN()
    {
        let stackTop: UInt16 = 0xff00
        let target: UInt16 = 0x0102
        let z80 = Z80A()
		z80.set(bytes: [ 0xed, 0x45 ], at: 0)
        reset(z80)
        z80.setRegister16("sp", value: stackTop)
        z80.set(bytes: [target.lowByte, target.highByte], at: stackTop)
        z80.setFlipFlop("iff2", value: true)
        z80.setFlipFlop("iff1", value: false)
		runWithTStateCheck(cpu: z80, tStates: 14, failDescription: "RETN failed")
        let sp = z80.register16("sp")!
        let pc = z80.register16("pc")!
        XCTAssert(pc == target &+ 1, "Invalid pc")
        XCTAssert(sp == stackTop &+ 2, "Invalid sp")
        let iff1 = z80.flipFlop("iff1")!
        XCTAssert(iff1, "iff1 should be set")
    }
}

