//
//  IOTests.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 04/06/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import Z80

class StubIODevice: IODevice
{
    var address: UInt16?
    var data: UInt8?

	func out(address: UInt16, data: UInt8)
    {
		self.address = address
        self.data = data
    }

    func inByte(address: UInt16) -> UInt8
    {
        self.address = address
        return data ?? 0
    }
}

class IOTests: XCTestCase
{

    func runWithTStateCheck(cpu: Z80A, tStates: Int, failDescription: String)
    {
        let startTStates = cpu.cycleCount
        cpu.run(tStates: tStates)
        let actualTStates = cpu.cycleCount - startTStates
        XCTAssert(actualTStates == tStates, "\(failDescription): mismatched tstates, actual = \(actualTStates), expected = \(tStates)")
    }

    func reset(_ cpu: Z80A)
    {
        cpu.reportFatal = {
            (message: String) in
            XCTFail(message)
        }
        cpu.notReset = false
        cpu.run(tStates: 3)
        cpu.notReset = true
        cpu.run(tStates: 1)
    }


    override func setUp()
    {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func test_out_n_a()
    {
        let z80 = Z80A()
        let io1 = StubIODevice()
        let io2 = StubIODevice()
        z80.registerIODevice(io1, forPort: 1)
        z80.registerIODevice(io2, forPort: 2)
        z80.set(bytes: [0xd3, 0x1], at: 0)	// out (1),a
        reset(z80)
        z80.setRegister8("a", value: 0xaa)
        runWithTStateCheck(cpu: z80, tStates: 11, failDescription: "out (1),a")
        if let address = io1.address, let data = io1.data
        {
			XCTAssert(address == 0xaa01, "Wrong address")
            XCTAssert(data == 0xaa, "Invalid data read")
        }
        else
        {
            XCTFail("Expected to write to io1")
        }
        if let _ = io2.address, let _ = io2.data
        {
            XCTFail("Expected not to write to io1")
        }
    }

    func test_out_c_r()
    {
        let z80 = Z80A()
        let io1 = StubIODevice()
        let io2 = StubIODevice()
        z80.registerIODevice(io1, forPort: 1)
        z80.registerIODevice(io2, forPort: 2)
        let testCases: [(String, UInt8, UInt8)] = [("b", 0x41, 0xbb), ("c", 0x49, 0x01),
                                                   ("d", 0x51, 0xdd), ("e", 0x59, 0xee),
                                                   ("h", 0x61, 0x11), ("l", 0x69, 0x22),
                                                   ("a", 0x79, 0xaa)]
        for (reg, opCode, result) in testCases
        {
            z80.set(bytes: [0xed, opCode], at: 0)	// out (c),d
            reset(z80)
            z80.setRegister8("b", value: 0xbb)
            z80.setRegister8("c", value: 0x01)
            z80.setRegister8(reg, value: result)
            runWithTStateCheck(cpu: z80, tStates: 12, failDescription: "out (c),\(reg)")
            if let address = io1.address, let data = io1.data
            {
                XCTAssert(address == 0xbb01, "Wrong address")
                XCTAssert(data == result, "Invalid data read")
            }
            else
            {
                XCTFail("Expected to write to io1")
            }
            if let _ = io2.address, let _ = io2.data
            {
                XCTFail("Expected not to write to io1")
            }

        }
    }

    func test_in_r_c()
    {
        let z80 = Z80A()
        let io1 = StubIODevice()
        z80.registerIODevice(io1, forPort: 1)
        z80.set(bytes: [0xed, 0x78], at: 0)	// in a,(c)
        let testCases: [(String, UInt8, UInt8)] = [
        				 ("b", 0x40, 0),
                         ("c", 0x48, 0xaa),
                         ("d", 0x50, 1),
                         ("e", 0x58, 2),
                         ("h", 0x60, 0xaa),
                         ("l", 0x68, 0xaa),
                         ("a", 0x78, 0xaa)]
        let address: UInt16 = 0xbb01
        for (reg, opcode, theByte) in testCases
        {
            io1.data = theByte
            z80.set(byte: opcode, at: 1)
            reset(z80)
            z80.setRegister8(reg, value: 0x0)
            z80.setRegister16("bc", value: address)
            runWithTStateCheck(cpu: z80, tStates: 12, failDescription: "in \(reg),(c)")
            if let ioAddress = io1.address
            {
                XCTAssert(ioAddress == address, "Wrong address")
            }
            else
            {
                XCTFail("Expected to write to io1")
            }
            let regValue = z80.register8(reg)!
            XCTAssert(regValue == theByte, "Wrong value for \(reg)")
            XCTAssert(z80.flags[.s] == (theByte > 0x7f))
            XCTAssert(z80.flags[.z] == (theByte  == 0))
            XCTAssert(z80.flags.intersection([.h, .n]).isEmpty)
            XCTAssert(z80.flags[.p] == theByte.evenParity)
        }
    }


    func test_in_a_n()
    {
        let z80 = Z80A()
        let io1 = StubIODevice()
        z80.registerIODevice(io1, forPort: 1)
        z80.set(bytes: [0xdb, 0x1], at: 0)	// in a,(n)
        reset(z80)
		z80.setRegister8("a", value: 0xaa)
        io1.data = 0xcc
        let startFlags = z80.flags
        runWithTStateCheck(cpu: z80, tStates: 11, failDescription: "in a,(1)")
        let a = z80.register8("a")!
        XCTAssert(a == 0xcc, "a not set to in value")
        XCTAssert(io1.address != nil && io1.address! == 0xaa01, "Address was not set correctly")
        XCTAssert(z80.flags == startFlags, "Invalid flags")
    }


    func testIM1()
    {
        let z80 = Z80A()
		z80.set(bytes: [
            0xed, 0x56,			// im1
            0xfb,				// ei
            0x00,				// nop
            0xc3, 0x03, 0x00 	// jp 0x4
            ], at: 0)
        reset(z80)
        z80.notInt = false
        runWithTStateCheck(cpu: z80, tStates: 8, failDescription: "im 1")
        var pc = z80.register16("pc")!
        XCTAssert(pc == 3, "im 1: Invalid pc")
        runWithTStateCheck(cpu: z80, tStates: 4, failDescription: "ei")
        pc = z80.register16("pc")!
        XCTAssert(pc == 4, "ei: Invalid pc")
        runWithTStateCheck(cpu: z80, tStates: 6, failDescription: "nop")
        pc = z80.register16("pc")!
        XCTAssert(pc == 0x4, "interrupt: Invalid pc")
        runWithTStateCheck(cpu: z80, tStates: 11, failDescription: "rst 0x38")
        pc = z80.register16("pc")!
        XCTAssert(pc == 0x39, "interrupt: Invalid pc")

    }
}
