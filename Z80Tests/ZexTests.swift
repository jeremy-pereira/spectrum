//
//  ZexTests.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 23/04/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import Z80
import Toolbox

// TODO: Make this a toolbox function
fileprivate extension UInt32
{
    var hexString: String
    {
        return UInt16(self >> 16).hexString + UInt16(self & 0xffff).hexString
    }
}

private let log = Logger.getLogger("Spectrum.Z80Tests.ZexTests")

fileprivate class ZexShiftMonitor: MemoryMappedDevice
{
    let portCount = 4
    let baseAddress: UInt16
    let reporter: (UInt32) -> ()
    weak var cpu: Z80A!
    var instructionBytes = [UInt8](repeating: 0, count: 4)

    init(cpu: Z80A, baseAddress: UInt16, reporter: @escaping (UInt32) -> ())
    {
        self.cpu = cpu
        self.baseAddress = baseAddress
        self.reporter = reporter
    }

    func didWrite(_ portNumber: UInt16, byte: inout UInt8)
    {
        instructionBytes[Int(portNumber)] = byte
    }

    var alreadyFoundInstructions = Set<UInt32>()

    func willRead(_ portNumber: UInt16, byte: inout UInt8)
    {
        // Assume the cpu is reading the instruction if the pc is at the first 
        // port when reading it
        if portNumber == 0 && cpu.register16("pc")! == baseAddress
        {
            var instruction: UInt32? = nil
            if instructionBytes[0] == 0xcb
            {
                instruction = 0xcb << 24 | UInt32(instructionBytes[1]) << 16
            }
            else if (instructionBytes[0] == 0xfd || instructionBytes[0] == 0xdd)
                	&& instructionBytes[1] == 0xcb
            {
                instruction = UInt32(instructionBytes[0]) << 24
                			| 0xcb << 16
                			| 0xdd00
                			| UInt32(instructionBytes[3])
            }
            if let instruction = instruction
            {
				if !alreadyFoundInstructions.contains(instruction)
                {
                    alreadyFoundInstructions.insert(instruction)
                    reporter(instruction)
                }
            }
        }
    }

    var previousDevice: [MemoryMappedDevice?] = [nil, nil]

    func pushDevice(_ device: MemoryMappedDevice, port: UInt16)
    {
        let myIndex = Int(port)
        if let previousDevice = previousDevice[myIndex]
        {
            let address = port &- baseAddress

            device.pushDevice(previousDevice, port: address &- device.baseAddress)
        }
        previousDevice[myIndex] = device
    }
}


/// Tests to riun the Z80 ZEXDOC and ZEXALL test suites
class ZexTests: XCTestCase
{

    override func setUp()
    {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func reset(_ cpu: Z80A)
    {
        cpu.reportFatal = {
            (message: String) in
            XCTFail(message)
        }
        cpu.notReset = false
        cpu.run(tStates: 3)
        cpu.notReset = true
        cpu.run(tStates: 1)
    }

    func createCPU(loadFileName: String) -> CPMSimulator
    {
        let simulator = CPMSimulator(cpu: Z80A())
        let myBundle = Bundle(for: type(of: self))
        let binaryFileUrl = myBundle.url(forResource: loadFileName, withExtension: "com")
        if let binaryFileUrl = binaryFileUrl
        {
            guard let program = try? Data(contentsOf: binaryFileUrl)
                else { fatalError("Could not read test coverage program") }

            var programBytes = [UInt8](repeating: 0, count: program.count)
            program.copyBytes(to: &programBytes, count: programBytes.count)
            simulator.cpu.set(bytes: [0xc3, 0x00, 0x01], at: 0) // jump to program start
            var currentAddress: UInt16 = 0x100
            for aByte in programBytes
            {
                simulator.cpu.set(byte: aByte, at: currentAddress&++)
            }
        }
        else
        {
            fatalError("Could not locate coverage test binary")
        }
        reset(simulator.cpu)
        return simulator
    }


    // Disabled bewcause of some weired error we have been geing since Xcode 8.3.3
//    func testZexDoc()
//    {
//        Logger.pushLevel(.info, forName: "Z80.Z80")
//        defer { Logger.popLevel(forName: "Z80.Z80") }
//        let simulator = createCPU(loadFileName: "zexdoc")
//        let startTStates = simulator.cpu.cycleCount
//        let statesToRun = 10000005
//		simulator.cpu.run(tStates: statesToRun)
//        let actualTStates = simulator.cpu.cycleCount - startTStates
//        XCTAssert(actualTStates == statesToRun, "Only ran for \(actualTStates) of \(statesToRun)")
//    }


    func testZexAll()
    {
        Logger.pushLevel(.info, forName: "Z80.Z80")
        defer { Logger.popLevel(forName: "Z80.Z80") }
        let simulator = createCPU(loadFileName: "zexall")
        let startTStates = simulator.cpu.cycleCount
        let statesToRun = 10000005
        simulator.cpu.run(tStates: statesToRun)
        let actualTStates = simulator.cpu.cycleCount - startTStates
        XCTAssert(actualTStates == statesToRun, "Only ran for \(actualTStates) of \(statesToRun)")
    }


//    func testZexRotoateAndShift()
//    {
//        Logger.pushLevel(.info, forName: "Z80.Z80")
//        defer { Logger.popLevel(forName: "Z80.Z80") }
//        log.pushLevel(.info)
//        defer { log.popLevel() }
//
//        let simulator = createCPU(loadFileName: "zexdoc")
//        let startTStates = simulator.cpu.cycleCount
//        let statesToRun = 9000010
//        simulator.cpu.set(bytes: [0xb0, 0x01 ], at: 0x120)	// Patch to skip to rotate tests
//        // Probe for instruction under test
//        let zexInstructionProbe = ZexShiftMonitor(cpu: simulator.cpu, baseAddress: 0x1d42)
//        {
//            (instruction: UInt32) in
//
//            log.info("Testing shift/rotate \(instruction.hexString)")
//
//        }
//        simulator.cpu.mapDevice(zexInstructionProbe)
//
//        simulator.cpu.run(tStates: statesToRun)
//        let actualTStates = simulator.cpu.cycleCount - startTStates
//        XCTAssert(actualTStates == statesToRun, "Only ran for \(actualTStates) of \(statesToRun)")
//    }
//
    func testPerformanceExample()
    {
        let simulator = createCPU(loadFileName: "zexdoc")
        let statesToRun = 10000000
        self.measure
        {
            simulator.cpu.run(tStates: statesToRun)
            // Put the code you want to measure the time of here.
        }
    }

}
