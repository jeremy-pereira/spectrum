//
//  IODevice.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 04/06/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//


/// Device used for the Z80 in, out instructiosn
public protocol IODevice: class
{

    /// Output a bute to the device
    ///
    /// - Parameters:
    ///   - address: The bits that were placed on the address bus.
    ///   - data: The bits that were placed on the databus.
    func out(address: UInt16, data: UInt8)


    /// Input a byte
    ///
    /// - Parameter address: The bits that were placed on the address bus
    /// - Returns: The byte on the given port.
    func inByte(address: UInt16) -> UInt8
}
