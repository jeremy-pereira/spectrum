//
//  Z80.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 28/03/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//
// Don't import the arithmetic operators from Toolbox because it reduces
// the possibilities for optimisation
import class Toolbox.Logger

infix operator &+=: AssignmentPrecedence
infix operator &-=: AssignmentPrecedence
prefix operator &--
postfix operator &++

private extension UInt16
{
    @discardableResult static postfix func &++(x: inout UInt16) -> UInt16
    {
        let ret = x
        x = x &+ 1
        return ret
    }

    @discardableResult static prefix func &--(x: inout UInt16) -> UInt16
    {
        x = x &- 1
        return x
    }

    static func &-= (lValue: inout UInt16, rValue: UInt16)
    {
        lValue = lValue &- rValue
    }

    static func &+= (lValue: inout UInt16, rValue: UInt16)
    {
        lValue = lValue &+ rValue
    }
}

public extension UInt8
{
    /// Do a signed extend to 16 bits.
    ///
    /// - Returns: The 16 bit 2's complewment equivalent of self interpreted as two's complement
	func signExtended() -> UInt16
    {
        return (self > 0x7f ? 0xff00 : 0x0000) | UInt16(self)
    }

	// TODO: Check if we can do this in a more efficient way
	var evenParity: Bool
    {
        var bits = self
        var isEvenParity = true
        while bits != 0
        {
			if bits & 1 != 0
            {
                isEvenParity = !isEvenParity
            }
            bits >>= 1
        }
        return isEvenParity
    }

	func isSet(bit: Int) -> Bool
    {
        return self & UInt8(1 << bit) != UInt8(0)
    }

	func intersection(_ flags: Z80A.Status) -> Z80A.Status
    {
		return Z80A.Status(rawValue: self).intersection(flags)
    }

    @discardableResult static prefix func &--(x: inout UInt8) -> UInt8
    {
        x = x &- 1
        return x
    }
}

fileprivate extension UInt16
{
	func signExtended() -> UInt
    {
        return (self > 0x7fff ? ~0xffff : 0x0) | UInt(self)
    }
}

fileprivate extension UInt
{
	var lowByte: UInt8
    {
        get { return UInt8(self & 0xff) }
        set { self = (self & ~0xff) | UInt(newValue) }
    }
	var highByte: UInt8
    {
        get { return UInt8((self >> 8) & 0xff) }
        set { self = (self & ~(0xff << 8)) | (UInt(newValue) << 8) }
    }

	func isSet(bit: Int) -> Bool
    {
        return self & UInt(1 << bit) != 0
    }

	func intersection(_ flags: Z80A.Status) -> Z80A.Status
    {
        return Z80A.Status(lowByteOf: self).intersection(flags)
    }

	var lowWord: UInt16
    {
        get { return UInt16(self & 0xffff) }
    }

}

private var log: Logger = Logger.getLogger("Z80.Z80")

///
/// Protocol that allows a device to be mapped to a memory address
///
/// A memory mapped device consists of a number of contiguous ports that should
/// be mapped starting from a particular base address.  Once mapped, the device
/// will be notified before each read and after each write.
///
public protocol MemoryMappedDevice: class
{
    /// Number of ports the
    var portCount: Int { get }
    /// Base address at which the ports should be mapped.
    var baseAddress: UInt16 { get }
    ///
    /// The CPU wrote to the given port.
    ///
    /// - Parameter portNumber: The port which was written to.
    /// - Parameter byte: The byte that was written.  This is an inout parameter
    /// to allow the device to modify the memory address if need be.
    ///
    func didWrite(_ portNumber: UInt16, byte: inout UInt8)
    ///
    /// The CPU is about to read from the given port.
    ///
    /// - Parameter portNumber: The port which will be read.
    /// - Parameter byte: The byte that will be read.  This is an inout parameter
    /// to allow the device to modify the memory address if need be.
    ///
    func willRead(_ portNumber: UInt16, byte: inout UInt8)
    ///
    /// Push a device for chaining.
    ///
    /// This allows a device to map to a port that is already mapped.  If the
    /// device doen't support chaining (for example, it has a performance cost)
    /// this function should fatalError.
    ///
    /// There is no built in support for device chaining.  Devices that do it
    /// must implement it in some way in their willRead and didWrite methods.
    ///
    /// - Parameter previousDevice: The device to push.
    /// - Parameter port: The port that device to push is on.
    ///
    func pushDevice(_ device: MemoryMappedDevice, port: UInt16)
}


/// Protocol for fdevices that are driven from the clock.
public protocol ClockDrivenDevice
{

    /// Synchronise the clock with the CPU clock
    ///
    /// - Parameter time: The current CPU cycm count
    func synchronise(time: Int)
    /// The next time this device needs to be called at.
    var nextCallTime: Int { get }


    /// Drive the device.
    func drive()
}


/// Models the Z80A microprocessor
public class Z80A
{

    /// An option set that represents the Z80 flags.
    public struct Status: OptionSet
    {
        public var rawValue: UInt8

        public init(rawValue: UInt8)
        {
            self.rawValue = rawValue
        }

        fileprivate init(lowByteOf aUInt: UInt)
        {
            self.rawValue = aUInt.lowByte
        }

        /// Sign flag
        public static let s = Status(rawValue: 0b10000000)
        /// Zero flag
        public static let z = Status(rawValue: 0b01000000)
        /// Undocumented, is a copy of bit 5 of the result
        public static let y = Status(rawValue: 0b00100000)
        /// Half carry flag
        public static let h = Status(rawValue: 0b00010000)
        /// Undocumented, a copy of bit 3 of the result
        public static let x = Status(rawValue: 0b00001000)
        /// The parity and signed overfleo flag
        public static let pv = Status(rawValue: 0b00000100)
        /// pv flag aliased as v
        public static let v = Status(rawValue: 0b00000100)
        /// pv flag aliased as p
        public static let p = Status(rawValue: 0b00000100)
        /// Shows whether the last op was addition (0) or subtraction(1)
        public static let n = Status(rawValue: 0b00000010)
        /// Carry flag
        public static let c = Status(rawValue: 0b00000001)

        public static let xyMask = Status([.x, .y])

        /// Set a flag based on the given boolean value
        ///
        /// - Parameters:
        ///   - flag: Flag to set or reset
        ///   - from: true to set, false to reset
        public mutating func set(_ flag: Status, from: Bool = true)
        {
            if from
            {
                self.insert(flag)
            }
            else
            {
                self.remove(flag)
            }
        }


        /// Check if a flag is set or not
        ///
        /// - Parameters:
        ///   - flag: Flag to check
        ///   - sense: which way should it be set, inverts the result
        /// - Returns: <#return value description#>
        public func isSet(_ flag: Status, sense: Bool = true) -> Bool
        {
            return sense ? self.contains(flag) : !self.contains(flag)
        }


        /// Subscript based on flags in option set
        /// The value of the subscript is true if all the options in the index are
        /// set in self and false otherwise. The setter either removes or 
        /// inserts all the flags depending on the value assigned.
        ///
        /// - Parameter index: Option set index spcifying the flags
        public subscript(index: Status) -> Bool
        {
            get
            {
                return self.intersection(index) == index
            }
            set
            {
                if newValue
                {
                    self.formUnion(index)
                }
                else
                {
                    self.subtract(index)
                }
            }
        }
    }

    private struct AccumulatorFile
    {
        fileprivate var a: UInt8 = 0
    	fileprivate var f: Status = Status(rawValue: 0)

        fileprivate var af: UInt16
        {
            get { return (UInt16(a) << 8) | UInt16(f.rawValue) }
            set
            {
                a = UInt8(newValue >> 8)
                f = Status(rawValue: UInt8(newValue & 0xff))
            }
        }
    }

    private struct RegisterFile
    {
        fileprivate var b: UInt8 = 0
        fileprivate var c: UInt8 = 0
        fileprivate var d: UInt8 = 0
        fileprivate var e: UInt8 = 0
        fileprivate var h: UInt8 = 0
        fileprivate var l: UInt8 = 0
        fileprivate var w: UInt8 = 0
        fileprivate var z: UInt8 = 0

        fileprivate var bc: UInt16
        {
            get { return UInt16(b) << 8 | UInt16(c) }
            set
            {
				b = newValue.highByte
                c = newValue.lowByte
            }
        }
        fileprivate var de: UInt16
        {
            get { return UInt16(d) << 8 | UInt16(e) }
            set
            {
                d = newValue.highByte
                e = newValue.lowByte
            }
        }
        fileprivate var hl: UInt16
        {
            get { return UInt16(h) << 8 | UInt16(l) }
            set
            {
                h = newValue.highByte
                l = newValue.lowByte
            }
        }


        /// wz is an internal register used for address calculation. It's not 
        /// directly accessible, but it is needed to calculate the X and Y flags.
        fileprivate var wz: UInt16
        {
            get { return UInt16(w) << 8 | UInt16(z) }
            set
            {
                w = newValue.highByte
                z = newValue.lowByte
            }
        }
  }

//    private enum IndexRegMode
//    {
//        case hl
//        case ix
//        case iy
//    }


    /// The size of the memory in this Z80.
    public static let memorySize = Int(UInt16.max) + 1
    var memory = UnsafeMutablePointer<UInt8>.allocate(capacity: memorySize)
    var locationIsRom = UnsafeMutablePointer<Bool>.allocate(capacity: memorySize)
    var devices = UnsafeMutablePointer<MemoryMappedDevice?>.allocate(capacity: memorySize)

    public init()
    {
		accumulator.af = 0xffff
        altAccumulator.af = 0xffff
		devices.initialize(repeating: nil, count: Z80A.memorySize)
		locationIsRom.initialize(repeating: false, count: Z80A.memorySize)
		memory.initialize(repeating: 0, count: Z80A.memorySize)
    }

	deinit
	{
		devices.deallocate()
	}

    /// Set a debug pattern for all registers. Mainly to test reset
    ///
    /// - Parameter pattern: pattern for ll registers
    func setDebug(pattern: UInt8)
    {
        let doublePattern = UInt16(pattern) << 8 | UInt16(pattern)
		accumulator.af = doublePattern
        altAccumulator.af = doublePattern
        gpRegs.bc = doublePattern
        gpRegs.de = doublePattern
        gpRegs.hl = doublePattern
        altGpRegs.bc = doublePattern
        altGpRegs.de = doublePattern
        altGpRegs.hl = doublePattern
        ix = doublePattern
        iy = doublePattern
        sp = doublePattern
        pc = doublePattern
        i = pattern
    }

    public var reportFatal: (String) -> () = { fatalError($0) }

    public var error: String? = nil

    private func raiseFatal(_ message: @autoclosure () -> String)
    {
        error = message()
        reportFatal(error!)
    }


    /// Reset flag, active low. When the flag is false, goes into the resetting 
    /// state during which the registers are set for restart. When it goes 
    /// high again, instructions start to be executed.
    public var notReset: Bool = true
    {
        didSet
        {
            if !notReset && oldValue
            {
                instruction = Decoder.reset
            }
        }
    }


    /// Interrupt flag. Igf false, and interrupts are enabled, this will cause 
    /// an interrupt to occur.
    public var notInt: Bool = true


    /// Indicates if the CPU is in a halt state.
    public private(set) var halt: Bool = false


    /// This is true if the cpu is in the middle of executing a prefixed 
    /// instruction.
    public var inPrefixMode: Bool
    {
        return decoder !== Decoder.standard
    }

    private var accumulator: AccumulatorFile = AccumulatorFile()
    private var gpRegs: RegisterFile = RegisterFile()
    private var altAccumulator: AccumulatorFile = AccumulatorFile()
    private var altGpRegs: RegisterFile = RegisterFile()

    private var ix: UInt16 = 0
    private var iy: UInt16 = 0
    private var sp: UInt16 = 0
    private var pc: UInt16 = 0
    private var i: UInt8 = 0	// Interrupt vector basde
    private var r: UInt8 = 0
    private var iff1 = false
    private var iff2 = false
    private var im: Int = 0
    private var instruction: Int = Decoder.stop

    private var dataBus: UInt8 = 0


    /// The number of cycles run since the processor was created.
    public final var cycleCount = 0

    private var decoder = Decoder.standard


    /// Runs the processor for the specified number of cycles. Notr that the
    /// processor always runs an exact number of instructions so the processor
    /// may run for a few more cycles than requested.
    ///
    /// - Parameter cycles: Number of z80 t states for which to run
    public final func run(tStates cycles: Int)
    {
        // If the processor has aborted, do nothing
        if error != nil
        {
            return
        }
        let maximumCycles = cycleCount + cycles
        while cycleCount < maximumCycles
        {
            let operation = decoder.opcodeInfo[instruction]
            cycleCount += operation.cycles

            var dest: UInt = 0
            var dest2: UInt = 0
            var displacementCalculated = false
            // If we have an IX+d or IY+d mode we need to read the displacement
            // because it comes before the operand in an immediate  operation
            // and for the dd cb opcodes it comes before the opcode.
            if operation.displacment
            {
                readData(address: pc&++)
                gpRegs.wz = dataBus.signExtended()
            }
            // MARK: Get source operands
            let source: UInt
            let source2: UInt
            if operation.source != .none
            {
                source = fetchSourceOperand(mode: operation.source,
                                            constant: operation.constant,
                                            displacementCalculated: &displacementCalculated)
                if operation.source2 != .none
                {
                    source2 = fetchSourceOperand(mode: operation.source2,
                                                 constant: operation.constant,
                                                 displacementCalculated: &displacementCalculated)
                }
                else
                {
                    source2 = 0
                }
            }
            else
            {
                source = 0
                source2 = 0
            }

            var destAddressMode = operation.dest
            var dest2AddressMode = operation.dest2

            // MARK: calculate result
            switch operation.functionGroup
            {
            case .none:
                break
            case .halt:
                halt = true
            case .load8:
                dest = source
            case .or8:
                dest = source2 | source
                setLogicFlags(result: dest, hSense: false)
            case .xor8:
                dest = source2 ^ source
                setLogicFlags(result: dest, hSense: false)
            case .and8:
                dest = source2 & source
                setLogicFlags(result: dest, hSense: true)
            case .cp8:
                let comparison = source2 &- source
                setCp8Flags(result: comparison, acc: source2, source: source)
            case .dec8:
                dest = source &- 1
                var flags = accumulator.f.intersection(.c).union(.n)
                flags[.z] = dest.lowByte == 0
                flags[.h] = (source & 0xf) == 0x0
                flags[.v] = source == 0x80
                flags.insert(dest.intersection([.s, .x, .y]))
                accumulator.f = flags
            case .inc8:
                dest = source &+ 1
                var flags = accumulator.f.intersection(.c)
                flags[.z] = dest.lowByte == 0
                flags[.h] = (source & 0xf) == 0xf
                flags[.v] = source == 0x7f
                flags.insert(Status(lowByteOf: dest).intersection([.s, .x, .y]))
                accumulator.f = flags
            case .rlca:
                let carry = source.isSet(bit: 7)
                dest = (source << 1) | (carry ? 1 : 0)
                var flags = accumulator.f.intersection([.s, .z, .p])
                flags[.c] = carry
                flags.insert(dest.intersection([.x, .y]))
                accumulator.f = flags
            case .rrca:
                let carry = source.isSet(bit: 0)
                dest = (source >> 1) | (carry ? 0x80 : 0)
                var flags = accumulator.f.intersection([.s, .z, .p])
                flags[.c] = carry
                flags.insert(dest.intersection([.x, .y]))
                accumulator.f = flags
            case .rlc:
                let carry = source.isSet(bit: 7)
                dest = (source << 1) | (carry ? 1 : 0)
                var flags = standardSZPFlags(result: dest)
                flags[.c] = carry
                accumulator.f = flags
            case .rrc:
                let carry = source.isSet(bit: 0)
                dest = (source >> 1) | (carry ? 0x80 : 0)
                var flags = standardSZPFlags(result: dest)
                flags[.c] = carry
                accumulator.f = flags
            case .rla:
                dest = source << 1
                if accumulator.f[.c]
                {
                    dest |= 1
                }
                accumulator.f.set(.c, from: source > 0x7f)
                accumulator.f.remove([.h, .n, .x, .y])
                accumulator.f.insert(dest.intersection([.x, .y]))

            case .rld:
                dest = (source << 4) | (source2 & 0xf)
                dest2 = (source2 & 0xf0) | ((source >> 4) & 0x0f)
                var flags = accumulator.f.intersection(.c)
                flags[.z] = dest2 == 0
                flags[.p] = dest2.lowByte.evenParity
                flags.insert(dest2.intersection([.s, .x, .y]))
                accumulator.f = flags
            case .rra:
                dest = source >> 1
                if accumulator.f[.c]
                {
                    dest |= 0x80
                }
                accumulator.f.set(.c, from: source & 1 != 0)
                accumulator.f.remove([.h, .n, .x, .y])
                accumulator.f.insert(dest.intersection([.x, .y]))
            case .rl:
                dest = source << 1
                if accumulator.f.contains(.c)
                {
                    dest |= 0x1
                }
                var flags = standardSZPFlags(result: dest)
                flags[.c] = source > 0x7f
                accumulator.f = flags
            case .rr:
                dest = source >> 1
                if accumulator.f.contains(.c)
                {
                    dest |= 0x80
                }
                var flags = standardSZPFlags(result: dest)
                flags[.c] = source & 1 != 0
                accumulator.f = flags
            case .rrd:
                dest = ((source >> 4) & 0xf) | ((source2 << 4) & 0xf0)
                dest2 = (source2 & 0xf0) | (source & 0x0f)
                var flags = accumulator.f.intersection(.c)
                flags[.z] = dest2 == 0
                flags[.p] = dest2.lowByte.evenParity
                flags.insert(dest2.intersection([.s, .x, .y]))
                accumulator.f = flags
            case .sla:
                dest = source << 1
                var flags = standardSZPFlags(result: dest)
                flags[.c] = source > 0x7f
                accumulator.f = flags
            case .sra:
                dest = source >> 1
                if source > 0x7f
                {
                    dest |= 0x80
                }
                var flags = standardSZPFlags(result: dest)
                flags[.c] = source & 1 != 0
                accumulator.f = flags
            case .sll:
                dest = source << 1 | 1
                var flags = standardSZPFlags(result: dest)
                flags[.c] = source > 0x7f
                accumulator.f = flags
            case .srl:
                dest = source >> 1
                var flags = standardSZPFlags(result: dest)
                flags[.c] = source & 1 != 0
                accumulator.f = flags
            case .load16:
                dest = source
                dest2 = source2
            case .load16Disp:
                dest = source &+ gpRegs.wz.signExtended()
            case .inc16:
                dest = source &+ 1
            case .dec16:
                dest = source &- 1
            case .retn:
                dest = source
                iff1 = iff2
            case .add8:
                dest = source2 &+ source
                setAdd8Flags(result: dest, acc: source2, source: source)
            case .adc8:
                dest = source2 &+ source &+ (accumulator.f.isSet(.c) ? 1 : 0)
                setAdd8Flags(result: dest, acc: source2, source: source)
            case .sub8:
                dest = source2 &- source
                setSub8Flags(result: dest, acc: source2, source: source)
            case .sbc8:
                let carryAdjust: UInt = accumulator.f.contains(.c) ? 1 : 0
                dest = source2 &- source &- carryAdjust
                setSub8Flags(result: dest, acc: source2, source: source)
           case .add16:
            	gpRegs.wz = (source2 &+ 1).lowWord
                dest = source2 &+ source
                setAdd16Flags(result: dest, acc: source2, source: source)
            case .adc16:
                gpRegs.wz = (source2 &+ 1).lowWord
                dest = source2 &+ source &+ (accumulator.f.contains(.c) ? 1 : 0)
                setAdc16Flags(result: dest, acc: source2, source: source)
            case .sbc16:
                gpRegs.wz = (source2 &+ 1).lowWord
                dest = source2 &- source &- (accumulator.f.isSet(.c) ? 1 : 0)
                setSbc16Flags(result: dest, acc: source2, source: source)
            case .condLoad16:
                if accumulator.f.isSet(operation.flag, sense: operation.flagSense)
               	{
                    dest = source
                    dest2 = source2
                    // Caused by doing he extra memory cycle. Should probably
                    // be adjuated in the destination save really.
                    if dest2AddressMode == .preDecAddressInSP
                    {
                        cycleCount += 7
                    }
                }
                else
                {
                    // Check if this is actually a jump. If the jump is taken 
                    // the dest address calculation will set wz
                    if destAddressMode == .pc
                    {
                        gpRegs.wz = source.lowWord
                    }
                    //
                    destAddressMode = .none
                    dest2AddressMode = .none
                }
            case .condLoad16Disp:
                if accumulator.f.isSet(operation.flag, sense: operation.flagSense)
               	{
                    dest = source &+ gpRegs.wz.signExtended()
                    dest2 = source2
                    cycleCount += 5
                }
                else
                {
                    // Check if this is actually a jump. If the jump is taken
                    // the dest address calculation will set wz
                    if destAddressMode == .pc
                    {
                        gpRegs.wz = source.lowWord
                    }
                    //
                    destAddressMode = .none
                    dest2AddressMode = .none
                }

            case .djnz:
                dest2 = source2 &- 1
                if dest2 != 0
                {
					dest = source &+ UInt(gpRegs.wz)
                    cycleCount += 5
                }
                else
                {
                    destAddressMode = .none
                }
            case .exx:
                (gpRegs, altGpRegs) = (altGpRegs, gpRegs)
            case .load8Flags:
                dest = source
                var newFlags = accumulator.f.intersection(Status.c)
                newFlags[.s] = dest > 0x7f
                newFlags[.z] = dest == 0
                newFlags[.p] = iff2
                newFlags.insert(dest.intersection([.x, .y]))
                accumulator.f = newFlags
            case .ldi, .ldir:
                readData(address: gpRegs.hl&++)
                writeData(address: gpRegs.de&++)
                gpRegs.bc &-= 1
                accumulator.f.remove(.h)
                accumulator.f.remove(.n)
                accumulator.f.set(.p, from: gpRegs.bc != 0)
                if operation.functionGroup == .ldir && gpRegs.bc != 0
                {
                    cycleCount += 5
					pc &-= 2
                    gpRegs.wz = pc &+ 1
                }
            // Undocumented flag effects
                let n = accumulator.a &+ dataBus
                accumulator.f.set(.y, from: n & 0b00000010 != 0)
                accumulator.f.set(.x, from: n & 0b00001000 != 0)
            case .ldd, .lddr:
                readData(address: gpRegs.hl)
                writeData(address: gpRegs.de)
               	&--gpRegs.hl
                &--gpRegs.de
                gpRegs.bc &-= 1
                accumulator.f.remove(.h)
                accumulator.f.remove(.n)
                accumulator.f.set(.p, from: gpRegs.bc != 0)
                if operation.functionGroup == .lddr && gpRegs.bc != 0
                {
                    cycleCount += 5
                    pc &-= 2
                    gpRegs.wz = pc &+ 1
                }
                // Undocumented flag effects
                let n = accumulator.a &+ dataBus
                accumulator.f.set(.y, from: n & 0b00000010 != 0)
                accumulator.f.set(.x, from: n & 0b00001000 != 0)

            case .cpi, .cpir:
                gpRegs.hl&++
                &--gpRegs.bc
                gpRegs.wz&++
                setCpdFlags(result: accumulator.a &- source.lowByte, acc: accumulator.a, source: source.lowByte)
                if operation.functionGroup == .cpir && gpRegs.bc != 0 && !accumulator.f.contains(.z)
                {
                    pc &-= 2
                    cycleCount += 5
                    gpRegs.wz = pc &+ 1
                }
            case .cpd, .cpdr:
                &--gpRegs.hl
                &--gpRegs.bc
                &--gpRegs.wz
                setCpdFlags(result: accumulator.a &- source.lowByte, acc: accumulator.a, source: source.lowByte)
                if operation.functionGroup == .cpdr && gpRegs.bc != 0 && !accumulator.f.contains(.z)
                {
                    pc &-= 2
                    cycleCount += 5
                    gpRegs.wz = pc &+ 1
                }
            case .condRet:
                if accumulator.f.isSet(operation.flag, sense: operation.flagSense)
               	{
                    dest = pop16()
                    cycleCount += 6
                }
                else
                {
                    destAddressMode = .none
                }
            case .daa:
                let lowNybble = source & 0xf
                let highNybble = source & 0xf0
                var adjust: UInt = 0
                if lowNybble > 9 || accumulator.f.contains(.h)
                {
                    adjust = 0x6
                }
                if highNybble > 0x90 ||  accumulator.f.contains(.c) || (highNybble == 0x90 && lowNybble > 9)
                {
					adjust |= 0x60
                }
                if accumulator.f.contains(.n)
                {
                    dest = source &- adjust
                }
                else
                {
                    dest = source &+ adjust
                }
                var newFlags = accumulator.f.intersection([.c, .n])
                if highNybble > 0x90 || (highNybble == 0x90 && lowNybble > 9)
                {
                    newFlags.insert(.c)
                }
                newFlags[.h] = (source ^ dest) & 0x10 != 0
                newFlags[.z] = dest.lowByte == 0
                newFlags.insert(dest.intersection([.s, .x, .y]))
                newFlags[.p] = dest.lowByte.evenParity
                accumulator.f = newFlags
            case .not:
                dest = ~source
                accumulator.f.insert([.h, .n])
                accumulator.f[.y] = dest & 0x20 != 0
                accumulator.f[.x] = dest & 0x08 != 0
            case .neg:
                dest = 0 &- source
                setSub8Flags(result: dest, acc: 0, source: source)
            case .scf:
                accumulator.f.insert(.c)
                accumulator.f.remove([.h, .n])
                accumulator.f.insert(accumulator.a.intersection([.x, .y]))
            case .ccf:
				accumulator.f[.h] = accumulator.f[.c]
                accumulator.f[.c] = !accumulator.f[.c]
                accumulator.f.remove(.n)
                accumulator.f.insert(Status(rawValue: accumulator.a).intersection([.x, .y]))

            case .bitTest:
                let result = source & source2
                var newFlags = Status.h.union(accumulator.f.intersection(.c))
                newFlags.insert(result.intersection(.s))
                newFlags[[.z, .p]] = result == 0
                newFlags[.x] = source.isSet(bit: 3)
                newFlags[.y] = source.isSet(bit: 5)
                accumulator.f = newFlags
            case .bitTestIndirect:
                let result = source & source2
                var newFlags = Status.h.union(accumulator.f.intersection(.c))
                newFlags.insert(result.intersection(.s))
                newFlags[[.z, .p]] = result == 0
                newFlags.insert(gpRegs.w.intersection([.x, .y]))
                accumulator.f = newFlags
            case .resetBit:
				dest = source & ~source2
            case .setBit:
                dest = source | source2
            case .enableInterrupt:
                iff1 = true
                iff2 = true
            case .disableInterrupt:
                iff1 = false
                iff2 = false
            case .im0:
				im = 0
            case .im1:
                im = 1
            case .im2:
                im = 2
            case .inByte:
                if let ioDevice = ioDevices[Int(source)]
                {
                    let address = gpRegs.bc
                    dest = UInt(ioDevice.inByte(address: address))
                    accumulator.f = accumulator.f.intersection(.c).union(standardSZPFlags(result: dest))
                }
            case .ina:
                if let ioDevice = ioDevices[Int(source)]
                {
					let address = UInt16(low: source.lowByte, high: source2.lowByte)
                    dest = UInt(ioDevice.inByte(address: address))
                }
            case .out:
                if let ioDevice = ioDevices[Int(source2)]
                {
                    let address: UInt16
                    if operation.source2 == .c
                    {
                        address = gpRegs.bc
                    }
                    else
                    {
                        address = UInt16(source << 8 | source2)
                    }
					ioDevice.out(address: address, data: UInt8(source))
                }
            case .stop:
                if notReset
                {
                    pc = 0
                    sp = 0xffff
                    accumulator.af = 0xffff
                    altAccumulator.af = 0xffff
                    iff2 = false
                    iff1 = false
                    im = 0
                }
            case .cbPrefix:
                decoder = Decoder.cbGroup
           case .ddPrefix:
                decoder = Decoder.ddGroup
            case .ddcbPrefix:
                decoder = Decoder.ddcbGroup
            case .edPrefix:
                decoder = Decoder.edGroup
            case .fdPrefix:
                decoder = Decoder.fdGroup
            case .fdcbPrefix:
                decoder = Decoder.fdcbGroup
            case .unimplemented:
                raiseFatal("Opcode \(UInt16(instruction).hexString) is not implemented at 0x\(pc.hexString) in \(decoder.name)")
                return
            }

            // MARK: set the destination

            if destAddressMode != .none
            {
                saveDest(mode: destAddressMode, dest: dest, sourceMode: operation.source, displacementCalculated: displacementCalculated)
            }
            if dest2AddressMode != .none
            {
                saveDest(mode: dest2AddressMode, dest: dest2, sourceMode: operation.source, displacementCalculated: displacementCalculated)
            }


            // MARK: Clean up and next instruction

            // Reset the instruction decoder if necessary
            if decoder !== Decoder.standard
                && operation.functionGroup != .edPrefix
                && operation.functionGroup != .ddPrefix
                && operation.functionGroup != .fdPrefix
                && operation.functionGroup != .cbPrefix
                && operation.functionGroup != .ddcbPrefix
                && operation.functionGroup != .fdcbPrefix
            {
                decoder = Decoder.standard
            }
            // TODO: Check for NMI
            if notReset
            {
                // Interrupt unless iff2 is false or notInterrupt is true or we 
                // have just executed EI
                if !iff1 || notInt || operation.inhibitInterrupt
                {
                    if !halt
                    {
                        readData(address: pc&++)
                        instruction = Int(dataBus)
                    }
                }
                else
                {
                    // TODO: Only implemeted interrupt mode 1 so far
                    cycleCount += 2		// Interrupt wait states
                    iff1 = false
                    iff2 = false
                    instruction = 0xff	// RST 0x38
                    halt = false
                }
            }
            for device in clockDevices
            {
                if device.nextCallTime <= cycleCount
                {
                    device.drive()
                }
            }
        }
    }

    private final func fetchSourceOperand(mode: Decoder.AddressMode,
                                          constant: UInt,
                                          displacementCalculated: inout Bool) -> UInt
    {
        var source: UInt = 0
        switch mode
        {
        case .a:
            source = UInt(accumulator.a)
        case .b:
            source = UInt(gpRegs.b)
        case .c:
            source = UInt(gpRegs.c)
        case .d:
            source = UInt(gpRegs.d)
        case .e:
            source = UInt(gpRegs.e)
        case .f:
            source = UInt(accumulator.f.rawValue)
        case .h:
            source = UInt(gpRegs.h)
        case .ixh:
            source = UInt(ix.highByte)
        case .ixl:
            source = UInt(ix.lowByte)
        case .iyh:
            source = UInt(iy.highByte)
        case .iyl:
            source = UInt(iy.lowByte)
        case .i:
            source = UInt(i)
        case .l:
            source = UInt(gpRegs.l)
        case .r:
            source = UInt(r)
        case .immediate8:
            readData(address: pc)
            pc = pc &+ 1
            source = UInt(dataBus)
        case .addressInBC:
            readData(address: gpRegs.bc)
            gpRegs.wz = gpRegs.bc &+ 1
            source = UInt(dataBus)
        case .addressInDE:
            readData(address: gpRegs.de)
            gpRegs.wz = gpRegs.de &+ 1
            source = UInt(dataBus)
        case .addressInHL:
            readData(address: gpRegs.hl)
            //gpRegs.wz = gpRegs.hl &+ 1
            source = UInt(dataBus)
        case .addressInIX_d:
            gpRegs.wz &+= ix
            readData(address: gpRegs.wz)
            source = UInt(dataBus)
            displacementCalculated = true
        case .addressInIY_d:
            gpRegs.wz &+= iy
            readData(address: gpRegs.wz)
            source = UInt(dataBus)
            displacementCalculated = true
        case .address8:
            readData(address: pc&++)
            gpRegs.z = dataBus
            readData(address: pc&++)
            gpRegs.w = dataBus
            readData(address: gpRegs.wz&++)
            source = UInt(dataBus)
        case .address16:
            readData(address: pc&++)
            gpRegs.z = dataBus
            readData(address: pc&++)
            gpRegs.w = dataBus
            readData(address: gpRegs.wz&++)
            source.lowByte = dataBus
            readData(address: gpRegs.wz)
            source.highByte = dataBus
        case .preDecAddressInSP:
            readData(address: &--sp)
            source.highByte = dataBus
            readData(address: &--sp)
            source.lowByte = dataBus
        case .addressInSPPostInc:
            source = pop16()
        case .immediate16:
            readData(address: pc)
            pc &+= 1
            source.lowByte = dataBus
            readData(address: pc)
            pc &+= 1
            source.highByte = dataBus
        case .af:
            source = UInt(accumulator.af)
        case .afAlt:
            source = UInt(altAccumulator.af)
        case .bc:
            source = UInt(gpRegs.bc)
        case .de:
            source = UInt(gpRegs.de)
        case .hl:
            source = UInt(gpRegs.hl)
        case .sp:
            source = UInt(sp)
        case .ix:
            source = UInt(ix)
        case .iy:
            source = UInt(iy)
        case .pc:
            source = UInt(pc)
        case .constant:
            source = constant
        case .none:
            break
        }
		return source
    }

    private final func saveDest(mode: Decoder.AddressMode, dest: UInt, sourceMode: Decoder.AddressMode, displacementCalculated: Bool)
    {
        switch mode
        {
        case .a:
            accumulator.a = dest.lowByte
        case .b:
            gpRegs.b = dest.lowByte
        case .c:
            gpRegs.c = dest.lowByte
        case .d:
            gpRegs.d = dest.lowByte
        case .e:
            gpRegs.e = dest.lowByte
        case .f:
            accumulator.f = Status(lowByteOf: dest)
        case .h:
            gpRegs.h = dest.lowByte
        case .ixh:
            ix.highByte = dest.lowByte
        case .ixl:
            ix.lowByte = dest.lowByte
        case .iyh:
            iy.highByte = dest.lowByte
        case .iyl:
            iy.lowByte = dest.lowByte
        case .i:
            i = dest.lowByte
        case .l:
            gpRegs.l = dest.lowByte
        case .r:
            r = dest.lowByte
        case .immediate8:
            raiseFatal("Immediate cannot be used for a destination")
            return
        case .addressInBC:
            dataBus = dest.lowByte
            writeData(address: gpRegs.bc)
            gpRegs.z = gpRegs.c &+ 1
            gpRegs.w = dest.lowByte
        case .addressInDE:
            dataBus = dest.lowByte
            writeData(address: gpRegs.de)
            gpRegs.z = gpRegs.e &+ 1
            gpRegs.w = dest.lowByte
        case .addressInHL:
            let destAddress = gpRegs.hl
            dataBus = dest.lowByte
            writeData(address: destAddress)
        case .addressInIX_d:
            if !displacementCalculated
            {
                gpRegs.wz &+= ix
            }
            dataBus = dest.lowByte
            writeData(address: gpRegs.wz)
        case .addressInIY_d:
            if !displacementCalculated
            {
                gpRegs.wz &+= iy
            }
            dataBus = dest.lowByte
            writeData(address: gpRegs.wz)
        case .address8:
            readData(address: pc&++)
            gpRegs.z = dataBus
            readData(address: pc&++)
            gpRegs.w = dataBus
            dataBus = dest.lowByte
            writeData(address: gpRegs.wz&++)
            // Curiously, w becones `a` for no good reason
            // TODO: Find out what the mytsterious BM1 is
            gpRegs.w = accumulator.a

        case .address16:
            readData(address: pc&++)
            gpRegs.z = dataBus
            readData(address: pc&++)
            gpRegs.w = dataBus
            dataBus = dest.lowByte
            writeData(address: gpRegs.wz&++)
            dataBus = dest.highByte
            writeData(address: gpRegs.wz)
        case .preDecAddressInSP:
            push(word16: dest)
        case .addressInSPPostInc:
            dataBus = dest.lowByte
            writeData(address: sp&++)
            dataBus = dest.highByte
            writeData(address: sp&++)
        case .immediate16:
            raiseFatal("Immediate cannot be used for a destination")
            return
        case .af:
            accumulator.af = dest.lowWord
        case .afAlt:
            altAccumulator.af = dest.lowWord
        case .bc:
            gpRegs.bc = dest.lowWord
        case .de:
            gpRegs.de = dest.lowWord
        case .hl:
            gpRegs.hl = dest.lowWord
        case .sp:
            sp = dest.lowWord
        case .ix:
            ix = dest.lowWord
        case .iy:
            iy = dest.lowWord
        case .pc:
            pc = dest.lowWord
            // All jumps and calls set wz to the destination address except
            // jumps to  a register pair
            if sourceMode != .ix
                && sourceMode  != .iy
                && sourceMode != .hl
            {
                gpRegs.wz = dest.lowWord
            }

        case .constant:
            raiseFatal("Opcode constants are not allowed for destinations at 0x\(pc.hexString) in \(decoder.name)")
            return
        case .none:
            break
        }
    }

    private final func standardSZPFlags(result: UInt) -> Status
    {

        var ret = result.intersection([.s, .x, .y])
        ret[.z] = result.lowByte == 0
        ret[.p] = result.lowByte.evenParity
        return ret
    }

    private final func setLogicFlags(result: UInt, hSense: Bool)
    {
        // based on the flag logic from
        // https://github.com/floooh/rz80/blob/master/src/cpu.rs
//        let v = val & 0xFF;
//        (if (v.count_ones() & 1) == 0 {PF} else {0}) |
//            (if v == 0 {ZF} else {v & SF}) | (v & (YF | XF))

        var newFlags = hSense ? Status.h : Status()
        newFlags[.p] = result.lowByte.evenParity
        newFlags[.s] = result > 0x7f
        newFlags[.z] = result == 0
        newFlags.insert(Status.xyMask.intersection(Status(rawValue: result.lowByte)))
        accumulator.f = newFlags
    }


    private final func setAdd8Flags(result: UInt, acc: UInt, source: UInt)
    {
        // based on the flag logic from
        // https://github.com/floooh/rz80/blob/master/src/cpu.rs
//        (if (res & 0xFF) == 0 {ZF} else {res & SF}) |
//            (res & (YF | XF)) | ((res >> 8) & CF) |
//            ((acc ^ add ^ res) & HF) | ((((acc ^ add ^ 0x80) & (add ^ res)) >> 5) & VF)
        var newFlags = Status()
        newFlags.set(.s, from: result & 0x80 != 0)
        newFlags.set(.z, from: result & 0xff == 0)
        newFlags.set(.c, from: result > 0xff)
        newFlags.set(.h, from: (acc ^ source ^ result) & 0x10 != 0)   // Not sure exactly how this one works
        newFlags.set(.v, from: (acc ^ source ^ 0x80) & (result ^ acc) & 0x80 != 0) // Or this one
        newFlags.insert(Status.xyMask.intersection(Status(rawValue: UInt8(result & 0xff))))
        accumulator.f = newFlags
    }

    private final func setAdd16Flags(result: UInt, acc: UInt, source: UInt)
    {
        // based on the flag logic from
        // https://github.com/floooh/rz80/blob/master/src/cpu.rs

//        let f = (self.reg.f() & (SF | ZF | VF)) | (((acc ^ res ^ add) >> 8) & HF) |
//            (res >> 16 & CF) | (res >> 8 & (YF | XF));

		var newFlags = accumulator.f.intersection([.s, .z, .v])
		newFlags.set(.h, from: (result ^ acc ^ source) & 0x1000 != 0)
        newFlags.set(.c, from: result > 0xffff)
        newFlags.insert(Status.xyMask.intersection(Status(rawValue: UInt8((result >> 8) & 0xff))))
        accumulator.f = newFlags
    }

    private final func setAdc16Flags(result: UInt, acc: UInt, source: UInt)
    {
        // based on the flag logic from
        // https://github.com/floooh/rz80/blob/master/src/cpu.rs

//        self.reg.set_f((((acc ^ res ^ add) >> 8) & HF) | ((res >> 16) & CF) |
//            ((res >> 8) & (SF | XF | YF)) |
//            (if (res & 0xFFFF) == 0 {ZF} else {0}) |
//            (((add ^ acc ^ 0x8000) & (add ^ res) & 0x8000) >> 13));
        var newFlags = Status()
        newFlags.set(.s, from: result & 0x8000 != 0)
        newFlags.set(.z, from: result & 0xffff == 0)
        newFlags.set(.h, from: (result ^ acc ^ source) & 0x1000 != 0)
        newFlags.set(.c, from: result > 0xffff)
        newFlags.set(.v, from: (acc ^ source ^ 0x8000) & (result ^ acc) & 0x8000 != 0) // Or this one
        newFlags.insert(Status.xyMask.intersection(Status(rawValue: UInt8((result >> 8) & 0xff))))
        accumulator.f = newFlags
    }
    
    private final func setSub8Flags(result: UInt, acc: UInt, source: UInt)
    {
        // based on the flag logic from 
        // https://github.com/floooh/rz80/blob/master/src/cpu.rs
//            NF | (if (res & 0xFF) == 0 {ZF} else {res & SF}) |
//                (res & (YF | XF)) | ((res >> 8) & CF) |
//                ((acc ^ sub ^ res) & HF) | ((((acc ^ sub) & (res ^ acc)) >> 5) & VF)
		var newFlags = Status.n
		newFlags.set(.s, from: result & 0x80 != 0)
        newFlags.set(.z, from: result & 0xff == 0)
		newFlags.set(.c, from: result > 0xff)
        newFlags.set(.h, from: (acc ^ source ^ result) & 0x10 != 0)   // Not sure exactly how this one works
        newFlags.set(.v, from: (acc ^ source) & (result ^ acc) & 0x80 != 0) // Or this one
        newFlags.insert(Status.xyMask.intersection(Status(rawValue: UInt8(result & 0xff))))
        accumulator.f = newFlags
    }

    private final func setCp8Flags(result: UInt, acc: UInt, source: UInt)
    {
        // based on the flag logic from
        // https://github.com/floooh/rz80/blob/master/src/cpu.rs
        // the only difference to flags_sub() is that the
        // 2 undocumented flag bits X and Y are taken from the
        // sub-value, not the result
//        NF | (if (res & 0xFF) == 0 {ZF} else {res & SF}) |
//            (sub & (YF | XF)) | ((res >> 8) & CF) |
//            ((acc ^ sub ^ res) & HF) | ((((acc ^ sub) & (res ^ acc)) >> 5) & VF)
        var newFlags = Status.n
        newFlags.set(.s, from: result & 0x80 != 0)
        newFlags.set(.z, from: result & 0xff == 0)
        newFlags.set(.c, from: result > 0xff)
        newFlags.set(.h, from: (acc ^ source ^ result) & 0x10 != 0)   // Not sure exactly how this one works
        newFlags.set(.v, from: (acc ^ source) & (result ^ acc) & 0x80 != 0) // Or this one
        newFlags.insert(Status.xyMask.intersection(Status(lowByteOf: source)))
        accumulator.f = newFlags
    }

    private final func setCpdFlags(result: UInt8, acc: UInt8, source: UInt8)
    {
        // C should be unaffected, N is always set
        var newFlags = accumulator.f.intersection(.c).union(.n)
        newFlags.set(.s, from: result > 0x7f)
        newFlags.set(.z, from: result == 0)
        let xyValue: UInt8
        if result & 0xf > acc & 0xf
        {
			newFlags.insert(.h)
            xyValue = result &- 1
        }
        else
        {
            newFlags.remove(.h)
            xyValue = result
        }
        newFlags.set(.p, from: gpRegs.bc != 0)
        newFlags.set(.x, from: xyValue & 0x8 != 0)
        newFlags.set(.y, from: xyValue & 0x2 != 0)
        accumulator.f = newFlags
    }
    private final func setSbc16Flags(result: UInt, acc: UInt, source: UInt)
    {
        // based on the flag logic from
        // https://github.com/floooh/rz80/blob/master/src/cpu.rs
//        self.reg.set_f(NF | (((acc ^ res ^ sub) >> 8) & HF) | ((res >> 16) & CF) |
//            ((res >> 8) & (SF | XF | YF)) |
//            (if (res & 0xFFFF) == 0 {ZF} else {0}) |
//            (((sub ^ acc) & (acc ^ res) & 0x8000) >> 13));
        var newFlags = Status.n
        newFlags.set(.s, from: result & 0x8000 != 0)
        newFlags.set(.z, from: result & 0xffff == 0)
        newFlags.set(.c, from: result > 0xffff)
        newFlags.set(.h, from: (acc ^ source ^ result) & 0x1000 != 0)   // Not sure exactly how this one works
        newFlags.set(.v, from: (acc ^ source) & (result ^ acc) & 0x8000 != 0) // Or this one
        newFlags.insert(Status.xyMask.intersection(Status(rawValue: UInt8((result >> 8) & 0xff))))
        accumulator.f = newFlags
    }

    private func pop16() -> UInt
    {
        readData(address: sp&++)
        let low = dataBus
        readData(address: sp&++)
        let high = dataBus
        return UInt(low) | (UInt(high) << 8)
    }

    // TODO: Might be able to genericise the two versions of push
    private func push(word16: UInt)
    {
        dataBus = word16.highByte
        writeData(address: &--sp)
        dataBus = word16.lowByte
        writeData(address: &--sp)
    }

    private func push(word16: UInt16)
    {
        dataBus = word16.highByte
        writeData(address: &--sp)
        dataBus = word16.lowByte
        writeData(address: &--sp)
    }

    private func readData(address: UInt16)
    {
        let index = Int(address)
        if let device = devices[index]
        {
            device.willRead(address &- device.baseAddress, byte: &memory[index])
        }
        dataBus = memory[index]
    }

    private func writeData(address: UInt16)
    {
        let index = Int(address)
        if !locationIsRom[index]
        {
            memory[Int(address)] = dataBus
        }
        if let device = devices[index]
        {
            device.didWrite(address &- device.baseAddress, byte: &memory[index])
        }
    }

    /// Return the named 16 bit register
    ///
    /// - Parameter name: Name of the register, case insensitive
    /// - Returns: The value of the register or nil if the name is not of a 
    ///            valid register
    public func register16(_ name: String) -> UInt16?
	{
        let ret: UInt16?
        switch name.lowercased()
        {
        case "af":
            ret = self.accumulator.af
        case "af'":
			ret = altAccumulator.af
        case "bc":
            ret = self.gpRegs.bc
        case "bc'":
            ret = self.altGpRegs.bc
        case "de":
            ret = self.gpRegs.de
        case "de'":
            ret = self.altGpRegs.de
        case "sp":
            ret = self.sp
        case "pc":
            ret = self.pc
        case "hl":
            ret = self.gpRegs.hl
        case "hl'":
            ret = self.altGpRegs.hl
        case "ix":
            ret = self.ix
        case "iy":
            ret = self.iy
        case "instruction" :
            ret = UInt16(self.instruction)
        case "ir" :
            ret = UInt16(low: r, high: i)
        default:
            ret = nil

        }
        return ret
    }


    /// Return the nemed 8 bit register
    ///
    /// - Parameter name: Name odf the register, case insensitive
    /// - Returns: The value of the register or nil if the name is not of a
    ///            valid register
    public func register8(_ name: String) -> UInt8?
    {
        let ret: UInt8?
        switch name.lowercased()
        {
        case "a":
            ret = accumulator.a
        case "f":
            ret = accumulator.f.rawValue
        case "b":
            ret = gpRegs.b
        case "c":
            ret = gpRegs.c
        case "d":
            ret = gpRegs.d
        case "e":
            ret = gpRegs.e
        case "h":
            ret = gpRegs.h
        case "i":
            ret = i
        case "l":
            ret = gpRegs.l
        case "r":
            ret = r
        case "ixh":
            ret = ix.highByte
        case "ixl":
            ret = ix.lowByte
        case "iyh":
            ret = iy.highByte
        case "iyl":
            ret = iy.lowByte
        case "a'":
            ret = altAccumulator.a
        case "f'":
            ret = altAccumulator.f.rawValue
        case "interruptmode":
            ret = UInt8(self.im)

        default:
            ret = nil
        }
        return ret
    }


    /// Gives access to the flags register
    public var flags: Status
    {
        get { return accumulator.f }
        set { accumulator.f = newValue }
    }


    /// A function that injects a byte into memory
    ///
    /// - Parameters:
    ///   - byte: The byte to inject
    ///   - address: The address at which to put it
    public func set(byte: UInt8, at address: UInt16)
    {
		memory[Int(address)] = byte
    }


    /// A function that copies an array into memory. If the array would overflow
    /// top of memory, we wrap round and put the remaining bytes at the bottom.
    ///
    /// - Parameters:
    ///   - bytes: Array of bytes to copy
    ///   - address: address of where to put them.
    public func set(bytes: [UInt8], at address: UInt16)
    {
        var currentAddress = address
        for byte in bytes
        {
            memory[Int(currentAddress)] = byte
            currentAddress = currentAddress &+ 1
        }
    }

    /// A function that inspects the byte in memory at an address
    ///
    /// - Parameter address: Address to look at
    /// - Returns: The byte at the given address
    public func byte(at address: UInt16) -> UInt8
    {
        return memory[Int(address)]
    }


    /// Return an array of bytes at starting from the given address. 
    ///
    ///
    /// - Parameters:
    ///   - address: Address to start at
    ///   - count: number of bytes to grab
    ///   - wrap: If `address + count > UInt16.max` and wrap is true the array
    ///           will wrap round and include bytes from 0 onwards.
    /// - Returns: An array of the bytes.
    public func bytes(at address: UInt16, count: UInt16, wrap: Bool = false) -> [UInt8]
    {
        var ret: [UInt8] = []
        for i in 0 ..< count
        {
            guard Int(address) + Int(i) < Z80A.memorySize || wrap else { break }
			ret.append(memory[Int(address &+ i)])
        }
        return ret
    }


    /// Mark memory locations as read only. This function does not wrap - 
    /// regardless of `count` it always stops when it hits the top of memory.
    ///
    /// - Parameters:
    ///   - address: The frist address to make ROM
    ///   - count: number of addresses to make ROM
    public func makeROM(from address: UInt16, count: UInt16)
    {
		let base = Int(address)
        for i in 0 ..< Int(count)
        {
            guard base + i < Z80A.memorySize else { break }
            locationIsRom[base + i] = true
        }
    }


    /// Set the register with the given name to the given value
    ///
    /// - Parameters:
    ///   - name: Name of the register
    ///   - value: The value to set
    /// - Returns: `true` if the register set ok, `false` if the register does not exist
    @discardableResult public func setRegister8(_ name: String, value: UInt8) -> Bool
    {
        var ret = true
        switch name.lowercased()
        {
        case "a":
            accumulator.a = value
        case "f":
            accumulator.f = Status(rawValue: value)
        case "a'":
            altAccumulator.a = value
        case "f'":
            altAccumulator.f = Status(rawValue: value)
        case "b":
            gpRegs.b = value
        case "c":
            gpRegs.c = value
        case "d":
            gpRegs.d = value
        case "e":
            gpRegs.e = value
        case "h":
            gpRegs.h = value
        case "l":
            gpRegs.l = value
        case "ixh":
            ix.highByte = value
        case "ixl":
            ix.lowByte = value
        case "iyh":
            iy.highByte = value
        case "iyl":
            iy.lowByte = value
        case "i":
            i = value
        case "r":
            r = value
        case "interruptmode":
            im = Int(value)
        default:
            ret = false
        }
        return ret
    }
    /// Set the 16 bit register with the given name to the given value
    ///
    /// - Parameters:
    ///   - name: Name of the register
    ///   - value: The value to set
    /// - Returns: `true` if the register set ok, `false` if the register does not exist
    @discardableResult public func setRegister16(_ name: String, value: UInt16) -> Bool
    {
        var ret = true
        switch name.lowercased()
        {
        case "af":
            accumulator.af = value
        case "af'":
            altAccumulator.af = value
        case "bc":
            gpRegs.bc = value
        case "de":
            gpRegs.de = value
        case "hl":
            gpRegs.hl = value
        case "bc'":
            altGpRegs.bc = value
        case "de'":
            altGpRegs.de = value
        case "hl'":
            altGpRegs.hl = value

        case "ix":
            ix = value
        case "iy":
            iy = value
        case "sp":
            sp = value
        case "pc":
            pc = value
        case "instruction":
            instruction = Int(value)
        default:
            ret = false
        }
        return ret
    }


    /// Retrieve the given flag
    ///
    /// - Parameter name: Name of the flag to get
    /// - Returns: The value of the flag or nil if it doesn't exist
    public func flipFlop(_ name: String) -> Bool?
    {
        let ret: Bool?
        switch name.lowercased()
        {
        case "iff1":
            ret = iff1
        case "iff2":
            ret = iff2
        default:
            ret = nil
        }

        return ret
    }


    /// Set a flip flop
    ///
    /// - Parameters:
    ///   - name: Name of the flip flop
    ///   - value: value to set
    @discardableResult public func setFlipFlop(_ name: String, value: Bool) -> Bool
    {
        var success = true
		switch name.lowercased()
        {
        case "iff1":
			iff1 = value
        case "iff2":
            iff2 = value
        default:
			success = false
        }
        return success
    }


    /// Map a device to a specific memory address
    ///
    /// - Parameter device: The device to map. We do not take ownership of the 
    ///                     device. The caller must take care to ensure it 
    ///                     lives as long as the Z80.
    public func mapDevice(_ device: MemoryMappedDevice)
    {
        let portCount = device.portCount
        for i in 0 ..< portCount
        {
            mapPort(UInt16(i), device: device)
        }
        log.info("Mapped \(device) at $\(device.baseAddress.hexString) with \(portCount) ports")
    }
    
    private func mapPort(_ portNumber: UInt16, device: MemoryMappedDevice)
    {
        let addressOfPort = device.baseAddress + portNumber
        /*
         *  Devices can support multiple devices at the same address.
         */
        if let previousDevice = devices[Int(addressOfPort)]
        {
            device.pushDevice(previousDevice, port: portNumber)
        }
        devices[Int(addressOfPort)] = device
    }

    // MARK: IO devices

    private var ioDevices: [IODevice?] = [IODevice?](repeating: nil, count: 256)


    /// Register an IO device for a specific port
    ///
    /// - Parameters:
    ///   - device: The IO device to register
    ///   - port: The port it should listen on
    public func registerIODevice(_ device: IODevice, forPort port: UInt8)
    {
        ioDevices[Int(port)] = device
    }

    private var clockDevices: [ClockDrivenDevice] = []

    public func unregister(clockDevice: ClockDrivenDevice)
    {
		assert(false, "Csn't unregister yet")
    }

    public func register(clockDevice: ClockDrivenDevice)
    {
		clockDevices.append(clockDevice)
        clockDevice.synchronise(time: cycleCount)
    }
}
