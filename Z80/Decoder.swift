//
//  Decoder.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 29/03/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//
import Toolbox

private var log: Logger = Logger.getLogger("Z80.Decoder")

/// Instruction decoding logic
class Decoder
{

    /// What function does the opcode do
    ///
    /// - `stop`: Processor is stopped
    /// - `unimplemented`: opcode functionality is unimplemented
    /// - `load8`: Loads an 8 birt register
    /// - `ixMode`: Same as normal except `hl -> ix`
    /// - `iyMode`: Same as normal except `hl -> iy`
    enum FunctionGroup
    {
        case stop
        case unimplemented
        case none
        case halt
        case load8
        case load16
        case load16Disp	// 16 bit load + displacement
        case inc16
        case dec16
        case add8
        case adc8
        case sub8
        case sbc8
        case add16
        case adc16
        case sbc16
        case daa
        case not, neg
        case condLoad16, condLoad16Disp
        case or8
        case xor8
        case and8
        case cp8
        case dec8
        case inc8
        case rl, rla, rlc, rlca, rld, rr, rra, rrca, rrc, rrd, sla, sra, sll,srl
        case exx
        case load8Flags
        case ldi, ldir, ldd, lddr, cpi, cpir, cpd, cpdr
        case condRet
        case enableInterrupt
        case disableInterrupt
        case bitTest, resetBit, setBit, bitTestIndirect
        case scf, ccf
        case djnz
        case ina, inByte, out
        case im0, im1, im2
        case retn
        case cbPrefix
        case ddPrefix
        case edPrefix
        case fdPrefix
        case ddcbPrefix
        case fdcbPrefix
    }


    /// Z80 address modes
    ///
    /// - none: No operand
    /// - a: a register
    /// - b: b register
    /// - c: c register
    /// - d: d register
    /// - e: e register
    /// - f: flags
    /// - h: h register
    /// - l: l register
    /// - hIgnorePrefix: h register b but no chanfge when `0xdd` or `0xfd` prefix
    /// - lIgnorePRefix: l register b but no chanfge when `0xdd` or `0xfd` prefix
    /// - immediate: immediate operand
    /// - addressInBC: Address of the operand is in `bc`
    /// - addressInDE: Address of the operand is in `de`
    /// - addressInHL: Address of the operand is in `hl`
    /// - address8: The 16 bit number after the instruction is the addrerss
    /// - address16: The 16 bit number after the instruction is the addrerss of a 16 bit number
    enum AddressMode
    {
		case none
        case a, b, c, d, e, f, h, i, l, r, ixh, ixl, iyh, iyl	// 8 bit registers
        case af, bc, de, hl, sp, ix, iy, pc
        case afAlt
        case immediate8
        case immediate16
        case addressInBC, addressInDE, addressInHL, addressInIX_d, addressInIY_d
        case preDecAddressInSP, addressInSPPostInc
        case address8, address16
        case constant
    }

    enum BitOperation: UInt8
    {
        case shiftRotate = 0b00000000
        case        test = 0b01000000
        case       reset = 0b10000000
        case         set = 0b11000000
        static func bitOperation(for byte: UInt8) -> BitOperation
        {
            return BitOperation(rawValue: byte & 0xc0)!
        }
    }

    /// Contaisnall the information about a specific opcode
    struct OpcodeInfo
    {
        /// The opcode, used to verify decoder tables to make sure they the info
        /// is at the right index.
        let opcode: Int
        /// The base number of cycles required to execute the opcode.
        let cycles: Int
        /// The function group of the opcode
        let functionGroup: FunctionGroup
        /// If true, the operation will fetch the displacement from the next byte
        let displacment: Bool
        /// Address mode for source
        let source: AddressMode
        /// Secondary source address mode
        let source2: AddressMode
        /// Address mode for destination
        let dest: AddressMode
        /// Second destination, if needed.
        let dest2: AddressMode
        /// For conditional operations, which flag
        let flag: Z80A.Status
        /// For conditional operations, which way should the flag be
        let flagSense: Bool
        /// A constant that can be used with the constant address mode
        let constant: UInt
        /// If true, the interrupt lines will not be checked follwoing the 
        /// instruction.
        let inhibitInterrupt: Bool

        init(opcode:Int,
             cycles: Int,
      functionGroup: FunctionGroup = .unimplemented,
       displacement: Bool = false,
             source: AddressMode = .none,
            source2: AddressMode = .none,
               dest: AddressMode = .none,
              dest2: AddressMode = .none,
               flag: Z80A.Status = Z80A.Status(rawValue: 0),
          flagSense: Bool = false,
           constant: UInt = 0,
   inhibitInterrupt: Bool = false)
        {
            precondition(source2 == .none || source != .none, "source2 cannot be present if source is not present")
            precondition(dest2 == .none || dest != .none, "dest2 cannot be present if dest is not present")
			self.opcode = opcode
            self.cycles = cycles
            self.functionGroup = functionGroup
            self.displacment = displacement
            self.source = source
            self.source2 = source2
            self.dest = dest
            self.dest2 = dest2
            self.flag = flag
            self.flagSense = flagSense
            self.constant = constant
            self.inhibitInterrupt = inhibitInterrupt
        }
    }

    // MARK: Internal opcodes
	/// Real opcodes are in the range 0 to 0xff. Internal opcodes are used for 
    /// things like reset and interrupts
	static let firstInternal = 0x100
    /// CPU does nothing just spinning and using cycles
    static let stop = firstInternal + 0
    /// CPU is in reset mode
    static let reset = firstInternal + 1

    /// unimplemented opcode should crash the emulator
    static let unimplemented = firstInternal + 2

    private static var placeHolder = OpcodeInfo(opcode: unimplemented, cycles: 1)

    // MARK: Standard opcode decoder
    /// The standard opcode decoder for all instructions without prefixes and
    /// possibly for ones that just change `HL` to `IX` or `IY`
    static let standard = Decoder(name: "standard",
        [
            // 0
            OpcodeInfo(opcode: 0x00, cycles: 4, functionGroup: .none),
			OpcodeInfo(opcode: 0x01, cycles: 10, functionGroup: .load16, source: .immediate16, dest: .bc),
			OpcodeInfo(opcode: 0x02, cycles: 7, functionGroup: .load8, source: .a, dest: .addressInBC),
			OpcodeInfo(opcode: 0x03, cycles: 6, functionGroup: .inc16, source: .bc, dest: .bc),
			OpcodeInfo(opcode: 0x04, cycles: 4, functionGroup: .inc8, source: .b, dest: .b),
			OpcodeInfo(opcode: 0x05, cycles: 4, functionGroup: .dec8, source: .b, dest: .b),
			OpcodeInfo(opcode: 0x06, cycles: 7, functionGroup: .load8, source: .immediate8, dest: .b),
			OpcodeInfo(opcode: 0x07, cycles: 4, functionGroup: .rlca, source: .a, dest: .a),
			OpcodeInfo(opcode: 0x08, cycles:  4, functionGroup: .load16, source: .afAlt, source2: .af, dest: .af, dest2: .afAlt),
			OpcodeInfo(opcode: 0x09, cycles: 11, functionGroup: .add16, source: .bc, source2: .hl, dest: .hl),
			OpcodeInfo(opcode: 0x0a, cycles:  7, functionGroup: .load8, source: .addressInBC, dest: .a),
			OpcodeInfo(opcode: 0x0b, cycles:  6, functionGroup: .dec16, source: .bc, dest: .bc),
			OpcodeInfo(opcode: 0x0c, cycles: 4, functionGroup: .inc8, source: .c, dest: .c),
			OpcodeInfo(opcode: 0x0d, cycles: 4, functionGroup: .dec8, source: .c, dest: .c),
			OpcodeInfo(opcode: 0x0e, cycles: 7, functionGroup: .load8, source: .immediate8, dest: .c),
			OpcodeInfo(opcode: 0x0f, cycles: 4, functionGroup: .rrca, source: .a, dest: .a),
			// 1
            OpcodeInfo(opcode: 0x10, cycles: 8, functionGroup: .djnz, displacement: true, source: .pc, source2: .b, dest: .pc, dest2: .b),
			OpcodeInfo(opcode: 0x11, cycles: 10, functionGroup: .load16, source: .immediate16, dest: .de),
			OpcodeInfo(opcode: 0x12, cycles: 7, functionGroup: .load8, source: .a, dest: .addressInDE),
			OpcodeInfo(opcode: 0x13, cycles: 6, functionGroup: .inc16, source: .de, dest: .de),
			OpcodeInfo(opcode: 0x14, cycles: 4, functionGroup: .inc8, source: .d, dest: .d),
			OpcodeInfo(opcode: 0x15, cycles: 4, functionGroup: .dec8, source: .d, dest: .d),
			OpcodeInfo(opcode: 0x16, cycles: 7, functionGroup: .load8, source: .immediate8, dest: .d),
			OpcodeInfo(opcode: 0x17, cycles: 4, functionGroup: .rla, source: .a, dest: .a),
			OpcodeInfo(opcode: 0x18, cycles: 12, functionGroup: .load16Disp, displacement: true, source: .pc, dest: .pc),
			OpcodeInfo(opcode: 0x19, cycles: 11, functionGroup: .add16, source: .de, source2: .hl, dest: .hl),
			OpcodeInfo(opcode: 0x1a, cycles:  7, functionGroup: .load8, source: .addressInDE, dest: .a),
			OpcodeInfo(opcode: 0x1b, cycles:  6, functionGroup: .dec16, source: .de, dest: .de),
			OpcodeInfo(opcode: 0x1c, cycles: 4, functionGroup: .inc8, source: .e, dest: .e),
			OpcodeInfo(opcode: 0x1d, cycles: 4, functionGroup: .dec8, source: .e, dest: .e),
			OpcodeInfo(opcode: 0x1e, cycles: 7, functionGroup: .load8, source: .immediate8, dest: .e),
			OpcodeInfo(opcode: 0x1f, cycles: 4, functionGroup: .rra, source: .a, dest: .a),
			// 2
            OpcodeInfo(opcode: 0x20, cycles: 7, functionGroup: .condLoad16Disp, displacement: true, source: .pc, dest: .pc, flag: .z, flagSense: false),
			OpcodeInfo(opcode: 0x21, cycles: 10, functionGroup: .load16, source: .immediate16, dest: .hl),
			OpcodeInfo(opcode: 0x22, cycles: 16, functionGroup: .load16, source: .hl, dest: .address16),
			OpcodeInfo(opcode: 0x23, cycles: 6, functionGroup: .inc16, source: .hl, dest: .hl),
			OpcodeInfo(opcode: 0x24, cycles: 4, functionGroup: .inc8, source: .h, dest: .h),
			OpcodeInfo(opcode: 0x25, cycles: 4, functionGroup: .dec8, source: .h, dest: .h),
			OpcodeInfo(opcode: 0x26, cycles: 7, functionGroup: .load8, source: .immediate8, dest: .h),
			OpcodeInfo(opcode: 0x27, cycles: 4, functionGroup: .daa, source: .a, dest: .a),
			OpcodeInfo(opcode: 0x28, cycles: 7, functionGroup: .condLoad16Disp, displacement: true, source: .pc, dest: .pc, flag: .z, flagSense: true),
			OpcodeInfo(opcode: 0x29, cycles: 11, functionGroup: .add16, source: .hl, source2: .hl, dest: .hl),
			OpcodeInfo(opcode: 0x2a, cycles: 16, functionGroup: .load16, source: .address16, dest: .hl),
			OpcodeInfo(opcode: 0x2b, cycles: 6, functionGroup: .dec16, source: .hl, dest: .hl),
			OpcodeInfo(opcode: 0x2c, cycles: 4, functionGroup: .inc8, source: .l, dest: .l),
			OpcodeInfo(opcode: 0x2d, cycles: 4, functionGroup: .dec8, source: .l, dest: .l),
			OpcodeInfo(opcode: 0x2e, cycles: 7, functionGroup: .load8, source: .immediate8, dest: .l),
			OpcodeInfo(opcode: 0x2f, cycles: 4, functionGroup: .not, source: .a, dest: .a),
			// 3
            OpcodeInfo(opcode: 0x30, cycles: 7, functionGroup: .condLoad16Disp, displacement: true, source: .pc, dest: .pc, flag: .c, flagSense: false),
			OpcodeInfo(opcode: 0x31, cycles: 10, functionGroup: .load16, source: .immediate16, dest: .sp),
			OpcodeInfo(opcode: 0x32, cycles: 13, functionGroup: .load8, source: .a, dest: .address8),
			OpcodeInfo(opcode: 0x33, cycles: 6, functionGroup: .inc16, source: .sp, dest: .sp),
			OpcodeInfo(opcode: 0x34, cycles: 11, functionGroup: .inc8, source: .addressInHL, dest: .addressInHL),
			OpcodeInfo(opcode: 0x35, cycles: 11, functionGroup: .dec8, source: .addressInHL, dest: .addressInHL),
			OpcodeInfo(opcode: 0x36, cycles: 10, functionGroup: .load8, source: .immediate8, dest: .addressInHL),
			OpcodeInfo(opcode: 0x37, cycles: 4, functionGroup: .scf, flag: .c, flagSense: true),
			OpcodeInfo(opcode: 0x38, cycles: 7, functionGroup: .condLoad16Disp, displacement: true, source: .pc, dest: .pc, flag: .c, flagSense: true),
			OpcodeInfo(opcode: 0x39, cycles: 11, functionGroup: .add16, source: .sp, source2: .hl, dest: .hl),
			OpcodeInfo(opcode: 0x3a, cycles: 13, functionGroup: .load8, source: .address8, dest: .a),
			OpcodeInfo(opcode: 0x3b, cycles: 6, functionGroup: .dec16, source: .sp, dest: .sp),
			OpcodeInfo(opcode: 0x3c, cycles: 4, functionGroup: .inc8, source: .a, dest: .a),
			OpcodeInfo(opcode: 0x3d, cycles: 4, functionGroup: .dec8, source: .a, dest: .a),
			OpcodeInfo(opcode: 0x3e, cycles: 7, functionGroup: .load8, source: .immediate8, dest: .a),
			OpcodeInfo(opcode: 0x3f, cycles: 4, functionGroup: .ccf),
			// 4 start of ld r,r'
            OpcodeInfo(opcode: 0x40, cycles: 4, functionGroup: .load8, source: .b, dest: .b),
            OpcodeInfo(opcode: 0x41, cycles: 4, functionGroup: .load8, source: .c, dest: .b),
            OpcodeInfo(opcode: 0x42, cycles: 4, functionGroup: .load8, source: .d, dest: .b),
            OpcodeInfo(opcode: 0x43, cycles: 4, functionGroup: .load8, source: .e, dest: .b),
            OpcodeInfo(opcode: 0x44, cycles: 4, functionGroup: .load8, source: .h, dest: .b),
            OpcodeInfo(opcode: 0x45, cycles: 4, functionGroup: .load8, source: .l, dest: .b),
            OpcodeInfo(opcode: 0x46, cycles: 7, functionGroup: .load8, source: .addressInHL, dest: .b),
            OpcodeInfo(opcode: 0x47, cycles: 4, functionGroup: .load8, source: .a, dest: .b),
            OpcodeInfo(opcode: 0x48, cycles: 4, functionGroup: .load8, source: .b, dest: .c),
            OpcodeInfo(opcode: 0x49, cycles: 4, functionGroup: .load8, source: .c, dest: .c),
            OpcodeInfo(opcode: 0x4a, cycles: 4, functionGroup: .load8, source: .d, dest: .c),
            OpcodeInfo(opcode: 0x4b, cycles: 4, functionGroup: .load8, source: .e, dest: .c),
            OpcodeInfo(opcode: 0x4c, cycles: 4, functionGroup: .load8, source: .h, dest: .c),
            OpcodeInfo(opcode: 0x4d, cycles: 4, functionGroup: .load8, source: .l, dest: .c),
            OpcodeInfo(opcode: 0x4e, cycles: 7, functionGroup: .load8, source: .addressInHL, dest: .c),
            OpcodeInfo(opcode: 0x4f, cycles: 4, functionGroup: .load8, source: .a, dest: .c),
			// 5
            OpcodeInfo(opcode: 0x50, cycles: 4, functionGroup: .load8, source: .b, dest: .d),
            OpcodeInfo(opcode: 0x51, cycles: 4, functionGroup: .load8, source: .c, dest: .d),
            OpcodeInfo(opcode: 0x52, cycles: 4, functionGroup: .load8, source: .d, dest: .d),
            OpcodeInfo(opcode: 0x53, cycles: 4, functionGroup: .load8, source: .e, dest: .d),
            OpcodeInfo(opcode: 0x54, cycles: 4, functionGroup: .load8, source: .h, dest: .d),
            OpcodeInfo(opcode: 0x55, cycles: 4, functionGroup: .load8, source: .l, dest: .d),
            OpcodeInfo(opcode: 0x56, cycles: 7, functionGroup: .load8, source: .addressInHL, dest: .d),
            OpcodeInfo(opcode: 0x57, cycles: 4, functionGroup: .load8, source: .a, dest: .d),
            OpcodeInfo(opcode: 0x58, cycles: 4, functionGroup: .load8, source: .b, dest: .e),
            OpcodeInfo(opcode: 0x59, cycles: 4, functionGroup: .load8, source: .c, dest: .e),
            OpcodeInfo(opcode: 0x5a, cycles: 4, functionGroup: .load8, source: .d, dest: .e),
            OpcodeInfo(opcode: 0x5b, cycles: 4, functionGroup: .load8, source: .e, dest: .e),
            OpcodeInfo(opcode: 0x5c, cycles: 4, functionGroup: .load8, source: .h, dest: .e),
            OpcodeInfo(opcode: 0x5d, cycles: 4, functionGroup: .load8, source: .l, dest: .e),
            OpcodeInfo(opcode: 0x5e, cycles: 7, functionGroup: .load8, source: .addressInHL, dest: .e),
            OpcodeInfo(opcode: 0x5f, cycles: 4, functionGroup: .load8, source: .a, dest: .e),
			// 6
            OpcodeInfo(opcode: 0x60, cycles: 4, functionGroup: .load8, source: .b, dest: .h),
            OpcodeInfo(opcode: 0x61, cycles: 4, functionGroup: .load8, source: .c, dest: .h),
            OpcodeInfo(opcode: 0x62, cycles: 4, functionGroup: .load8, source: .d, dest: .h),
            OpcodeInfo(opcode: 0x63, cycles: 4, functionGroup: .load8, source: .e, dest: .h),
            OpcodeInfo(opcode: 0x64, cycles: 4, functionGroup: .load8, source: .h, dest: .h),
            OpcodeInfo(opcode: 0x65, cycles: 4, functionGroup: .load8, source: .l, dest: .h),
            OpcodeInfo(opcode: 0x66, cycles: 7, functionGroup: .load8, source: .addressInHL, dest: .h),
            OpcodeInfo(opcode: 0x67, cycles: 4, functionGroup: .load8, source: .a, dest: .h),
            OpcodeInfo(opcode: 0x68, cycles: 4, functionGroup: .load8, source: .b, dest: .l),
            OpcodeInfo(opcode: 0x69, cycles: 4, functionGroup: .load8, source: .c, dest: .l),
            OpcodeInfo(opcode: 0x6a, cycles: 4, functionGroup: .load8, source: .d, dest: .l),
            OpcodeInfo(opcode: 0x6b, cycles: 4, functionGroup: .load8, source: .e, dest: .l),
            OpcodeInfo(opcode: 0x6c, cycles: 4, functionGroup: .load8, source: .h, dest: .l),
            OpcodeInfo(opcode: 0x6d, cycles: 4, functionGroup: .load8, source: .l, dest: .l),
            OpcodeInfo(opcode: 0x6e, cycles: 7, functionGroup: .load8, source: .addressInHL, dest: .l),
            OpcodeInfo(opcode: 0x6f, cycles: 4, functionGroup: .load8, source: .a, dest: .l),
			// 7
            OpcodeInfo(opcode: 0x70, cycles: 7, functionGroup: .load8, source: .b, dest: .addressInHL),
            OpcodeInfo(opcode: 0x71, cycles: 7, functionGroup: .load8, source: .c, dest: .addressInHL),
            OpcodeInfo(opcode: 0x72, cycles: 7, functionGroup: .load8, source: .d, dest: .addressInHL),
            OpcodeInfo(opcode: 0x73, cycles: 7, functionGroup: .load8, source: .e, dest: .addressInHL),
            OpcodeInfo(opcode: 0x74, cycles: 7, functionGroup: .load8, source: .h, dest: .addressInHL),
            OpcodeInfo(opcode: 0x75, cycles: 7, functionGroup: .load8, source: .l, dest: .addressInHL),
            OpcodeInfo(opcode: 0x76, cycles: 4, functionGroup: .halt),
            OpcodeInfo(opcode: 0x77, cycles: 7, functionGroup: .load8, source: .a, dest: .addressInHL),
			OpcodeInfo(opcode: 0x78, cycles: 4, functionGroup: .load8, source: .b, dest: .a),
			OpcodeInfo(opcode: 0x79, cycles: 4, functionGroup: .load8, source: .c, dest: .a),
			OpcodeInfo(opcode: 0x7a, cycles: 4, functionGroup: .load8, source: .d, dest: .a),
			OpcodeInfo(opcode: 0x7b, cycles: 4, functionGroup: .load8, source: .e, dest: .a),
			OpcodeInfo(opcode: 0x7c, cycles: 4, functionGroup: .load8, source: .h, dest: .a),
			OpcodeInfo(opcode: 0x7d, cycles: 4, functionGroup: .load8, source: .l, dest: .a),
			OpcodeInfo(opcode: 0x7e, cycles: 7, functionGroup: .load8, source: .addressInHL, dest: .a),
			OpcodeInfo(opcode: 0x7f, cycles: 4, functionGroup: .load8, source: .a, dest: .a),
			// 8
            OpcodeInfo(opcode: 0x80, cycles: 4, functionGroup: .add8, source: .b, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x81, cycles: 4, functionGroup: .add8, source: .c, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x82, cycles: 4, functionGroup: .add8, source: .d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x83, cycles: 4, functionGroup: .add8, source: .e, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x84, cycles: 4, functionGroup: .add8, source: .h, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x85, cycles: 4, functionGroup: .add8, source: .l, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0x86, cycles: 7, functionGroup: .add8, source: .addressInHL, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0x87, cycles: 4, functionGroup: .add8, source: .a, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0x88, cycles: 4, functionGroup: .adc8, source: .b, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0x89, cycles: 4, functionGroup: .adc8, source: .c, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0x8a, cycles: 4, functionGroup: .adc8, source: .d, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0x8b, cycles: 4, functionGroup: .adc8, source: .e, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0x8c, cycles: 4, functionGroup: .adc8, source: .h, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0x8d, cycles: 4, functionGroup: .adc8, source: .l, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0x8e, cycles: 7, functionGroup: .adc8, source: .addressInHL, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0x8f, cycles: 4, functionGroup: .adc8, source: .a, source2: .a, dest: .a),
			// 9
            OpcodeInfo(opcode: 0x90, cycles: 4, functionGroup: .sub8, source: .b, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x91, cycles: 4, functionGroup: .sub8, source: .c, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x92, cycles: 4, functionGroup: .sub8, source: .d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x93, cycles: 4, functionGroup: .sub8, source: .e, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x94, cycles: 4, functionGroup: .sub8, source: .h, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x95, cycles: 4, functionGroup: .sub8, source: .l, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x96, cycles: 7, functionGroup: .sub8, source: .addressInHL, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x97, cycles: 4, functionGroup: .sub8, source: .a, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x98, cycles: 4, functionGroup: .sbc8, source: .b, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x99, cycles: 4, functionGroup: .sbc8, source: .c, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x9a, cycles: 4, functionGroup: .sbc8, source: .d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x9b, cycles: 4, functionGroup: .sbc8, source: .e, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x9c, cycles: 4, functionGroup: .sbc8, source: .h, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x9d, cycles: 4, functionGroup: .sbc8, source: .l, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x9e, cycles: 7, functionGroup: .sbc8, source: .addressInHL, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x9f, cycles: 4, functionGroup: .sbc8, source: .a, source2: .a, dest: .a),
			// a
            OpcodeInfo(opcode: 0xa0, cycles: 4, functionGroup: .and8, source: .b, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xa1, cycles: 4, functionGroup: .and8, source: .c, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xa2, cycles: 4, functionGroup: .and8, source: .d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xa3, cycles: 4, functionGroup: .and8, source: .e, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xa4, cycles: 4, functionGroup: .and8, source: .h, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xa5, cycles: 4, functionGroup: .and8, source: .l, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xa6, cycles: 7, functionGroup: .and8, source: .addressInHL, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xa7, cycles: 4, functionGroup: .and8, source: .a, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xa8, cycles: 4, functionGroup: .xor8, source: .b, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xa9, cycles: 4, functionGroup: .xor8, source: .c, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xaa, cycles: 4, functionGroup: .xor8, source: .d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xab, cycles: 4, functionGroup: .xor8, source: .e, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xac, cycles: 4, functionGroup: .xor8, source: .h, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xad, cycles: 4, functionGroup: .xor8, source: .l, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0xae, cycles: 7, functionGroup: .xor8, source: .addressInHL, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0xaf, cycles: 4, functionGroup: .xor8, source: .a, source2: .a, dest: .a),
			// b
            OpcodeInfo(opcode: 0xb0, cycles: 4, functionGroup: .or8, source: .b, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xb1, cycles: 4, functionGroup: .or8, source: .c, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xb2, cycles: 4, functionGroup: .or8, source: .d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xb3, cycles: 4, functionGroup: .or8, source: .e, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xb4, cycles: 4, functionGroup: .or8, source: .h, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xb5, cycles: 4, functionGroup: .or8, source: .l, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0xb6, cycles: 7, functionGroup: .or8, source: .addressInHL, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0xb7, cycles: 4, functionGroup: .or8, source: .a, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0xb8, cycles: 4, functionGroup: .cp8, source: .b, source2: .a, dest: .none),
			OpcodeInfo(opcode: 0xb9, cycles: 4, functionGroup: .cp8, source: .c, source2: .a, dest: .none),
			OpcodeInfo(opcode: 0xba, cycles: 4, functionGroup: .cp8, source: .d, source2: .a, dest: .none),
			OpcodeInfo(opcode: 0xbb, cycles: 4, functionGroup: .cp8, source: .e, source2: .a, dest: .none),
			OpcodeInfo(opcode: 0xbc, cycles: 4, functionGroup: .cp8, source: .h, source2: .a, dest: .none),
			OpcodeInfo(opcode: 0xbd, cycles: 4, functionGroup: .cp8, source: .l, source2: .a, dest: .none),
			OpcodeInfo(opcode: 0xbe, cycles: 7, functionGroup: .cp8, source: .addressInHL, source2: .a, dest: .none),
			OpcodeInfo(opcode: 0xbf, cycles: 4, functionGroup: .cp8, source: .a, source2: .a, dest: .none),
			// c
            OpcodeInfo(opcode: 0xc0, cycles: 5, functionGroup: .condRet, source: .none, dest: .pc, flag: .z, flagSense: false),
			OpcodeInfo(opcode: 0xc1, cycles: 10, functionGroup: .load16, source: .addressInSPPostInc, dest: .bc),
			OpcodeInfo(opcode: 0xc2, cycles: 10, functionGroup: .condLoad16, source: .immediate16, dest: .pc, flag: .z, flagSense: false),
			OpcodeInfo(opcode: 0xc3, cycles: 10, functionGroup: .load16, source: .immediate16, dest: .pc),
			OpcodeInfo(opcode: 0xc4, cycles: 10, functionGroup: .condLoad16, source: .immediate16, source2: .pc, dest: .pc, dest2: .preDecAddressInSP, flag: .z, flagSense: false),
			OpcodeInfo(opcode: 0xc5, cycles: 11, functionGroup: .load16, source: .bc, dest: .preDecAddressInSP),
			OpcodeInfo(opcode: 0xc6, cycles: 7, functionGroup: .add8, source: .immediate8, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0xc7, cycles: 11, functionGroup: .load16, source: .constant, source2: .pc, dest: .pc, dest2: .preDecAddressInSP, constant: 0x0),
			OpcodeInfo(opcode: 0xc8, cycles: 5, functionGroup: .condRet, source: .none, dest: .pc, flag: .z, flagSense: true),
			OpcodeInfo(opcode: 0xc9, cycles: 10, functionGroup: .load16, source: .addressInSPPostInc, dest: .pc),
			OpcodeInfo(opcode: 0xca, cycles: 10, functionGroup: .condLoad16, source: .immediate16, dest: .pc, flag: .z, flagSense: true),
			OpcodeInfo(opcode: 0xcb, cycles: 4, functionGroup: .cbPrefix, inhibitInterrupt: true),
			OpcodeInfo(opcode: 0xcc, cycles: 10, functionGroup: .condLoad16, source: .immediate16, source2: .pc, dest: .pc, dest2: .preDecAddressInSP, flag: .z, flagSense: true),
			OpcodeInfo(opcode: 0xcd, cycles: 17, functionGroup: .load16, source: .immediate16, source2: .pc, dest: .pc, dest2: .preDecAddressInSP),
			OpcodeInfo(opcode: 0xce, cycles: 7, functionGroup: .adc8, source: .immediate8, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0xcf, cycles: 11, functionGroup: .load16, source: .constant, source2: .pc, dest: .pc, dest2: .preDecAddressInSP, constant: 0x08),
			// d
            OpcodeInfo(opcode: 0xd0, cycles: 5, functionGroup: .condRet, source: .none, dest: .pc, flag: .c, flagSense: false),
			OpcodeInfo(opcode: 0xd1, cycles: 10, functionGroup: .load16, source: .addressInSPPostInc, dest: .de),
			OpcodeInfo(opcode: 0xd2, cycles: 10, functionGroup: .condLoad16, source: .immediate16, dest: .pc, flag: .c, flagSense: false),
			OpcodeInfo(opcode: 0xd3, cycles: 11, functionGroup: .out, source: .a, source2: .immediate8),
			OpcodeInfo(opcode: 0xd4, cycles: 10, functionGroup: .condLoad16, source: .immediate16, source2: .pc, dest: .pc, dest2: .preDecAddressInSP, flag: .c, flagSense: false),
			OpcodeInfo(opcode: 0xd5, cycles: 11, functionGroup: .load16, source: .de, dest: .preDecAddressInSP),
			OpcodeInfo(opcode: 0xd6, cycles: 7, functionGroup: .sub8, source: .immediate8, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0xd7, cycles: 11, functionGroup: .load16, source: .constant, source2: .pc, dest: .pc, dest2: .preDecAddressInSP, constant: 0x10),
			OpcodeInfo(opcode: 0xd8, cycles: 5, functionGroup: .condRet, source: .none, dest: .pc, flag: .c, flagSense: true),
			OpcodeInfo(opcode: 0xd9, cycles: 4, functionGroup: .exx),
			OpcodeInfo(opcode: 0xda, cycles: 10, functionGroup: .condLoad16, source: .immediate16, dest: .pc, flag: .c, flagSense: true),
			OpcodeInfo(opcode: 0xdb, cycles: 11, functionGroup: .ina, source: .immediate8, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0xdc, cycles: 10, functionGroup: .condLoad16, source: .immediate16, source2: .pc, dest: .pc, dest2: .preDecAddressInSP, flag: .c, flagSense: true),
			OpcodeInfo(opcode: 0xdd, cycles: 4, functionGroup: .ddPrefix, inhibitInterrupt: true),
			OpcodeInfo(opcode: 0xde, cycles: 7, functionGroup: .sbc8, source: .immediate8, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0xdf, cycles: 11, functionGroup: .load16, source: .constant, source2: .pc, dest: .pc, dest2: .preDecAddressInSP, constant: 0x18),
			// e
            OpcodeInfo(opcode: 0xe0, cycles: 5, functionGroup: .condRet, source: .none, dest: .pc, flag: .pv, flagSense: false),
			OpcodeInfo(opcode: 0xe1, cycles: 10, functionGroup: .load16, source: .addressInSPPostInc, dest: .hl),
			OpcodeInfo(opcode: 0xe2, cycles: 10, functionGroup: .condLoad16, source: .immediate16, dest: .pc, flag: .pv, flagSense: false),
			OpcodeInfo(opcode: 0xe3, cycles: 19, functionGroup: .load16, source: .hl, source2: .addressInSPPostInc, dest: .preDecAddressInSP, dest2: .hl),
			OpcodeInfo(opcode: 0xe4, cycles: 10, functionGroup: .condLoad16, source: .immediate16, source2: .pc, dest: .pc, dest2: .preDecAddressInSP, flag: .pv, flagSense: false),
			OpcodeInfo(opcode: 0xe5, cycles: 11, functionGroup: .load16, source: .hl, dest: .preDecAddressInSP),
			OpcodeInfo(opcode: 0xe6, cycles: 7, functionGroup: .and8, source: .immediate8, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0xe7, cycles: 11, functionGroup: .load16, source: .constant, source2: .pc, dest: .pc, dest2: .preDecAddressInSP, constant: 0x20),
			OpcodeInfo(opcode: 0xe8, cycles: 5, functionGroup: .condRet, source: .none, dest: .pc, flag: .pv, flagSense: true),
			OpcodeInfo(opcode: 0xe9, cycles: 4, functionGroup: .load16, source: .hl, dest: .pc),
			OpcodeInfo(opcode: 0xea, cycles: 10, functionGroup: .condLoad16, source: .immediate16, dest: .pc, flag: .pv, flagSense: true),
			OpcodeInfo(opcode: 0xeb, cycles: 4, functionGroup: .load16, source: .hl, source2: .de, dest: .de, dest2: .hl),
			OpcodeInfo(opcode: 0xec, cycles: 10, functionGroup: .condLoad16, source: .immediate16, source2: .pc, dest: .pc, dest2: .preDecAddressInSP, flag: .pv, flagSense: true),
			OpcodeInfo(opcode: 0xed, cycles: 4, functionGroup: .edPrefix, inhibitInterrupt: true),
			OpcodeInfo(opcode: 0xee, cycles: 7, functionGroup: .xor8, source: .immediate8, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0xef, cycles: 11, functionGroup: .load16, source: .constant, source2: .pc, dest: .pc, dest2: .preDecAddressInSP, constant: 0x28),
			// f
            OpcodeInfo(opcode: 0xf0, cycles: 5, functionGroup: .condRet, source: .none, dest: .pc, flag: .s, flagSense: false),
			OpcodeInfo(opcode: 0xf1, cycles: 10, functionGroup: .load16, source: .addressInSPPostInc, dest: .af),
			OpcodeInfo(opcode: 0xf2, cycles: 10, functionGroup: .condLoad16, source: .immediate16, dest: .pc, flag: .s, flagSense: false),
			OpcodeInfo(opcode: 0xf3, cycles: 4, functionGroup: .disableInterrupt),
			OpcodeInfo(opcode: 0xf4, cycles: 10, functionGroup: .condLoad16, source: .immediate16, source2: .pc, dest: .pc, dest2: .preDecAddressInSP, flag: .s, flagSense: false),
			OpcodeInfo(opcode: 0xf5, cycles: 11, functionGroup: .load16, source: .af, dest: .preDecAddressInSP),
			OpcodeInfo(opcode: 0xf6, cycles: 7, functionGroup: .or8, source: .immediate8, source2: .a, dest: .a),
			OpcodeInfo(opcode: 0xf7, cycles: 11, functionGroup: .load16, source: .constant, source2: .pc, dest: .pc, dest2: .preDecAddressInSP, constant: 0x30),
			OpcodeInfo(opcode: 0xf8, cycles: 5, functionGroup: .condRet, source: .none, dest: .pc, flag: .s, flagSense: true),
			OpcodeInfo(opcode: 0xf9, cycles: 6, functionGroup: .load16, source: .hl, dest: .sp),
			OpcodeInfo(opcode: 0xfa, cycles: 10, functionGroup: .condLoad16, source: .immediate16, dest: .pc, flag: .s, flagSense: true),
			OpcodeInfo(opcode: 0xfb, cycles: 4, functionGroup: .enableInterrupt, inhibitInterrupt: true),
			OpcodeInfo(opcode: 0xfc, cycles: 10, functionGroup: .condLoad16, source: .immediate16, source2: .pc, dest: .pc, dest2: .preDecAddressInSP, flag: .s, flagSense: true),
			OpcodeInfo(opcode: 0xfd, cycles: 4, functionGroup: .fdPrefix, inhibitInterrupt: true),
			OpcodeInfo(opcode: 0xfe, cycles: 7, functionGroup: .cp8, source: .immediate8, source2: .a, dest: .none),
			OpcodeInfo(opcode: 0xff, cycles: 11, functionGroup: .load16, source: .constant, source2: .pc, dest: .pc, dest2: .preDecAddressInSP, constant: 0x38),

			// 0x100
			OpcodeInfo(opcode: stop, cycles: 1, functionGroup: .stop),
            OpcodeInfo(opcode: reset, cycles: 1, functionGroup: .stop)
       ]
    )

    static let ddGroup = Decoder(name: "ddGroup", base: standard, diffs:
        [
            OpcodeInfo(opcode: 0x09, cycles: 11, functionGroup: .add16, source: .bc, source2: .ix, dest: .ix),
            OpcodeInfo(opcode: 0x19, cycles: 11, functionGroup: .add16, source: .de, source2: .ix, dest: .ix),
            OpcodeInfo(opcode: 0x21, cycles: 10, functionGroup: .load16, source: .immediate16, dest: .ix),
            OpcodeInfo(opcode: 0x22, cycles: 16, functionGroup: .load16, source: .ix, dest: .address16),
            OpcodeInfo(opcode: 0x23, cycles: 6, functionGroup: .inc16, source: .ix, dest: .ix),
            OpcodeInfo(opcode: 0x24, cycles: 4, functionGroup: .inc8, source: .ixh, dest: .ixh),
            OpcodeInfo(opcode: 0x25, cycles: 4, functionGroup: .dec8, source: .ixh, dest: .ixh),
            OpcodeInfo(opcode: 0x26, cycles: 7, functionGroup: .load8, source: .immediate8, dest: .ixh),
            OpcodeInfo(opcode: 0x29, cycles: 11, functionGroup: .add16, source: .ix, source2: .ix, dest: .ix),
            OpcodeInfo(opcode: 0x2a, cycles: 16, functionGroup: .load16, source: .address16, dest: .ix),
            OpcodeInfo(opcode: 0x2b, cycles: 6, functionGroup: .dec16, source: .ix, dest: .ix),
            OpcodeInfo(opcode: 0x2c, cycles: 4, functionGroup: .inc8, source: .ixl, dest: .ixl),
            OpcodeInfo(opcode: 0x2d, cycles: 4, functionGroup: .dec8, source: .ixl, dest: .ixl),
            OpcodeInfo(opcode: 0x2e, cycles: 7, functionGroup: .load8, source: .immediate8, dest: .ixl),
            OpcodeInfo(opcode: 0x34, cycles: 19, functionGroup: .inc8, displacement: true, source: .addressInIX_d, dest: .addressInIX_d),
            OpcodeInfo(opcode: 0x35, cycles: 19, functionGroup: .dec8, displacement: true, source: .addressInIX_d, dest: .addressInIX_d),
            OpcodeInfo(opcode: 0x36, cycles: 15, functionGroup: .load8, displacement: true, source: .immediate8, dest: .addressInIX_d),
            OpcodeInfo(opcode: 0x39, cycles: 11, functionGroup: .add16, source: .sp, source2: .ix, dest: .ix),
            OpcodeInfo(opcode: 0x44, cycles: 4, functionGroup: .load8, source: .ixh, dest: .b),
            OpcodeInfo(opcode: 0x45, cycles: 4, functionGroup: .load8, source: .ixl, dest: .b),
            OpcodeInfo(opcode: 0x46, cycles: 15, functionGroup: .load8, displacement: true, source: .addressInIX_d, dest: .b),
            OpcodeInfo(opcode: 0x4c, cycles: 4, functionGroup: .load8, source: .ixh, dest: .c),
            OpcodeInfo(opcode: 0x4d, cycles: 4, functionGroup: .load8, source: .ixl, dest: .c),
            OpcodeInfo(opcode: 0x4e, cycles: 15, functionGroup: .load8, displacement: true, source: .addressInIX_d, dest: .c),
            OpcodeInfo(opcode: 0x54, cycles: 4, functionGroup: .load8, source: .ixh, dest: .d),
            OpcodeInfo(opcode: 0x55, cycles: 4, functionGroup: .load8, source: .ixl, dest: .d),
            OpcodeInfo(opcode: 0x56, cycles: 15, functionGroup: .load8, displacement: true, source: .addressInIX_d, dest: .d),
            OpcodeInfo(opcode: 0x5c, cycles: 4, functionGroup: .load8, source: .ixh, dest: .e),
            OpcodeInfo(opcode: 0x5d, cycles: 4, functionGroup: .load8, source: .ixl, dest: .e),
            OpcodeInfo(opcode: 0x5e, cycles: 15, functionGroup: .load8, displacement: true, source: .addressInIX_d, dest: .e),
            OpcodeInfo(opcode: 0x60, cycles: 4, functionGroup: .load8, source: .b, dest: .ixh),
            OpcodeInfo(opcode: 0x61, cycles: 4, functionGroup: .load8, source: .c, dest: .ixh),
            OpcodeInfo(opcode: 0x62, cycles: 4, functionGroup: .load8, source: .d, dest: .ixh),
            OpcodeInfo(opcode: 0x63, cycles: 4, functionGroup: .load8, source: .e, dest: .ixh),
            OpcodeInfo(opcode: 0x64, cycles: 4, functionGroup: .load8, source: .ixh, dest: .ixh),
            OpcodeInfo(opcode: 0x65, cycles: 4, functionGroup: .load8, source: .ixl, dest: .ixh),
            OpcodeInfo(opcode: 0x66, cycles: 15, functionGroup: .load8, displacement: true, source: .addressInIX_d, dest: .h),
            OpcodeInfo(opcode: 0x67, cycles: 4, functionGroup: .load8, source: .a, dest: .ixh),
            OpcodeInfo(opcode: 0x68, cycles: 4, functionGroup: .load8, source: .b, dest: .ixl),
            OpcodeInfo(opcode: 0x69, cycles: 4, functionGroup: .load8, source: .c, dest: .ixl),
            OpcodeInfo(opcode: 0x6a, cycles: 4, functionGroup: .load8, source: .d, dest: .ixl),
            OpcodeInfo(opcode: 0x6b, cycles: 4, functionGroup: .load8, source: .e, dest: .ixl),
            OpcodeInfo(opcode: 0x6c, cycles: 4, functionGroup: .load8, source: .ixh, dest: .ixl),
            OpcodeInfo(opcode: 0x6d, cycles: 4, functionGroup: .load8, source: .ixl, dest: .ixl),
            OpcodeInfo(opcode: 0x6e, cycles: 15, functionGroup: .load8, displacement: true, source: .addressInIX_d, dest: .l),
            OpcodeInfo(opcode: 0x6f, cycles: 4, functionGroup: .load8, source: .a, dest: .ixl),
            OpcodeInfo(opcode: 0x70, cycles: 15, functionGroup: .load8, displacement: true, source: .b, dest: .addressInIX_d),
            OpcodeInfo(opcode: 0x71, cycles: 15, functionGroup: .load8, displacement: true, source: .c, dest: .addressInIX_d),
            OpcodeInfo(opcode: 0x72, cycles: 15, functionGroup: .load8, displacement: true, source: .d, dest: .addressInIX_d),
            OpcodeInfo(opcode: 0x73, cycles: 15, functionGroup: .load8, displacement: true, source: .e, dest: .addressInIX_d),
            OpcodeInfo(opcode: 0x74, cycles: 15, functionGroup: .load8, displacement: true, source: .h, dest: .addressInIX_d),
            OpcodeInfo(opcode: 0x75, cycles: 15, functionGroup: .load8, displacement: true, source: .l, dest: .addressInIX_d),
            OpcodeInfo(opcode: 0x77, cycles: 15, functionGroup: .load8, displacement: true, source: .a, dest: .addressInIX_d),

            OpcodeInfo(opcode: 0x7c, cycles: 4, functionGroup: .load8, source: .ixh, dest: .a),
            OpcodeInfo(opcode: 0x7d, cycles: 4, functionGroup: .load8, source: .ixl, dest: .a),
            OpcodeInfo(opcode: 0x7e, cycles: 15, functionGroup: .load8, displacement: true, source: .addressInIX_d, dest: .a),
            OpcodeInfo(opcode: 0x84, cycles: 4, functionGroup: .add8, source: .ixh, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x85, cycles: 4, functionGroup: .add8, source: .ixl, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x86, cycles: 15, functionGroup: .add8, displacement: true, source: .addressInIX_d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x8c, cycles: 4, functionGroup: .adc8, source: .ixh, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x8d, cycles: 4, functionGroup: .adc8, source: .ixl, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x8e, cycles: 15, functionGroup: .adc8, displacement: true, source: .addressInIX_d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x94, cycles: 4, functionGroup: .sub8, source: .ixh, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x95, cycles: 4, functionGroup: .sub8, source: .ixl, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x96, cycles: 15, functionGroup: .sub8, displacement: true, source: .addressInIX_d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x9c, cycles: 4, functionGroup: .sbc8, source: .ixh, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x9d, cycles: 4, functionGroup: .sbc8, source: .ixl, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x9e, cycles: 15, functionGroup: .sbc8, displacement: true, source: .addressInIX_d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xa4, cycles: 4, functionGroup: .and8, source: .ixh, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xa5, cycles: 4, functionGroup: .and8, source: .ixl, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xa6, cycles: 15, functionGroup: .and8, displacement: true, source: .addressInIX_d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xac, cycles: 4, functionGroup: .xor8, source: .ixh, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xad, cycles: 4, functionGroup: .xor8, source: .ixl, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xae, cycles: 15, functionGroup: .xor8, displacement: true, source: .addressInIX_d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xb4, cycles: 4, functionGroup: .or8, source: .ixh, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xb5, cycles: 4, functionGroup: .or8, source: .ixl, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xb6, cycles: 15, functionGroup: .or8, displacement: true, source: .addressInIX_d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xbc, cycles: 4, functionGroup: .cp8, source: .ixh, source2: .a, dest: .none),
            OpcodeInfo(opcode: 0xbd, cycles: 4, functionGroup: .cp8, source: .ixl, source2: .a, dest: .none),
            OpcodeInfo(opcode: 0xbe, cycles: 15, functionGroup: .cp8, displacement: true, source: .addressInIX_d, source2: .a, dest: .none),
            OpcodeInfo(opcode: 0xcb, cycles: 8, functionGroup: .ddcbPrefix, displacement: true, inhibitInterrupt: true),
            OpcodeInfo(opcode: 0xe1, cycles: 10, functionGroup: .load16, source: .addressInSPPostInc, dest: .ix),
       		OpcodeInfo(opcode: 0xe3, cycles: 19, functionGroup: .load16, source: .ix, source2: .addressInSPPostInc, dest: .preDecAddressInSP, dest2: .ix),
            OpcodeInfo(opcode: 0xe5, cycles: 11, functionGroup: .load16, source: .ix, dest: .preDecAddressInSP),
            OpcodeInfo(opcode: 0xe9, cycles: 4, functionGroup: .load16, source: .ix, dest: .pc),
            OpcodeInfo(opcode: 0xf9, cycles: 6, functionGroup: .load16, source: .ix, dest: .sp),

        ])

    static let fdGroup = Decoder(name: "fdGroup", base: standard, diffs:
        [
            OpcodeInfo(opcode: 0x09, cycles: 11, functionGroup: .add16, source: .bc, source2: .iy, dest: .iy),
            OpcodeInfo(opcode: 0x19, cycles: 11, functionGroup: .add16, source: .de, source2: .iy, dest: .iy),
            OpcodeInfo(opcode: 0x21, cycles: 10, functionGroup: .load16, source: .immediate16, dest: .iy),
            OpcodeInfo(opcode: 0x22, cycles: 16, functionGroup: .load16, source: .iy, dest: .address16),
            OpcodeInfo(opcode: 0x23, cycles: 6, functionGroup: .inc16, source: .iy, dest: .iy),
            OpcodeInfo(opcode: 0x24, cycles: 4, functionGroup: .inc8, source: .iyh, dest: .iyh),
            OpcodeInfo(opcode: 0x25, cycles: 4, functionGroup: .dec8, source: .iyh, dest: .iyh),
            OpcodeInfo(opcode: 0x26, cycles: 7, functionGroup: .load8, source: .immediate8, dest: .iyh),
            OpcodeInfo(opcode: 0x29, cycles: 11, functionGroup: .add16, source: .iy, source2: .iy, dest: .iy),
            OpcodeInfo(opcode: 0x2a, cycles: 16, functionGroup: .load16, source: .address16, dest: .iy),
            OpcodeInfo(opcode: 0x2b, cycles: 6, functionGroup: .dec16, source: .iy, dest: .iy),
            OpcodeInfo(opcode: 0x2c, cycles: 4, functionGroup: .inc8, source: .iyl, dest: .iyl),
           	OpcodeInfo(opcode: 0x2d, cycles: 4, functionGroup: .dec8, source: .iyl, dest: .iyl),
            OpcodeInfo(opcode: 0x2e, cycles: 7, functionGroup: .load8, source: .immediate8, dest: .iyl),
            OpcodeInfo(opcode: 0x34, cycles: 19, functionGroup: .inc8, displacement: true, source: .addressInIY_d, dest: .addressInIY_d),
            OpcodeInfo(opcode: 0x35, cycles: 19, functionGroup: .dec8, displacement: true, source: .addressInIY_d, dest: .addressInIY_d),
            OpcodeInfo(opcode: 0x36, cycles: 15, functionGroup: .load8, displacement: true, source: .immediate8, dest: .addressInIY_d),
            OpcodeInfo(opcode: 0x39, cycles: 11, functionGroup: .add16, source: .sp, source2: .iy, dest: .iy),
            OpcodeInfo(opcode: 0x44, cycles: 4, functionGroup: .load8, source: .iyh, dest: .b),
            OpcodeInfo(opcode: 0x45, cycles: 4, functionGroup: .load8, source: .iyl, dest: .b),
            OpcodeInfo(opcode: 0x46, cycles: 15, functionGroup: .load8, displacement: true, source: .addressInIY_d, dest: .b),
            OpcodeInfo(opcode: 0x4c, cycles: 4, functionGroup: .load8, source: .iyh, dest: .c),
            OpcodeInfo(opcode: 0x4d, cycles: 4, functionGroup: .load8, source: .iyl, dest: .c),
            OpcodeInfo(opcode: 0x4e, cycles: 15, functionGroup: .load8, displacement: true, source: .addressInIY_d, dest: .c),
            OpcodeInfo(opcode: 0x54, cycles: 4, functionGroup: .load8, source: .iyh, dest: .d),
            OpcodeInfo(opcode: 0x55, cycles: 4, functionGroup: .load8, source: .iyl, dest: .d),
            OpcodeInfo(opcode: 0x56, cycles: 15, functionGroup: .load8, displacement: true, source: .addressInIY_d, dest: .d),
            OpcodeInfo(opcode: 0x5c, cycles: 4, functionGroup: .load8, source: .iyh, dest: .e),
            OpcodeInfo(opcode: 0x5d, cycles: 4, functionGroup: .load8, source: .iyl, dest: .e),
            OpcodeInfo(opcode: 0x5e, cycles: 15, functionGroup: .load8, displacement: true, source: .addressInIY_d, dest: .e),
            OpcodeInfo(opcode: 0x60, cycles: 4, functionGroup: .load8, source: .b, dest: .iyh),
            OpcodeInfo(opcode: 0x61, cycles: 4, functionGroup: .load8, source: .c, dest: .iyh),
            OpcodeInfo(opcode: 0x62, cycles: 4, functionGroup: .load8, source: .d, dest: .iyh),
            OpcodeInfo(opcode: 0x63, cycles: 4, functionGroup: .load8, source: .e, dest: .iyh),
            OpcodeInfo(opcode: 0x64, cycles: 4, functionGroup: .load8, source: .iyh, dest: .iyh),
            OpcodeInfo(opcode: 0x65, cycles: 4, functionGroup: .load8, source: .iyl, dest: .iyh),
            OpcodeInfo(opcode: 0x66, cycles: 15, functionGroup: .load8, displacement: true, source: .addressInIY_d, dest: .h),
            OpcodeInfo(opcode: 0x67, cycles: 4, functionGroup: .load8, source: .a, dest: .iyh),
            OpcodeInfo(opcode: 0x68, cycles: 4, functionGroup: .load8, source: .b, dest: .iyl),
            OpcodeInfo(opcode: 0x69, cycles: 4, functionGroup: .load8, source: .c, dest: .iyl),
            OpcodeInfo(opcode: 0x6a, cycles: 4, functionGroup: .load8, source: .d, dest: .iyl),
            OpcodeInfo(opcode: 0x6b, cycles: 4, functionGroup: .load8, source: .e, dest: .iyl),
            OpcodeInfo(opcode: 0x6c, cycles: 4, functionGroup: .load8, source: .iyh, dest: .iyl),
            OpcodeInfo(opcode: 0x6d, cycles: 4, functionGroup: .load8, source: .iyl, dest: .iyl),
            OpcodeInfo(opcode: 0x6e, cycles: 15, functionGroup: .load8, displacement: true, source: .addressInIY_d, dest: .l),
            OpcodeInfo(opcode: 0x6f, cycles: 4, functionGroup: .load8, source: .a, dest: .iyl),
            OpcodeInfo(opcode: 0x70, cycles: 15, functionGroup: .load8, displacement: true, source: .b, dest: .addressInIY_d),
            OpcodeInfo(opcode: 0x71, cycles: 15, functionGroup: .load8, displacement: true, source: .c, dest: .addressInIY_d),
            OpcodeInfo(opcode: 0x72, cycles: 15, functionGroup: .load8, displacement: true, source: .d, dest: .addressInIY_d),
            OpcodeInfo(opcode: 0x73, cycles: 15, functionGroup: .load8, displacement: true, source: .e, dest: .addressInIY_d),
            OpcodeInfo(opcode: 0x74, cycles: 15, functionGroup: .load8, displacement: true, source: .h, dest: .addressInIY_d),
            OpcodeInfo(opcode: 0x75, cycles: 15, functionGroup: .load8, displacement: true, source: .l, dest: .addressInIY_d),
            OpcodeInfo(opcode: 0x77, cycles: 15, functionGroup: .load8, displacement: true, source: .a, dest: .addressInIY_d),

            OpcodeInfo(opcode: 0x7c, cycles: 4, functionGroup: .load8, source: .iyh, dest: .a),
            OpcodeInfo(opcode: 0x7d, cycles: 4, functionGroup: .load8, source: .iyl, dest: .a),
            OpcodeInfo(opcode: 0x7e, cycles: 15, functionGroup: .load8, displacement: true, source: .addressInIY_d, dest: .a),
            OpcodeInfo(opcode: 0x84, cycles: 4, functionGroup: .add8, source: .iyh, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x85, cycles: 4, functionGroup: .add8, source: .iyl, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x86, cycles: 15, functionGroup: .add8, displacement: true, source: .addressInIY_d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x8c, cycles: 4, functionGroup: .adc8, source: .iyh, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x8d, cycles: 4, functionGroup: .adc8, source: .iyl, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x8e, cycles: 15, functionGroup: .adc8, displacement: true, source: .addressInIY_d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x94, cycles: 4, functionGroup: .sub8, source: .iyh, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x95, cycles: 4, functionGroup: .sub8, source: .iyl, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x96, cycles: 15, functionGroup: .sub8, displacement: true, source: .addressInIY_d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x9c, cycles: 4, functionGroup: .sbc8, source: .iyh, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x9d, cycles: 4, functionGroup: .sbc8, source: .iyl, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0x9e, cycles: 15, functionGroup: .sbc8, displacement: true, source: .addressInIY_d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xa4, cycles: 4, functionGroup: .and8, source: .iyh, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xa5, cycles: 4, functionGroup: .and8, source: .iyl, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xa6, cycles: 15, functionGroup: .and8, displacement: true, source: .addressInIY_d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xac, cycles: 4, functionGroup: .xor8, source: .iyh, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xad, cycles: 4, functionGroup: .xor8, source: .iyl, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xae, cycles: 15, functionGroup: .xor8, displacement: true, source: .addressInIY_d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xb4, cycles: 4, functionGroup: .or8, source: .iyh, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xb5, cycles: 4, functionGroup: .or8, source: .iyl, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xb6, cycles: 15, functionGroup: .or8, displacement: true, source: .addressInIY_d, source2: .a, dest: .a),
            OpcodeInfo(opcode: 0xbc, cycles: 4, functionGroup: .cp8, source: .iyh, source2: .a, dest: .none),
            OpcodeInfo(opcode: 0xbd, cycles: 4, functionGroup: .cp8, source: .iyl, source2: .a, dest: .none),
            OpcodeInfo(opcode: 0xbe, cycles: 15, functionGroup: .cp8, displacement: true, source: .addressInIY_d, source2: .a, dest: .none),
            OpcodeInfo(opcode: 0xcb, cycles: 8, functionGroup: .fdcbPrefix, displacement: true, inhibitInterrupt: true),
            OpcodeInfo(opcode: 0xe1, cycles: 10, functionGroup: .load16, source: .addressInSPPostInc, dest: .iy),
            OpcodeInfo(opcode: 0xe3, cycles: 19, functionGroup: .load16, source: .iy, source2: .addressInSPPostInc, dest: .preDecAddressInSP, dest2: .iy),
            OpcodeInfo(opcode: 0xe5, cycles: 11, functionGroup: .load16, source: .iy, dest: .preDecAddressInSP),
            OpcodeInfo(opcode: 0xe9, cycles: 4, functionGroup: .load16, source: .iy, dest: .pc),
            OpcodeInfo(opcode: 0xf9, cycles: 6, functionGroup: .load16, source: .iy, dest: .sp),
        ])

    static let edGroup = Decoder(name: "edGroup", [
        // ED 0
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, 
        // ED 1
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        // ED 2
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        // ED 3
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        // ED 4
        OpcodeInfo(opcode: 0x40, cycles: 8, functionGroup: .inByte, source: .c, dest: .b),
        OpcodeInfo(opcode: 0x41, cycles: 8, functionGroup: .out, source: .b, source2: .c),
        OpcodeInfo(opcode: 0x42, cycles: 11, functionGroup: .sbc16, source: .bc, source2: .hl, dest: .hl),
        OpcodeInfo(opcode: 0x43, cycles: 16, functionGroup: .load16, source: .bc, dest: .address16),
        OpcodeInfo(opcode: 0x44, cycles: 4, functionGroup: .neg, source: .a, dest: .a),
        OpcodeInfo(opcode: 0x45, cycles: 10, functionGroup: .retn, source: .addressInSPPostInc, dest: .pc),
        placeHolder,
        OpcodeInfo(opcode: 0x47, cycles: 5, functionGroup: .load8, source: .a, dest: .i),
        OpcodeInfo(opcode: 0x48, cycles: 8, functionGroup: .inByte, source: .c, dest: .c),
        OpcodeInfo(opcode: 0x49, cycles: 8, functionGroup: .out, source: .c, source2: .c),
        OpcodeInfo(opcode: 0x4a, cycles: 11, functionGroup: .adc16, source: .bc, source2: .hl, dest: .hl),
        OpcodeInfo(opcode: 0x4b, cycles: 16, functionGroup: .load16, source: .address16, dest: .bc),
        placeHolder,
        OpcodeInfo(opcode: 0x4d, cycles: 10, functionGroup: .load16, source: .addressInSPPostInc, dest: .pc), // TODO: reti might need extra stuff
        placeHolder,
        OpcodeInfo(opcode: 0x4f, cycles: 5, functionGroup: .load8, source: .a, dest: .r),
        // ED 5
        OpcodeInfo(opcode: 0x50, cycles: 8, functionGroup: .inByte, source: .c, dest: .d),
        OpcodeInfo(opcode: 0x51, cycles: 8, functionGroup: .out, source: .d, source2: .c),
        OpcodeInfo(opcode: 0x52, cycles: 11, functionGroup: .sbc16, source: .de, source2: .hl, dest: .hl),
        OpcodeInfo(opcode: 0x53, cycles: 16, functionGroup: .load16, source: .de, dest: .address16),
        placeHolder,
        placeHolder,
        OpcodeInfo(opcode: 0x56, cycles: 4, functionGroup: .im1),
        OpcodeInfo(opcode: 0x57, cycles: 5, functionGroup: .load8Flags, source: .i, dest: .a),
        OpcodeInfo(opcode: 0x58, cycles: 8, functionGroup: .inByte, source: .c, dest: .e),
        OpcodeInfo(opcode: 0x59, cycles: 8, functionGroup: .out, source: .e, source2: .c),
        OpcodeInfo(opcode: 0x5a, cycles: 11, functionGroup: .adc16, source: .de, source2: .hl, dest: .hl),
        OpcodeInfo(opcode: 0x5b, cycles: 16, functionGroup: .load16, source: .address16, dest: .de),
        placeHolder, placeHolder, placeHolder,
        OpcodeInfo(opcode: 0x5f, cycles: 5, functionGroup: .load8Flags, source: .r, dest: .a),
        // ED 6
        OpcodeInfo(opcode: 0x60, cycles: 8, functionGroup: .inByte, source: .c, dest: .h),
        OpcodeInfo(opcode: 0x61, cycles: 8, functionGroup: .out, source: .h, source2: .c),
        OpcodeInfo(opcode: 0x62, cycles: 11, functionGroup: .sbc16, source: .hl, source2: .hl, dest: .hl),
        OpcodeInfo(opcode: 0x63, cycles: 16, functionGroup: .load16, source: .hl, dest: .address16),
        placeHolder, placeHolder, placeHolder,
        OpcodeInfo(opcode: 0x67, cycles: 14, functionGroup: .rrd, source: .addressInHL, source2: .a, dest: .addressInHL, dest2: .a),
        OpcodeInfo(opcode: 0x68, cycles: 8, functionGroup: .inByte, source: .c, dest: .l),
        OpcodeInfo(opcode: 0x69, cycles: 8, functionGroup: .out, source: .l, source2: .c),
        OpcodeInfo(opcode: 0x6a, cycles: 11, functionGroup: .adc16, source: .hl, source2: .hl, dest: .hl),
        OpcodeInfo(opcode: 0x6b, cycles: 16, functionGroup: .load16, source: .address16, dest: .hl),
        placeHolder, placeHolder, placeHolder,
        OpcodeInfo(opcode: 0x6f, cycles: 14, functionGroup: .rld, source: .addressInHL, source2: .a, dest: .addressInHL, dest2: .a),
        // ED 7
        OpcodeInfo(opcode: 0x70, cycles: 8, functionGroup: .inByte, source: .c, dest: .none),
        placeHolder,
        OpcodeInfo(opcode: 0x72, cycles: 11, functionGroup: .sbc16, source: .sp, source2: .hl, dest: .hl),
        OpcodeInfo(opcode: 0x73, cycles: 16, functionGroup: .load16, source: .sp, dest: .address16),
        placeHolder, placeHolder, placeHolder, placeHolder,
        OpcodeInfo(opcode: 0x78, cycles: 8, functionGroup: .inByte, source: .c, dest: .a),
        OpcodeInfo(opcode: 0x79, cycles: 8, functionGroup: .out, source: .a, source2: .c),
        OpcodeInfo(opcode: 0x7a, cycles: 11, functionGroup: .adc16, source: .sp, source2: .hl, dest: .hl),
        OpcodeInfo(opcode: 0x7b, cycles: 16, functionGroup: .load16, source: .address16, dest: .sp),
        placeHolder, placeHolder, placeHolder, placeHolder,
        // ED 8
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        // ED 9
        placeHolder,        placeHolder,

        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        // ED A
        OpcodeInfo(opcode: 0xa0, cycles: 12, functionGroup: .ldi),
        OpcodeInfo(opcode: 0xa1, cycles: 12, functionGroup: .cpi, source: .addressInHL),
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        OpcodeInfo(opcode: 0xa8, cycles: 12, functionGroup: .ldd),
        OpcodeInfo(opcode: 0xa9, cycles: 12, functionGroup: .cpd, source: .addressInHL),
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        // ED B
        OpcodeInfo(opcode: 0xb0, cycles: 12, functionGroup: .ldir),
        OpcodeInfo(opcode: 0xb1, cycles: 12, functionGroup: .cpir, source: .addressInHL),
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        OpcodeInfo(opcode: 0xb8, cycles: 12, functionGroup: .lddr),
        OpcodeInfo(opcode: 0xb9, cycles: 12, functionGroup: .cpdr, source: .addressInHL),
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        // ED C
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        // ED D
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        // ED E
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        // ED F
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder, placeHolder,
        // 0x100
        OpcodeInfo(opcode: stop, cycles: 1, functionGroup: .stop),
        OpcodeInfo(opcode: reset, cycles: 1, functionGroup: .stop)
    ])

    static let cbGroup = Decoder(name: "cbGroup",
    [
        // CB 0
        OpcodeInfo(opcode: 0x00, cycles: 4, functionGroup: .rlc, source: .b, dest: .b),
        OpcodeInfo(opcode: 0x01, cycles: 4, functionGroup: .rlc, source: .c, dest: .c),
        OpcodeInfo(opcode: 0x02, cycles: 4, functionGroup: .rlc, source: .d, dest: .d),
        OpcodeInfo(opcode: 0x03, cycles: 4, functionGroup: .rlc, source: .e, dest: .e),
        OpcodeInfo(opcode: 0x04, cycles: 4, functionGroup: .rlc, source: .h, dest: .h),
        OpcodeInfo(opcode: 0x05, cycles: 4, functionGroup: .rlc, source: .l, dest: .l),
        OpcodeInfo(opcode: 0x06, cycles: 11, functionGroup: .rlc, source: .addressInHL, dest: .addressInHL),
        OpcodeInfo(opcode: 0x07, cycles: 4, functionGroup: .rlc, source: .a, dest: .a),
        OpcodeInfo(opcode: 0x08, cycles: 4, functionGroup: .rrc, source: .b, dest: .b),
        OpcodeInfo(opcode: 0x09, cycles: 4, functionGroup: .rrc, source: .c, dest: .c),
        OpcodeInfo(opcode: 0x0a, cycles: 4, functionGroup: .rrc, source: .d, dest: .d),
        OpcodeInfo(opcode: 0x0b, cycles: 4, functionGroup: .rrc, source: .e, dest: .e),
        OpcodeInfo(opcode: 0x0c, cycles: 4, functionGroup: .rrc, source: .h, dest: .h),
        OpcodeInfo(opcode: 0x0d, cycles: 4, functionGroup: .rrc, source: .l, dest: .l),
        OpcodeInfo(opcode: 0x0e, cycles: 11, functionGroup: .rrc, source: .addressInHL, dest: .addressInHL),
        OpcodeInfo(opcode: 0x0f, cycles: 4, functionGroup: .rrc, source: .a, dest: .a),
        // CB 1
        OpcodeInfo(opcode: 0x10, cycles: 4, functionGroup: .rl, source: .b, dest: .b),
        OpcodeInfo(opcode: 0x11, cycles: 4, functionGroup: .rl, source: .c, dest: .c),
        OpcodeInfo(opcode: 0x12, cycles: 4, functionGroup: .rl, source: .d, dest: .d),
        OpcodeInfo(opcode: 0x13, cycles: 4, functionGroup: .rl, source: .e, dest: .e),
        OpcodeInfo(opcode: 0x14, cycles: 4, functionGroup: .rl, source: .h, dest: .h),
        OpcodeInfo(opcode: 0x15, cycles: 4, functionGroup: .rl, source: .l, dest: .l),
        OpcodeInfo(opcode: 0x16, cycles: 11, functionGroup: .rl, source: .addressInHL, dest: .addressInHL),
        OpcodeInfo(opcode: 0x17, cycles: 4, functionGroup: .rl, source: .a, dest: .a),
        OpcodeInfo(opcode: 0x18, cycles: 4, functionGroup: .rr, source: .b, dest: .b),
        OpcodeInfo(opcode: 0x19, cycles: 4, functionGroup: .rr, source: .c, dest: .c),
        OpcodeInfo(opcode: 0x1a, cycles: 4, functionGroup: .rr, source: .d, dest: .d),
        OpcodeInfo(opcode: 0x1b, cycles: 4, functionGroup: .rr, source: .e, dest: .e),
        OpcodeInfo(opcode: 0x1c, cycles: 4, functionGroup: .rr, source: .h, dest: .h),
        OpcodeInfo(opcode: 0x1d, cycles: 4, functionGroup: .rr, source: .l, dest: .l),
        OpcodeInfo(opcode: 0x1e, cycles: 11, functionGroup: .rr, source: .addressInHL, dest: .addressInHL),
        OpcodeInfo(opcode: 0x1f, cycles: 4, functionGroup: .rr, source: .a, dest: .a),
        // CB 2
        OpcodeInfo(opcode: 0x20, cycles: 4, functionGroup: .sla, source: .b, dest: .b),
        OpcodeInfo(opcode: 0x21, cycles: 4, functionGroup: .sla, source: .c, dest: .c),
        OpcodeInfo(opcode: 0x22, cycles: 4, functionGroup: .sla, source: .d, dest: .d),
        OpcodeInfo(opcode: 0x23, cycles: 4, functionGroup: .sla, source: .e, dest: .e),
        OpcodeInfo(opcode: 0x24, cycles: 4, functionGroup: .sla, source: .h, dest: .h),
        OpcodeInfo(opcode: 0x25, cycles: 4, functionGroup: .sla, source: .l, dest: .l),
        OpcodeInfo(opcode: 0x26, cycles: 11, functionGroup: .sla, source: .addressInHL, dest: .addressInHL),
        OpcodeInfo(opcode: 0x27, cycles: 4, functionGroup: .sla, source: .a, dest: .a),
        OpcodeInfo(opcode: 0x28, cycles: 4, functionGroup: .sra, source: .b, dest: .b),
        OpcodeInfo(opcode: 0x29, cycles: 4, functionGroup: .sra, source: .c, dest: .c),
        OpcodeInfo(opcode: 0x2a, cycles: 4, functionGroup: .sra, source: .d, dest: .d),
        OpcodeInfo(opcode: 0x2b, cycles: 4, functionGroup: .sra, source: .e, dest: .e),
        OpcodeInfo(opcode: 0x2c, cycles: 4, functionGroup: .sra, source: .h, dest: .h),
        OpcodeInfo(opcode: 0x2d, cycles: 4, functionGroup: .sra, source: .l, dest: .l),
        OpcodeInfo(opcode: 0x2e, cycles: 11, functionGroup: .sra, source: .addressInHL, dest: .addressInHL),
        OpcodeInfo(opcode: 0x2f, cycles: 4, functionGroup: .sra, source: .a, dest: .a),
        // CB 3
        OpcodeInfo(opcode: 0x30, cycles: 4, functionGroup: .sll, source: .b, dest: .b),
        OpcodeInfo(opcode: 0x31, cycles: 4, functionGroup: .sll, source: .c, dest: .c),
        OpcodeInfo(opcode: 0x32, cycles: 4, functionGroup: .sll, source: .d, dest: .d),
        OpcodeInfo(opcode: 0x33, cycles: 4, functionGroup: .sll, source: .e, dest: .e),
        OpcodeInfo(opcode: 0x34, cycles: 4, functionGroup: .sll, source: .h, dest: .h),
        OpcodeInfo(opcode: 0x35, cycles: 4, functionGroup: .sll, source: .l, dest: .l),
        OpcodeInfo(opcode: 0x36, cycles: 11, functionGroup: .sll, source: .addressInHL, dest: .addressInHL),
        OpcodeInfo(opcode: 0x37, cycles: 4, functionGroup: .sll, source: .a, dest: .a),
        OpcodeInfo(opcode: 0x38, cycles: 4, functionGroup: .srl, source: .b, dest: .b),
        OpcodeInfo(opcode: 0x39, cycles: 4, functionGroup: .srl, source: .c, dest: .c),
        OpcodeInfo(opcode: 0x3a, cycles: 4, functionGroup: .srl, source: .d, dest: .d),
        OpcodeInfo(opcode: 0x3b, cycles: 4, functionGroup: .srl, source: .e, dest: .e),
        OpcodeInfo(opcode: 0x3c, cycles: 4, functionGroup: .srl, source: .h, dest: .h),
        OpcodeInfo(opcode: 0x3d, cycles: 4, functionGroup: .srl, source: .l, dest: .l),
        OpcodeInfo(opcode: 0x3e, cycles: 11, functionGroup: .srl, source: .addressInHL, dest: .addressInHL),
        OpcodeInfo(opcode: 0x3f, cycles: 4, functionGroup: .srl, source: .a, dest: .a),
        // CB 4
        OpcodeInfo(opcode: 0x40, cycles: 4, functionGroup: .bitTest, source: .b, source2: .constant, constant: 1),
        OpcodeInfo(opcode: 0x41, cycles: 4, functionGroup: .bitTest, source: .c, source2: .constant, constant: 1),
        OpcodeInfo(opcode: 0x42, cycles: 4, functionGroup: .bitTest, source: .d, source2: .constant, constant: 1),
        OpcodeInfo(opcode: 0x43, cycles: 4, functionGroup: .bitTest, source: .e, source2: .constant, constant: 1),
        OpcodeInfo(opcode: 0x44, cycles: 4, functionGroup: .bitTest, source: .h, source2: .constant, constant: 1),
        OpcodeInfo(opcode: 0x45, cycles: 4, functionGroup: .bitTest, source: .l, source2: .constant, constant: 1),
        OpcodeInfo(opcode: 0x46, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInHL, source2: .constant, constant: 1),
        OpcodeInfo(opcode: 0x47, cycles: 4, functionGroup: .bitTest, source: .a, source2: .constant, constant: 1),
        OpcodeInfo(opcode: 0x48, cycles: 4, functionGroup: .bitTest, source: .b, source2: .constant, constant: 0b10),
        OpcodeInfo(opcode: 0x49, cycles: 4, functionGroup: .bitTest, source: .c, source2: .constant, constant: 0b10),
        OpcodeInfo(opcode: 0x4a, cycles: 4, functionGroup: .bitTest, source: .d, source2: .constant, constant: 0b10),
        OpcodeInfo(opcode: 0x4b, cycles: 4, functionGroup: .bitTest, source: .e, source2: .constant, constant: 0b10),
        OpcodeInfo(opcode: 0x4c, cycles: 4, functionGroup: .bitTest, source: .h, source2: .constant, constant: 0b10),
        OpcodeInfo(opcode: 0x4d, cycles: 4, functionGroup: .bitTest, source: .l, source2: .constant, constant: 0b10),
        OpcodeInfo(opcode: 0x4e, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInHL, source2: .constant, constant: 0b10),
        OpcodeInfo(opcode: 0x4f, cycles: 4, functionGroup: .bitTest, source: .a, source2: .constant, constant: 0b10),
        // CB 5
        OpcodeInfo(opcode: 0x50, cycles: 4, functionGroup: .bitTest, source: .b, source2: .constant, constant: 0b100),
        OpcodeInfo(opcode: 0x51, cycles: 4, functionGroup: .bitTest, source: .c, source2: .constant, constant: 0b100),
        OpcodeInfo(opcode: 0x52, cycles: 4, functionGroup: .bitTest, source: .d, source2: .constant, constant: 0b100),
        OpcodeInfo(opcode: 0x53, cycles: 4, functionGroup: .bitTest, source: .e, source2: .constant, constant: 0b100),
        OpcodeInfo(opcode: 0x54, cycles: 4, functionGroup: .bitTest, source: .h, source2: .constant, constant: 0b100),
        OpcodeInfo(opcode: 0x55, cycles: 4, functionGroup: .bitTest, source: .l, source2: .constant, constant: 0b100),
        OpcodeInfo(opcode: 0x56, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInHL, source2: .constant, constant: 0b100),
        OpcodeInfo(opcode: 0x57, cycles: 4, functionGroup: .bitTest, source: .a, source2: .constant, constant: 0b100),
        OpcodeInfo(opcode: 0x58, cycles: 4, functionGroup: .bitTest, source: .b, source2: .constant, constant: 0b1000),
        OpcodeInfo(opcode: 0x59, cycles: 4, functionGroup: .bitTest, source: .c, source2: .constant, constant: 0b1000),
        OpcodeInfo(opcode: 0x5a, cycles: 4, functionGroup: .bitTest, source: .d, source2: .constant, constant: 0b1000),
        OpcodeInfo(opcode: 0x5b, cycles: 4, functionGroup: .bitTest, source: .e, source2: .constant, constant: 0b1000),
        OpcodeInfo(opcode: 0x5c, cycles: 4, functionGroup: .bitTest, source: .h, source2: .constant, constant: 0b1000),
        OpcodeInfo(opcode: 0x5d, cycles: 4, functionGroup: .bitTest, source: .l, source2: .constant, constant: 0b1000),
        OpcodeInfo(opcode: 0x5e, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInHL, source2: .constant, constant: 0b1000),
        OpcodeInfo(opcode: 0x5f, cycles: 4, functionGroup: .bitTest, source: .a, source2: .constant, constant: 0b1000),
        // CB 6
        OpcodeInfo(opcode: 0x60, cycles: 4, functionGroup: .bitTest, source: .b, source2: .constant, constant: 0b10000),
        OpcodeInfo(opcode: 0x61, cycles: 4, functionGroup: .bitTest, source: .c, source2: .constant, constant: 0b10000),
        OpcodeInfo(opcode: 0x62, cycles: 4, functionGroup: .bitTest, source: .d, source2: .constant, constant: 0b10000),
        OpcodeInfo(opcode: 0x63, cycles: 4, functionGroup: .bitTest, source: .e, source2: .constant, constant: 0b10000),
        OpcodeInfo(opcode: 0x64, cycles: 4, functionGroup: .bitTest, source: .h, source2: .constant, constant: 0b10000),
        OpcodeInfo(opcode: 0x65, cycles: 4, functionGroup: .bitTest, source: .l, source2: .constant, constant: 0b10000),
        OpcodeInfo(opcode: 0x66, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInHL, source2: .constant, constant: 0b10000),
        OpcodeInfo(opcode: 0x67, cycles: 4, functionGroup: .bitTest, source: .a, source2: .constant, constant: 0b10000),
        OpcodeInfo(opcode: 0x68, cycles: 4, functionGroup: .bitTest, source: .b, source2: .constant, constant: 0b100000),
        OpcodeInfo(opcode: 0x69, cycles: 4, functionGroup: .bitTest, source: .c, source2: .constant, constant: 0b100000),
        OpcodeInfo(opcode: 0x6a, cycles: 4, functionGroup: .bitTest, source: .d, source2: .constant, constant: 0b100000),
        OpcodeInfo(opcode: 0x6b, cycles: 4, functionGroup: .bitTest, source: .e, source2: .constant, constant: 0b100000),
        OpcodeInfo(opcode: 0x6c, cycles: 4, functionGroup: .bitTest, source: .h, source2: .constant, constant: 0b100000),
        OpcodeInfo(opcode: 0x6d, cycles: 4, functionGroup: .bitTest, source: .l, source2: .constant, constant: 0b100000),
        OpcodeInfo(opcode: 0x6e, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInHL, source2: .constant, constant: 0b100000),
        OpcodeInfo(opcode: 0x6f, cycles: 4, functionGroup: .bitTest, source: .a, source2: .constant, constant: 0b100000),
        // CB 7
        OpcodeInfo(opcode: 0x70, cycles: 4, functionGroup: .bitTest, source: .b, source2: .constant, constant: 0b1000000),
        OpcodeInfo(opcode: 0x71, cycles: 4, functionGroup: .bitTest, source: .c, source2: .constant, constant: 0b1000000),
        OpcodeInfo(opcode: 0x72, cycles: 4, functionGroup: .bitTest, source: .d, source2: .constant, constant: 0b1000000),
        OpcodeInfo(opcode: 0x73, cycles: 4, functionGroup: .bitTest, source: .e, source2: .constant, constant: 0b1000000),
        OpcodeInfo(opcode: 0x74, cycles: 4, functionGroup: .bitTest, source: .h, source2: .constant, constant: 0b1000000),
        OpcodeInfo(opcode: 0x75, cycles: 4, functionGroup: .bitTest, source: .l, source2: .constant, constant: 0b1000000),
        OpcodeInfo(opcode: 0x76, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInHL, source2: .constant, constant: 0b1000000),
        OpcodeInfo(opcode: 0x77, cycles: 4, functionGroup: .bitTest, source: .a, source2: .constant, constant: 0b1000000),
        OpcodeInfo(opcode: 0x78, cycles: 4, functionGroup: .bitTest, source: .b, source2: .constant, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0x79, cycles: 4, functionGroup: .bitTest, source: .c, source2: .constant, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0x7a, cycles: 4, functionGroup: .bitTest, source: .d, source2: .constant, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0x7b, cycles: 4, functionGroup: .bitTest, source: .e, source2: .constant, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0x7c, cycles: 4, functionGroup: .bitTest, source: .h, source2: .constant, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0x7d, cycles: 4, functionGroup: .bitTest, source: .l, source2: .constant, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0x7e, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInHL, source2: .constant, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0x7f, cycles: 4, functionGroup: .bitTest, source: .a, source2: .constant, constant: 0b1000_0000),
        // CB 8
        OpcodeInfo(opcode: 0x80, cycles: 4, functionGroup: .resetBit, source: .b, source2: .constant, dest: .b, constant: 0b1),
        OpcodeInfo(opcode: 0x81, cycles: 4, functionGroup: .resetBit, source: .c, source2: .constant, dest: .c, constant: 0b1),
        OpcodeInfo(opcode: 0x82, cycles: 4, functionGroup: .resetBit, source: .d, source2: .constant, dest: .d, constant: 0b1),
        OpcodeInfo(opcode: 0x83, cycles: 4, functionGroup: .resetBit, source: .e, source2: .constant, dest: .e, constant: 0b1),
        OpcodeInfo(opcode: 0x84, cycles: 4, functionGroup: .resetBit, source: .h, source2: .constant, dest: .h, constant: 0b1),
        OpcodeInfo(opcode: 0x85, cycles: 4, functionGroup: .resetBit, source: .l, source2: .constant, dest: .l, constant: 0b1),
        OpcodeInfo(opcode: 0x86, cycles: 11, functionGroup: .resetBit, source: .addressInHL, source2: .constant, dest: .addressInHL, constant: 0b1),
        OpcodeInfo(opcode: 0x87, cycles: 4, functionGroup: .resetBit, source: .a, source2: .constant, dest: .a, constant: 0b1),
        OpcodeInfo(opcode: 0x88, cycles: 4, functionGroup: .resetBit, source: .b, source2: .constant, dest: .b, constant: 0b10),
        OpcodeInfo(opcode: 0x89, cycles: 4, functionGroup: .resetBit, source: .c, source2: .constant, dest: .c, constant: 0b10),
        OpcodeInfo(opcode: 0x8a, cycles: 4, functionGroup: .resetBit, source: .d, source2: .constant, dest: .d, constant: 0b10),
        OpcodeInfo(opcode: 0x8b, cycles: 4, functionGroup: .resetBit, source: .e, source2: .constant, dest: .e, constant: 0b10),
        OpcodeInfo(opcode: 0x8c, cycles: 4, functionGroup: .resetBit, source: .h, source2: .constant, dest: .h, constant: 0b10),
        OpcodeInfo(opcode: 0x8d, cycles: 4, functionGroup: .resetBit, source: .l, source2: .constant, dest: .l, constant: 0b10),
        OpcodeInfo(opcode: 0x8e, cycles: 11, functionGroup: .resetBit, source: .addressInHL, source2: .constant, dest: .addressInHL, constant: 0b10),
        OpcodeInfo(opcode: 0x8f, cycles: 4, functionGroup: .resetBit, source: .a, source2: .constant, dest: .a, constant: 0b10),
        // CB 9
        OpcodeInfo(opcode: 0x90, cycles: 4, functionGroup: .resetBit, source: .b, source2: .constant, dest: .b, constant: 0b100),
        OpcodeInfo(opcode: 0x91, cycles: 4, functionGroup: .resetBit, source: .c, source2: .constant, dest: .c, constant: 0b100),
        OpcodeInfo(opcode: 0x92, cycles: 4, functionGroup: .resetBit, source: .d, source2: .constant, dest: .d, constant: 0b100),
        OpcodeInfo(opcode: 0x93, cycles: 4, functionGroup: .resetBit, source: .e, source2: .constant, dest: .e, constant: 0b100),
        OpcodeInfo(opcode: 0x94, cycles: 4, functionGroup: .resetBit, source: .h, source2: .constant, dest: .h, constant: 0b100),
        OpcodeInfo(opcode: 0x95, cycles: 4, functionGroup: .resetBit, source: .l, source2: .constant, dest: .l, constant: 0b100),
        OpcodeInfo(opcode: 0x96, cycles: 11, functionGroup: .resetBit, source: .addressInHL, source2: .constant, dest: .addressInHL, constant: 0b100),
        OpcodeInfo(opcode: 0x97, cycles: 4, functionGroup: .resetBit, source: .a, source2: .constant, dest: .a, constant: 0b100),
        OpcodeInfo(opcode: 0x98, cycles: 4, functionGroup: .resetBit, source: .b, source2: .constant, dest: .b, constant: 0b1000),
        OpcodeInfo(opcode: 0x99, cycles: 4, functionGroup: .resetBit, source: .c, source2: .constant, dest: .c, constant: 0b1000),
        OpcodeInfo(opcode: 0x9a, cycles: 4, functionGroup: .resetBit, source: .d, source2: .constant, dest: .d, constant: 0b1000),
        OpcodeInfo(opcode: 0x9b, cycles: 4, functionGroup: .resetBit, source: .e, source2: .constant, dest: .e, constant: 0b1000),
        OpcodeInfo(opcode: 0x9c, cycles: 4, functionGroup: .resetBit, source: .h, source2: .constant, dest: .h, constant: 0b1000),
        OpcodeInfo(opcode: 0x9d, cycles: 4, functionGroup: .resetBit, source: .l, source2: .constant, dest: .l, constant: 0b1000),
        OpcodeInfo(opcode: 0x9e, cycles: 11, functionGroup: .resetBit, source: .addressInHL, source2: .constant, dest: .addressInHL, constant: 0b1000),
        OpcodeInfo(opcode: 0x9f, cycles: 4, functionGroup: .resetBit, source: .a, source2: .constant, dest: .a, constant: 0b1000),
        // CB A
        OpcodeInfo(opcode: 0xa0, cycles: 4, functionGroup: .resetBit, source: .b, source2: .constant, dest: .b, constant: 0b1_0000),
        OpcodeInfo(opcode: 0xa1, cycles: 4, functionGroup: .resetBit, source: .c, source2: .constant, dest: .c, constant: 0b1_0000),
        OpcodeInfo(opcode: 0xa2, cycles: 4, functionGroup: .resetBit, source: .d, source2: .constant, dest: .d, constant: 0b1_0000),
        OpcodeInfo(opcode: 0xa3, cycles: 4, functionGroup: .resetBit, source: .e, source2: .constant, dest: .e, constant: 0b1_0000),
        OpcodeInfo(opcode: 0xa4, cycles: 4, functionGroup: .resetBit, source: .h, source2: .constant, dest: .h, constant: 0b1_0000),
        OpcodeInfo(opcode: 0xa5, cycles: 4, functionGroup: .resetBit, source: .l, source2: .constant, dest: .l, constant: 0b1_0000),
        OpcodeInfo(opcode: 0xa6, cycles: 11, functionGroup: .resetBit, source: .addressInHL, source2: .constant, dest: .addressInHL, constant: 0b1_0000),
        OpcodeInfo(opcode: 0xa7, cycles: 4, functionGroup: .resetBit, source: .a, source2: .constant, dest: .a, constant: 0b1_0000),
        OpcodeInfo(opcode: 0xa8, cycles: 4, functionGroup: .resetBit, source: .b, source2: .constant, dest: .b, constant: 0b10_0000),
        OpcodeInfo(opcode: 0xa9, cycles: 4, functionGroup: .resetBit, source: .c, source2: .constant, dest: .c, constant: 0b10_0000),
        OpcodeInfo(opcode: 0xaa, cycles: 4, functionGroup: .resetBit, source: .d, source2: .constant, dest: .d, constant: 0b10_0000),
        OpcodeInfo(opcode: 0xab, cycles: 4, functionGroup: .resetBit, source: .e, source2: .constant, dest: .e, constant: 0b10_0000),
        OpcodeInfo(opcode: 0xac, cycles: 4, functionGroup: .resetBit, source: .h, source2: .constant, dest: .h, constant: 0b10_0000),
        OpcodeInfo(opcode: 0xad, cycles: 4, functionGroup: .resetBit, source: .l, source2: .constant, dest: .l, constant: 0b10_0000),
        OpcodeInfo(opcode: 0xae, cycles: 11, functionGroup: .resetBit, source: .addressInHL, source2: .constant, dest: .addressInHL, constant: 0b10_0000),
        OpcodeInfo(opcode: 0xaf, cycles: 4, functionGroup: .resetBit, source: .a, source2: .constant, dest: .a, constant: 0b10_0000),
        // CB B
        OpcodeInfo(opcode: 0xb0, cycles: 4, functionGroup: .resetBit, source: .b, source2: .constant, dest: .b, constant: 0b100_0000),
        OpcodeInfo(opcode: 0xb1, cycles: 4, functionGroup: .resetBit, source: .c, source2: .constant, dest: .c, constant: 0b100_0000),
        OpcodeInfo(opcode: 0xb2, cycles: 4, functionGroup: .resetBit, source: .d, source2: .constant, dest: .d, constant: 0b100_0000),
        OpcodeInfo(opcode: 0xb3, cycles: 4, functionGroup: .resetBit, source: .e, source2: .constant, dest: .e, constant: 0b100_0000),
        OpcodeInfo(opcode: 0xb4, cycles: 4, functionGroup: .resetBit, source: .h, source2: .constant, dest: .h, constant: 0b100_0000),
        OpcodeInfo(opcode: 0xb5, cycles: 4, functionGroup: .resetBit, source: .l, source2: .constant, dest: .l, constant: 0b100_0000),
        OpcodeInfo(opcode: 0xb6, cycles: 11, functionGroup: .resetBit, source: .addressInHL, source2: .constant, dest: .addressInHL, constant: 0b100_0000),
        OpcodeInfo(opcode: 0xb7, cycles: 4, functionGroup: .resetBit, source: .a, source2: .constant, dest: .a, constant: 0b100_0000),
        OpcodeInfo(opcode: 0xb8, cycles: 4, functionGroup: .resetBit, source: .b, source2: .constant, dest: .b, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0xb9, cycles: 4, functionGroup: .resetBit, source: .c, source2: .constant, dest: .c, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0xba, cycles: 4, functionGroup: .resetBit, source: .d, source2: .constant, dest: .d, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0xbb, cycles: 4, functionGroup: .resetBit, source: .e, source2: .constant, dest: .e, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0xbc, cycles: 4, functionGroup: .resetBit, source: .h, source2: .constant, dest: .h, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0xbd, cycles: 4, functionGroup: .resetBit, source: .l, source2: .constant, dest: .l, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0xbe, cycles: 11, functionGroup: .resetBit, source: .addressInHL, source2: .constant, dest: .addressInHL, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0xbf, cycles: 4, functionGroup: .resetBit, source: .a, source2: .constant, dest: .a, constant: 0b1000_0000),
        // CB C
        OpcodeInfo(opcode: 0xc0, cycles: 4, functionGroup: .setBit, source: .b, source2: .constant, dest: .b, constant: 0b1),
        OpcodeInfo(opcode: 0xc1, cycles: 4, functionGroup: .setBit, source: .c, source2: .constant, dest: .c, constant: 0b1),
        OpcodeInfo(opcode: 0xc2, cycles: 4, functionGroup: .setBit, source: .d, source2: .constant, dest: .d, constant: 0b1),
        OpcodeInfo(opcode: 0xc3, cycles: 4, functionGroup: .setBit, source: .e, source2: .constant, dest: .e, constant: 0b1),
        OpcodeInfo(opcode: 0xc4, cycles: 4, functionGroup: .setBit, source: .h, source2: .constant, dest: .h, constant: 0b1),
        OpcodeInfo(opcode: 0xc5, cycles: 4, functionGroup: .setBit, source: .l, source2: .constant, dest: .l, constant: 0b1),
        OpcodeInfo(opcode: 0xc6, cycles: 11, functionGroup: .setBit, source: .addressInHL, source2: .constant, dest: .addressInHL, constant: 0b1),
        OpcodeInfo(opcode: 0xc7, cycles: 4, functionGroup: .setBit, source: .a, source2: .constant, dest: .a, constant: 0b1),
        OpcodeInfo(opcode: 0xc8, cycles: 4, functionGroup: .setBit, source: .b, source2: .constant, dest: .b, constant: 0b10),
        OpcodeInfo(opcode: 0xc9, cycles: 4, functionGroup: .setBit, source: .c, source2: .constant, dest: .c, constant: 0b10),
        OpcodeInfo(opcode: 0xca, cycles: 4, functionGroup: .setBit, source: .d, source2: .constant, dest: .d, constant: 0b10),
        OpcodeInfo(opcode: 0xcb, cycles: 4, functionGroup: .setBit, source: .e, source2: .constant, dest: .e, constant: 0b10),
        OpcodeInfo(opcode: 0xcc, cycles: 4, functionGroup: .setBit, source: .h, source2: .constant, dest: .h, constant: 0b10),
        OpcodeInfo(opcode: 0xcd, cycles: 4, functionGroup: .setBit, source: .l, source2: .constant, dest: .l, constant: 0b10),
        OpcodeInfo(opcode: 0xce, cycles: 11, functionGroup: .setBit, source: .addressInHL, source2: .constant, dest: .addressInHL, constant: 0b10),
        OpcodeInfo(opcode: 0xcf, cycles: 4, functionGroup: .setBit, source: .a, source2: .constant, dest: .a, constant: 0b10),
        // CB D
        OpcodeInfo(opcode: 0xd0, cycles: 4, functionGroup: .setBit, source: .b, source2: .constant, dest: .b, constant: 0b100),
        OpcodeInfo(opcode: 0xd1, cycles: 4, functionGroup: .setBit, source: .c, source2: .constant, dest: .c, constant: 0b100),
        OpcodeInfo(opcode: 0xd2, cycles: 4, functionGroup: .setBit, source: .d, source2: .constant, dest: .d, constant: 0b100),
        OpcodeInfo(opcode: 0xd3, cycles: 4, functionGroup: .setBit, source: .e, source2: .constant, dest: .e, constant: 0b100),
        OpcodeInfo(opcode: 0xd4, cycles: 4, functionGroup: .setBit, source: .h, source2: .constant, dest: .h, constant: 0b100),
        OpcodeInfo(opcode: 0xd5, cycles: 4, functionGroup: .setBit, source: .l, source2: .constant, dest: .l, constant: 0b100),
        OpcodeInfo(opcode: 0xd6, cycles: 11, functionGroup: .setBit, source: .addressInHL, source2: .constant, dest: .addressInHL, constant: 0b100),
        OpcodeInfo(opcode: 0xd7, cycles: 4, functionGroup: .setBit, source: .a, source2: .constant, dest: .a, constant: 0b100),
        OpcodeInfo(opcode: 0xd8, cycles: 4, functionGroup: .setBit, source: .b, source2: .constant, dest: .b, constant: 0b1000),
        OpcodeInfo(opcode: 0xd9, cycles: 4, functionGroup: .setBit, source: .c, source2: .constant, dest: .c, constant: 0b1000),
        OpcodeInfo(opcode: 0xda, cycles: 4, functionGroup: .setBit, source: .d, source2: .constant, dest: .d, constant: 0b1000),
        OpcodeInfo(opcode: 0xdb, cycles: 4, functionGroup: .setBit, source: .e, source2: .constant, dest: .e, constant: 0b1000),
        OpcodeInfo(opcode: 0xdc, cycles: 4, functionGroup: .setBit, source: .h, source2: .constant, dest: .h, constant: 0b1000),
        OpcodeInfo(opcode: 0xdd, cycles: 4, functionGroup: .setBit, source: .l, source2: .constant, dest: .l, constant: 0b1000),
        OpcodeInfo(opcode: 0xde, cycles: 11, functionGroup: .setBit, source: .addressInHL, source2: .constant, dest: .addressInHL, constant: 0b1000),
        OpcodeInfo(opcode: 0xdf, cycles: 4, functionGroup: .setBit, source: .a, source2: .constant, dest: .a, constant: 0b1000),
        // CB E
        OpcodeInfo(opcode: 0xe0, cycles: 4, functionGroup: .setBit, source: .b, source2: .constant, dest: .b, constant: 0b1_0000),
        OpcodeInfo(opcode: 0xe1, cycles: 4, functionGroup: .setBit, source: .c, source2: .constant, dest: .c, constant: 0b1_0000),
        OpcodeInfo(opcode: 0xe2, cycles: 4, functionGroup: .setBit, source: .d, source2: .constant, dest: .d, constant: 0b1_0000),
        OpcodeInfo(opcode: 0xe3, cycles: 4, functionGroup: .setBit, source: .e, source2: .constant, dest: .e, constant: 0b1_0000),
        OpcodeInfo(opcode: 0xe4, cycles: 4, functionGroup: .setBit, source: .h, source2: .constant, dest: .h, constant: 0b1_0000),
        OpcodeInfo(opcode: 0xe5, cycles: 4, functionGroup: .setBit, source: .l, source2: .constant, dest: .l, constant: 0b1_0000),
        OpcodeInfo(opcode: 0xe6, cycles: 11, functionGroup: .setBit, source: .addressInHL, source2: .constant, dest: .addressInHL, constant: 0b1_0000),
        OpcodeInfo(opcode: 0xe7, cycles: 4, functionGroup: .setBit, source: .a, source2: .constant, dest: .a, constant: 0b1_0000),
        OpcodeInfo(opcode: 0xe8, cycles: 4, functionGroup: .setBit, source: .b, source2: .constant, dest: .b, constant: 0b10_0000),
        OpcodeInfo(opcode: 0xe9, cycles: 4, functionGroup: .setBit, source: .c, source2: .constant, dest: .c, constant: 0b10_0000),
        OpcodeInfo(opcode: 0xea, cycles: 4, functionGroup: .setBit, source: .d, source2: .constant, dest: .d, constant: 0b10_0000),
        OpcodeInfo(opcode: 0xeb, cycles: 4, functionGroup: .setBit, source: .e, source2: .constant, dest: .e, constant: 0b10_0000),
        OpcodeInfo(opcode: 0xec, cycles: 4, functionGroup: .setBit, source: .h, source2: .constant, dest: .h, constant: 0b10_0000),
        OpcodeInfo(opcode: 0xed, cycles: 4, functionGroup: .setBit, source: .l, source2: .constant, dest: .l, constant: 0b10_0000),
        OpcodeInfo(opcode: 0xee, cycles: 11, functionGroup: .setBit, source: .addressInHL, source2: .constant, dest: .addressInHL, constant: 0b10_0000),
        OpcodeInfo(opcode: 0xef, cycles: 4, functionGroup: .setBit, source: .a, source2: .constant, dest: .a, constant: 0b10_0000),
        // CB F
        OpcodeInfo(opcode: 0xf0, cycles: 4, functionGroup: .setBit, source: .b, source2: .constant, dest: .b, constant: 0b100_0000),
        OpcodeInfo(opcode: 0xf1, cycles: 4, functionGroup: .setBit, source: .c, source2: .constant, dest: .c, constant: 0b100_0000),
        OpcodeInfo(opcode: 0xf2, cycles: 4, functionGroup: .setBit, source: .d, source2: .constant, dest: .d, constant: 0b100_0000),
        OpcodeInfo(opcode: 0xf3, cycles: 4, functionGroup: .setBit, source: .e, source2: .constant, dest: .e, constant: 0b100_0000),
        OpcodeInfo(opcode: 0xf4, cycles: 4, functionGroup: .setBit, source: .h, source2: .constant, dest: .h, constant: 0b100_0000),
        OpcodeInfo(opcode: 0xf5, cycles: 4, functionGroup: .setBit, source: .l, source2: .constant, dest: .l, constant: 0b100_0000),
        OpcodeInfo(opcode: 0xf6, cycles: 11, functionGroup: .setBit, source: .addressInHL, source2: .constant, dest: .addressInHL, constant: 0b100_0000),
        OpcodeInfo(opcode: 0xf7, cycles: 4, functionGroup: .setBit, source: .a, source2: .constant, dest: .a, constant: 0b100_0000),
        OpcodeInfo(opcode: 0xf8, cycles: 4, functionGroup: .setBit, source: .b, source2: .constant, dest: .b, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0xf9, cycles: 4, functionGroup: .setBit, source: .c, source2: .constant, dest: .c, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0xfa, cycles: 4, functionGroup: .setBit, source: .d, source2: .constant, dest: .d, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0xfb, cycles: 4, functionGroup: .setBit, source: .e, source2: .constant, dest: .e, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0xfc, cycles: 4, functionGroup: .setBit, source: .h, source2: .constant, dest: .h, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0xfd, cycles: 4, functionGroup: .setBit, source: .l, source2: .constant, dest: .l, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0xfe, cycles: 11, functionGroup: .setBit, source: .addressInHL, source2: .constant, dest: .addressInHL, constant: 0b1000_0000),
        OpcodeInfo(opcode: 0xff, cycles: 4, functionGroup: .setBit, source: .a, source2: .constant, dest: .a, constant: 0b1000_0000),

        // 0x100
        OpcodeInfo(opcode: stop, cycles: 1, functionGroup: .stop),
        OpcodeInfo(opcode: reset, cycles: 1, functionGroup: .stop)

    ])

    static let ddcbGroup = Decoder(name: "ddcbGroup", base: cbGroup, diffs:
        [
            OpcodeInfo(opcode: 0x06, cycles: 11, functionGroup: .rlc, source: .addressInIX_d, dest: .addressInIX_d),
            OpcodeInfo(opcode: 0x0e, cycles: 11, functionGroup: .rrc, source: .addressInIX_d, dest: .addressInIX_d),
            OpcodeInfo(opcode: 0x16, cycles: 11, functionGroup: .rl, source: .addressInIX_d, dest: .addressInIX_d),
            OpcodeInfo(opcode: 0x1e, cycles: 11, functionGroup: .rr, source: .addressInIX_d, dest: .addressInIX_d),
            OpcodeInfo(opcode: 0x26, cycles: 11, functionGroup: .sla, source: .addressInIX_d, dest: .addressInIX_d),
            OpcodeInfo(opcode: 0x2e, cycles: 11, functionGroup: .sra, source: .addressInIX_d, dest: .addressInIX_d),
            OpcodeInfo(opcode: 0x36, cycles: 11, functionGroup: .sll, source: .addressInIX_d, dest: .addressInIX_d),
            OpcodeInfo(opcode: 0x3e, cycles: 11, functionGroup: .srl, source: .addressInIX_d, dest: .addressInIX_d),
           	OpcodeInfo(opcode: 0x46, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInIX_d, source2: .constant, constant: 0b1),
            OpcodeInfo(opcode: 0x4e, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInIX_d, source2: .constant, constant: 0b10),
            OpcodeInfo(opcode: 0x56, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInIX_d, source2: .constant, constant: 0b100),
            OpcodeInfo(opcode: 0x5e, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInIX_d, source2: .constant, constant: 0b1000),
            OpcodeInfo(opcode: 0x66, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInIX_d, source2: .constant, constant: 0b1_0000),
            OpcodeInfo(opcode: 0x6e, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInIX_d, source2: .constant, constant: 0b10_0000),
            OpcodeInfo(opcode: 0x76, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInIX_d, source2: .constant, constant: 0b100_0000),
            OpcodeInfo(opcode: 0x7e, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInIX_d, source2: .constant, constant: 0b1000_0000),
            OpcodeInfo(opcode: 0x86, cycles: 11, functionGroup: .resetBit, source: .addressInIX_d, source2: .constant, dest: .addressInIX_d, constant: 0b1),
            OpcodeInfo(opcode: 0x8e, cycles: 11, functionGroup: .resetBit, source: .addressInIX_d, source2: .constant, dest: .addressInIX_d, constant: 0b10),
            OpcodeInfo(opcode: 0x96, cycles: 11, functionGroup: .resetBit, source: .addressInIX_d, source2: .constant, dest: .addressInIX_d, constant: 0b100),
            OpcodeInfo(opcode: 0x9e, cycles: 11, functionGroup: .resetBit, source: .addressInIX_d, source2: .constant, dest: .addressInIX_d, constant: 0b1000),
            OpcodeInfo(opcode: 0xa6, cycles: 11, functionGroup: .resetBit, source: .addressInIX_d, source2: .constant, dest: .addressInIX_d, constant: 0b1_0000),
            OpcodeInfo(opcode: 0xae, cycles: 11, functionGroup: .resetBit, source: .addressInIX_d, source2: .constant, dest: .addressInIX_d, constant: 0b10_0000),
            OpcodeInfo(opcode: 0xb6, cycles: 11, functionGroup: .resetBit, source: .addressInIX_d, source2: .constant, dest: .addressInIX_d, constant: 0b100_0000),
            OpcodeInfo(opcode: 0xbe, cycles: 11, functionGroup: .resetBit, source: .addressInIX_d, source2: .constant, dest: .addressInIX_d, constant: 0b1000_0000),
            OpcodeInfo(opcode: 0xc6, cycles: 11, functionGroup: .setBit, source: .addressInIX_d, source2: .constant, dest: .addressInIX_d, constant: 0b1),
            OpcodeInfo(opcode: 0xce, cycles: 11, functionGroup: .setBit, source: .addressInIX_d, source2: .constant, dest: .addressInIX_d, constant: 0b10),
            OpcodeInfo(opcode: 0xd6, cycles: 11, functionGroup: .setBit, source: .addressInIX_d, source2: .constant, dest: .addressInIX_d, constant: 0b100),
            OpcodeInfo(opcode: 0xde, cycles: 11, functionGroup: .setBit, source: .addressInIX_d, source2: .constant, dest: .addressInIX_d, constant: 0b1000),
            OpcodeInfo(opcode: 0xe6, cycles: 11, functionGroup: .setBit, source: .addressInIX_d, source2: .constant, dest: .addressInIX_d, constant: 0b1_0000),
            OpcodeInfo(opcode: 0xee, cycles: 11, functionGroup: .setBit, source: .addressInIX_d, source2: .constant, dest: .addressInIX_d, constant: 0b10_0000),
            OpcodeInfo(opcode: 0xf6, cycles: 11, functionGroup: .setBit, source: .addressInIX_d, source2: .constant, dest: .addressInIX_d, constant: 0b100_0000),
            OpcodeInfo(opcode: 0xfe, cycles: 11, functionGroup: .setBit, source: .addressInIX_d, source2: .constant, dest: .addressInIX_d, constant: 0b1000_0000),
        ])

    static let fdcbGroup = Decoder(name: "fdcbGroup", base: cbGroup, diffs:
        [
            OpcodeInfo(opcode: 0x06, cycles: 11, functionGroup: .rlc, source: .addressInIY_d, dest: .addressInIY_d),
            OpcodeInfo(opcode: 0x0e, cycles: 11, functionGroup: .rrc, source: .addressInIY_d, dest: .addressInIY_d),
            OpcodeInfo(opcode: 0x16, cycles: 11, functionGroup: .rl, source: .addressInIY_d, dest: .addressInIY_d),
            OpcodeInfo(opcode: 0x1e, cycles: 11, functionGroup: .rr, source: .addressInIY_d, dest: .addressInIY_d),
            OpcodeInfo(opcode: 0x26, cycles: 11, functionGroup: .sla, source: .addressInIY_d, dest: .addressInIY_d),
            OpcodeInfo(opcode: 0x2e, cycles: 11, functionGroup: .sra, source: .addressInIY_d, dest: .addressInIY_d),
            OpcodeInfo(opcode: 0x36, cycles: 11, functionGroup: .sll, source: .addressInIY_d, dest: .addressInIY_d),
            OpcodeInfo(opcode: 0x3e, cycles: 11, functionGroup: .srl, source: .addressInIY_d, dest: .addressInIY_d),
           	OpcodeInfo(opcode: 0x46, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInIY_d, source2: .constant, constant: 0b1),
            OpcodeInfo(opcode: 0x4e, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInIY_d, source2: .constant, constant: 0b10),
            OpcodeInfo(opcode: 0x56, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInIY_d, source2: .constant, constant: 0b100),
            OpcodeInfo(opcode: 0x5e, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInIY_d, source2: .constant, constant: 0b1000),
            OpcodeInfo(opcode: 0x66, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInIY_d, source2: .constant, constant: 0b1_0000),
            OpcodeInfo(opcode: 0x6e, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInIY_d, source2: .constant, constant: 0b10_0000),
            OpcodeInfo(opcode: 0x76, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInIY_d, source2: .constant, constant: 0b100_0000),
            OpcodeInfo(opcode: 0x7e, cycles: 8, functionGroup: .bitTestIndirect, source: .addressInIY_d, source2: .constant, constant: 0b1000_0000),
            OpcodeInfo(opcode: 0x86, cycles: 11, functionGroup: .resetBit, source: .addressInIY_d, source2: .constant, dest: .addressInIY_d, constant: 0b1),
            OpcodeInfo(opcode: 0x8e, cycles: 11, functionGroup: .resetBit, source: .addressInIY_d, source2: .constant, dest: .addressInIY_d, constant: 0b10),
            OpcodeInfo(opcode: 0x96, cycles: 11, functionGroup: .resetBit, source: .addressInIY_d, source2: .constant, dest: .addressInIY_d, constant: 0b100),
            OpcodeInfo(opcode: 0x9e, cycles: 11, functionGroup: .resetBit, source: .addressInIY_d, source2: .constant, dest: .addressInIY_d, constant: 0b1000),
            OpcodeInfo(opcode: 0xa6, cycles: 11, functionGroup: .resetBit, source: .addressInIY_d, source2: .constant, dest: .addressInIY_d, constant: 0b1_0000),
            OpcodeInfo(opcode: 0xae, cycles: 11, functionGroup: .resetBit, source: .addressInIY_d, source2: .constant, dest: .addressInIY_d, constant: 0b10_0000),
            OpcodeInfo(opcode: 0xb6, cycles: 11, functionGroup: .resetBit, source: .addressInIY_d, source2: .constant, dest: .addressInIY_d, constant: 0b100_0000),
            OpcodeInfo(opcode: 0xbe, cycles: 11, functionGroup: .resetBit, source: .addressInIY_d, source2: .constant, dest: .addressInIY_d, constant: 0b1000_0000),
            OpcodeInfo(opcode: 0xc6, cycles: 11, functionGroup: .setBit, source: .addressInIY_d, source2: .constant, dest: .addressInIY_d, constant: 0b1),
            OpcodeInfo(opcode: 0xce, cycles: 11, functionGroup: .setBit, source: .addressInIY_d, source2: .constant, dest: .addressInIY_d, constant: 0b10),
            OpcodeInfo(opcode: 0xd6, cycles: 11, functionGroup: .setBit, source: .addressInIY_d, source2: .constant, dest: .addressInIY_d, constant: 0b100),
            OpcodeInfo(opcode: 0xde, cycles: 11, functionGroup: .setBit, source: .addressInIY_d, source2: .constant, dest: .addressInIY_d, constant: 0b1000),
            OpcodeInfo(opcode: 0xe6, cycles: 11, functionGroup: .setBit, source: .addressInIY_d, source2: .constant, dest: .addressInIY_d, constant: 0b1_0000),
            OpcodeInfo(opcode: 0xee, cycles: 11, functionGroup: .setBit, source: .addressInIY_d, source2: .constant, dest: .addressInIY_d, constant: 0b10_0000),
            OpcodeInfo(opcode: 0xf6, cycles: 11, functionGroup: .setBit, source: .addressInIY_d, source2: .constant, dest: .addressInIY_d, constant: 0b100_0000),
            OpcodeInfo(opcode: 0xfe, cycles: 11, functionGroup: .setBit, source: .addressInIY_d, source2: .constant, dest: .addressInIY_d, constant: 0b1000_0000),
            ])


    let name: String
    /// The table that decodes opcodes
    let opcodeInfo: [OpcodeInfo]

    init(name: String, _ info: [OpcodeInfo])
    {
		self.opcodeInfo = info
        self.name = name
        Decoder.verify(table: info, name: name)
    }

    convenience init(name: String, base: Decoder, diffs: [OpcodeInfo])
    {
        var info = base.opcodeInfo
        for opcode in diffs
        {
            info[opcode.opcode] = opcode
        }
        self.init(name: name, info)
    }

    private static func verify(table: [OpcodeInfo], name: String)
    {
        log.info("Verifying \(name) opcode table")
        if table.count < Decoder.unimplemented || table.count > Decoder.unimplemented
        {
            log.info("\(name) table has inconsistent opcode count, \(table.count), should be \(Decoder.unimplemented)")
        }
        var unimplementedCount = 0
        var outOfPlaceCount = 0
        for i in 0 ..< table.count
        {
            if table[i].opcode == unimplemented
            {
                log.debug("\(name):\(UInt16(i).hexString): not implemented")
                unimplementedCount++
            }
			else if table[i].opcode != i
            {
				log.info("\(name):\(UInt16(i).hexString): wrong opcode \(UInt16(table[i].opcode).hexString)")
                outOfPlaceCount++
            }
        }
        if outOfPlaceCount > 0 || unimplementedCount > 0
        {
            log.warn("\(name): Out of place opcodes: \(outOfPlaceCount), unimplemented opcodes \(unimplementedCount)")
        }
        else
        {
            log.info("\(name): no errors")
        }
    }
}
