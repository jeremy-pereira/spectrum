//
//  CPMSimulator.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 01/05/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//
import Toolbox
import Foundation

/// This class simulaters the CPM operating system to a point. In its initial
/// incarnation it only intercepts the calls to output a character.
///
/// It works by being a memory mapped device on address 5 which is the CPM entry
/// point. If it gets a read and the program counter is 5, we assume that the 
/// CPU has just jumped to that location and is trying to reasd the instruction
/// there. The simulator performs the desired action and makes sure the CPU will
/// read a ret instruction.
///
/// You need to supply it with a function that deals with output characters.
/// Also, the machine has no code associated with it except a jmp to 0x100, so
/// before you run it, make sure there is a program there.
public class CPMSimulator: MemoryMappedDevice, Machine
{

    /// Currently only intercept one address
    public var portCount: Int { return 1 }

    /// The CPM entry point is address 5
    public var baseAddress: UInt16 { return 5 }

    /// The cpu of the CPM machine
    public let cpu: Z80A

    private let outputChar: (UInt8) -> ()

    public init(cpu: Z80A, outputChar: @escaping (UInt8) -> () = {
        let ascii = Character(UnicodeScalar($0))
        print("\(ascii)", terminator: "")
        })
    {
        self.cpu = cpu
        self.outputChar = outputChar
        cpu.mapDevice(self)
        cpu.set(bytes: [0xc3, 0x00, 0x01], at: 0)	// A jump to 0x100
    }

    /// Nothing happens if you try to write to the port
    ///
    /// - Parameters:
    ///   - portNumber: ignored
    ///   - byte: Ignored
    public func didWrite(_ portNumber: UInt16, byte: inout UInt8) {}


    /// Does nothing unless the read was an instruction fetch in which caase the 
    /// CPM call is emulated.
    ///
    /// - Parameters:
    ///   - portNumber: Address to read offset from the base addfress of this device.
    ///   - byte: The byte to read
    public func willRead(_ portNumber: UInt16, byte: inout UInt8)
    {
        let pc = cpu.register16("pc")!
        if pc == 6 // Assume that we are about to read the instriuction at 5
        {
            // http://floooh.github.io/2016/07/12/z80-rust-ms1.html
            let operation = cpu.register8("c")!
            switch operation
            {
            case 2:
                outputChar(cpu.register8("e")!)
            case 9:
                outputDollarString(cpu.register16("de")!)

            default:
                fatalError("Unsupported CPM")
            }
        }
        byte = 0xc9	// Return because we did the function
    }

    private func outputDollarString(_ address: UInt16)
    {
        let terminator = "$".utf8.first!
        var currentAddress = address

        var currentChar = cpu.byte(at: currentAddress&++)
        while currentChar != terminator
        {
            outputChar(currentChar)
            currentChar = cpu.byte(at: currentAddress&++)
        }
    }

    public func pushDevice(_ device: MemoryMappedDevice, port: UInt16)
    {
        fatalError("CPM simultor does not yet support push")
    }

    public func start()
    {

    }
    public func stop()
    {
        
    }
}
