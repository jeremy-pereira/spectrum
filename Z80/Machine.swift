//
//  Machine.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 04/06/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

/// A protocol that represents a machine containing a CPU and maybe other stuff
/// e.g the CPMMachine provides basic CPM functionality to run `zexall` and `zexdoc`
public protocol Machine: class
{
    /// The machine's CPU
    var cpu: Z80A { get }


    /// Start the machine. This function should do everything needed to get the
    /// machine running except run the CPU.
    func start()


    /// Stop the machine. This function should do everything needed to stop the
    /// machine except stop the CPU (which must already have been stopped)
    func stop()
}
