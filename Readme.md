# Spectrum Project

## Install

Clone the repository and the Toolbox repository

    https://bitbucket.org/jeremy-pereira/toolbox

into directories on your Mac in the same parent directory. In both cases, use the `develop` branch. Create a workspace with both projects in and build `Toolbox`, then `Z80Diagnostic` or `Spectrum`. One day, I'll get round to doing proper packages.

Currently everything is built with Swift 3.1 and Xcode 8.3.2

## Spectrum

The Spectrum 48k ROM copyright is held vy Amstrad. Redistribution is allowed for non-commercial purposes, which is to say you are not allowed to sell anything for money that uses them. I'm not doiung that, so I have included the ROM. If the copyright owner disagrees, pease contact me and I'll take down the ROM distribution.

ROMs obtained from:

    http://www.shadowmagic.org.uk/spectrum/roms.html

## Z80A

### The mysterious `wz`

The `wz` register appears to be an extra pair of general purpose registers to which the programmer has no direct access.  It's main purpose is for calculating indirect addresses and that is what we use it for. 

The apparently definitive list of instructions is as below. Bits in italicas are my comments.

**`LD A,(addr)`**

	MEMPTR = addr + 1

**`LD (addr),A`**

	MEMPTR_low = (addr + 1) & #FF,  MEMPTR_hi = A

	Note for *BM1: MEMPTR_low = (addr + 1) & #FF,  MEMPTR_hi = 0
*As far as I can tell so far, these cover the only two instructions that use an indirect address to load an 8 bit value. I think the rule can be simplified to "anything that uses `(addr)`.*

**`LD A,(rp)  where rp -- BC or DE`**

	MEMPTR = rp + 1


**`
LD (rp),A  where rp -- BC or DE`**

	MEMPTR_low = (rp + 1) & #FF,  MEMPTR_hi = A

	Note for *BM1: MEMPTR_low = (rp + 1) & #FF,  MEMPTR_hi = 0

*As above, I only have `ld a,(rr)` and `ld (rr),a` for these two, so I'm going to assume it's always true for the address mode.* 

**`LD (addr), rp` and `LD rp,(addr)`**

	MEMPTR = addr + 1

*This seems to cover all the 16 bit address instructions, so I'll just assume a 16 bit `(address)` does the same no matter where it is.* 

**`EX (SP),rp`**

	MEMPTR = rp value after the operation


**`ADD/ADC/SBC rp1,rp2`**

	MEMPTR = rp1_before_operation + 1

**`RLD/RRD`**

	MEMPTR = HL + 1

*The source address mode is set to `.addressInHL` which already calculates wz correctly for this instruction.*

**`JR/DJNZ/RET/RETI/RST` (jumping to addr)**

	MEMPTR = addr
	
*Taken care of by setting wz in dest address mode calculation.*

**`JP`(except `JP rp`)/`CALL addr` (even in case of conditional call/jp, independantly on condition satisfied or not)**

	MEMPTR = addr

*I am assuming that a destination address mode of `.pc` will cover all jumps. We'll need an exception for the `jp (rr)` case. As conditional jumps change the destination mode to none sometimes, we will need special code for these cases.*

IN A,(port)

	MEMPTR = (A_before_operation << 8) + port + 1



IN A,(C)

	MEMPTR = BC + 1



OUT (port),A

	MEMPTR_low = (port + 1) & #FF,  MEMPTR_hi = A

	Note for *BM1: MEMPTR_low = (port + 1) & #FF,  MEMPTR_hi = 0



OUT (C),A

	MEMPTR = BC + 1



**`LDIR`/`LDDR`**

	when BC == 1: MEMPTR is not changed

	when BC <> 1: MEMPTR = PC + 1, where PC = instruction address

**`CPI`**

	MEMPTR = MEMPTR + 1

**`CPD`**

	MEMPTR = MEMPTR - 1

**`CPIR`**

	when BC=1 or A=(HL): exactly as CPI

	In other cases MEMPTR = PC + 1 on each step, where PC = instruction address.

	Note* since at the last execution BC=1 or A=(HL), resulting MEMPTR = PC + 1 + 1 

	  (if there were not interrupts during the execution) 

**`CPDR`**

	when BC=1 or A=(HL): exactly as CPD

	In other cases MEMPTR = PC + 1 on each step, where PC = instruction address.

	Note* since at the last execution BC=1 or A=(HL), resulting MEMPTR = PC + 1 - 1 

	  (if there were not interrupts during the execution)

INI

	MEMPTR = BC_before_decrementing_B + 1



IND

	MEMPTR = BC_before_decrementing_B - 1



INIR

	exactly as INI on each execution.

	I.e. resulting MEMPTR = ((1 << 8) + C) + 1 



INDR

	exactly as IND on each execution.

	I.e. resulting MEMPTR = ((1 << 8) + C) - 1 



OUTI

	MEMPTR = BC_after_decrementing_B + 1



OUTD

	MEMPTR = BC_after_decrementing_B - 1



OTIR

	exactly as OUTI on each execution. I.e. resulting MEMPTR = C + 1 



OTDR

	exactly as OUTD on each execution. I.e. resulting MEMPTR = C - 1



Any instruction with (INDEX+d):

	MEMPTR = INDEX+d



Interrupt call to addr:

	As usual CALL. I.e. MEMPTR = addr

## Resources

The Z80asm page.

    http://www.nongnu.org/z80asm/index.html

The undocumented Z80, a useful respource with a reference for all Z80 instructions, documented or not.

    http://www.z80.info/zip/z80-documented.pdf

Blog about another Z80 emulation, written in Rust. This is the blog that lead me to `zexdoc` and `zexall`.

    http://floooh.github.io/2016/07/12/z80-rust-ms1.html

The CPU source code for the above emulation. This served as the reference implementation for my emulator since it passes all of the `zexall` tests.

    https://github.com/floooh/rz80/blob/master/src/cpu.rs

Documents the mysterious behaviour of the wz register

    http://forums.bannister.org/ubbthreads.php?ubb=showflat&Number=2649&PHPSESSID=53459e1fdeb0580d09e07a652111ea0f