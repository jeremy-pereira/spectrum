# Release 2

This file is in *Markdown* format. *MacDown* is a *MarkDown* editor, so if you want to view this file with nice formatting, install *MacDown* and then open this file with it.

# What to Do?

Run the `Spectrum.app` application. Press the `start` button at the top of the window called `Z80A CPU`. To type stuff in, make sure the screen window has focus and then type. I have included a picture of an original keyboard, for your convenience. The Macintosh `shift` key maps to the `caps shift` key and the Macintosh `alt` key maps to the `symbol shift` key. You have to respect the original Spectrum mappings. For example, to type a `"` do not use `"` fron the Mac keyboard, type `symbol shift` `p`. 

# What Works?

It's still not finished but the improvements are many and varied. 

I haven't fixed the problem with loading Z80 snapshots, but I have managed to get an alternate simpler format to work called "SNA". The enclosed sna for Manic Miner works and is playable.

To load a snapshot, use the `Open` option from the file menu. In theory, you can load any Spectrum SNA file from the Internet, but note that I haven't fully implemented the instruction set, so the program may crash. If it does, the crashdump will tell you which instruction is missing, so it can be added.

To save a snapshot, use the `Save`` option from the file menu.  Yes, now you can write programs and save them to disk. It doesn't give the full experience of tape loading, but you probably won't miss that.

Opening and saving both stop the processor so they can make the snapshot consistent. You'll need to press the start button again afterwards.

# What doesn't work?

There's still no sound.


# Source Code

The source code is included. It's in a git repository (two git repos actually) and git is shit, but can be made less painful with *SourceTree*. You'll need the latest version of *Xcode* from the Apple Mac app store to build it with. If you want the latest code, pull the latest changes from `Toolbox` and `Spectrum` and then build Spectrum with Xcode