//
//  AppDelegate.swift
//  Z80Diagnostic
//
//  Created by Jeremy Pereira on 29/04/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Cocoa
import Toolbox
import Z80UI
import Z80

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate
{
    var mainWindowController: MainWindowController?
    fileprivate var cpmOutputStream: FileHandle? = nil


    func applicationDidFinishLaunching(_ aNotification: Notification)
    {
        Logger.setLevel(.info, forName: "Z80.Z80")
        Logger.setLevel(.info, forName: "Z80.Decoder")

        // TODO: Let me choose where to put the file
        let cpmPath = "/Users/jeremyp/cpm.out"
        let fm = FileManager.default
        if !fm.fileExists(atPath: cpmPath)
        {
            fm.createFile(atPath: cpmPath, contents: Data())
        }
        cpmOutputStream = FileHandle(forUpdatingAtPath: cpmPath)
        if let cpmOutputStream = cpmOutputStream
        {
            cpmOutputStream.seekToEndOfFile()
            cpmOutputStream.write("\n\nRun at \(Date())\n\n")
        }


        mainWindowController = MainWindowController()
        mainWindowController!.environmentHelper = self
        mainWindowController!.showWindow(nil)
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    @IBAction func viewMemory(sender: Any)
    {
    	if let mainWindowController = mainWindowController
    	{
            mainWindowController.viewMemory(sender: sender)
    	}
    }
}

extension AppDelegate: EnvironmentHelper
{

    func loadROM(cpu: Z80A) throws
    {
        let myBundle = Bundle(for: type(of: self))
        let binaryFileUrl = myBundle.url(forResource: "zexall", withExtension: "com")
        //let binaryFileUrl = myBundle.url(forResource: "memorywindowbounds", withExtension: "bin")
        if let binaryFileUrl = binaryFileUrl
        {
            guard let program = try? Data(contentsOf: binaryFileUrl)
                else { fatalError("Could not read test coverage program") }

            var programBytes = [UInt8](repeating: 0, count: program.count)
            program.copyBytes(to: &programBytes, count: programBytes.count)
            cpu.set(bytes: [0xc3, 0x00, 0x01], at: 0) // jump to program start
            var currentAddress: UInt16 = 0x100
            for aByte in programBytes
            {
                cpu.set(byte: aByte, at: currentAddress)
                currentAddress += 1
            }
        }
        else
        {
            throw Z80UIError.couldNotLoadROM(fileName: "zexall.com")
        }
    }

    private func cpmOutputChar(_ theChar: UInt8)
    {
        let data = Data(bytes: [theChar])
        if let cpmOutputStream = cpmOutputStream
        {
            cpmOutputStream.write(data)
            cpmOutputStream.synchronizeFile()
        }
        else
        {
            FileHandle.standardOutput.write(data)
        }
    }

    func createMachine() -> Machine
    {
        let cpmMachine = CPMSimulator(cpu: Z80A(), outputChar: cpmOutputChar)
        return cpmMachine
    }

    func report(message: @autoclosure () -> String)
    {
        cpmOutputStream?.write("\n\(message())\n")
    }
}
