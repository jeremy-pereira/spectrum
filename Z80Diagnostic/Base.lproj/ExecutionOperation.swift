//
//  ExecutionOperation.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 29/04/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Foundation

final class ExecutionOperation
{
    var isCancelled = false
    var cyclesToRun: CycleCount
    var cyclesBetweenUpdates: CycleCount
    var speedInMegaHerz: Double = 1
    var cpu: CPU6502
    private var updateBlock: (ExecutionOperation) -> ()
    private var completionBlock: (ExecutionOperation) -> ()


    /// Create a new execution operation
    ///
    /// - Parameters:
    ///   - cpu: The CPU to run
    ///   - cyclesToRun: The number of cycles to run the CPU for
    ///   - cyclesBetweenUpdates: The number of cycles to run before doing an update
    ///   - updateBlock: A block to run to give updates. This block runs on the
    ///                  execution thread.
    ///   - completionBlock: A bock that runs when the CPU hasd executed all the
    ///                      cycles. This block runs on the execution thread,
    init(                cpu: Z80A,
                 cyclesToRun: CycleCount,
        cyclesBetweenUpdates: CycleCount,
                 updateBlock: @escaping (ExecutionOperation) -> (),
             completionBlock: @escaping(ExecutionOperation) -> ())
    {
        self.cpu = cpu
        self.cyclesToRun = cyclesToRun
        self.cyclesBetweenUpdates = cyclesBetweenUpdates
        self.updateBlock = updateBlock
        self.completionBlock = completionBlock
    }

    final func main()
    {
        var cyclesToGo = cyclesToRun
        let startNanoseconds = DispatchTime.now().uptimeNanoseconds
        var cyclesUsed: CycleCount = 0
        while cyclesToGo > 0 && !self.isCancelled
        {
            updateBlock(self)
            let startCycles = self.cpu.clock
            cpu.runForCycles(cyclesBetweenUpdates)
            cyclesToGo -= cpu.clock - startCycles
            let currentTime = DispatchTime.now()
            let elapsedTime = currentTime.uptimeNanoseconds - startNanoseconds
            cyclesUsed = self.cyclesToRun - cyclesToGo
            // To get the speed in Hertz, divide by time / 1E9
            // To get the speed in MHz, divide by 1E6 * (time / 1E9) = time / 1E3
            speedInMegaHerz = Double(cyclesUsed) / (Double(elapsedTime) / 1000)
        }
       	updateBlock(self)
        // TODO: Move to the completion handler
        let elapsedTime = Double(DispatchTime.now().uptimeNanoseconds - startNanoseconds) / 1e09
        print("\(cyclesUsed) cycles in \(elapsedTime) secs for \(Double(cyclesUsed) / (elapsedTime * 1000000)) MHz")
        completionBlock(self)

    }
}
