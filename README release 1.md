# Happy Birthday

Firstly, this file is in *Markdown* format. *MacDown* is a *MarkDown* editor, so if you want to view this file with nice formatting, install *MacDown* and then open this file with it.

# What to Do?

Run the `Spectrum.app` application. Press the `start` button at the top of the window called `Z80A CPU`. To type stuff in, make sure the screen window has focus and then type. I have included a picture of an original keyboard, for your convenience. The Macintosh `shift` key maps to the `caps shift` key and the Macintosh `alt` key maps to the `symbol shift` key. You have to respect the original Spectrum mappings. For example, to type a `"` do not use `"` fron the Mac keyboard, type `symbol shift` `p`. 

# What Works?

It's not finished yet. Firstly, it runs six times too fast, so, typing stuff in can be difficult.

Secondly, you can't load stuff yet. Think of it as like not having a tape recorder.

You can load z80 snapshot files, but there is a bug and you'll see with the included Manic Miner, that they load OK (you see the splash screen) but they don't run, this is the bit I really wanted to get working but your birthday is a hard deadline.

By the way, load Z80 snapshots by stopping the processor, and clicking `Open` in the `File` menu. Then start the processor again. Similarly, you can view the entire 64k of memory by clicking `View Memory` in the `Window` menu. But, definitely make sure the processor is stopped when you do this, or the program will crash.

# Source Code

The source code is included. It's in a git repository (two git repos actually) and git is shit, but can be made less painful with *SourceTree*. You'll need the latest version of *Xcode* from the Apple Mac app store to build it with. If you want the latest code, pull the latest changes from `Toolbox` and `Spectrum` and then build Spectrum with Xcode