//
//  MemoryWindowController.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 01/05/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Cocoa
import Z80
import Toolbox

private let log = Logger.getLogger("Z80Diagnostic.MemoryWindowController")

class MemoryWindowController: NSWindowController, MemoryViewDataSource, MemoryMappedDevice
{
    override var windowNibName: String? { return "MemoryWindowController" }
    override var owner: AnyObject? { return self }

    @IBOutlet weak var memoryView: MemoryView!
    @IBOutlet weak var addressField: NSTextField!
    private var addressPlaceHolder = ""

    override func windowDidLoad()
    {
        super.windowDidLoad()

        memoryView.setWidthAndHeight()
        addressPlaceHolder = addressField.placeholderString ?? ""
    }

    var touched: [Bool] = [Bool](repeating: false, count: 0x10000)

    var cpu: Z80A?
    {
        didSet
        {
            cpu?.mapDevice(self)
            memoryView.dataSource = self
            memoryView.needsDisplay = true
        }
    }

    // MARK: MemoryViewDataSource

    func byte(at: UInt16) -> UInt8
    {
        if let cpu = cpu
        {
            return cpu.byte(at: at)
        }
        else
        {
            return 0
        }
    }

    func isTouched(address: UInt16) -> Bool
    {
        return touched[Int(address)]
    }

    var addressUnderMouse: UInt16?
    {
        didSet
        {
			if addressUnderMouse != oldValue
            {
                if let addressUnderMouse = addressUnderMouse
                {
                    addressField.stringValue = "0x\(addressUnderMouse.hexString)"
                }
                else
                {
                    addressField.stringValue = ""
                }
            }
        }
    }

    // MARK: MemoryMappedDevice

    private lazy var pushedDevices: UnsafeMutablePointer<MemoryMappedDevice?> = {
        var ret = UnsafeMutablePointer<MemoryMappedDevice?>.allocate(capacity: Z80A.memorySize)
        for i in 0 ..< Z80A.memorySize
        {
            ret[i] = nil
        }
        return ret
    }()

    var portCount: Int { return 0x10000 }
    var baseAddress: UInt16 { return 0 }

    func willRead(_ portNumber: UInt16, byte: inout UInt8)
    {
        let address = baseAddress + portNumber
        if let previousDevice = pushedDevices[Int(address)]
        {
            previousDevice.willRead(address - previousDevice.baseAddress, byte: &byte)
        }
    }

    func didWrite(_ portNumber: UInt16, byte: inout UInt8)
    {
        let address = baseAddress + portNumber
        if let previousDevice = pushedDevices[Int(address)]
        {
            previousDevice.didWrite(address - previousDevice.baseAddress, byte: &byte)
        }
        touched[Int(address)] = true
        DispatchQueue.main.async
		{
            [weak self] in
            self?.memoryView.invalidate(address: address)
        }
    }


    func pushDevice(_ device: MemoryMappedDevice, port: UInt16)
    {
        let address = baseAddress + port
        pushedDevices[Int(address)] = device
    }
}
