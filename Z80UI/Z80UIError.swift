//
//  Z80UIError.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 19/06/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Foundation

/// Errors that might ber thrown when using this UI
///
/// - couldNotLoadROM: Failed to find or read a ROM image
public enum Z80UIError: Error
{
    case couldNotLoadROM(fileName: String)
}

