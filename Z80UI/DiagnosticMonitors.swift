//
//  DiagnosticMonitors.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 19/06/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Foundation
import Z80


/// A monitor that reports the opcodes tested by ZexAll during the ALU IX, IY
/// tests.
///
/// Note that it is probably broken because the PC may already have been incremented
/// when willRead is called.
class ZexALUIXYHLMonitor: MemoryMappedDevice
{
    let portCount = 2
    let baseAddress: UInt16
    let reporter: (UInt16) -> ()
    weak var cpu: Z80A!
    var instructionBytes: [UInt8]

    init(cpu: Z80A, baseAddress: UInt16, reporter: @escaping (UInt16) -> ())
    {
        self.cpu = cpu
        self.baseAddress = baseAddress
        self.reporter = reporter
        self.instructionBytes = [UInt8](repeating: 0, count: portCount)
    }

    func didWrite(_ portNumber: UInt16, byte: inout UInt8)
    {
        instructionBytes[Int(portNumber)] = byte
    }

    var alreadyFoundInstructions = Set<UInt16>()

    func willRead(_ portNumber: UInt16, byte: inout UInt8)
    {
        // Assume the cpu is reading the instruction if the pc is at the first
        // port when reading it
        if portNumber == 0 && cpu.register16("pc")! == baseAddress
        {
            let instruction: UInt16
            if instructionBytes[0] == 0xfd || instructionBytes[0] == 0xdd
            {
                instruction = UInt16(instructionBytes[0]) << 8 | UInt16(instructionBytes[1])
            }
            else
            {
                instruction = UInt16(instructionBytes[0])
            }
            if !alreadyFoundInstructions.contains(instruction)
            {
                alreadyFoundInstructions.insert(instruction)
                reporter(instruction)
            }
        }
    }

    var previousDevice: [MemoryMappedDevice?] = [nil, nil]

    func pushDevice(_ device: MemoryMappedDevice, port: UInt16)
    {
        let myIndex = Int(port)
        if let previousDevice = previousDevice[myIndex]
        {
            let address = port &- baseAddress

            device.pushDevice(previousDevice, port: address &- device.baseAddress)
        }
        previousDevice[myIndex] = device
    }
}

/// A monitor that reports the opcodes tested by ZexAll during the extended
/// shift tests.
///
/// Note that it is probably broken because the PC may already have been incremented
/// when willRead is called.
class ZexExtendedShiftMonitor: MemoryMappedDevice
{
    let portCount = 4
    let baseAddress: UInt16
    let reporter: (UInt32) -> ()
    weak var cpu: Z80A!
    var instructionBytes: [UInt8]

    init(cpu: Z80A, baseAddress: UInt16, reporter: @escaping (UInt32) -> ())
    {
        self.cpu = cpu
        self.baseAddress = baseAddress
        self.reporter = reporter
        self.instructionBytes = [UInt8](repeating: 0, count: portCount)
    }

    func didWrite(_ portNumber: UInt16, byte: inout UInt8)
    {
        instructionBytes[Int(portNumber)] = byte
    }

    var alreadyFoundInstructions = Set<UInt32>()

    func willRead(_ portNumber: UInt16, byte: inout UInt8)
    {
        // Assume the cpu is reading the instruction if the pc is at the first
        // port when reading it
        if portNumber == 0 && cpu.register16("pc")! == baseAddress
        {
            let instruction: UInt32
            if instructionBytes[0] == 0xfd || instructionBytes[0] == 0xdd
            {
                instruction = (UInt32(instructionBytes[0]) << 24)
                    | (UInt32(instructionBytes[1]) << 16)
                    | UInt32(instructionBytes[3])
            }
            else
            {
                instruction = (UInt32(instructionBytes[0]) << 24) | (UInt32(instructionBytes[1]) << 16)
            }
            if !alreadyFoundInstructions.contains(instruction)
            {
                alreadyFoundInstructions.insert(instruction)
                reporter(instruction)
            }
        }
    }

    var previousDevice: [MemoryMappedDevice?] = [nil, nil]

    func pushDevice(_ device: MemoryMappedDevice, port: UInt16)
    {
        let myIndex = Int(port)
        if let previousDevice = previousDevice[myIndex]
        {
            let address = port &- baseAddress

            device.pushDevice(previousDevice, port: address &- device.baseAddress)
        }
        previousDevice[myIndex] = device
    }
}


class ZexOpcodeMonitor: MemoryMappedDevice
{
    let portCount = 2
    let baseAddress: UInt16
    let reporter: (UInt8, UInt8) -> ()

    init(baseAddress: UInt16, reporter: @escaping (UInt8, UInt8) -> ())
    {
        self.baseAddress = baseAddress
        self.reporter = reporter
    }

    var currentByte1: UInt8 = 0x00
    {
        didSet
        {
            if oldValue != currentByte1
            {
                reporter(currentByte1, currentByte2)
            }
        }
    }
    var currentByte2: UInt8 = 0x00
    {
        didSet
        {
            if oldValue != currentByte2
            {
                switch currentByte1
                {
                case 0xcb, 0xdd, 0xed, 0xfd:
                    reporter(currentByte1, currentByte2)
                default:
                    break
                }
            }
        }
    }


    func didWrite(_ portNumber: UInt16, byte: inout UInt8)
    {
        if portNumber == 0
        {
            currentByte1 = byte
        }
        else if portNumber == 1
        {
            currentByte2 = byte
        }
    }

    func willRead(_ portNumber: UInt16, byte: inout UInt8) {}

    var previousDevice: [MemoryMappedDevice?] = [nil, nil]

    func pushDevice(_ device: MemoryMappedDevice, port: UInt16)
    {
        let myIndex = Int(port)
        if let previousDevice = previousDevice[myIndex]
        {
            let address = port &- baseAddress

            device.pushDevice(previousDevice, port: address &- device.baseAddress)
        }
        previousDevice[myIndex] = device
    }
}
