//
//  MemoryView.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 01/05/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Cocoa
import Toolbox

private let log: Logger = Logger.getLogger("Z80Diagnostic.MemoryView")

protocol MemoryViewDataSource
{
    func byte(at: UInt16) -> UInt8
    func isTouched(address: UInt16) -> Bool
    var addressUnderMouse: UInt16? { get set }
}

class MemoryView: NSView
{
    private static let defaultFont: NSFont = NSFont(name: "Monaco", size: 10)!

    override var isOpaque: Bool { return true }
    override var isFlipped: Bool { return true }

    struct DefaultDataSource: MemoryViewDataSource
    {
        var addressUnderMouse: UInt16?

        func byte(at: UInt16) -> UInt8
        {
            return at.lowByte
        }

        func isTouched(address: UInt16) -> Bool
        {
            return false
        }
    }

    var dataSource: MemoryViewDataSource = DefaultDataSource()

    override func draw(_ dirtyRect: NSRect)
    {
        super.draw(dirtyRect)

        NSColor.white.set()
        NSBezierPath.fill(dirtyRect)

        let boxSize = byteSize

        let minPage = Int(dirtyRect.origin.y / boxSize.height)
        let maxPage = Int((dirtyRect.origin.y + dirtyRect.size.height) / boxSize.height)
        let minByte = Int(dirtyRect.origin.x / boxSize.width)
        let maxByte = Int((dirtyRect.origin.x + dirtyRect.size.width) / boxSize.width)
        for page in  minPage ... maxPage
        {
            for byte in minByte ... maxByte
            {
                if byte <= Int(UInt8.max) && page <= Int(UInt8.max)
                {
                    let address = UInt16(page << 8 | byte)
                    let byteToDraw = dataSource.byte(at: address)
                    let stringToDraw: NSString = "\(byteToDraw.hexString)" as NSString
                    let coordinate = NSPoint(x: CGFloat(byte) * boxSize.width, y: CGFloat(page) * boxSize.height)
                    let theAttributes = dataSource.isTouched(address: address) ? touchedFontAttributes : screenFontAttributes
                    stringToDraw.draw(at: coordinate, withAttributes: convertToOptionalNSAttributedStringKeyDictionary(theAttributes))

                }
            }
        }

    }

    private var screenFontAttributes: [String : Any] = [ convertFromNSAttributedStringKey(NSAttributedString.Key.font) : defaultFont,
                                              convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : NSColor.black]
    private var touchedFontAttributes: [String : Any] = [ convertFromNSAttributedStringKey(NSAttributedString.Key.font) : defaultFont,
                                                  convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : NSColor.red]


    var byteSize: NSSize
    {
        var fullSize = "FF".size(withAttributes: convertToOptionalNSAttributedStringKeyDictionary(screenFontAttributes))
        fullSize.width += 2	// Add some separation
        return fullSize
    }

    @IBOutlet var widthConstraint: NSLayoutConstraint!
    @IBOutlet var heightConstraint: NSLayoutConstraint!

    override var intrinsicContentSize: NSSize
    {
        return NSSize(width: byteSize.width * (CGFloat(UInt8.max) + 1),
                      height: byteSize.height * (CGFloat(UInt8.max) + 1))
    }

    func setWidthAndHeight()
    {
        widthConstraint.constant = intrinsicContentSize.width
        heightConstraint.constant = intrinsicContentSize.height
    }

    func invalidate(address: UInt16)
    {
        let page = address >> 8
        let byte = address & 0xff
		let minX = CGFloat(byte) * byteSize.width
        let minY = CGFloat(page) * byteSize.height
		let rect = NSRect(x: minX, y: minY, width: byteSize.width, height: byteSize.height)
        self.setNeedsDisplay(rect)
    }

    // MARK: Tracking the mouse stuff

    var trackingRect: NSView.TrackingRectTag? = nil

    override func viewDidMoveToWindow()
    {
        log.debug("Setting up tracking rect")

        trackingRect = self.addTrackingRect(self.bounds, owner: self, userData: nil, assumeInside: false)
    }

    override var frame: NSRect
    {
        didSet
        {
            log.debug("Bounds: \(self.bounds), frame: \(self.frame)")
            guard let trackingRect = trackingRect else { return }
            self.removeTrackingRect(trackingRect)
            self.trackingRect = self.addTrackingRect(self.bounds, owner: self, userData: nil, assumeInside: false)
        }
    }

    override var bounds: NSRect
    {
        didSet
        {
            log.debug("Bounds: \(self.bounds), frame: \(self.frame)")
            guard let trackingRect = trackingRect else { return }
            self.removeTrackingRect(trackingRect)
            self.trackingRect = self.addTrackingRect(self.bounds, owner: self, userData: nil, assumeInside: false)
        }
    }

	private var wasAcceptingMouseEvents = false

    override func mouseEntered(with event: NSEvent)
    {
        log.debug("mouse entered")
        guard let window = self.window else { return }
        wasAcceptingMouseEvents = window.acceptsMouseMovedEvents
        window.acceptsMouseMovedEvents = true
        window.makeFirstResponder(self)
    }

    override func mouseExited(with event: NSEvent)
    {
        log.debug("mouse exited")
        dataSource.addressUnderMouse = nil
        self.window?.acceptsMouseMovedEvents = wasAcceptingMouseEvents
    }


    override func mouseMoved(with event: NSEvent)
    {
        let mousePoint = self.convert(event.locationInWindow, from: nil)
        let page = Int(mousePoint.y / byteSize.height)
        let byte = Int(mousePoint.x / byteSize.width)
        if page >= 0 && page <= 0xff && byte >= 0 && byte <= 0xff
        {
			let address = UInt16(low: UInt8(byte), high: UInt8(page))
            log.debug("mouse at: \(mousePoint) 0x\(address.hexString)")
            dataSource.addressUnderMouse = address
        }
        else
        {
            log.debug("mouse at: \(mousePoint)")
            dataSource.addressUnderMouse = nil
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
