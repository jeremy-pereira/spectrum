//
//  MainWindowController.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 29/04/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Cocoa
import Z80
import Toolbox

private let log = Logger.getLogger("Spectrum.Z80Diagnostic.MainWindowController")

/// Object that will help the window do things that depend on the external
/// environment e.g. where to get the ROMS from

public protocol EnvironmentHelper
{

    /// Load a ROM into the CPU's memory
    ///
    /// - Parameter cpu: The CPU for which to load a ROM
    /// - Throws: If the ROM can't be located or read.
    func loadROM(cpu: Z80A) throws


    /// Create a z80 based computer e.g. the CPM machine or a Spectrum
    ///
    /// - Returns: A machine to be used by this class.
    func createMachine() -> Machine


    /// Report messages back from the window controller
    ///
    /// - Parameter message: The message to report
    func report(message: @autoclosure () -> String)
}

public class MainWindowController: NSWindowController
{
    public var environmentHelper: EnvironmentHelper?

    override public var windowNibName: String? { return "MainWindowController" }
    override public var owner: AnyObject? { return self }
    var theMachine: Machine!

    @IBOutlet var generalRegisters: RegisterDataSource!
    @IBOutlet var shadowRegisters: RegisterDataSource!
    @IBOutlet var otherRegisters: RegisterDataSource!
    @IBOutlet var specialRegisters: RegisterDataSource!

    @IBOutlet weak var generalRegisterTable: NSTableView!
    @IBOutlet weak var shadowRegisterTable: NSTableView!
    @IBOutlet weak var otherRegisterTable: NSTableView!
    @IBOutlet weak var specialRegisterTable: NSTableView!

    @IBOutlet weak var sFlag: NSButton!
    @IBOutlet weak var zFlag: NSButton!
    @IBOutlet weak var yFlag: NSButton!
    @IBOutlet weak var hFlag: NSButton!
    @IBOutlet weak var xFlag: NSButton!
    @IBOutlet weak var pFlag: NSButton!
    @IBOutlet weak var nFlag: NSButton!
    @IBOutlet weak var cFlag: NSButton!
    @IBOutlet weak var speedMHZ: NSTextField!
    @IBOutlet weak var speedMHZFormatter: NumberFormatter!

    var flags: [NSButton] = []

    fileprivate var zexInstructionProbe: ZexExtendedShiftMonitor!
    var testedInstructions = Set<UInt16>()

    override public func windowDidLoad()
    {
        super.windowDidLoad()
        log.level = .info

        if let speedFormatter = speedMHZ.formatter as? NumberFormatter
        {
            speedFormatter.minimumFractionDigits = 1
            speedFormatter.maximumFractionDigits = 1
        }

        theMachine = environmentHelper!.createMachine()
        if let environmentHelper = environmentHelper
        {
            do
            {
                try environmentHelper.loadROM(cpu: theMachine.cpu)
            }
            catch
            {
                self.presentError(error)
            }
        }
        let z80 = theMachine.cpu
        //z80.set(bytes: [0xb0, 0x01 ], at: 0x120)	// Patch to skip to rotates
        reset(sender: self)
        generalRegisters.use(cpu: z80, registers: ["af", "bc", "de", "hl"])
        shadowRegisters.use(cpu: z80, registers: ["af'", "bc'", "de'", "hl'"])
        otherRegisters.use(cpu: z80, registers: ["ix", "iy", "sp", "pc"])
        specialRegisters.use(cpu: z80, registers: ["ir", "instruction"])

        flags = [cFlag, nFlag, pFlag, xFlag, hFlag, yFlag, zFlag, sFlag]

        updateUIForCPUWith(operation: nil)
    }


    /// Send a reset to the Z80
    ///
    /// - Parameter sender: controller sending the action
    @IBAction func reset(sender: Any)
    {
        guard let z80 = theMachine?.cpu else { return }
        // Reset the Z80
        z80.notReset = false
        z80.run(tStates: 3)
        z80.notReset = true
        z80.run(tStates: 1)
    }

    private lazy var executionQueue: DispatchQueue = {
        var queue = DispatchQueue(label: "cpuqueue",
                                    qos: DispatchQoS.default,
                             attributes: DispatchQueue.Attributes(rawValue: 0))
        return queue
    }()

    var operation: ExecutionOperation?

    /// Start the Z80. Will refuse to start if it appears to be running.
    ///
    /// - Parameter sender: <#sender description#>
    @IBAction func start(sender: Any)
    {
        guard let theMachine = theMachine else { return }

        guard operation == nil else { return }

        operation = ExecutionOperation(machine: theMachine,
                                   cyclesToRun: Int.max,
                                updateInterval: 0.1,
                                   updateBlock:
            {
                operation in
                DispatchQueue.main.async
                {
                        self.updateUIForCPUWith(operation: operation)
                }
        	},
                               completionBlock:
            {
                [weak self] operation in
                if let controller = self
                {
                     DispatchQueue.main.async
                    {
                        controller.complete(operation: operation)
                    }
                    let speedReport = "\(operation.cyclesUsed) t-states in \(operation.totalTime) secs for \(Double(operation.cyclesUsed) / (operation.totalTime * 1000000)) MHz"
                    log.info(speedReport)
                    controller.environmentHelper?.report(message: "\n\(speedReport)\n")
                }
        })
        executionQueue.async(execute: operation!.main)
    }

    @IBAction public func stop(sender: Any?)
    {
        guard let operation = operation else { return }
        operation.isCancelled = true
    }

    @IBAction func singleStep(sender: Any)
    {
        guard operation == nil else { return }

        operation = ExecutionOperation(machine: theMachine,
                                   cyclesToRun: 4,
                                updateInterval: 0.1,
                                   updateBlock:
            {
                operation in
                DispatchQueue.main.async
                {
                    self.updateUIForCPUWith(operation: operation)
                }
            },
                                   completionBlock:
            {
                [weak self] operation in
                if let controller = self
                {
                    DispatchQueue.main.async
                    {
                        controller.complete(operation: operation)
                    }
                }
        })
        executionQueue.async(execute: operation!.main)
    }

    private func updateUIForCPUWith(operation: ExecutionOperation?)
    {
        assert(Thread.current === Thread.main, "updateUIForCPUWith(operation:) should only run on the main thread")

		generalRegisterTable.reloadData()
        shadowRegisterTable.reloadData()
        otherRegisterTable.reloadData()
        specialRegisterTable.reloadData()
        guard let currentFlags = theMachine?.cpu.flags else { return }
        for bit in 0 ..< 8
        {
			let thisFlag = Z80A.Status(rawValue: 1 << UInt8(bit))
            flags[bit].state = currentFlags.contains(thisFlag) ? NSControl.StateValue.on : NSControl.StateValue.off
        }
        if let operation = operation
        {
            speedMHZ.doubleValue = operation.speedInMegaHerz
        }
    }


    /// Adds an action to be performed when the Z80 stops. If the Z80 is 
    /// apparently not running, the action is performed immediately
    ///
    /// - Parameter action: The action to perform when the Z80 stops
    public func executeWhenZ80Stopped(action: @escaping () -> ())
	{
        assert(Thread.current === Thread.main, "executeWhenZ80Stopped(action:) should only run on the main thread")
        if operation != nil
        {
            operationCompletionActions.append({ _ in action() })
        }
        else
        {
            action()
        }
    }

    private var operationCompletionActions: [(ExecutionOperation?) -> ()] = []

    private func complete(operation: ExecutionOperation)
    {
        assert(operation === self.operation, "Operation is the wrong one")
        assert(Thread.current === Thread.main, "complete(operation:) should only run on the main thread")
        for action in operationCompletionActions
        {
            action(operation)
        }
        operationCompletionActions.removeAll()
        self.operation = nil
    }

    @IBOutlet var memoryWindowController: MemoryWindowController!

    @IBAction public func viewMemory(sender: Any)
    {
        memoryWindowController.showWindow(sender)
        memoryWindowController.cpu = theMachine?.cpu
    }
}
