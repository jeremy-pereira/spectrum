//
//  RegisterDataSource.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 29/04/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Cocoa
import Z80

class RegisterDataSource: NSObject, NSTableViewDataSource
{
    var cpu: Z80A? = nil
    var registers: [String] = []

    func use(cpu: Z80A, registers: [String])
    {
        self.cpu = cpu
        self.registers = registers
    }

    func numberOfRows(in tableView: NSTableView) -> Int
    {
        return registers.count
    }

    func tableView(_ tableView: NSTableView, objectValueFor tableColumn: NSTableColumn?, row: Int) -> Any?
    {
        guard let tableColumnIdentifier = tableColumn?.identifier
        else { return "Unknown column" }
        let identifier = convertFromNSUserInterfaceItemIdentifier(tableColumnIdentifier)

        let ret: String
        switch identifier
        {
        case "name":
            ret = registers[row]
        case "value":
            if let cpu = cpu
            {
                if let value = cpu.register16(registers[row])
                {
                    ret = value.hexString
                }
                else
                {
                    ret = "unkown register"
                }
            }
            else
            {
                ret = ""
            }
        default:
            ret = "unkown column"
        }
        return ret
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSUserInterfaceItemIdentifier(_ input: NSUserInterfaceItemIdentifier) -> String {
	return input.rawValue
}
