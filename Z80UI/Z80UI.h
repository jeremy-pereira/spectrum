//
//  Z80UI.h
//  Z80UI
//
//  Created by Jeremy Pereira on 04/06/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for Z80UI.
FOUNDATION_EXPORT double Z80UIVersionNumber;

//! Project version string for Z80UI.
FOUNDATION_EXPORT const unsigned char Z80UIVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Z80UI/PublicHeader.h>


