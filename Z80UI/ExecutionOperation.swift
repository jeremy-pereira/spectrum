//
//  ExecutionOperation.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 29/04/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Foundation
import Z80
import Toolbox

private var log = Logger.getLogger("Spectrum.Z80Diagnostics.ExecutionOperation")

final class ExecutionOperation
{
    var isCancelled = false
    var cyclesToRun: Int
    var cyclesBetweenUpdates: Int

    /// The nominal speed of the CPU in Megahertz for the last update interval
    var speedInMegaHerz: Double = 1
    /// The total time used by the CPU
    var totalTime: TimeInterval = 0
    /// The total cycles used by the CPU
    var cyclesUsed = 0

    let updateInterval: Double

    let machine: Machine
    var cpu: Z80A { return machine.cpu }


    /// An event that can happen during the life cycle of the operation
    typealias ExecutionEvent = (ExecutionOperation) -> ()

    private var startBlock: ExecutionEvent
    private var updateBlock: ExecutionEvent
    private var completionBlock: ExecutionEvent


    /// Create a new execution operation
    ///
    /// - Parameters:
    ///   - machine: The machine to run
    ///   - cyclesToRun: The number of cycles to run the CPU for
    ///   - startBlock: Things that need to happen before the CPU starts running
    ///   - updateInterval: Target interval betwee updates. The operation will guess
    ///                     the number of cycles to run on each iteration based
    ///                     on the actual speed measured in the previous interval.
    ///                     We start by guessing 4MHz
    ///   - updateBlock: A block to run to give updates. This block runs on the
    ///                  execution thread.
    ///   - completionBlock: A bock that runs when the CPU hasd executed all the
    ///                      cycles. This block runs on the execution thread,
    init(               machine: Machine,
                    cyclesToRun: Int,
                 updateInterval: Double,
                     startBlock: @escaping ExecutionEvent = { _ in },
                    updateBlock: @escaping ExecutionEvent = { _ in },
                completionBlock: @escaping ExecutionEvent = { _ in })
    {
        self.machine = machine
        self.cyclesToRun = cyclesToRun
        self.updateInterval = updateInterval
        self.cyclesBetweenUpdates = Int(4_000_000 * updateInterval)
        self.startBlock = startBlock
        self.updateBlock = updateBlock
        self.completionBlock = completionBlock
    }


    final func main()
    {
        var cyclesToGo = cyclesToRun
        let startNanoseconds = DispatchTime.now().uptimeNanoseconds
        startBlock(self)
        machine.start()
        while cyclesToGo > 0 && !self.isCancelled
        {
            updateBlock(self)
            let startCycles = self.cpu.cycleCount
            cpu.run(tStates: cyclesBetweenUpdates)
            cyclesToGo -= cpu.cycleCount - startCycles
            let currentTime = DispatchTime.now()
            let elapsedTime = currentTime.uptimeNanoseconds - startNanoseconds
            cyclesUsed = self.cyclesToRun - cyclesToGo
            // To get the speed in tstates/s, divide by time / 1E9
            // To get the speed in Mtstates/s, divide by 1E6 * (time / 1E9) = time / 1E3
            speedInMegaHerz = Double(cyclesUsed) / (Double(elapsedTime) / 1000)
            totalTime = Double(elapsedTime) / 1e9
            let speedInHz = Double(cyclesUsed) / totalTime
            cyclesBetweenUpdates = Int(speedInHz * updateInterval)
            if log.isDebug
            {
                log.debug("speed = \(speedInMegaHerz)MHz, cycles between updatews = \(cyclesBetweenUpdates)")
            }
        }
       	updateBlock(self)
        machine.stop()
        completionBlock(self)
    }
}
