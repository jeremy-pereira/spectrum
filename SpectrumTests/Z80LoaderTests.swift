//
//  Z80LoaderTests.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 18/06/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import Spectrum

class Z80LoaderTests: XCTestCase
{
    let binaryFileUrl = URL(fileURLWithPath: "/Users/jeremyp/dev/Spectrum/Spectrum/ManicMiner.z80")
    var programBytes: Data!
    override func setUp()
    {
        super.setUp()
        do
        {
            programBytes = try Data(contentsOf: binaryFileUrl)
        }
        catch
        {
            // For copyright reasons, the binary is not included in the 
            // source code repository.
			fatalError("Could not read \(binaryFileUrl), you need a z80 snapshot file")
        }
    }
    
    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testLoad()
    {
        let snapshot: Z80Snapshot
        do
        {
            snapshot = try Z80Snapshot.load(data: programBytes)
            print("Loaded version 1 \(snapshot.version)")
        }
        catch
        {
            XCTFail("Failed to load snapshot: \(error)")
            return
        }
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
