//
//  Spectrum.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 04/06/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//
import Z80
import Toolbox

private var log = Logger.getLogger("Spectrum.Spectrum")

fileprivate class IOStub: IODevice
{
    func out(address: UInt16, data: UInt8)
    {
        log.info("OUT to port \(address.lowByte), data=0x\(data.hexString), address=0x\(address.hexString)")
    }

    func inByte(address: UInt16) -> UInt8
    {
        log.info("IN from port \(address.lowByte), address=0x\(address.hexString)")
        return 0
    }
}


/// Models a ZX Spectrum 48K
class Spectrum: Machine
{

	/// The spectrum's CPU
	let cpu: Z80A = Z80A()
    private let ioDevice = IOStub()

    /// The spectrum's ULA
    let ula = ULA()

    init()
    {
        for port: UInt8 in 0 ... 255
        {
            if port % 2 == 0
            {
                cpu.registerIODevice(ula, forPort: port)
            }
            else
            {
                cpu.registerIODevice(ioDevice, forPort: port)
            }
        }
        ula.spectrum = self
    }

    func start()
    {
        ula.start()
    }

    func stop()
    {
        ula.stop()
    }
}
