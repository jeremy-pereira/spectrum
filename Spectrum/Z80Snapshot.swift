//
//  Z80Snapshot.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 18/06/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Foundation
import Toolbox
import Z80

fileprivate enum Offset: Int
{
    case a = 0
    case f = 1
    case bc = 2
    case de = 13
    case hl = 4
    case ix = 25
    case iy = 23
    case sp = 8
    case aAlt = 21
    case fAlt = 22
    case bcAlt = 15
    case deAlt = 17
    case hlAlt = 19
    case pcV1 = 6
    case i = 10
    case r0to6 = 11
    case miscFlags = 12
    case iff1 = 27
    case iff2 = 28
    case interruptMode = 29
    case header2Count = 30
}

extension Data
{
    fileprivate subscript(offset: Offset) -> UInt8
    {
        return self[offset.rawValue]
    }
    fileprivate subscript(offset: Offset) -> UInt16
    {
        return UInt16(low: self[offset.rawValue], high: self[offset.rawValue + 1])
    }
}

class Z80Snapshot: Snapshot
{
    static let v1Size = 30
    static let v2Size = v1Size + 23
    static let v3Size = v1Size + 54

    static func load(data: Data) throws -> Z80Snapshot
    {
        guard data.count >= v1Size else { throw SpectrumError.z80FileTruncated }
        let pc: UInt16 = data[.pcV1]
		if pc != 0
        {
            return try Z80SnapshotV1(data: data)
        }
        else
        {
            guard data.count >= v1Size + 2 else { throw SpectrumError.z80FileTruncated }
            let header2Count: UInt16 = data[.header2Count]
            let headerSize = Int(header2Count) + v1Size
            guard data.count >= Int(headerSize) else { throw SpectrumError.z80FileTruncated }
			if headerSize == v2Size
            {
                return try Z80SnapshotV2(data: data)
            }
            else if headerSize == v3Size || headerSize == v3Size + 1
            {
                return try Z80SnapshotV3(data: data)
            }
            else
            {
                throw SpectrumError.invalidV2V3Header
            }
        }
    }

    let data: Data

    fileprivate init(data: Data) throws
    {
		self.data = data
    }

    var version: String { return "unknown" }

    var a: UInt8 { return data[.a] }
    var f: UInt8 { return data[.f] }
    var bc: UInt16 { return data[.bc] }
    var de: UInt16 { return data[.de] }
    var hl: UInt16 { return data[.hl] }
    var aAlt: UInt8 { return data[.aAlt] }
    var fAlt: UInt8 { return data[.fAlt] }
    var bcAlt: UInt16 { return data[.bcAlt] }
    var deAlt: UInt16 { return data[.deAlt] }
    var hlAlt: UInt16 { return data[.hlAlt] }
    var ix: UInt16 { return data[.ix] }
    var iy: UInt16 { return data[.iy] }
    var sp: UInt16 { return data[.sp] }
    var pc: UInt16 { return data[.pcV1] }
    var i: UInt8 { return data[.i] }
    var r: UInt8
    {
        let r0to6: UInt8 = data[.r0to6] & 0xfe
        let r7: UInt8 = data[.miscFlags] & 1
        return r0to6 | r7
    }
    var borderColour: UInt8
    {
        return (data[.miscFlags] >> 1) & 0x7
    }
    var samRom: Bool
    {
        let flags: UInt8 = data[.miscFlags]
        return (flags & 0b0001_0000) != 0
    }
    var compressed: Bool
    {
        let flags: UInt8 = data[.miscFlags]
        return (flags & 0b0010_0000) != 0
    }

    var iff1: Bool
    {
        return data[.iff1] != UInt8(0)
    }
    var iff2: Bool
    {
        return data[.iff2] != UInt8(0)
    }

    var interruptMode: Int
    {
        return Int(data[.interruptMode] & UInt8(0b0000_0011))
    }

    var image: [UInt8]?
    {
        fatalError("Cannot get image for snapshot version \(version)")
    }

    private enum DecompressState: StateType
    {
        case normal
        case oneED
        case twoED
        case twoEDCount(UInt8)
        case endMarker

        func canTransition(to: Z80Snapshot.DecompressState) -> TransitionShould<Z80Snapshot.DecompressState>
        {
			switch (self, to)
            {
            case (.normal, .normal), (.normal, .oneED):
                return TransitionShould.carryOn
            case (.oneED, .normal), (.oneED, .twoED):
                return TransitionShould.carryOn
            case (.twoED, .twoEDCount), (.twoED, .endMarker):
                return TransitionShould.carryOn
            case (.twoEDCount, .normal):
                return TransitionShould.carryOn
            default:
                return TransitionShould.abort
            }
        }
    }

    private func decompress(data: Data) throws -> [UInt8]
    {
        let stateMachine = StateMachine(startState: DecompressState.normal)
        var i = data.makeIterator()
        var error: Error? = nil
        stateMachine.onFailure = { _,_ in error = SpectrumError.decompression }
        var output: [UInt8] = []
        while let byte = i.next()
        {
            guard error == nil else { break }
            switch stateMachine.state
            {
            case .normal:
                switch byte
                {
                case 0xed:
                    stateMachine.transition(to: .oneED)
                default:
                    stateMachine.transition(to: .normal)
 					output.append(byte)                }
            case .oneED:
                switch byte
                {
                case 0xed:
                    stateMachine.transition(to: .twoED)
                default:
                    stateMachine.transition(to: .normal)
                    output.append(byte)
                }
            case .twoED:
                switch byte
                {
                case 0x00:
                    stateMachine.transition(to: .endMarker)
                default:
                    stateMachine.transition(to: .twoEDCount(byte))
                }
            case .twoEDCount(let count):
                stateMachine.transition(to: .normal)
                for _ in 0 ..< count
                {
                    output.append(byte)
                }
            case .endMarker:
                error = SpectrumError.excessBytes
            }
        }
        if let error = error
        {
            throw error
        }
        return output
    }

    private class Z80SnapshotV1: Z80Snapshot
    {
        override var version: String { return "1" }


        private var _image: [UInt8]? = nil
        private var imageError: Error? = nil

        override var image: [UInt8]?
        {
            if _image == nil && imageError == nil
            {
                do
                {
                    let dataForImage = data.advanced(by: Z80Snapshot.v1Size)
                    if self.compressed
                    {
                        _image = try decompress(data: dataForImage)
                    }
                    else
                    {
                        _image = dataForImage.map{ $0 }
                    }
                }
                catch
                {
                    imageError = error
                }
            }
            return _image
        }

    }

    private class Z80SnapshotV2: Z80Snapshot
    {
        override var version: String { return "2" }

        fileprivate override init(data: Data) throws
        {
            throw SpectrumError.versionNotImplemented("2")
        }
    }

    private class Z80SnapshotV3: Z80Snapshot
    {
        override var version: String { return "3" }

        fileprivate override init(data: Data) throws
        {
            throw SpectrumError.versionNotImplemented("3")
        }

    }

    func prepareToResume(cpu: Z80A)
    {
        let pc = self.pc
        let instruction = cpu.byte(at: pc)
        assert(cpu.setRegister16("instruction", value: UInt16(instruction)), "no set instruction")
        assert(cpu.setRegister16("pc", value: pc &+ 1), "no set pc")
    }
}
