//
//  ULA.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 11/06/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Foundation
import Z80
import Toolbox

private let log = Logger.getLogger("Spectrum.ULA")
private let keyLog = Logger.getLogger("Spectrum.KeyLogger")


/// Protocol describing an object that can be the screen
protocol ULAScreen
{

    /// ULA calls this when it is time to do a vertical retrace. Typically, 
    /// this means the screen should draw itself
    ///
    /// - Parameter ula: The ULA asking the screen to retrace.
    func verticalRetrace(ula: ULA)
}


/// Represents a line of pixels on the screen. This includes the border and the
/// main content of the screen.
struct ScreenLine
{

    /// idth of the border in pixels
    static let borderWidth = 48
    /// Content width in pixels i.e. the bit betweeen the borders
    static let contentWidth = 256
    /// Width of a screen line.
    static let width = borderWidth * 2 + contentWidth	// 2x border + usable screen.
    /// Number of bits needed to represent a pixel eight colours + bright
    static let pixelDepth = 4
    /// Number of lines of the screen containing usable content
    static let contentLines = 192
    /// Number of lines of the screen including the border
    static let lines = contentLines + borderWidth * 2

    private static let fieldCapacity = 64		// We will use 64 bit uints for this
    private static let colourMask: UInt8 = 0b0000_0111
    private static let brightnessMask: UInt8 = 0b0000_1000
    private static let fullMask: UInt64   = UInt64(colourMask | brightnessMask)


    private var fieldArray: [UInt64]

    init()
    {
        let requiredBits = ScreenLine.width * ScreenLine.pixelDepth
        let requiredFields = (requiredBits + ScreenLine.fieldCapacity - 1) / ScreenLine.fieldCapacity
        fieldArray = [UInt64](repeating: 0, count: requiredFields)
    }

    private func calculateFieldAndOffset(pixel: Int) -> (Int, UInt64)
    {
        let totalShift = pixel * ScreenLine.pixelDepth
        let fieldNumber = totalShift / ScreenLine.fieldCapacity
        let fieldShift = UInt64(totalShift % ScreenLine.fieldCapacity)
		return (fieldNumber, fieldShift)
    }

    fileprivate mutating func set(pixel: Int, colour: UInt8, isBright: Bool)
    {
        let (fieldNumber, fieldShift) = calculateFieldAndOffset(pixel: pixel)
		let baseBits = fieldArray[fieldNumber] & ~(ScreenLine.fullMask << fieldShift)
        let bitsToInsert = UInt64((colour & ScreenLine.colourMask) | (isBright ? 0b1000 : 0)) << fieldShift
        fieldArray[fieldNumber] = baseBits | bitsToInsert
    }


    /// Get the pixel at a specific location
    ///
    /// - Parameter location: Which pixel to get
    /// - Returns: The colour and brightness of thepixel as a 4 bit pattern. The
    ///            most significant bit is the brightness
    func pixel(at location: Int) -> UInt8
    {
        let (fieldNumber, fieldShift) = calculateFieldAndOffset(pixel: location)
        let field = fieldArray[fieldNumber]
        return UInt8((field >> fieldShift) & ScreenLine.fullMask)
    }

    fileprivate mutating func fillWith(colour: UInt8, bright: Bool)
    {
        for pixel in 0 ..< ScreenLine.width
        {
            set(pixel: pixel, colour: colour & ScreenLine.colourMask, isBright: bright)
        }
    }
}


/// Describes the screen attributes
struct ScreenAttribute: OptionSet
{
    var rawValue: UInt8

    init(rawValue: UInt8)
    {
        self.rawValue = rawValue
    }

    static let flash     = ScreenAttribute(rawValue: 0b1000_0000)
    static let bright    = ScreenAttribute(rawValue: 0b0100_0000)
    static let paperMask = ScreenAttribute(rawValue: 0b0011_1000)
    static let inkMask   = ScreenAttribute(rawValue: 0b0000_0111)
}

/// Emulation of the Spectrum's ULA
class ULA: IODevice
{

    /// The keyboard matrix has 8 rows of 5 switches. Each row corresponds to 
    /// half a row on the Spectrum keyboard.
    struct KeyMatrix: OptionSet
    {
        static let keysPerRow: UInt64 = 5
        var rawValue: UInt64
        var keyDescription: String?


        init(rawValue: UInt64, description: String?)
        {
            self.rawValue = rawValue
            keyDescription = description
        }

        init(rawValue: UInt64)
        {
            self.init(rawValue: rawValue, description: nil)
        }

        init(row: UInt64, bit: UInt64, description: String? = nil)
        {
			let mask = 1 << (row * KeyMatrix.keysPerRow + bit)
            self.init(rawValue: UInt64(mask), description: description)
        }

        static func keyFor(string: String) -> KeyMatrix?
        {
            switch string.lowercased()
            {
            case "a":
                return .a
            case "b":
                return .b
            case "c":
                return .c
            case "d":
                return .d
            case "e":
                return .e
            case "f":
                return .f
            case "g":
                return .g
            case "h":
                return .h
            case "i":
                return .i
            case "j":
                return .j
            case "k":
                return .k
            case "l":
                return .l
            case "m":
                return .m
            case "n":
                return .n
            case "o":
                return .o
            case "p":
				return .p
            case "q":
                return .q
            case "r":
                return .r
            case "s":
                return .s
            case "t":
                return .t
            case "u":
                return .u
            case "v":
                return .v
            case "w":
                return .w
            case "x":
                return .x
            case "y":
                return .y
            case "z":
                return .z
            case "0", ")":
                return ._0
            case "1", "!":
                return ._1
            case "2", "@":
                return ._2
            case "3", "£":
                return ._3
            case "4", "$":
                return ._4
            case "5", "%":
                return ._5
            case "6", "^":
                return ._6
            case "7", "&":
                return ._7
            case "8", "*":
                return ._8
            case "9", "(":
                return ._9
            case "\n", "\r":
                return .enter
            case " ":
                return .space
            default:
                return nil
            }
        }

        func ulaBits(row: UInt64) -> UInt8
        {
            return ~UInt8((rawValue >> (row * KeyMatrix.keysPerRow)) & 0x1f)
        }

        static let shift = KeyMatrix(row: 0, bit: 0, description: ".shift")
        static let z = KeyMatrix(row: 0, bit: 1, description: ".z")
        static let x = KeyMatrix(row: 0, bit: 2, description: ".x")
        static let c = KeyMatrix(row: 0, bit: 3, description: ".c")
        static let v = KeyMatrix(row: 0, bit: 4, description: ".v")

        static let a = KeyMatrix(row: 1, bit: 0, description: ".a")
        static let s = KeyMatrix(row: 1, bit: 1, description: ".s")
        static let d = KeyMatrix(row: 1, bit: 2, description: ".d")
        static let f = KeyMatrix(row: 1, bit: 3, description: ".f")
        static let g = KeyMatrix(row: 1, bit: 4, description: ".g")

        static let q = KeyMatrix(row: 2, bit: 0, description: ".q")
        static let w = KeyMatrix(row: 2, bit: 1, description: ".w")
        static let e = KeyMatrix(row: 2, bit: 2, description: ".e")
        static let r = KeyMatrix(row: 2, bit: 3, description: ".r")
        static let t = KeyMatrix(row: 2, bit: 4, description: ".t")

        static let _1 = KeyMatrix(row: 3, bit: 0, description: "._1")
        static let _2 = KeyMatrix(row: 3, bit: 1, description: "._2")
        static let _3 = KeyMatrix(row: 3, bit: 2, description: "._3")
        static let _4 = KeyMatrix(row: 3, bit: 3, description: "._4")
        static let _5 = KeyMatrix(row: 3, bit: 4, description: "._5")

        static let _0 = KeyMatrix(row: 4, bit: 0, description: "._0")
        static let _9 = KeyMatrix(row: 4, bit: 1, description: "._9")
        static let _8 = KeyMatrix(row: 4, bit: 2, description: "._8")
        static let _7 = KeyMatrix(row: 4, bit: 3, description: "._7")
        static let _6 = KeyMatrix(row: 4, bit: 4, description: "._6")

        static let p = KeyMatrix(row: 5, bit: 0, description: ".p")
        static let o = KeyMatrix(row: 5, bit: 1, description: ".o")
        static let i = KeyMatrix(row: 5, bit: 2, description: ".i")
        static let u = KeyMatrix(row: 5, bit: 3, description: ".u")
        static let y = KeyMatrix(row: 5, bit: 4, description: ".y")

        static let enter = KeyMatrix(row: 6, bit: 0, description: ".enter")
        static let l = KeyMatrix(row: 6, bit: 1, description: ".l")
        static let k = KeyMatrix(row: 6, bit: 2, description: ".k")
        static let j = KeyMatrix(row: 6, bit: 3, description: ".j")
        static let h = KeyMatrix(row: 6, bit: 4, description: ".h")

        static let space = KeyMatrix(row: 7, bit: 0, description: ".space")
        static let symbolShift = KeyMatrix(row: 7, bit: 1, description: ".symbolShift")
        static let m = KeyMatrix(row: 7, bit: 2, description: ".m")
        static let n = KeyMatrix(row: 7, bit: 3, description: ".n")
        static let b = KeyMatrix(row: 7, bit: 4, description: ".b")
    }

    /// The colour of the Spectrum's border from 0 to 7
    var borderColour: UInt8 = 0

    // MARK: IO \Device API
    func out(address: UInt16, data: UInt8)
    {
        borderColour = data & 0b0000_0111
    }

    private var keyboardMatrix = KeyMatrix()

    func keyDown(key: KeyMatrix)
    {
        keyLog.info("{ $0.keyDown(key: \(key)) },")
        keyboardMatrix.insert(key)
    }

    func keyUp(key: KeyMatrix)
    {
        keyLog.info("{ $0.keyUp(key: \(key)) },")
        keyboardMatrix.remove(key)
    }

    typealias ULAAction = (ULA) -> Void
    private var injectedKeyPresses: [ULAAction] =
        [
//            // Set border
//            { $0.keyDown(key: ._5) },
//            { $0.keyUp(key: ._5) },
//            { $0.keyDown(key: .b) },
//            { $0.keyUp(key: .b) },
//            { $0.keyDown(key: ._3) },
//            { $0.keyUp(key: ._3) },
//            { $0.keyDown(key: .enter) },
//            { $0.keyUp(key: .enter) },
//            // Ink and paper
//            { $0.keyDown(key: ._6) },
//            { $0.keyUp(key: ._6) },
//            { $0.keyDown(key: .shift) },
//            { $0.keyDown(key: .symbolShift) },
//            { $0.keyUp(key: .symbolShift) },
//            { $0.keyUp(key: .shift) },
//            { $0.keyDown(key: .shift) },
//            { $0.keyDown(key: .x) },
//            { $0.keyUp(key: .x) },
//            { $0.keyUp(key: .shift) },
//            { $0.keyDown(key: ._8) },
//            { $0.keyUp(key: ._8) },
//            { $0.keyDown(key: .symbolShift) },
//            { $0.keyDown(key: .z) },
//            { $0.keyUp(key: .z) },
//            { $0.keyUp(key: .symbolShift) },
//            { $0.keyDown(key: .shift) },
//            { $0.keyDown(key: .symbolShift) },
//            { $0.keyUp(key: .symbolShift) },
//            { $0.keyUp(key: .shift) },
//            { $0.keyDown(key: .shift) },
//            { $0.keyDown(key: .c) },
//            { $0.keyUp(key: .c) },
//            { $0.keyUp(key: .shift) },
//            { $0.keyDown(key: ._4) },
//            { $0.keyUp(key: ._4) },
//            { $0.keyDown(key: .enter) },
//            { $0.keyUp(key: .enter) },
//            // Print message
//            { $0.keyDown(key: ._1)},
//            { $0.keyUp(key: ._1)},
//            { $0.keyDown(key: ._0)},
//            { $0.keyUp(key: ._0)},
//            { $0.keyDown(key: .p)},
//            { $0.keyUp(key: .p)},
//            { $0.keyDown(key: .symbolShift)},
//            { $0.keyDown(key: .p)},
//            { $0.keyUp(key: .p)},
//            { $0.keyUp(key: .symbolShift)},
//            { $0.keyDown(key: .shift)},
//            { $0.keyDown(key: .h)},
//            { $0.keyUp(key: .h)},
//            { $0.keyUp(key: .shift)},
//            { $0.keyDown(key: .a)},
//            { $0.keyUp(key: .a)},
//            { $0.keyDown(key: .p)},
//            { $0.keyUp(key: .p)},
//            { $0.keyDown(key: .p)},
//            { $0.keyUp(key: .p)},
//            { $0.keyDown(key: .y)},
//            { $0.keyUp(key: .y)},
//            { $0.keyUp(key: .symbolShift)},
//            { $0.keyDown(key: .shift)},
//            { $0.keyDown(key: ._0)},
//            { $0.keyUp(key: ._0)},
//            { $0.keyUp(key: .symbolShift)},
//            { $0.keyUp(key: .shift)},
//            { $0.keyUp(key: .symbolShift)},
//            { $0.keyDown(key: .shift)},
//            { $0.keyDown(key: ._0)},
//            { $0.keyUp(key: ._0)},
//            { $0.keyUp(key: .symbolShift)},
//            { $0.keyUp(key: .shift)},
//            { $0.keyDown(key: .p)},
//            { $0.keyUp(key: .p)},
//            { $0.keyDown(key: .y)},
//            { $0.keyUp(key: .y)},
//            { $0.keyDown(key: .space)},
//            { $0.keyUp(key: .space)},
//            { $0.keyDown(key: .b)},
//            { $0.keyUp(key: .b)},
//            { $0.keyDown(key: .i)},
//            { $0.keyUp(key: .i)},
//            { $0.keyDown(key: .r)},
//            { $0.keyUp(key: .r)},
//            { $0.keyDown(key: .t)},
//            { $0.keyUp(key: .t)},
//            { $0.keyDown(key: .h)},
//            { $0.keyUp(key: .h)},
//            { $0.keyDown(key: .d)},
//            { $0.keyUp(key: .d)},
//            { $0.keyDown(key: .a)},
//            { $0.keyUp(key: .a)},
//            { $0.keyDown(key: .y)},
//            { $0.keyUp(key: .y)},
//            { $0.keyDown(key: .space)},
//            { $0.keyUp(key: .space)},
//            { $0.keyDown(key: .shift)},
//            { $0.keyDown(key: .r)},
//            { $0.keyUp(key: .r)},
//            { $0.keyUp(key: .shift)},
//            { $0.keyDown(key: .i)},
//            { $0.keyUp(key: .i)},
//            { $0.keyDown(key: .c)},
//            { $0.keyUp(key: .c)},
//            { $0.keyDown(key: .h)},
//            { $0.keyUp(key: .h)},
//            { $0.keyDown(key: .a)},
//            { $0.keyUp(key: .a)},
//            { $0.keyDown(key: .r)},
//            { $0.keyUp(key: .r)},
//            { $0.keyDown(key: .d)},
//            { $0.keyUp(key: .d)},
//            { $0.keyDown(key: .symbolShift)},
//            { $0.keyDown(key: .p)},
//            { $0.keyUp(key: .p)},
//            { $0.keyUp(key: .symbolShift)},
//            { $0.keyDown(key: .enter)},
//            { $0.keyUp(key: .enter)},
//            // Loop round
//            { $0.keyDown(key: ._2)},
//            { $0.keyUp(key: ._2)},
//            { $0.keyDown(key: ._0)},
//            { $0.keyUp(key: ._0)},
//            { $0.keyDown(key: .space)},
//            { $0.keyUp(key: .space)},
//            { $0.keyDown(key: .g)},
//            { $0.keyUp(key: .g)},
//            { $0.keyDown(key: ._1)},
//            { $0.keyUp(key: ._1)},
//            { $0.keyDown(key: ._0)},
//            { $0.keyUp(key: ._0)},
//            { $0.keyDown(key: .enter)},
//            { $0.keyUp(key: .enter)},
//            { $0.keyDown(key: .r)},
//            { $0.keyUp(key: .r)},
//            { $0.keyDown(key: .enter)},
//            { $0.keyUp(key: .enter)},
    ]
    private var nextKeyAction = 0
    private var scanCount: Int = 0
    {
        didSet
        {
            if scanCount >= 100 && nextKeyAction < injectedKeyPresses.count
            {
                if scanCount % 4 == 0
                {
                    log.debug("key action \(nextKeyAction)")
                    injectedKeyPresses[nextKeyAction](self)
                    nextKeyAction++
                }
            }
        }
    }

    func inByte(address: UInt16) -> UInt8
    {
        if address >> 8 == 0xfe
        {
			scanCount++
        }

        var ret: UInt8 = 0xff
        for bit: UInt16 in 8 ... 15
        {
			if address & (1 << bit) == 0
            {
                ret &= keyboardMatrix.ulaBits(row: UInt64(bit - 8))
            }
        }
        if ret != 0xff
        {
            log.debug("IN from port \(address.lowByte), address=0x\(address.hexString), returning 0x\(ret.hexString)")
        }
       	return ret
    }

    fileprivate var nextClockCycles: Int = Int.max
    var screen: ULAScreen?
    weak var spectrum: Spectrum?
    {
        willSet
        {
            if let cpu = spectrum?.cpu
            {
                cpu.unregister(clockDevice: self)
            }
        }
        didSet
        {
            if let cpu = spectrum?.cpu
            {
                cpu.register(clockDevice: self)
            }

        }
    }

    // MARK: - Screen rendering

    fileprivate var retraceCount = 0
    {
        didSet
        {
            if retraceCount % 16 == 0
            {
                flashReversed = !flashReversed
            }
        }
    }
    private(set) var flashReversed = false

    fileprivate var screenLines: [ScreenLine] = [ScreenLine](repeating: ScreenLine(), count: ScreenLine.lines)


    fileprivate class ScreenTraceState
	{
        fileprivate var maxCount: Int { return 1 }
        fileprivate var tStates: Int { return Int.max }
        fileprivate var baseLineNumber: Int { return 0 }

        fileprivate init() {}

        fileprivate func normalAction(ula: ULA, invocationCount: Int) {}
        fileprivate func startAction(ula: ULA) {}
        fileprivate func endAction(ula: ULA) {}

        class VRetrace1: ScreenTraceState
        {
            override var tStates: Int
            {
                // The bit after the interrupt and before we start drawing the 
                // border lasts the equivalent of 64 lines minus the time to 
                // draw the border. The interrupt will be held high for 25 
                // cycles too, so we subtract that as well.
                return cyclesBetweenLines * (64 - ScreenLine.borderWidth) - holdIntHigh
            }

            override func startAction(ula: ULA)
            {
                if let screen = ula.screen
                {
                    screen.verticalRetrace(ula: ula)
                }
            }
        }

        class TopBorder: ScreenTraceState
        {
            override var baseLineNumber: Int { return 0 }
            override var maxCount: Int { return ScreenLine.borderWidth }
            override var tStates: Int { return cyclesBetweenLines }

            override func normalAction(ula: ULA, invocationCount: Int)
            {
                ula.screenLines[baseLineNumber + invocationCount].fillWith(colour: ula.borderColour, bright: false)
            }
        }

        class Content: ScreenTraceState
        {
            override var baseLineNumber: Int { return ScreenLine.borderWidth }
            override var maxCount: Int { return ScreenLine.contentLines }
            override var tStates: Int { return cyclesBetweenLines }

            override func normalAction(ula: ULA, invocationCount: Int)
            {
                let screenLinesIndex = baseLineNumber + invocationCount
                for i in 0 ..< ScreenLine.borderWidth
                {
                    ula.screenLines[screenLinesIndex].set(pixel: i, colour: ula.borderColour, isBright: false)
                }
                guard let cpu = ula.spectrum?.cpu
                    else { fatalError("Trying to generate a line for a CPU that doesn't exist") }
                let pixels = pixelBytes(cpu: cpu, row: invocationCount)
                let attributes = attributeBytes(cpu: cpu, row: invocationCount)
                var pixelIndex = ScreenLine.borderWidth
                for (pixelByte, attribute) in zip(pixels, attributes)
                {
                    let shouldReverse = attribute.flash && ula.flashReversed
                    for bit: UInt8 in 0 ... 7
                    {
						if shouldReverse == (pixelByte & (1 << (7 - bit)) != 0)
                        {
                            ula.screenLines[screenLinesIndex].set(pixel: pixelIndex,
                                                                 colour: attribute.paper,
                                                               isBright: attribute.bright)
                        }
                        else
                        {
                            ula.screenLines[screenLinesIndex].set(pixel: pixelIndex,
                                                                 colour: attribute.ink,
                                                               isBright: attribute.bright)
                        }
                        pixelIndex++
                    }
                }
                for i in 0 ..< ScreenLine.borderWidth
                {
                    ula.screenLines[screenLinesIndex].set(pixel: pixelIndex + i, colour: ula.borderColour, isBright: false)
                }

            }

            private func pixelBytes(cpu: Z80A, row: Int) -> [UInt8]
            {
                var ret: [UInt8] = []

                for col in 0 ..< (ScreenLine.contentWidth / 8)
                {
                    let colComponent = col & 0b1_1111
                    let rowComponent1 = ((row & 0b0000_0111) << 8)
                    let rowComponent2 = ((row & 0b0011_1000) << 2)
                    let rowComponent3 = ((row & 0b1100_0000) << 5)
                    let address = UInt16( 0x4000
                                        | colComponent
                                        | rowComponent1
                                        | rowComponent2
                                        | rowComponent3)
                    ret.append(cpu.byte(at: address))
                }
                return ret
            }

            private func attributeBytes(cpu: Z80A, row: Int) -> [PixelAttribute]
            {
                var ret: [PixelAttribute] = []

                for col in 0 ..< (ScreenLine.contentWidth / 8)
                {
                    let address: UInt16 = 0x5800 + UInt16((((row / 8) << 5) + col) & 0xffff)
                    ret.append(PixelAttribute(byte: cpu.byte(at: address)))
                }
                return ret
            }


        }

        class BottomBorder: ScreenTraceState
        {
            override var baseLineNumber: Int { return ScreenLine.borderWidth + ScreenLine.contentLines }

            override var maxCount: Int { return ScreenLine.borderWidth }
            override var tStates: Int { return cyclesBetweenLines }
            override func normalAction(ula: ULA, invocationCount: Int)
            {
                ula.screenLines[baseLineNumber + invocationCount].fillWith(colour: ula.borderColour, bright: false)
            }

        }

        class VRetrace2: ScreenTraceState
        {
            override var tStates: Int
            {
                // There's a total of 56 lines worh of time after drawing the
                // content, but this includes 48 lines of border.
                return cyclesBetweenLines * (56 - ScreenLine.borderWidth)
            }
        }

        class DoInterrupt: ScreenTraceState
        {
            override var tStates: Int { return holdIntHigh }

            override func startAction(ula: ULA)
            {
                guard let cpu = ula.spectrum?.cpu
                else
                {
                    fatalError("ULA being driven even though there is no spectrum")
                }
                cpu.notInt = false
            }

            override func endAction(ula: ULA)
            {
                guard let cpu = ula.spectrum?.cpu
                else
                {
                    fatalError("ULA being driven even though there is no spectrum")
                }
                cpu.notInt = true
                ula.retraceCount++
                ula.synchroniser.wait()
            }
        }
    }

    fileprivate var screenTraceStates: [ScreenTraceState] =
        [
            ScreenTraceState.VRetrace1(),
            ScreenTraceState.TopBorder(),
            ScreenTraceState.Content(),
            ScreenTraceState.BottomBorder(),
            ScreenTraceState.VRetrace2(),
            ScreenTraceState.DoInterrupt()
    	]
    fileprivate var traceStateIndex = 0
    fileprivate var inStateIndex = 0
    fileprivate let synchroniser = Synchroniser(interval: DispatchTimeInterval.milliseconds(20))

    func start()
    {
        synchroniser.start()
    }

    func stop()
    {
		synchroniser.stop()
    }
}

// Reference: http://www.worldofspectrum.org/faq/reference/48kreference.htm
fileprivate let cyclesBetweenVerticalRetraces = 69888
fileprivate let cyclesBetweenLines = 224
fileprivate let holdIntHigh = 25

extension ULA: ClockDrivenDevice
{

    func synchronise(time: Int)
    {
        traceStateIndex = 0
        inStateIndex = 0
        nextClockCycles = time + screenTraceStates[0].tStates
    }

    var nextCallTime: Int { return nextClockCycles }

    func drive()
    {
		var state = screenTraceStates[traceStateIndex]
        state.normalAction(ula: self, invocationCount: inStateIndex)
        inStateIndex += 1
        if inStateIndex == state.maxCount
        {
            state.endAction(ula: self)
            inStateIndex = 0
            traceStateIndex = (traceStateIndex + 1) % screenTraceStates.count
            state = screenTraceStates[traceStateIndex]
            state.startAction(ula: self)
        }
        nextClockCycles += state.tStates
    }
}

extension ULA: ScreenViewDataSource
{
    func lines(range: Range<Int>) -> ArraySlice<ScreenLine>
    {
        return screenLines[range]
    }
}


extension ULA.KeyMatrix: CustomStringConvertible
{
    var description: String
    {
        if let keyDescription = keyDescription
        {
            return keyDescription
        }
        else
        {
            return "KeyMatrix(rawValue: \(rawValue))"
        }
    }
}


