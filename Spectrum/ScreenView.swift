//
//  ScreenView.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 07/06/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Cocoa


struct PixelPoint
{
    let x: Int
    let y: Int

    init(x: Int, y: Int)
    {
        self.x = x
        self.y = y
    }
}

struct PixelSize
{
    let width: Int
    let height: Int

    init(width: Int, height: Int)
    {
        self.width = width
        self.height = height
    }

    static prefix func -(p: PixelSize) -> PixelSize
    {
        return PixelSize(width: -p.width, height: -p.height)
    }
}

/// Rect that defines an area on the screen in pixel bytes
struct PixelRect
{
    /// The coordinate of the top left corner
    let origin: PixelPoint
    /// The size in pixels
    let size: PixelSize

    init(x: Int, y: Int, width: Int, height: Int)
    {
        self.origin = PixelPoint(x: x, y: y)
        self.size = PixelSize(width: width, height: height)
    }

    init(rect: NSRect, pixelSize: NSSize)
    {
		let newX = floor(rect.origin.x / pixelSize.width)
        let newY = floor(rect.origin.y / pixelSize.height)
        // We need to be careful that our new rectangle includes the whole of
        // the NSRect, and since we just rounded down the top left coordinate,
        // just rounding up the width and height might not be enough
        let newMaxX = ceil(rect.maxX / pixelSize.width)
        let newMaxY = ceil(rect.maxY / pixelSize.height)
        self.init(x: Int(newX), y: Int(newY), width: Int(newMaxX - newX), height: Int(newMaxY - newY))
    }

    func translated(_ v: PixelSize) -> PixelRect
    {
        return PixelRect(x: self.origin.x + v.width, y: self.origin.y + v.height, width: self.size.width, height: self.size.height)
    }

    var maxX: Int { return origin.x + size.width }
    var maxY: Int { return origin.y + size.height }
}

/// Data source for this view
protocol ScreenViewDataSource: class
{
    /// Get the range of screen lines for the requested range
    ///
    /// - Parameter range: Range of lines to get
    /// - Returns: The screen lines got
    func lines(range: Range<Int>) -> ArraySlice<ScreenLine>
}

struct PixelAttribute
{
    let flash: Bool
    let bright: Bool
    let paper: UInt8
    let ink: UInt8

    init(flash: Bool, bright: Bool, paper: UInt8, ink: UInt8)
    {
        self.flash = flash
        self.bright = bright
        self.paper = paper
        self.ink = ink
    }

    init(byte: UInt8)
    {
        self.init(flash: byte & 0x80 != 0,
                 bright: byte & 0x40 != 0,
                  paper: (byte >> 3) & 0x7,
                    ink: byte & 0x7)
    }

    func correctedInk(flashReversed: Bool) -> UInt8
    {
        let ret = (flash && flashReversed) ? paper : ink
        return ret
    }


    func correctedPaper(flashReversed: Bool) -> UInt8
    {
        let ret = (flash && flashReversed) ? ink : paper
        return ret
    }

}

struct Palette
{
    private let colours: [NSColor]
    private let normal: CGFloat = 0.85
    private let bright: CGFloat = 1.0

    init()
    {
        colours =
        	[
        		NSColor(red: 0     , green: 0     , blue: 0     , alpha: 1),
              	NSColor(red: 0     , green: 0     , blue: normal, alpha: 1),
              	NSColor(red: normal, green: 0     , blue: 0     , alpha: 1),
              	NSColor(red: normal, green: 0     , blue: normal, alpha: 1),
              	NSColor(red: 0     , green: normal, blue: 0     , alpha: 1),
                NSColor(red: 0     , green: normal, blue: normal, alpha: 1),
              	NSColor(red: normal, green: normal, blue: 0     , alpha: 1),
              	NSColor(red: normal, green: normal, blue: normal, alpha: 1),

              	NSColor(red: 0     , green: 0     , blue: 0     , alpha: 1),
              	NSColor(red: 0     , green: 0     , blue: bright, alpha: 1),
              	NSColor(red: bright, green: 0     , blue: 0     , alpha: 1),
              	NSColor(red: bright, green: 0     , blue: bright, alpha: 1),
              	NSColor(red: 0     , green: bright, blue: 0     , alpha: 1),
              	NSColor(red: 0     , green: bright, blue: bright, alpha: 1),
              	NSColor(red: bright, green: bright, blue: 0     , alpha: 1),
              	NSColor(red: bright, green: bright, blue: bright, alpha: 1)
        	]
    }

    func colour(index: UInt8) -> NSColor
    {
        return colours[Int(index)]
    }
}

class ScreenView: NSView
{
    let palette = Palette()
    var pixelSize = NSSize(width: 2, height: 2)
	var borderPixelSize = PixelSize(width: 48, height: 48)
    var usablePixelSize = PixelSize(width: 256, height: 192)
    weak var dataSource: ScreenViewDataSource?
    private var _usableScreen: NSRect?
    private var usableScreen: NSRect
    {
        if _usableScreen == nil
        {
            _usableScreen = NSRect(x: CGFloat(borderPixelSize.width) * pixelSize.width,
                                   y: CGFloat(borderPixelSize.height) * pixelSize.height,
                                   width: CGFloat(usablePixelSize.width) * pixelSize.width,
                                   height: CGFloat(usablePixelSize.height) * pixelSize.height)
        }
        return _usableScreen!
    }

    override var isFlipped: Bool { return true }
    override var isOpaque: Bool { return true }

    override func draw(_ dirtyRect: NSRect)
    {
        super.draw(dirtyRect)
        NSColor.white.set()

        if let dataSource = dataSource
        {
            let requiredRect = PixelRect(rect: dirtyRect, pixelSize: pixelSize)
            let lines = dataSource.lines(range: requiredRect.origin.y ..< requiredRect.maxY)
            var drawRect = NSRect(x: dirtyRect.origin.x,
                                  y: dirtyRect.origin.y,
                              width: pixelSize.width,
                             height: pixelSize.height)
            for rowNum in requiredRect.origin.y ..< requiredRect.maxY
            {
                for pixelIndex in requiredRect.origin.x ..< requiredRect.maxX
                {
                    palette.colour(index: lines[rowNum].pixel(at: pixelIndex)).set()
                    NSBezierPath.fill(drawRect)
                    drawRect.origin.x += pixelSize.width
                }
                drawRect.origin.x = dirtyRect.origin.x
                drawRect.origin.y += pixelSize.height
            }
        }
        else
        {
            NSColor(red: 1, green: 0, blue: 0, alpha: 0.3).set()
            NSBezierPath.fill(dirtyRect)
        }
    }
}
