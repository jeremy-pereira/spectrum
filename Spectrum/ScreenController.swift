//
//  ScreenController.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 07/06/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Cocoa
import Z80
import Toolbox

private var log = Logger.getLogger("Spectrum.ScreenController")

extension PixelRect
{
    var firstCol: Int { return self.origin.x / 8 }
    var numCols: Int
    {
        let lastCol = (self.size.width + self.origin.x + 7) / 8
        return lastCol - firstCol
    }
}

class ScreenController: NSWindowController
{
    override public var windowNibName: String? { return "ScreenController" }
    override public var owner: AnyObject? { return self }
    @IBOutlet weak var screenView: ScreenView?
    @IBOutlet weak var screenWidth: NSLayoutConstraint!
    @IBOutlet weak var screenHeight: NSLayoutConstraint!

    var spectrum: Spectrum?
    {
        willSet
        {
            if let spectrum = spectrum
            {
                spectrum.ula.screen = nil
                screenView?.dataSource = nil
            }
        }
        didSet
        {
            if let spectrum = spectrum
            {
                spectrum.ula.screen = self
                if let screenView = screenView
                {
                    screenView.dataSource = spectrum.ula
                    screenView.needsDisplay = true
                }
            }
        }
    }

    override func windowDidLoad()
    {
        super.windowDidLoad()
    	guard let screenView = screenView else { fatalError("No screen view on windowDidLoad") }
        screenWidth.constant = CGFloat(48 + 48 + 256) * screenView.pixelSize.width
        screenHeight.constant = CGFloat(48 + 48 + 192) * screenView.pixelSize.height
        if let spectrum = spectrum
        {
            screenView.dataSource = spectrum.ula
        }
    }

    @IBAction func invalidate(_ sender: Any)
    {
        screenView?.needsDisplay = true
    }

    fileprivate var nextClockCycles: Int = 0
}

// MARK: - Functions to intercept key strokes
extension ScreenController
{
    override func keyDown(with theEvent: NSEvent)
    {
        log.debug("key down: modifiers=\(theEvent.modifierFlags)\ncharacters=\(theEvent.characters ?? "")/\(theEvent.charactersIgnoringModifiers ?? "")\nkey code= \(theEvent.keyCode)")
        if let ula = spectrum?.ula
        {
            if let characters = theEvent.charactersIgnoringModifiers
            {
                if let key = ULA.KeyMatrix.keyFor(string: characters)
                {
                    ula.keyDown(key: key)
                }
            }
        }
    }
    override func keyUp(with theEvent: NSEvent)
    {
        log.debug("key up: modifiers=\(theEvent.modifierFlags)\ncharacters=\(theEvent.characters ?? "")/\(theEvent.charactersIgnoringModifiers ?? "")\nkey code= \(theEvent.keyCode)")
        if let ula = spectrum?.ula
        {
            if let characters = theEvent.charactersIgnoringModifiers
            {
                if let key = ULA.KeyMatrix.keyFor(string: characters)
                {
                    ula.keyUp(key: key)
                }
            }
        }
    }

    override func flagsChanged(with event: NSEvent)
    {
        log.debug("flags changed: modifiers=\(event.modifierFlags)")
        if let ula = spectrum?.ula
        {
            set(modifiers: event.modifierFlags, ula: ula)
        }
    }

    private func set(modifiers: NSEvent.ModifierFlags, ula: ULA)
    {
		if modifiers.contains(NSEvent.ModifierFlags.option)
        {
            ula.keyDown(key: .symbolShift)
        }
        else
        {
            ula.keyUp(key: .symbolShift)
        }
        if modifiers.contains(NSEvent.ModifierFlags.shift)
        {
            ula.keyDown(key: .shift)
        }
        else
        {
            ula.keyUp(key: .shift)
        }
    }
}

extension ScreenController: ULAScreen
{
    func verticalRetrace(ula: ULA)
    {
        DispatchQueue.main.async {
            [weak self] in
            self?.invalidate(self!)
        }
    }
}
