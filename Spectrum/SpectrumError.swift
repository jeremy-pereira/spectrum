//
//  SpectrumError.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 18/06/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Foundation


/// Errors that the Spectrum might generate
enum SpectrumError: Error
{
	case z80FileTruncated
    case invalidV2V3Header
    case versionNotImplemented(String)
    case decompression
    case excessBytes
    case unsupportedSnapshotType(String)
}
