//
//  AppDelegate.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 28/03/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Cocoa
import Toolbox
import Z80UI
import Z80

private var log = Logger.getLogger("Spectrum.AppDelegate")

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate
{
    var mainWindowController: MainWindowController?
    var screenController: ScreenController?

    func applicationDidFinishLaunching(_ aNotification: Notification)
    {
        Logger.setLevel(.info, forName: "Z80.Z80")
        Logger.setLevel(.info, forName: "Z80.Decoder")
        Logger.setLevel(.info, forName: "Spectrum.ULA")
        Logger.setLevel(.info, forName: "Spectrum.ScreenController")
        Logger.setLevel(.info, forName: "Spectrum.AppDelegate")
        Logger.setLevel(.info, forName: "Spectrum.KeyLogger")

        mainWindowController = MainWindowController()
        mainWindowController!.environmentHelper = self
        mainWindowController!.showWindow(nil)
        screenController = ScreenController()
        screenController!.spectrum = spectrum
        screenController!.showWindow(nil)
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    var spectrum: Spectrum?

    @IBAction func dumpRAM(sender: Any?)
    {
        guard let mainWindowController = mainWindowController else { return }
        guard let cpu = self.spectrum?.cpu
        else
        {
            log.warn("Spectrum seems to have gone away")
            return
        }

        let savePanel = NSSavePanel()
        savePanel.canCreateDirectories = true
        savePanel.allowedFileTypes = [ "bin" ]
        savePanel.allowsOtherFileTypes = true

        savePanel.beginSheetModal(for: mainWindowController.window!)
        {
            response in
            switch response
            {
            case NSApplication.ModalResponse.OK:
                if let url = savePanel.url
                {
                    let dataToSave = Data(bytes: cpu.bytes(at: 0x4000, count: 0xc000))
                    do
                    {
                        try dataToSave.write(to: url)
                    }
                    catch
                    {
                        mainWindowController.window?.presentError(error)
                    }
                }

            case NSApplication.ModalResponse.cancel:
                break
            default:
                fatalError("Invalid open panel response")
            }
        }

    }

    @IBAction func openZ80(sender: Any?)
    {
        guard let mainWindowController = mainWindowController else { return }
        guard let spectrum = self.spectrum
        else
        {
            log.warn("Spectrun seems to have gone away")
            return
        }

        let openFile = NSOpenPanel()
        openFile.allowsMultipleSelection = false
        openFile.canChooseDirectories = false
        openFile.canChooseFiles = true
        openFile.title = "Load Program"
        openFile.message = "Pick a file to load"
        openFile.prompt = "Load"
        openFile.allowedFileTypes = ["z80", "sna"]
        openFile.allowsOtherFileTypes = true

        openFile.beginSheetModal(for: mainWindowController.window!)
        {
            [unowned self] response in
            switch response
            {
            case NSApplication.ModalResponse.OK:
                let url = openFile.urls[0]
                do
                {
                    let snapshot: Snapshot
                    if url.pathExtension == "z80"
                    {
                        snapshot = try self.loadZ80(url: url)
                    }
                    else if url.pathExtension == "sna"
                    {
						snapshot = try self.loadSNA(url: url)
                    }
                    else
                    {
                        throw SpectrumError.unsupportedSnapshotType(url.pathExtension)
                    }
                    if let image = snapshot.image
                    {
                        let cpu = spectrum.cpu
                        let ula = spectrum.ula
                        log.debug("image length = \(image.count)")
                        mainWindowController.stop(sender: sender)
                        self.mainWindowController?.executeWhenZ80Stopped
                        {
                            cpu.set(bytes: image, at: 0x4000)
                            assert(cpu.setRegister8("a", value: snapshot.a), "no set a")
                            assert(cpu.setRegister8("f", value: snapshot.f), "no set f")
                            assert(cpu.setRegister16("bc", value: snapshot.bc), "no set bc")
                            assert(cpu.setRegister16("de", value: snapshot.de), "no set de")
                            assert(cpu.setRegister16("hl", value: snapshot.hl), "no set hl")
                            assert(cpu.setRegister8("a'", value: snapshot.aAlt), "no set a'")
                            assert(cpu.setRegister8("f'", value: snapshot.fAlt), "no set f'")
                            assert(cpu.setRegister16("bc'", value: snapshot.bcAlt), "no set bc'")
                            assert(cpu.setRegister16("de'", value: snapshot.deAlt), "no set de'")
                            assert(cpu.setRegister16("hl'", value: snapshot.hlAlt), "no set hl'")
                            assert(cpu.setRegister16("ix", value: snapshot.ix), "no set ix")
                            assert(cpu.setRegister16("iy", value: snapshot.iy), "no set iy")
                            assert(cpu.setRegister16("sp", value: snapshot.sp), "no set sp")
                            assert(cpu.setFlipFlop("iff1", value: snapshot.iff1), "no set iff1")
                            assert(cpu.setFlipFlop("iff2", value: snapshot.iff2), "no set iff2")
                            assert(cpu.setRegister8("interruptMode", value: UInt8(snapshot.interruptMode)), "no set im")
                            assert(cpu.setRegister8("i", value: snapshot.i), "no set i")
                            ula.borderColour = snapshot.borderColour

                            // In order to get the CPU executing at the correct
                            // location, we inject the byte at the pc as the
                            // instruction and increment the pc
                            snapshot.prepareToResume(cpu: cpu)
                        }

                    }
                    else
                    {
                        log.debug("Could not decompress the image")
                    }

                }
                catch
                {
                    mainWindowController.window?.presentError(error)
                }
            case NSApplication.ModalResponse.cancel:
                break
            default:
                fatalError("Invalid open panel response")
            }
        }
    }

    @IBAction func saveSpectrum(sender: Any?)
    {
        guard let mainWindowController = mainWindowController else { return }
        guard let spectrum = self.spectrum
        else
        {
            log.warn("Spectrun seems to have gone away")
            return
        }

        let savePanel = NSSavePanel()
        savePanel.canCreateDirectories = true
        savePanel.allowedFileTypes = [ "sna" ]
        savePanel.allowsOtherFileTypes = false

        savePanel.beginSheetModal(for: mainWindowController.window!)
        {
            response in
            switch response
            {
            case NSApplication.ModalResponse.OK:
                mainWindowController.stop(sender: sender)
                mainWindowController.executeWhenZ80Stopped
                {
                    let snapshot = SNASnapshot(spectrum: spectrum)
                    do
                    {
                        try snapshot.data.write(to: savePanel.url!)
                    }
                    catch
                    {
                        mainWindowController.window?.presentError(error)
                    }

                }
            case NSApplication.ModalResponse.cancel:
                break
            default:
                fatalError("Invalid open panel response")
            }
        }
    }

    private func loadZ80(url: URL) throws -> Snapshot
    {
        let data = try Data(contentsOf: url)
        let z80Snapshot = try Z80Snapshot.load(data: data)
        log.debug("a = \(z80Snapshot.a.hexString)")
        log.debug("f = \(z80Snapshot.f.hexString)")
        log.debug("bc = \(z80Snapshot.bc.hexString)")
        log.debug("de = \(z80Snapshot.de.hexString)")
        log.debug("hl = \(z80Snapshot.hl.hexString)")
        log.debug("i = \(z80Snapshot.i.hexString)")
        log.debug("r = \(z80Snapshot.r.hexString)")
        log.debug("a' = \(z80Snapshot.aAlt.hexString)")
        log.debug("f' = \(z80Snapshot.fAlt.hexString)")
        log.debug("bc' = \(z80Snapshot.bcAlt.hexString)")
        log.debug("de' = \(z80Snapshot.deAlt.hexString)")
        log.debug("hl' = \(z80Snapshot.hlAlt.hexString)")
        log.debug("ix = \(z80Snapshot.ix.hexString)")
        log.debug("iy = \(z80Snapshot.iy.hexString)")
        log.debug("sp = \(z80Snapshot.sp.hexString)")
        log.debug("pc = \(z80Snapshot.pc.hexString)")
        log.debug("border colour = \(z80Snapshot.borderColour)")
        log.debug("SamRom = \(z80Snapshot.samRom)")
        log.debug("Compressed = \(z80Snapshot.compressed)")
        return z80Snapshot
    }

    private func loadSNA(url: URL) throws -> Snapshot
    {
        let data = try Data(contentsOf: url)
        let snapshot = SNASnapshot(data: data)
        return snapshot
    }

}

extension AppDelegate: EnvironmentHelper
{

    func loadROM(cpu: Z80A) throws
    {
        let myBundle = Bundle(for: type(of: self))
        let binaryFileUrl = myBundle.url(forResource: "48", withExtension: "rom")
        //let binaryFileUrl = myBundle.url(forResource: "memorywindowbounds", withExtension: "bin")
        if let binaryFileUrl = binaryFileUrl
        {
            guard let program = try? Data(contentsOf: binaryFileUrl)
                else { fatalError("Could not read test coverage program") }

            var programBytes = [UInt8](repeating: 0, count: program.count)
            program.copyBytes(to: &programBytes, count: programBytes.count)
            var currentAddress: UInt16 = 0x0
            for aByte in programBytes
            {
                cpu.set(byte: aByte, at: currentAddress)
                cpu.makeROM(from: currentAddress, count: 1)
                currentAddress += 1
            }
        }
        else
        {
            throw Z80UIError.couldNotLoadROM(fileName: "zexall.com")
        }
    }

    func createMachine() -> Machine
    {
        Logger.setLevel(.info, forName: "Spectrum.Spectrum")
        spectrum = Spectrum()
        return spectrum!
    }

    func report(message: @autoclosure () -> String)
    {
        log.info(message)
    }
    
    @IBAction func viewMemory(sender: Any)
    {
        if let mainWindowController = mainWindowController
        {
            mainWindowController.viewMemory(sender: sender)
        }
    }

}
