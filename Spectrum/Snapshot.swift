//
//  Snapshot.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 07/07/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Foundation
import Z80

protocol Snapshot
{
    var image: [UInt8]? { get }
    var a: UInt8 { get }
    var f: UInt8 { get }
    var bc: UInt16 { get }
    var de: UInt16 { get }
    var hl: UInt16 { get }
    var aAlt: UInt8 { get }
    var fAlt: UInt8 { get }
    var bcAlt: UInt16 { get }
    var deAlt: UInt16 { get }
    var hlAlt: UInt16 { get }
    var ix: UInt16 { get }
    var iy: UInt16 { get }
    var sp: UInt16 { get }

    var iff1: Bool { get }
    var iff2: Bool { get }
    var interruptMode: Int { get }
    var i: UInt8 { get }
    var borderColour: UInt8 { get }

    func prepareToResume(cpu: Z80A)
}
