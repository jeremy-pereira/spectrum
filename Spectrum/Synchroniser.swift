//
//  Synchroniser.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 14/07/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Foundation

fileprivate extension DispatchTimeInterval
{
    var absoluteNanoseconds: UInt64
    {
        let signedRet: Int64
        switch self
        {
        case .seconds(let seconds):
            signedRet =  Int64(seconds) * 1_000_000_000
        case .milliseconds(let milliSeconds):
            signedRet =  Int64(milliSeconds) * 1_000_000
        case .microseconds(let microSeconds):
            signedRet = Int64(microSeconds) * 1_000
        case .nanoseconds(let nanoSeconds):
            signedRet =  Int64(nanoSeconds)
        case .never:
            signedRet = Int64.max // About 292 years
        }
        return UInt64(abs(signedRet))
    }
}


/// Used to synchronise a machine to a certain time interval.
class Synchroniser
{
    private let semaphore = DispatchSemaphore(value: 0)
    private let targetInterval: DispatchTimeInterval
    private var timer: DispatchSourceTimer?


    init(interval: DispatchTimeInterval)
    {
        targetInterval = interval
    }

    func start()
    {
        timer = DispatchSource.makeTimerSource(flags: DispatchSource.TimerFlags(),
                                               queue: DispatchQueue.main)
        timer!.setEventHandler(handler: {
            [weak self] in
            self!.semaphore.signal()
        })
        timer!.schedule(deadline: DispatchTime.now(), repeating: targetInterval)
        timer!.resume()
    }

    func stop()
    {
        if let timer = timer
        {
            timer.cancel()
        }
    }

    func wait()
    {
		let _ = semaphore.wait(timeout: DispatchTime(uptimeNanoseconds: DispatchTime.now().rawValue + targetInterval.absoluteNanoseconds))

    }
}
