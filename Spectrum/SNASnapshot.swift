//
//  SNASnapshot.swift
//  Spectrum
//
//  Created by Jeremy Pereira on 07/07/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Foundation
import Z80

fileprivate enum Offset8: Int
{
    case i = 0
    case fAlt = 7
    case aAlt = 8
    case iff2 = 19
    case r = 20
    case f = 21
    case a = 22
    case interruptMode = 25
    case borderColour = 26
    case image = 27
}

fileprivate enum Offset16: Int
{
    case hlAlt = 1
    case deAlt = 3
    case bcAlt = 5
    case hl = 9
    case de = 11
    case bc = 13
    case iy = 15
    case ix = 17
    case sp = 23
}

extension Data
{
    fileprivate subscript(offset: Offset8) -> UInt8
    {
        get { return self[offset.rawValue] }
        set { self[offset.rawValue] = newValue }
    }
    fileprivate subscript(offset: Offset16) -> UInt16
    {
        get { return UInt16(low: self[offset.rawValue], high: self[offset.rawValue + 1]) }
        set
        {
            self[offset.rawValue] = newValue.lowByte
            self[offset.rawValue + 1] = newValue.highByte
        }
    }
}


struct SNASnapshot: Snapshot
{
    var data: Data

    init(data: Data)
    {
        self.data = data
    }

    init(spectrum: Spectrum)
    {
        // The snapshot c annot capture state mid prefixed instruction so
        // run it until it is not in a prefixed instruction
        while spectrum.cpu.inPrefixMode && !spectrum.cpu.halt
        {
            spectrum.cpu.run(tStates: 1)
        }

        data = Data(count: Offset8.image.rawValue + 0xc000)

        // Save the 8 bit data
        self.i = spectrum.cpu.register8("i")!
        self.fAlt = spectrum.cpu.register8("f'")!
        self.aAlt = spectrum.cpu.register8("a'")!
        self.iff2 = spectrum.cpu.flipFlop("iff1")! // Will be restored by RETN
        self.f = spectrum.cpu.register8("f")!
        self.a = spectrum.cpu.register8("a")!
        self.interruptMode = Int(spectrum.cpu.register8("interruptMode")!)
        self.borderColour = spectrum.ula.borderColour


        // Save the 16 it data
        self.hlAlt = spectrum.cpu.register16("hl'")!
        self.deAlt = spectrum.cpu.register16("de'")!
        self.bcAlt = spectrum.cpu.register16("bc'")!
        self.hl = spectrum.cpu.register16("hl")!
        self.de = spectrum.cpu.register16("de")!
        self.bc = spectrum.cpu.register16("bc")!
        self.iy = spectrum.cpu.register16("iy")!
        self.ix = spectrum.cpu.register16("ix")!

        // Save the RAM image from 0x4000 (first RAM addfress) to the end.
        for i in 0 ..< 0xc000
        {
            data[Offset8.image.rawValue + i] = spectrum.cpu.byte(at: UInt16(i + 0x4000))
        }

        // We need the PC on the stack so on restore we can execute a RETN.
        // Remember that the PC in the cpu will already have been incremented
        // following dfetch of the current opcode
        let sp = spectrum.cpu.register16("sp")! &- 2
        let pc = spectrum.cpu.register16("pc")! &- 1
        let stackOffset = Offset8.image.rawValue + Int(sp &- 0x4000)
        data[stackOffset] = pc.lowByte
        data[stackOffset + 1] = pc.highByte

		self.sp = sp
    }

    var image: [UInt8]?
    {
        let imageData = data.subdata(in: Offset8.image.rawValue ..< data.count)
        return [UInt8](imageData)
    }
    var a: UInt8
    {
        set { data[.a] = newValue }
        get { return data[.a] }
    }
    var f: UInt8
    {
        set { data[.f] = newValue }
        get { return data[.f] }
    }
    var bc: UInt16
    {
        get { return data[.bc] }
        set { data[Offset16.bc] = newValue }
    }
    var de: UInt16
    {
        get { return data[.de] }
        set { data[Offset16.de] = newValue }
    }
    var hl: UInt16
    {
        get { return data[.hl] }
        set { data[Offset16.hl] = newValue }
    }
    var aAlt: UInt8
    {
        set { data[.aAlt] = newValue }
        get { return data[.aAlt] }
    }
    var fAlt: UInt8
    {
        set { data[.fAlt] = newValue }
        get { return data[.fAlt] }
    }
    var bcAlt: UInt16
    {
        get { return data[.bcAlt] }
        set { data[Offset16.bcAlt] = newValue }
    }
    var deAlt: UInt16
    {
        get { return data[.deAlt] }
        set { data[Offset16.deAlt] = newValue }
    }
    var hlAlt: UInt16
    {
        get { return data[.hlAlt] }
        set { data[Offset16.hlAlt] = newValue }
    }
    var ix: UInt16
    {
        get { return data[.ix] }
        set { data[Offset16.ix] = newValue }
    }
    var iy: UInt16
    {
        get { return data[.iy] }
        set { data[Offset16.iy] = newValue }
    }
    var sp: UInt16
    {
        get { return data[.sp] }
        set { data[Offset16.sp] = newValue }
    }
    var pc: UInt16 { return 0 }
    var iff1: Bool { return true }
    var iff2: Bool
    {
        get { return data[.iff2] & UInt8(0b100) != 0 }
        set
        {
            data[.iff2] = newValue ? 0b100 : 0
        }
    }
    var interruptMode: Int
    {
        get
        {
            let im: UInt8 = data[.interruptMode]
            return Int(im)
        }
        set
        {
            data[.interruptMode] = UInt8(newValue & 0xff)
        }
    }
    var i: UInt8
    {
        set { data[.i] = newValue }
        get { return data[.i] }
    }

    var borderColour: UInt8
    {
        set { data[.borderColour] = newValue }
        get { return data[.borderColour] }
    }

    func prepareToResume(cpu: Z80A)
    {
        // Restart the CPU by forcing it to execute a RETN 0xed, 0x45
        cpu.setRegister16("instruction", value: 0xed)
        cpu.run(tStates: 4)
        cpu.setRegister16("instruction", value: 0x45)
        cpu.run(tStates: 10)
    }
}
